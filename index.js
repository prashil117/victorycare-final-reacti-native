import { AppRegistry, Platform } from 'react-native';
import App from './App';
import 'react-native-gesture-handler';
import { name as appName } from './app.json';
import messaging from '@react-native-firebase/messaging';

if (Platform.OS === 'ios') {
    messaging().setBackgroundMessageHandler(async remoteMessage => {
        console.log('Message handled in the background!', remoteMessage);
        Alert.alert('BACKGROUND', JSON.stringify(remoteMessage));
    });
}

AppRegistry.registerComponent(appName, () =>
    App
);