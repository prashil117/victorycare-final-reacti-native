import AsyncStorage from '@react-native-community/async-storage';
import React, { useEffect, useState } from 'react';

export const setLng = (data) => {
    data = JSON.stringify(data)
    return AsyncStorage.setItem('language', data)
}

export const getLng = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem('language').then(data => {
          console.log("data=>",data)
            resolve(JSON.parse(data))
        })
    })
}

export const getLanguageData = () => {
  useEffect(() => {
    selectedLng()
  }, [])

  const selectedLng = async () => {
    const lngData = await getLng()

    if (!!lngData || lngData != null) {
      strings.setLanguage(lngData)
    }
    console.log("selected Language data next file==>>>", lngData)
  }
}
