export const baby_car = require("../../Image/baby_car_icon.png");
export const back_arrow = require("../../Image/back_arrow_icon.png");
export const calendar = require("../../Image/calendar_icon.png");
export const chart = require("../../Image/chart_icon.png");
export const cloth_icon = require("../../Image/cloth_icon.png");
export const down_arrow = require("../../Image/down_arrow.png");
export const education = require("../../Image/education_icon.png");
export const food = require("../../Image/food_icon.png");
export const healthcare = require("../../Image/healthcare_icon.png");
export const menu = require("../../Image/menu_icon.png");
export const more = require("../../Image/more_icon.png");
export const pin = require("../../Image/pin.png");
export const sports_icon = require("../../Image/sports_icon.png");
export const up_arrow = require("../../Image/up_arrow.png");
export const stopwatch = require("../../Image/stopwatch.png");
export const photo_camera = require("../../Image/photo-camera.png");
export const video_camera = require("../../Image/video-camera.png");
export const inner_clock = require("../../Image/inner-clock.png");

export default {
    baby_car,
    back_arrow,
    calendar,
    chart,
    cloth_icon,
    down_arrow,
    education,
    food,
    healthcare,
    menu,
    more,
    pin,
    sports_icon,
    up_arrow,
    stopwatch,
    photo_camera,
    video_camera,
    inner_clock
}
