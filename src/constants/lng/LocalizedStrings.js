import LocalizedStrings from 'react-native-localization';
import en from './en';
import no from './no';
import da from './da';
import sv from './sv';
import de from './de';
import es from './es';
import fr from './fr';

let strings = new LocalizedStrings({
  en: en,
  no: no,
  da: da,
  sv: sv,
  de: de,
  es: es,
  fr: fr,
  
});

export default strings;
