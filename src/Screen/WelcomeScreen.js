import React, { useState, createRef } from 'react';
import {
    StyleSheet,
    TextInput,
    View,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    KeyboardAvoidingView,
} from 'react-native';
import Logo from '../../Image/logo-victory.png'
import strings from '../constants/lng/LocalizedStrings';
const WelcomeScreen = ({ navigation }) => {

    return (
        <View style={styles.mainBody}>
            <ScrollView
                keyboardShouldPersistTaps="handled"
                contentContainerStyle={{
                    flex: 1,
                    justifyContent: 'center',
                    alignContent: 'center',
                }}>
                <View>
                    <KeyboardAvoidingView enabled>
                        <View style={{ alignItems: 'center', width: 560, flexDirection: 'row', flexWrap: 'wrap' }}>
                            <View style={{ left:170, marginBottom: 20 }}>
                                <Image style={{ height: 200, width: 200 }} source={Logo} />
                            </View>
                            <View>
                                <Text style={{ textAlign: 'center', marginBottom: 20, color: 'white', fontWeight: '600', fontSize: 30 }}>{strings.WELCOME_TO_VICTOR_Y_CARE}</Text>
                                <Text style={{ textAlign: 'center', color: '#81889f', fontWeight: '500', fontSize: 12 }}>
                                    {strings.WE_GIVE_CHRONICAL_ILL_AND}
                            </Text>
                            </View>
                            <TouchableOpacity
                                style={[styles.buttonStyle, { backgroundColor: '#516b93' }]}
                                activeOpacity={0.5}
                                onPress={() => navigation.navigate('RegisterScreen')}
                            >
                                <Text style={styles.buttonTextStyle}>{strings.SIGN_UP}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity
                                style={styles.buttonStyle}
                                activeOpacity={0.5}
                                onPress={() => navigation.navigate('LoginScreen')}
                            >
                                <Text style={styles.buttonTextStyle}>{strings.LOGIN}</Text>
                            </TouchableOpacity>
                        </View>
                    </KeyboardAvoidingView>
                </View>
            </ScrollView>
        </View >
    );
};
export default WelcomeScreen;

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 428,
        justifyContent: 'center',
        textAlign: 'center'
    },
    labelSectionStyle: {
        flexDirection: 'row',
        top: 20,
        marginLeft: 14,
        fontSize: 18,
        width: 450,
        justifyContent: 'center',
        textAlign: 'center'
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 50,
        width: 150,
        alignItems: 'center',
        alignSelf: 'center',
        borderRadius: 30,
        marginLeft: 50,
        left: 50,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        marginTop: 5,
        fontSize: 16,
        fontWeight: '600',
        textTransform:'uppercase'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        flexDirection: 'row',
    },
    registerTextStyle: {
        color: 'grey',
        textAlign: 'center',
        fontSize: 16,
        alignSelf: 'center',
        padding: 10,
    },
    errorTextStyle: {
        color: '#fb7e7e',
        textAlign: 'left',
        fontSize: 14,
        left: 12
    },
    alertbox: {
        backgroundColor: '#000',
    },
    button: {
        backgroundColor: '#4ba37b',
        width: 100,
        borderRadius: 50,
        alignItems: 'center',
        marginTop: 100
    },
    label: {
        fontFamily: 'HelveticaNeue-Light',
        fontSize: 18,
        color: '#fff',
        flex: 1,
        flexDirection: 'row',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "#2b3249",
        borderRadius: 10,
        padding: 15,
        height: 100,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 20,
            height: 20
        },
        shadowOpacity: 0.35,
        shadowRadius: 3.84,
        elevation: 5
    },

    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginTop: 15,
        marginBottom: 15,
        textAlign: "center",
        color: 'white'
    }
});
