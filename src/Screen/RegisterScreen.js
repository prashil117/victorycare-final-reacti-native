import React, { useState, createRef, useEffect } from 'react';
import {
  TextInput,
  View,
  Text,
  KeyboardAvoidingView,
  TouchableOpacity,
  ScrollView,
  Alert,
  TouchableHighlight
} from 'react-native'; 
import { useSelector } from 'react-redux'
import axios from 'axios'
import config from './appconfig/config';
import Loader from './Components/Loader';
import styles from './css/style'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Modal, Portal, Provider, Button, FAB } from 'react-native-paper';
import { CheckBox } from 'react-native-elements'
import AboutVictoryScreen from './DrawerScreens/TermsAndConditonScreen';
import iid from '@react-native-firebase/iid';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DropDownPicker from 'react-native-dropdown-picker';
import strings from '../constants/lng/LocalizedStrings';

// import firebase from "react-native-firebase";
const RegisterScreen = ({ navigation }) => {
  const [check, setCheck] = useState(false);
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [userEmail, setUserEmail] = useState('');
  const [userRePassword, setUserRePassword] = useState('');
  const [userPassword, setUserPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [fnameErr, setFnametext] = useState('');
  const [lnameErr, setLnametext] = useState('');
  const [checkErr, setCheckErr] = useState('');
  const [emailErr, setEmailtext] = useState('');
  const [ModalErr, setModalErr] = useState(false);
  const [visible, setVisible] = useState(false);
  const [visibleTerms, setVisibleTerms] = useState(false);
  const [passErr, setPasswordtext] = useState('');
  const [rePassErr, setRePasswordtext] = useState('');
  const [showPassword, setshowPassword] = useState(false);
  const [showPassword1, setshowPassword1] = useState(false);
  const lastInputRef = createRef();
  const [token, setToken] = useState('');
  const emailInputRef = createRef();
  const passwordInputRef = createRef();
  const repasswordInputRef = createRef();
  const hideModal = () => setVisible(false);
  const hideModal1 = () => setVisibleTerms(false);
  const [registerAs, setRegisterAs] = useState('');
  const containerStyle = { backgroundColor: '#2b3249', marginBottom: 30, height: 200, padding: 10, alignItems: 'center', width: '40%', alignSelf: 'center', borderRadius: 8 };
  const containerStyle1 = { backgroundColor: '#2b3249', showsVerticalScrollIndicator: false, marginBottom: 30, height: 700, padding: 10, alignItems: 'center', width: '80%', alignSelf: 'center', borderRadius: 8 };

  // useEffect(() => { requestUserPermission }, [])

  // const requestUserPermission = async () => {
  //   const authStatus = await messaging().requestPermission();
  //   const enabled =
  //     authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
  //     authStatus === messaging.AuthorizationStatus.PROVISIONAL;

  //   if (enabled) {
  //     getFcmToken() //<---- Add this
  //     console.log('Authorization status:', authStatus);
  //   }
  // }

  // const getFcmToken = async () => {
  //   const fcmToken = await messaging().getToken();
  //   if (fcmToken) {
  //     console.log(fcmToken);
  //     console.log("Your Firebase Token is:", fcmToken);
  //   } else {
  //     console.log("Failed", "No token received");
  //   }
  // }

  // useEffect(() => {
  //   requestUserPermission();
  //   const unsubscribe = messaging().onMessage(async remoteMessage => {
  //     Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
  //   });

  //   return unsubscribe;
  //  }, []);

  async function getToken() {
    const id = await iid().getToken();
    setToken(id);
    return id;
  }

  useEffect(() => {
    getToken();
  }, [])

  const setPassword = (e) => {
    if (e === '') {
      setPasswordtext(strings.RE_TYPE_YOUR_PASSWORD_HER)
    }
    else {
      setUserPassword(e)
      setPasswordtext('')
    }
  }

  const setRePassword = (e) => {
    if (e === '') {
      setRePasswordtext(strings.RE_TYPE_YOUR_PASSWORD_HER)
    }
    else {
      setUserRePassword(e)
      setRePasswordtext('')
    }
  }


  const clearFName=()=>{
    setfname('')
}
const clearLName=()=>{
  setlname('')
}

const clearEMail=()=>{
  setEmail('')
}

  const setfname = (e) => {
    if (e === '') {
      setFirstName('')
      setFnametext(strings.FIRSTNAME_REQUIRED)
    }
    else {
      setFirstName(e)
      setFnametext('')
    }
  }
  const setlname = (e) => {
    if (e === '') {
      setLastName('')
      setLnametext(strings.LASTNAME_REQUIRED)
    }
    else {
      setLastName(e)
      setLnametext('')
    }
  }
  const setEmail = (e) => {
    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if (e === '' || reg.test(e) === false) {
      setUserEmail(e)
      setEmailtext(strings.EMAIL_IN_PROPER_FORMAT)
    }
    else {
      setUserEmail(e)
      setEmailtext('')
    }
  }

  const handleSubmitButton = () => {
    if (!firstName) {
      setFnametext(strings.FIRSTNAME_REQUIRED)
      return;
    }
    if (!lastName) {
      setLnametext(strings.LASTNAME_REQUIRED)
      return;
    }
    if (!userEmail) {
      setEmailtext(strings.EMAIL_REQUIRED)
      return;
    }
    if (!userRePassword) {
      setPasswordtext(strings.PLEASE_ENTER_PASSWORD)
      return;
    }
    if (!userPassword) {
      setRePasswordtext(strings.RE_TYPE_YOUR_PASSWORD_HER)
      return;
    }
    if (userPassword !== userRePassword) {
      setRePasswordtext(strings.PASSWORD_DOES_NOT_MATCH)
      return;
    }
    if (!check) {
      setCheckErr(strings.PLEASE_ACCEPT_TERMS)
      return;
    }
    //Show Loader

    var dataToSend = {
      firstname: firstName,
      lastname: lastName,
      email: userEmail,
      password: userPassword,
      // usertype: 'patient',
      appsecret: config.appsecret,
      action: 'insert',
      base_url:config.apiUrl,
      fcmtoken: token,
      base_url: config.apiUrl,
      usertype:registerAs,
      sex:'m',
      firsttimelogin:1
    };
    // console.log("dataTOsend", dataToSend); return false;
    setLoading(true);
    axios.post(config.apiUrl + "register.php", dataToSend)
      .then(function (response) {
        setLoading(false)
        console.log("registered data:", response.data)
        let data = response.data;
        if (data.status1 === "exists") {
          console.log("data", data)
          setModalErr('Email Id already exists');
          // navigation.navigate('WelcomeScreen')
          setVisible(true);
        }else if(data.status1 === "success") {
          setModalErr(strings.YOUR_ACCOUNT_WAS_SUCCESSFULLY_CREATED_BUT_FEW_STEPS_TO_GO);
          setFirstName('')
          setLastName('')
          setUserEmail('')
          setUserPassword('')
          setUserRePassword('')
          setVisible(true);
          // navigation.navigate('WelcomeScreen')
        }else if(data.status1 === "errorEmail") {
          setModalErr(strings.YOUR_ACCOUNT_IS_CREATED_BUT_EMAIL_COULDNT_SENT);
          setVisible(true);
        }else{
          setModalErr(strings.THERE_WAS_A_PROBLEM_CREATING_ACCOUNT);
          setVisible(true);
        }
      }).catch(function (error) {
        console.log("err", error)
      })
  };
  console.log("visisble", visible)
  return (
    <View style={styles.body}>
      <Loader loading={loading} />
      {/*<ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          justifyContent: 'center',
          alignContent: 'center',
        }}>*/}
      <KeyboardAwareScrollView enableOnAndroid={true}>
        <View>
        <KeyboardAvoidingView enabled>
          <View style={{ alignItems: 'center' }}>
            <Text style={{ color: 'white', fontSize: 34, fontFamily: 'HelveticaNeue' }}>Sign Up  </Text>
          </View>
          <Text style={styles.registerTextStyle} onPress={() => navigation.navigate('LoginScreen')}>
            Already have an account? &nbsp;&nbsp;&nbsp;
              <Text style={{ color: '#69c2d1' }}>
              Login Here
               </Text>
          </Text>
          <View style={[styles.labelSectionStyle], {zIndex:100, paddingHorizontal:10}}>
            <Text style={styles.label}>
              Register as: <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text>
            </Text>
            <DropDownPicker
                items={[{value:"patient", label:'Patient'},{value:"admin", label:'Admin'}]}
                placeholder={strings.PLEASE_CHOOSE_OPTION}
                labelStyle={{ fontSize: 14, textAlign: 'left', color: 'white', }}
                defaultValue={'patient'}
                containerStyle={{ height: 40, color: 'white', borderWidth: 0,width:450  }}
                style={{ backgroundColor: '#2b3249', color: 'white', borderWidth: 0, borderBottomWidth: 1, }}
                itemStyle={{
                    justifyContent: 'flex-start', color: 'white'
                }}
                dropDownStyle={{ backgroundColor: '#2b3249', color: 'white' }}
                onChangeItem={item => setRegisterAs(item.value)}
                // onChangeText={(Name) => setfname(Name)}
            />
          </View>
          <View style={styles.labelSectionStyle}>
            <Text style={styles.label}>
              First Name <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text>
             </Text>
          </View>
          <View style={[styles.SectionStyle2,{borderBottomColor: fnameErr!=''&& firstName!='' ? '#D26C67': '#dadae8' }]}>
            <TextInput
              style={styles.inputStyle1}
              onChangeText={(Name) => setfname(Name)}
              underlineColorAndroid="#f000"
              value={firstName}
              placeholder={strings.PLEASE_ENTER_FIRST_NAME}
              placeholderTextColor="#8b9cb5"
              autoCapitalize="sentences"
              returnKeyType="next"
              onSubmitEditing={() =>
                lastInputRef.current && lastInputRef.current.focus()
              }
              blurOnSubmit={false}
            />
             {fnameErr==''&&firstName!=''?<Icon style={{marginRight:10}}name="check" size={22} color="#4AC47F" />:null}
               {firstName!==''?<TouchableOpacity onPress={()=>clearFName()}>
             <Icon name="close" size={22} color={fnameErr!=''?"#D26C67":"grey"} />
             </TouchableOpacity>:null}
          </View>
          <View>
            {fnameErr != '' ? (
              <Text style={styles.errorTextStyle}>
                {fnameErr}
              </Text>
            ) : null}
          </View>
          <View style={styles.labelSectionStyle}>
            <Text style={styles.label}>
              Last Name <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text>
             </Text>
          </View>
          <View style={[styles.SectionStyle2,{borderBottomColor: lnameErr!=''&& lastName!='' ? '#D26C67': '#dadae8' }]}>
            <TextInput
              style={styles.inputStyle1}
              onChangeText={(LastName) => setlname(LastName)}
              value={lastName}
              underlineColorAndroid="#f000"
              placeholder={strings.PLEASE_ENTER_LAST_NAME}
              placeholderTextColor="#8b9cb5"
              ref={lastInputRef}
              autoCapitalize="sentences"
              returnKeyType="next"
              onSubmitEditing={() =>
                emailInputRef.current && emailInputRef.current.focus()
              }
              blurOnSubmit={false}
            />
            {lnameErr==''&&lastName!=''?<Icon style={{marginRight:10}}name="check" size={22} color="#4AC47F" />:null}
               {lastName!==''?<TouchableOpacity onPress={()=>clearLName()}>
             <Icon name="close" size={22} color={lnameErr!=''?"#D26C67":"grey"} />
             </TouchableOpacity>:null}
          </View>
          <View>
            {lnameErr != '' ? (
              <Text style={styles.errorTextStyle}>
                {lnameErr}
              </Text>
            ) : null}
          </View>
          <View style={styles.labelSectionStyle}>
            <Text style={styles.label}>
              Email Address <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text>
             </Text>
          </View>
          <View style={[styles.SectionStyle2,{borderBottomColor: emailErr!=''&& userEmail!='' ? '#D26C67': '#dadae8' }]}>
            <TextInput
              style={styles.inputStyle1}
              onChangeText={(UserEmail) => setEmail(UserEmail)}
              value={userEmail}
              underlineColorAndroid="#f000"
              placeholder={strings.PLEASE_ENTER_EMAIL}
              placeholderTextColor="#8b9cb5"
              keyboardType="email-address"
              ref={emailInputRef}
              returnKeyType="next"
              onSubmitEditing={() =>
                passwordInputRef.current &&
                passwordInputRef.current.focus()
              }
              blurOnSubmit={false}
            />
            {emailErr=='' && userEmail!=''?<Icon style={{marginRight:10}}name="check" size={22} color="#4AC47F" />:null}
               {userEmail!==''?<TouchableOpacity onPress={()=>clearEMail()}>
             <Icon name="close" size={22} color={emailErr!=''?"#D26C67":"grey"} />
             </TouchableOpacity>:null}
          </View>
          <View>
            {emailErr != '' ? (
              <Text style={styles.errorTextStyle}>
                {emailErr}
              </Text>
            ) : null}
          </View>
          <View style={styles.labelSectionStyle}>
            <Text style={styles.label}>
              Password <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text>
             </Text>
          </View>
          <View style={styles.SectionStyle1}>
            <TextInput
              style={styles.inputStyle}
              onChangeText={(UserPassword) =>
                setPassword(UserPassword)
              }
              underlineColorAndroid="#f000"
              placeholder={strings.PLEASE_ENTER_PASSWORD}
              placeholderTextColor="#8b9cb5"
              ref={passwordInputRef}
              returnKeyType="next"
              secureTextEntry={showPassword ? false : true}
              onSubmitEditing={() =>
                repasswordInputRef.current &&
                repasswordInputRef.current.focus()
              }
              blurOnSubmit={false}
            />
            <View style={{ zIndex: 999, right: 30 }}>
              {showPassword ?
                <TouchableOpacity onPress={() => setshowPassword(false)}>
                  <Icon name="visibility-off" color="grey" size={24} />
                </TouchableOpacity> :
                <TouchableOpacity onPress={() => setshowPassword(true)}>
                  <Icon name="visibility" color="grey" size={24} />
                </TouchableOpacity>
              }
            </View>
          </View>
          <View>
            {passErr != '' ? (
              <Text style={styles.errorTextStyle}>
                {passErr}
              </Text>
            ) : null}
          </View>
          <View style={styles.labelSectionStyle}>
            <Text style={styles.label}>
              Re-Enter Password <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text>
             </Text>
          </View>
          <View style={styles.SectionStyle1}>
            <TextInput
              label="Email"
              style={styles.inputStyle}
              onChangeText={(userRePassword) =>
                setRePassword(userRePassword)
              }
              underlineColorAndroid="#f000"
              placeholder={strings.RE_TYPE_YOUR_PASSWORD_HER}
              placeholderTextColor="#8b9cb5"
              ref={repasswordInputRef}
              returnKeyType="next"
              secureTextEntry={showPassword1 ? false : true}
              blurOnSubmit={false}
            />
            <View style={{ zIndex: 999, right: 30 }}>
              {showPassword1 ?
                <TouchableOpacity onPress={() => setshowPassword1(false)}>
                  <Icon name="visibility-off" color="grey" size={24} />
                </TouchableOpacity> :
                <TouchableOpacity onPress={() => setshowPassword1(true)}>
                  <Icon name="visibility" color="grey" size={24} />
                </TouchableOpacity>
              }
            </View>
          </View>
          <View>
            {rePassErr !== '' ? (
              <Text style={styles.errorTextStyle}>
                {rePassErr}
              </Text>
            ) : userRePassword !== userPassword ? (
              <Text style={styles.errorTextStyle}>
                {strings.PASSWORD_DOES_NOT_MATCH}
              </Text>
            ) : null}
          </View>
          <View style={{ width: 400 }}>
            <CheckBox
              backgroundColor='transparent'
              containerStyle={{ backgroundColor: 'transparent', borderWidth: 0 }}
              title={<TouchableOpacity onPress={() => setVisibleTerms(true)}><Text style={{ color: '#69c2d1', left: 10, fontWeight: '500', fontSize: 14 }}>click here to read terms and condition</Text></TouchableOpacity>}
              checkedColor={'#69c2d1'}
              onIconPress={() => setCheck(!check)}
              checked={check}
            />
          </View>
          <View>
            {checkErr != '' ? (
              <Text style={styles.errorTextStyle}>
                {checkErr}
              </Text>
            ) : null}
          </View>
          <TouchableOpacity
            style={styles.buttonStyle}
            activeOpacity={0.5}
            onPress={handleSubmitButton}>
            <Text style={styles.buttonTextStyle}>Sign Up</Text>
          </TouchableOpacity>
          </KeyboardAvoidingView>
        </View>
      </KeyboardAwareScrollView>
      {/*</ScrollView>*/}
      <Provider>
        <Portal>
          <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
            <View style={{ alignSelf: 'flex-end', marginBottom: 30 }}>
              <TouchableOpacity onPress={hideModal}>
                <Icon name="close" color="grey" size={20} />
              </TouchableOpacity>
            </View>
            <Text style={{ color: 'white', fontSize: 16, fontWeight: '600', textAlign: 'center', bottom: 10 }}>{ModalErr}</Text>
            <TouchableOpacity
              style={[styles.buttonStyle, { right: 60, marginBottom: 20 }]}
              activeOpacity={0.5}
              onPress={hideModal}>
              <Text style={styles.buttonTextStyle}>Ok</Text>
            </TouchableOpacity>
            <AboutVictoryScreen />
          </Modal>
        </Portal>
      </Provider>
      <Provider>
        <Portal>
          <Modal visible={visibleTerms} contentContainerStyle={containerStyle1} >
            <View style={{ marginBottom: 0, flexDirection: 'row', flexWrap: 'wrap' }}>
              <Text style={{ color: 'white', fontWeight: '700', fontSize: 16 }}>Terms and Conditions</Text>
              <View style={{ left: 290 }}>
                <TouchableOpacity onPress={hideModal1}>
                  <Icon name="close" color="grey" size={20} />
                </TouchableOpacity>
              </View>
            </View>
            <Text style={{ color: 'white', fontSize: 16, fontWeight: '600', textAlign: 'center', bottom: 10 }}>{ModalErr}</Text>
            <AboutVictoryScreen />
          </Modal>
        </Portal>
      </Provider>
    </View>
  );
};

export default RegisterScreen;
