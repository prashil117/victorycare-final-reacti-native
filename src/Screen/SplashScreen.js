import React, { useState, useEffect } from 'react';
import { storeUserLogin, CheckRemember } from '../Screen/redux/action'
import { useSelector, useDispatch } from 'react-redux'
import {
  ActivityIndicator,
  View,
  StyleSheet,
  Image
} from 'react-native';
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import AsyncStorage from '@react-native-community/async-storage';

const SplashScreen = ({ navigation }) => {
  const dispatch = useDispatch();
  //State for ActivityIndicator animation
  const [animating, setAnimating] = useState(true);

  useEffect(() => {
    setTimeout(async () => {
      setAnimating(false);
      //Check if user_id is set or not
      //If not then send for Authentication
      //else send to Home Screen
      AsyncStorage.getItem('remember').then((value) => {
        dispatch(CheckRemember(value))
      })
      await AsyncStorage.getItem('user').then((value) => {
        dispatch(storeUserLogin(value))
        var firstlogin = value && value !== null ? JSON.parse(value).firsttimelogin : null
        var usertype = value && value !== null ? JSON.parse(value).usertype : null
        console.log("firstlogin", usertype)
        if (value === null) {
          navigation.replace('Auth');
          return
        }
        if (parseInt(firstlogin) === 1 && usertype === 'patient') {
          navigation.replace('Board');
          return
        }
        if (parseInt(firstlogin) === 0 && usertype === 'patient') {
          navigation.replace('DrawerNavigationRoutes', { screen: 'timelineScreenStack' });
          return
        } else {
          navigation.replace('DrawerNavigationRoutes', { screen: 'allUsersScreenStack' });
          return
        }
        // navigation.replace(
        //   value === null ? 'Auth' : firstlogin === '1' && usertype === 'patient' ? 'Board' : usertype === 'patient' ? `${'DrawerNavigationRoutes', { screen: 'timelineScreenStack' }}` : 'DrawerNavigationRoutes', { screen: 'allUsersScreenStack' }
        // )

      }
      );
    }, 1000);
  }, []);

  return (
    <View style={styles.container}>
      <Image
        source={require('../../Image/splash.gif')}
        style={{ width: '100%', resizeMode: 'contain', margin: 30 }}
      />
      <ActivityIndicator
        animating={animating}
        color="#FFFFFF"
        size="large"
        style={styles.activityIndicator}
      />
    </View>
  );
};


export default SplashScreen
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#3a4159',
  },
  activityIndicator: {
    alignItems: 'center',
    height: 80,
  },
});
