import { StyleSheet } from 'react-native';

export default styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "#2b3249",
    borderRadius: 10,
    padding: 15,
    height:100,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 20,
      height: 20
    },
    shadowOpacity: 0.35,
    shadowRadius: 3.84,
    elevation: 5
  },

  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginTop:15,
    marginBottom: 15,
    textAlign: "center",
    color:'white'
  },
    body: {
        flex: 1, backgroundColor: '#2b3249', flex: 1,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        // marginLeft: 35,
        // marginRight: 35,
        margin: 10,
        width: 450,
        justifyContent: 'center',
        textAlign: 'center',
    },
    SectionStyle1: {
      flexDirection: 'row',
      height: 40,
      marginTop: 20,
      // marginLeft: 35,
      // marginRight: 35,
      margin: 10,
      width: 472,
      justifyContent: 'center',
      textAlign: 'center'
  },
  SectionStyle2: {
    flexDirection: 'row',
    height: 40,
    marginTop: 20,
    // marginLeft: 35,
    // marginRight: 35,
    margin: 10,
    width: 450,
    justifyContent: 'center',
    textAlign: 'center',
borderBottomWidth: 1,
},
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'column',
        justifyContent: 'space-around',
        backgroundColor: '#00000040',
      },
      activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        height: 100,
        width: 100,
        borderRadius: 10,
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'space-around',
      },
      activityIndicator: {
        alignItems: 'center',
        height: 80,
      },
    registerTextStyle: {
        color: 'grey',
        textAlign: 'center',
        fontSize: 16,
        alignSelf: 'center',
        padding: 10,
      },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        // marginRight: 75,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
        fontWeight:'600'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        paddingLeft: 5,
        paddingRight: 15,
        borderBottomWidth: 1,
        borderColor: '#dadae8',
    },
    inputStyle1: {
      flex: 1,
      color: 'white',
      paddingLeft: 5,
      paddingRight: 15,
  },
    errorTextStyle: {
        color: '#fb7e7e',
        textAlign: 'left',
        fontSize: 14,
        left:12
    },
    successTextStyle: {
        color: 'white',
        textAlign: 'center',
        fontSize: 18,
        padding: 30,
    },
    labelSectionStyle: { 
      top:20,
      marginLeft:14,
      fontSize:18,
      width: 450,
      justifyContent: 'center',
      textAlign: 'center',
      zIndex:-1,
    },
    label:{
      fontFamily:'HelveticaNeue-Light',
      fontSize:18,
      color:'#fff',
      flex:1,
      flexDirection:'row',
    }
});
