import React, { useRef, useState } from "react";
import {
    StyleSheet,
    ScrollView,
    SafeAreaView,
    StatusBar,
    View,
    Image,
    Text,
    TouchableOpacity,
    Animated
} from 'react-native';
// import DropDownPicker from 'react-native-dropdown-picker';
import { VictoryPie, VictoryLabel, VictoryGroup, VictoryScatter, Circle } from 'victory-native';
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/Feather';
import Timeline from 'react-native-timeline-flatlist'; // use npm install react-native-timeline-flatlist --save

import Svg from 'react-native-svg';
import AwesomeAlert from 'react-native-awesome-alerts';
import { COLORS, FONTS, SIZES, icons, images } from '../../constants';
const TestScreen = () => {
  const confirmStatus = "C"
  const pendingStatus = "P"
  let categoriesData = [
    { x: "1", y: 35 },
    { x: "2", y: 40 },
    { x: "3", y: 55 },
    { x: "4", y: 20 },
    { x: "5", y: 45 },
    ]
    const renderChart = () => {

  }
  const [viewMode, setViewMode] = React.useState("chart")
  const [awesomeAlert, setAwesomeAlert] = useState(false)
  const showAlert = () => {
    setAwesomeAlert(true);
  };

  const hideAlert = () => {
    setAwesomeAlert(false);
  };

  return (
    <View style={styles.container}>

      <Text>I'm AwesomeAlert</Text>
      <TouchableOpacity onPress={() => {
        showAlert();
      }}>
        <View style={styles.button}>
          <Text style={styles.text}>Try me!</Text>
        </View>
      </TouchableOpacity>
      <AwesomeAlert
        style={{backgroundColor: "#ff0000"}}
        show={awesomeAlert}
        showProgress={false}
        title="Some Text Here"
        message="Are you sure you want to delete this record?"
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        showCancelButton={true}
        showConfirmButton={true}
        cancelText="No, cancel"
        confirmText="Yes, delete it"
        cancelButtonColor="#516b93"
        confirmButtonColor="#69c2d1"
        confirmButtonBorderRadius="0"
        onCancelPressed={() => {
          setAwesomeAlert(false);
        }}
        onConfirmPressed={() => {
          hideAlert();
        }}
      />
    </View>

  )
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#fff',
  },
  button: {
    margin: 10,
    paddingHorizontal: 10,
    paddingVertical: 7,
    borderRadius: 5,
    backgroundColor: "#AEDEF4",
  },
  text: {
    color: '#fff',
    fontSize: 15
  }
});

export default TestScreen;
