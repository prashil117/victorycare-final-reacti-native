import * as types from './constant'
import AsyncStorage from '@react-native-community/async-storage';
export function storeUserLogin(loginData) {
    return (dispatch) => {
        if (loginData) {
            dispatch({ type: types.USERLOGIN, data: loginData });
        }
        else {
            AsyncStorage.getItem('user', (err, result) => {
                dispatch({ type: types.USERLOGIN, data: result });
            })
        }
    }
}

export function CheckRemember(data) {
    return (dispatch) => {
        if (data) {
            dispatch({ type: types.REMEMBER, data: data });
        }
        else {
            AsyncStorage.getItem('remember', (err, result) => {
                dispatch({ type: types.REMEMBER, data: result });
            })
        }
    }
}
export function setSession(data) {
    console.log("darta",data)
    return (dispatch) => {
        dispatch({ type: types.requestedUser, data: data });
    }
}