import { cos } from 'react-native-reanimated';
import * as types from './constant'

const initialSettings = {
    userLogin: null,
    userid: null,
    rememberData: null,
    requestedUser: {}

};

const settings = (state = initialSettings, action) => {
    switch (action.type) {
        case types.USERLOGIN:
            const data1 = action.data
            const userLogin = action.data ? JSON.parse(data1) : null;
            const userId = action.data ? JSON.parse(data1).id : null;
            return {
                ...state,
                userLogin: userLogin,
                userId: userId
            };
        case types.REMEMBER:
            const data = action.data;
            console.log("data123", data)
            return {
                ...state,
                rememberData: data ? JSON.parse(data) : '',
            };
        case types.requestedUser:
            console.log("data45668", action.data)
            return {
                ...state,
                requestedUser: action.data,
            };
        default:
            return state;
    }
};
export default settings
