import React, { useState, useRef, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux'
import {
    ScrollView,
    View,
    Text,
    TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';
export default MAW = (props) => {
    return (
        <View>
            <View style={{ width: '100%', flexDirection: 'row', alignSelf: 'center', marginTop: 670 }}>
                <View style={{ width: '33%', height: 170 }}>
                    <Text style={{ color: '#e0e3ee', fontSize: 16, fontWeight: 'bold' }}>{strings.WELLBEING}</Text>
                    <ScrollView showsVerticalScrollIndicator={false} style={{ height: 70 }}>
                        {props.wellbeingWheelList && props.wellbeingWheelList.length > 0 && props.wellbeingWheelList.map((item, index) => {
                            return (
                                <View key={index} style={{ flexDirection: 'row', paddingTop: 5, paddingBottom: 5 }}>
                                    <View style={{ height: 20, width: 20, backgroundColor: item.color, borderRadius: 10, }}></View>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#fff', marginLeft: 10 }}>{item.name}</Text>
                                </View>
                            )
                        })}
                    </ScrollView>

                </View>
                <View style={{ width: '33%', height: 170 }}>
                    <Text style={{ color: '#e0e3ee', fontSize: 16, fontWeight: 'bold' }}>{strings.MEDICINE}</Text>
                    <ScrollView showsVerticalScrollIndicator={false} style={{ height: 70 }}>
                        {props.medicineWheelList && props.medicineWheelList.length > 0 && props.medicineWheelList.map((item, index) => {
                            return (
                                <View key={index} style={{ flexDirection: 'row', paddingTop: 5, paddingBottom: 5 }}>
                                    <View style={{ height: 20, width: 20, backgroundColor: item.color, borderRadius: 10, }}></View>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#fff', marginLeft: 10 }}>{item.name}</Text>
                                </View>
                            )
                        })}
                    </ScrollView>
                </View>
                <View style={{ width: '33%', height: 170 }}>
                    <Text style={{ color: '#e0e3ee', fontSize: 16, fontWeight: 'bold' }}>{strings.ACTIVITIES}</Text>
                    <ScrollView showsVerticalScrollIndicator={false} style={{ height: 70 }}>
                        {props.activityWheelList && props.activityWheelList.length > 0 && props.activityWheelList.map((item, index) => {
                            return (
                                <View key={index} style={{ flexDirection: 'row', paddingTop: 5, paddingBottom: 5 }}>
                                    <View style={{ height: 20, width: 20, backgroundColor: item.color, borderRadius: 10, }}></View>
                                    <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#fff', marginLeft: 10 }}>{item.name}</Text>
                                </View>
                            )
                        })}
                    </ScrollView>
                </View>
            </View>
        </View >
    )
}
