import React, { useState, useRef, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux'
import {
    TextInput,
    Switch,
    StyleSheet,
    ScrollView,
    SafeAreaView,
    View,
    Alert,
    Image,
    Text,
    TouchableOpacity,
    Animated,
    RefreshControl
} from 'react-native';
import Timeline from 'react-native-timeline-flatlist';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default  TimelineDisplay = (props) => {
    // console.log("props....", props.sortedActivities)
    return (
        <View style={{ alignItems: 'center', justifyContent: 'center', width: 370, height: 750, position: "absolute", top: -10, left: -20, fontFamily: 'HelveticaNeue'}}>
            <TouchableOpacity onPress={props.onRefresh}>
                <Text style={styles.title}>TIMELINE <Icon style={{top:5}} name="refresh" color="white" size={20} /></Text>
            </TouchableOpacity>
            <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: 60, width: '100%' }}>
                <View style={styles.container}>
                    <Timeline
                        data={props.sortedActivities}
                        circleSize={30}

                        lineColor="#fff"
                        timeContainerStyle={{ minWidth: 52, minHeight: 70, marginTop: -5, color: "#fff", showsVerticalScrollIndicator: false }}
                        timeStyle={{
                            textAlign: 'center',
                            backgroundColor: 'transparent',
                            fontWeight: 'bold',
                            color: '#fff',
                            padding: 5,
                            borderRadius: 13,
                        }}
                        titleStyle={{ color: '#fff' }}
                        descriptionStyle={{ color: '#fff', width: 200, }}
                        options={{
                            style: { paddingTop: 5, color: "#fff" },
                        }}
                        innerCircle={'icon'}
                        renderDetail={props.renderDetail}
                    />
                </View>
            </ScrollView>
        </View>
    )
}

const styles = StyleSheet.create({
    
    container: {
        flex: 1,
        color: "#fff",
        // borderWidth:1,
        // borderColor:"#fff",
        width: 320,
        position: "relative",
        left: 0
    },
    title: {
        padding: 16,
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        color: "#fff",
        textTransform:'uppercase',
    },
})
