import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet } from 'react-native';

export default function RadioButtons({ options, selectedOption, onSelect }) {
    console.log("selecgeIptions", selectedOption)
    console.log("selecgeIptions", options)
    return (
        <View style={{ flexDirection: 'row' }}>
            {options.map((item) => {
                return (
                    <View key={item.key} style={styles.buttonContainer}>
                        <Text style={{ color: 'white', fontSize: 18, fontWeight: '500' }}>{item.text}</Text>
                        <TouchableOpacity
                            style={selectedOption === item.key  ? styles.selectedcircle : styles.circle}
                            onPress={() => {
                                onSelect(item);
                            }}>
                            <Text style={{top:3,left:0.5}}>
                                {selectedOption === item.key &&
                                    <View style={styles.checkedCircle} />
                                }
                            </Text>
                        </TouchableOpacity>
                    </View>
                );
            })}
        </View>
    );
}

const styles = StyleSheet.create({
    buttonContainer: {
        flexDirection: 'row',
        marginLeft: 10,
        marginRight: 20,
        justifyContent: 'space-between',
        alignItems: 'center',
        marginTop: 20,
    },

    circle: {
        height: 22,
        width: 22,
        borderRadius: 12,
        borderWidth: 2,
        marginLeft: 10,
        borderColor: 'white',
        alignItems: 'center',
        justifyContent: 'center',
    },
    selectedcircle: {
        height: 22,
        width: 22,
        borderRadius: 12,
        borderWidth: 3,
        marginLeft: 10,
        borderColor: '#69c2d1',
        alignItems: 'center',
        justifyContent: 'center',
    },

    checkedCircle: {
        width: 10,
        height: 10,
        borderRadius: 10,
        position:'relative',
        backgroundColor: '#69c2d1',
    },
});
