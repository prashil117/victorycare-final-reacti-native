import React, { useState, useRef } from "react";
import { View, Text, Alert, StyleSheet, Image } from 'react-native';
import AwesomeAlert from 'react-native-awesome-alerts';
import { useDispatch, useSelector } from 'react-redux'
import { setSession } from '../redux/action'
import axios from 'axios'
import config from '../appconfig/config';
import {
    DrawerContentScrollView,
    DrawerItemList,
    DrawerItem,
} from '@react-navigation/drawer';

import AsyncStorage from '@react-native-community/async-storage';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

// import AwesomeAlert from 'react-native-awesome-alerts';

const CustomSidebarMenu = (props) => {
    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const [awesomeAlert, setAwesomeAlert] = useState(false);
    const dispatch = useDispatch();
    const Logout = () => {
        setAwesomeAlert(false)
        var logData = {
            action: 'insertlog',
            userid: currentUser.id,
            page:'logoutmenu',
            activity: 'loggedout',
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "auditlogs.php", logData).then(result => {
            console.log("get audit logs:", result.data.status)
            if (result.data.status == 'success') {
                console.log("Log entered successfully!")
            }
        })
        AsyncStorage.removeItem('user');
        dispatch(setSession())
        props.navigation.replace('Auth');
    }
    return (
        <View style={stylesSidebar.sideMenuContainer}>
            <DrawerContentScrollView {...props}>
                <DrawerItemList {...props} />
                <DrawerItem
                    label={({ color }) =>
                        // <Text style={{ color: '#d8d8d8', fontSize:12 }}>
                        //   <Image style={{position:'relative', top:-60, left:'30%', width:35, height:35}} source={require('../../Image/logout.png')} />
                        //     {'\n\nLOGOUT'}
                        //   </Text>
                        <View style={{ height: 72, left: 14, width: "100%", alignItems: 'center', top: -8 }}>
                            <View style={{ backgroundColor: '#fb7e7e', borderRadius: 50, height: 50, width: 50 }}>
                            </View>
                            <Image style={{ top: -35, width: 25, height: 25, marginBottom: 5 }}
                                source={require('../../../Image/logout-white.png')}
                            />
                            <Text style={{ color: 'white', top: -23, fontWeight: '600', fontSize: 12 }}>{strings.LOGOUT}</Text>
                        </View>
                    }
                    style={{
                        width: "100%", height: 80, color: 'white', right: 10, borderBottomWidth: 1, borderBottomColor: '#697395'
                    }}
                    onPress={() => {
                        props.navigation.toggleDrawer();
                        setAwesomeAlert(true);
                        // showAlert();
                        // setShowAlert(true)
                        // Alert.alert(
                        //     'Logout',
                        //     'Are you sure? You want to logout?',
                        //     [
                        //         {
                        //             text: 'Cancel',
                        //             onPress: () => {
                        //                 return null;
                        //             },
                        //         },
                        //         {
                        //             text: 'Confirm',
                        //             onPress: () => {
                        //                 AsyncStorage.removeItem('user');
                        //                 props.navigation.replace('Auth');
                        //             },
                        //         },
                        //     ],
                        //     { cancelable: false },
                        // );
                    }}
                />
            </DrawerContentScrollView>

            <View>
                <AwesomeAlert
                    contentContainerStyle={{
                        backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 12,
                        },
                        shadowOpacity: 0.58,
                        shadowRadius: 16.00,

                        elevation: 24
                    }}
                    show={awesomeAlert}
                    showProgress={false}
                    // title={<Text>Are you sure wa</Text>}
                    message="Are you sure you want to logout?"
                    messageStyle={{ color: 'white', fontSize: 22, marginBottom: 24, fontWeight: '600' }}
                    closeOnTouchOutside={false}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText={strings.NO}
                    confirmText={strings.YES}
                    cancelButtonColor="#516b93"
                    confirmButtonColor="#69c2d1"
                    confirmButtonBorderRadius={40}
                    confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                    cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                    cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                    confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                    onCancelPressed={() => {
                        setAwesomeAlert(false);
                    }}
                    onConfirmPressed={() => {
                        Logout()
                    }}
                />
            </View>
        </View>
    );
};

export default CustomSidebarMenu;

const stylesSidebar = StyleSheet.create({
    sideMenuContainer: {
        width: '100%',
        height: '100%',
        backgroundColor: '#3a4159',
        // paddingTop: 40,
        color: 'white',
    },
    profileHeader: {
        flexDirection: 'row',
        backgroundColor: '#3a4159',
        padding: 15,
        textAlign: 'center',
    },
    profileHeaderPicCircle: {
        width: 60,
        height: 60,
        borderRadius: 60 / 2,
        color: 'white',
        backgroundColor: '#ffffff',
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center',
    },
    profileHeaderText: {
        color: 'white',
        alignSelf: 'center',
        paddingHorizontal: 10,
        fontWeight: 'bold',
    },
    profileHeaderLine: {
        height: 1,
        marginHorizontal: 20,
        backgroundColor: '#e2e2e2',
        marginTop: 15,
    },
});
