import React, { useState, useEffect } from 'react';
import { Modal, Portal, Text, FAB, Provider, Snackbar } from 'react-native-paper';

export default Snack = (props) => {

    const [visibleSnackbar, setVisibleSnack] = React.useState(props.value);
    const onDismissSnackBar = () => setVisibleSnack(false);

    return (
        <Snackbar
            style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
            visible={visibleSnackbar}
            duration={3000}
            onDismiss={onDismissSnackBar}
        >
            {props.msg}
        </Snackbar>
    )

}