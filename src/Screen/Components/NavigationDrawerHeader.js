import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { View, Image, TouchableOpacity, Text } from 'react-native';
import drawerWhite from '../../../Image/drawerWhite.png'


const NavigationDrawerHeader = (props) => {
  const currentUser = useSelector(state => state.auth.userLogin)
  const toggleDrawer = () => {
    props.navigationProps.toggleDrawer();
  };

  return (
    <View style={{ flexDirection: 'row' }}>
      <TouchableOpacity onPress={toggleDrawer}>
        <Image
          source={drawerWhite}
          style={{ width: 25, height: 25, marginLeft: 5 }}
        />
      </TouchableOpacity>
      <View>
      <Text style={{color:'#fff', paddingLeft:5, fontSize:12, fontStyle:'italic'}}>Welcome {currentUser.firstname} {currentUser.lastname}</Text>
        <Text style={{color:'#81889F', paddingLeft:5, fontSize:12, fontStyle:'italic'}}>Logged in as: {currentUser.usertype}</Text>
      </View>
    </View>
  );
};
export default NavigationDrawerHeader;