import React from 'react';
import { StyleSheet, View, Modal, Text, Image, ActivityIndicator } from 'react-native';
import { COLORS, FONTS, SIZES, icons, images } from '../../constants';

const Loader = (props) => {
  const { loading, ...attributes } = props;

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={loading}
      onRequestClose={() => {
      }}>
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          {/*<ActivityIndicator
            animating={true}
            color="grey"
            size="large"
            style={styles.activityIndicator}
          />*/}
          <Text style={{ color: '#81889f', fontWeight: '500', position:'absolute', top:'50%', left:'60%', zIndex:9970, textAlign:'center' }}>Please Wait,{'\n'} Components of wheel are loading...</Text>
          <Image source={require('../../../Image/splash-wheel.png')} style={{position:'absolute',top:0, left:330, height:570, width:570}} />
        </View>
      </View>
    </Modal>
  );
};

export default Loader;

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040',
  },
  activityIndicatorWrapper: {
    backgroundColor: 'transparent',
    height: '80%',
    width: '80%',
    flexDirection: 'row',
    flexWrap: 'wrap',
    alignItems: 'center',
    marginBottom:50,
    justifyContent: 'space-around',
    // opacity:0.9
  },
  activityIndicator: {
    alignItems: 'center',
    height: 80,
  },
});
