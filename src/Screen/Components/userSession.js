import React from 'react';
import { useSelector, useDispatch } from 'react-redux'
import { StyleSheet, View, Text, Image, TouchableOpacity, Alert } from 'react-native';
import { TouchableItem } from 'react-native-tab-view';
import { setSession, allDataNull, medicineLoading, activityLoading, wellBeingLoading } from '../redux/action'
import { useNavigation } from '@react-navigation/native';
import axios from 'axios'
import config from '../appconfig/config';
const Loader = () => {
    const navigation = useNavigation();
    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const goBack = useSelector(state => state.auth.goBack)
    console.log("goBack", goBack)
    console.log("getProps", navigation)
    const dispatch = useDispatch();
    const removeSession = () => {
        var dataToSend = {
            action: 'insertlog',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : '',
            siblinguserid:currentUser.id,
            page:'header',
            activity: 'sessionend',
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "auditlogs.php", dataToSend).then(result => {
            console.log("get audit logs:", result.data.data)
            if (result.data.data == 'success') {
                console.log("Log entered successfully!")
            }
        })
        dispatch(setSession())
        dispatch(allDataNull())
        dispatch(medicineLoading(false));
        dispatch(activityLoading(false));
        dispatch(wellBeingLoading(false));
        // navigation.navigate(goBack);
        navigation.navigate('AllUsersScreen')
    }

    return (
        <View>
            {sessionUser && sessionUser.userid &&
                <View style={{ flexDirection: 'row', right: 30, position: 'absolute' }}>

                    <View style={styles.modalBackground}>
                        <Text style={{color:'#fff', paddingLeft:5, fontSize:12, fontStyle:'italic'}}>Impersonating:
                        </Text>
                        <Text style={{color:'#81889F', paddingLeft:5, fontSize:12, top:-5, fontStyle:'italic'}}>{sessionUser && sessionUser.userid ? sessionUser.firstname : ''}</Text>
                    </View>
                    <TouchableOpacity onPress={() => {
                        Alert.alert(
                            'Logout of Session',
                            'Stop viewing users profile?',
                            [
                                {
                                    text: 'Cancel',
                                    onPress: () => {
                                        return null;
                                    },
                                },
                                {
                                    text: 'Confirm',
                                    onPress: () => {
                                        removeSession();
                                    },
                                },
                            ],
                            { cancelable: false },
                        );
                    }}>
                        <View style={{ backgroundColor: '#fb7e7e', borderRadius: 30, top: 0, width: 40, height: 40, padding: 4 }}>
                            <Image style={{ height: 30 }} source={require('../../../Image/logout-white.png')} />
                        </View>
                    </TouchableOpacity>
                </View>
            }
        </View>
    );

};

export default Loader;

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        right: 30,
        top:5,
        alignItems: 'flex-end',
        flexDirection: 'column',
        justifyContent: 'space-around',
        // backgroundColor: '#00000040',
    },
    activityIndicatorWrapper: {
        backgroundColor: '#2b3249',
        height: 70,
        width: 190,
        flexDirection: 'row',
        justifyContent: 'space-around',
    },
    activityIndicator: {
        alignItems: 'center',
        height: 80,
    },
});
