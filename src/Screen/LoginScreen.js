import React, { useState, createRef, useEffect } from 'react';
import {
  StyleSheet,
  TextInput,
  View,
  Text,
  ScrollView,
  Image,
  Keyboard,
  TouchableOpacity,
  KeyboardAvoidingView,
  AppState,
  TouchableHighlight,
} from 'react-native';
import { storeUserLogin, CheckRemember, checkDependency } from '../Screen/redux/action'
import { useSelector, useDispatch } from 'react-redux'
import Icon from 'react-native-vector-icons/MaterialIcons';
import AsyncStorage from '@react-native-community/async-storage';
import iid from '@react-native-firebase/iid';
import axios from 'axios'
import config from './appconfig/config';
import style from './css/style'
import Loader from './Components/Loader';
import { Modal, Portal, Provider, Button, FAB } from 'react-native-paper';
import { CheckBox } from 'react-native-elements'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import strings from '../constants/lng/LocalizedStrings';
import { setLng, getLng } from '../helper/changeLng';
import { LoginManager, GraphRequest, GraphRequestManager } from "react-native-fbsdk";
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
import { appleAuth } from '@invertase/react-native-apple-authentication';

const LoginScreen = ({ navigation }) => {
  useEffect(() => { selectedLng() }, [])
  const selectedLng = async () => {
    const lngData = await getLng()

    if (!!lngData || lngData != null) {
      strings.setLanguage(lngData)
    }
  }
  const dispatch = useDispatch();
  const logindata = useSelector(state => state.auth.rememberData)
  console.log('loginData', logindata)
  const [modalVisible, setModalVisible] = useState(false);
  const [userEmail, setUserEmail] = useState(logindata ? logindata.email : '');
  const [userPassword, setUserPassword] = useState(logindata ? logindata.password : '');
  const [check, setCheck] = useState(logindata ? true : false);
  const [visible, setVisible] = useState(false);
  const [visibleEmailModal, setVisibleEmail] = useState(false);
  const [loading, setLoading] = useState(false);
  const [ModalErr, setModalErr] = useState(false);
  const [emailErr, setEmailError] = useState('');
  const [showPassword, setshowPassword] = useState(false);
  const [passwordErr, setPasswordError] = useState('');
  const passwordInputRef = createRef();
  const hideModal = () => setVisible(false);
  const [token, setToken] = useState('');
  const hideModalEmail = () => setVisibleEmail(false);
  // const appState = useRef(AppState.currentState)
  const containerStyle = { backgroundColor: '#2b3249', marginBottom: 30, height: 200, padding: 10, alignItems: 'center', width: '40%', alignSelf: 'center', borderRadius: 8 };
  const containerStyle1 = { backgroundColor: '#2b3249', marginBottom: 30, height: 200, alignItems: 'center', width: '40%', alignSelf: 'center', borderRadius: 8 };
  async function appleLogin() {
    // performs login request
    const authRes = await appleAuth.performRequest({
      requestedOperation: appleAuth.Operation.LOGIN,
      requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
    });
    console.log("Apple Auth Response:", authRes);
    var appleData = {
      firstname: authRes.fullName.givenName ? authRes.fullName.givenName : "Undefined",
      lastname: authRes.fullName.familyName ? authRes.fullName.familyName : "Undefined",
      email: authRes.fullName.email,
      appleid:authRes.user,
      // profilPic: userInfo.user.photo,
      signupfrom:'apple',
      usertype: 'patient',
      action: 'socialmedialogin',
      base_url:config.apiUrl,
      fcmtoken: token,
      token:'expired',
      base_url: config.apiUrl,
      appsecret: config.appsecret,
    };
    loginregisterWithSocialMedia(appleData, 'apple')
  }
  const googleLogin = async() => {
    try{
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn()
      console.log("user info:", userInfo.user)
      var googleData = {
        firstname: userInfo.user.givenName,
        lastname: userInfo.user.familyName,
        email: userInfo.user.email,
        profilPic: userInfo.user.photo,
        // password: userPassword,
        signupfrom:'google',
        usertype: 'patient',
        action: 'socialmedialogin',
        base_url:config.apiUrl,
        fcmtoken: token,
        token:'expired',
        base_url: config.apiUrl,
        appsecret: config.appsecret,
      };
      loginregisterWithSocialMedia(googleData, 'google')
    }catch (error) {
      if(error.code === statusCodes.SIGN_IN_CANCELLED){
        console.log(error)
      }else if(error.code === statusCodes.IN_PROGRESS){
        console.log(error)
      }else if(error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE){
        console.log(error)
      }else{
        console.log(error)
      }
    }
  };
  const fbLogin = (restCallback) => {
    LoginManager.logOut();
    return LoginManager.logInWithPermissions(['email', 'public_profile']).then(
      result => {
        console.log("result==>>", result)
        if(result.declinedPermissions && result.declinedPermissions.includes("email")){
          restCallback({message:"Email is required"})
        }
        if(result.isCancelled){
          console.log("Error")
        }else{
          const infoRequest = new GraphRequest(
            '/me?fields=email,name,picture',
            null,
            restCallback
          );
          new GraphRequestManager().addRequest(infoRequest).start()
        }
      },
      function (error){
        console.log("Login failed with error:"+error)
      }
    )
  }

  const onFbLogin = async() => {
    try{
      await fbLogin(_responseInfoCallback)
    }catch(error){
      console.log("error raised", error)
    }
  }
  const _responseInfoCallback = async(error, result) => {
    if(error){
      console.log("error top", error)
    }else{
      const userData = result;
      console.log("fb userData:", userData)
      const fullname = userData.name.split(' ');
      var fbData = {
        firstname: fullname[0],
        lastname: fullname[1],
        email: userData.email,
        profilPic: userData.picture.data.url,
        // password: userPassword,
        signupfrom:'facebook',
        usertype: 'patient',
        action: 'socialmedialogin',
        base_url:config.apiUrl,
        fcmtoken: token,
        token:'expired',
        base_url: config.apiUrl,
        appsecret: config.appsecret,
      };
      loginregisterWithSocialMedia(fbData, 'facebook')
    }
  }

  const loginregisterWithSocialMedia = (data, platform) => {
    console.log("data:123==>>",data)
    // var dataToSend = {
    //   signupfrom:platform
    // };
    // dataToSend = data;
    console.log("dataTOsend", data); 
    axios.post(config.apiUrl + "login.php", data)
      .then(function (response) {
        let data = response.data;
        setLoading(false);
        console.log("afterlogin data::", data);
        setLoading(true);
        if (data.status === 'success') {
          AsyncStorage.removeItem('user');
          AsyncStorage.setItem('dependency', data.dependency);
          AsyncStorage.setItem('user', JSON.stringify(data.data));
          if (check) {
            AsyncStorage.setItem('remember', JSON.stringify(data));
          }
          else {
            AsyncStorage.removeItem('remember');
          }
          
          dispatch(storeUserLogin(''))
          dispatch(CheckRemember(''))
          AsyncStorage.getItem('dependency').then((value) => {
            dispatch(checkDependency(value))
          })
          // console.log("data==============>",data.data);return false;
          setTimeout(() => {
            setLoading(false)
            var logData = {
                action: 'insertlog',
                userid: data.data.id,
                page:'loginscreen',
                activity: 'loggedin',
                appsecret: config.appsecret,
            }
            axios.post(config.apiUrl + "auditlogs.php", logData).then(result => {
                console.log("get audit logs:", result.data);
                if (result.data.status == 'success') {
                    console.log("Log entered successfully!")
                }
            })
            if (data.data.usertype == "patient" && data.data.firsttimelogin == 1) {
              navigation.replace('Board')
            }
            else if(data.data.usertype == "patient" && data.data.firsttimelogin == '0'){
              // alert("Adsf")
              navigation.replace('DrawerNavigationRoutes', { screen: 'timelineScreenStack' });
            }
            else{
              console.log("hello")
              delete data.data.image
              console.log("data",data.data)
              navigation.replace('DrawerNavigationRoutes', { screen: 'allUsersScreenStack' });
            }
          }, 1000)
        } else if(data.status === 'loginfromother') {
          setModalErr(strings.YOU_ARE_LOGGEDIN+data.signupfrom+strings.PLEASE_LOGIN_FROM_THAT_ACCOUNT);
          setVisible(true);
          setLoading(false)
        } else if(data.status === 'errorEmail') {
          setModalErr(strings.YOUR_ACCOUNT_IS_CREATED_BUT_EMAIL_COULDNT_SENT);
          setVisible(true);
          setLoading(false)
        } else if(data.status === 'errorNotification') {
          setModalErr(strings.PROBLEM_CREATING_MIDDLEWAY_DATA);
          setVisible(true);
          setLoading(false)
        } else {
          setModalErr(strings.PROBLEM_WITH_LOGIN);
          setVisible(true);
          setLoading(false)
        }
      }).catch(function (error) {
        console.log("err", error)
      })
  };
  const setEmail = (e) => {

    let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
    if (e === '' || reg.test(e) === false) {
      setUserEmail(e)
      setEmailError(strings.EMAIL_IN_PROPER_FORMAT)
    }
    else {
      console.log("eifelse", e)
      setUserEmail(e)
      setEmailError('')
    }
  }

  async function getToken() {
    const id = await iid().getToken();
    console.log("id",id)
    setToken(id);
    return id;
  }

  useEffect(() => {
    GoogleSignin.configure()
    getToken();
  }, [])


  const setPassword = (e) => {
    if (e === '') {
      setPasswordError(strings.PASS)
    }
    else {
      setUserPassword(e)
      setPasswordError('')
    }
  }

  const handleSubmitPress = () => {
    if (!userEmail) {
      setEmailError(strings.EMAIL_REQUIRED)
      return;
    }
    if (!userPassword) {
      setPasswordError(strings.PLEASE_ENTER_PASSWORD)
      return;
    }
    setLoading(true);
    let dataToSend = {
      email: userEmail,
      password: userPassword,
      appsecret: config.appsecret,
      signupfrom:'manual',
      fcmtoken: token,
      check: check,
      action: 'getdetails'
    };
    axios.post(config.apiUrl + "login.php", dataToSend)
      .then(function (response) {
        let data = response.data;
        console.log("afterlogin data::", data)
        if (data.status === 'success') {
          AsyncStorage.removeItem('user');
          AsyncStorage.setItem('dependency', data.dependency);
          AsyncStorage.setItem('user', JSON.stringify(data.data));
          if (check) {
            AsyncStorage.setItem('remember', JSON.stringify(dataToSend));
          }
          else {
            AsyncStorage.removeItem('remember');
          }
          
          dispatch(storeUserLogin(''))
          dispatch(CheckRemember(''))
          AsyncStorage.getItem('dependency').then((value) => {
            dispatch(checkDependency(value))
          })
          // console.log("data==============>",data.data);return false;
          setTimeout(() => {
            setLoading(false)
            var logData = {
                action: 'insertlog',
                userid: data.data.id,
                page:'loginscreen',
                activity: 'loggedin',
                appsecret: config.appsecret,
            }
            axios.post(config.apiUrl + "auditlogs.php", logData).then(result => {
                console.log("get audit logs:", result.data);
                if (result.data.status == 'success') {
                    console.log("Log entered successfully!")
                }
            })
            if (data.data.usertype == "patient" && data.data.firsttimelogin == 1) {
              navigation.replace('Board')
            }
            else if(data.data.usertype == "patient" && data.data.firsttimelogin == '0'){
              // alert("Adsf")
              navigation.replace('DrawerNavigationRoutes', { screen: 'timelineScreenStack' });
            }
            else{
              console.log("hello")
              delete data.data.image
              console.log("data",data.data)
              navigation.replace('DrawerNavigationRoutes', { screen: 'allUsersScreenStack' });
            }
          }, 1000)
        } else if(data.status === 'inactive') {
          setModalErr(strings.YOUR_ACCOUNT_IS_NOT_ACTIVE);
          setVisible(true);
          setLoading(false)
        } else if(data.status === 'mismatch') {
          setModalErr(strings.YOUR_CREDENTIALS_DID_NOT_MATCH);
          setVisible(true);
          setLoading(false)
        } else if(data.status === 'loginfromother') {
          setModalErr(strings.YOU_ARE_LOGGEDIN+data.signupfrom+strings.PLEASE_LOGIN_FROM_THAT_ACCOUNT);
          setVisible(true);
          setLoading(false)
        }  else {
          setModalErr(strings.PROBLEM_WITH_LOGIN);
          setVisible(true);
          setLoading(false)
        }
      }).catch(function (error) {
        console.log("err", error)
      })
  };

  const handleForgotPassPress = () => {
    if (!userEmail) {
      setEmailError(strings.PLEASE_ENTER_EMAIL)
      return;
    }
    setLoading(true);
    let dataToSend = {
      email: userEmail,
      appsecret: config.appsecret,
      action: 'passwordreset'
    };
    // console.log("datattosend::-", dataToSend); return false;
    axios.post(config.apiUrl + "resetpassword.php", dataToSend)
      .then(function (response) {
        setLoading(false)
        console.log("reset pwd data:", response.data)
        let data = response.data;
        if (data.status === 'success') {
          setVisibleEmail(false);
          setVisible(true);
          setModalErr(strings.RESET_EMAIL_HAS_BEEN_SENT);

        } else {
          setModalErr(strings.PROBLEM_SETTING_PASSWORD);
          setVisible(true);
        }
      }).catch(function (error) {
        console.log("err", error)
      })
  };

  const clearEmailAddress=()=>{
    setUserEmail('')
    setEmailError('')
  }

  return (
    <View style={styles.mainBody}>
      <Loader loading={loading} />
      <View style={styles.centeredView}>
        <Modal
          animationType='fade'
          transparent={true}
          shadowOpacity={0.5}
          shadowColor={'#fb7e7e'}
          visible={modalVisible}
          onRequestClose={() => {
            console.log("model closed")
          }}
        >
          <View style={styles.centeredView}>
            <View style={styles.modalView}>
              <TouchableHighlight
                style={{ alignSelf: 'flex-end', marginTop: -4 }}
                onPress={() => {
                  setModalVisible(!modalVisible);
                }}
              >
                <Icon name="close" size={22} color="grey" />
              </TouchableHighlight>
              <Text style={styles.modalText}>{ModalErr}</Text>
            </View>
          </View>
        </Modal>
      </View>
      {/* <ScrollView
        keyboardShouldPersistTaps="handled"
        contentContainerStyle={{
          flex: 1,
          justifyContent: 'center',
          alignContent: 'center',
        }}> */}
     <KeyboardAwareScrollView
     enableOnAndroid={true}>
        <View>
          <KeyboardAvoidingView enabled>
            <View style={{ alignItems: 'center' }}>
              <Text style={{ color: 'white', fontSize: 34, fontFamily: 'HelveticaNeue' }}>{strings.LOGIN}</Text>
            </View>
            <Text
              style={styles.registerTextStyle}
              onPress={() => navigation.navigate('RegisterScreen')}>
              {strings.DON_T_HAVE_AN_ACCOUNT}&nbsp;&nbsp;
              <Text style={{ color: '#69c2d1', }}>
                {strings.SIGN_UP}
               </Text>
            </Text>
            <View style={styles.labelSectionStyle}>
              <Text style={styles.label}>
                {strings.EMAIL_ADDRESS}
               </Text>
            </View>

            <View style={[styles.SectionStyle,{ borderBottomColor: emailErr!=''&&userEmail!='' ? '#D26C67': '#dadae8' }]}>
              <TextInput
                style={styles.inputStyle}
                onChangeText={(UserEmail) =>
                  setEmail(UserEmail)
                }
                value={userEmail}
                placeholder={strings.PLEASE_ENTER_EMAIL} //dummy@abc.com
                placeholderTextColor="#8b9cb5"
                autoCapitalize="none"
                keyboardType="email-address"
                returnKeyType="next"
                onSubmitEditing={() =>
                  passwordInputRef.current &&
                  passwordInputRef.current.focus()
                }
                 underlineColorAndroid="#f000"
                blurOnSubmit={false}/>

               {emailErr==''&&userEmail!=''?<Icon style={{marginRight:10}}name="check" size={22} color="#4AC47F" />:null}
               {userEmail!==''?<TouchableOpacity onPress={()=>clearEmailAddress()}>
             <Icon name="close" size={22} color={emailErr!=''?"#D26C67":"grey"} />
             </TouchableOpacity>:null}

            </View>
            <View>{emailErr != '' ? (
              <Text style={styles.errorTextStyle}>
                {emailErr}
              </Text>
            ) : null}
            </View>
            <View style={styles.labelSectionStyle}>
              <Text style={styles.label}>
                {strings.PASSWORD}
               </Text>
            </View>
            <View style={style.SectionStyle}>
              <TextInput
                style={style.inputStyle}
                onChangeText={(UserPassword) =>
                  setPassword(UserPassword)
                }
                value={userPassword}
                placeholder={strings.PLEASE_ENTER_PASSWORD}
                placeholderTextColor="#8b9cb5"
                keyboardType="default"
                ref={passwordInputRef}
                onSubmitEditing={Keyboard.dismiss}
                blurOnSubmit={false}
                secureTextEntry={showPassword ? false : true}
                underlineColorAndroid="#f000"
                returnKeyType="next"
              />
              <View style={{ zIndex: 999, right: 30 }}>
                {showPassword ?
                  <TouchableOpacity onPress={() => setshowPassword(false)}>
                    <Icon name="visibility-off" color="grey" size={24} />
                  </TouchableOpacity> :
                  <TouchableOpacity onPress={() => setshowPassword(true)}>
                    <Icon name="visibility" color="grey" size={24} />
                  </TouchableOpacity>
                }
              </View>
            </View>
            <View>{passwordErr != '' ? (
              <Text style={styles.errorTextStyle}>
                {passwordErr}
              </Text>
            ) : null}</View>
            <View style={{ left: 15, marginBottom: 30 }}>
              <Text style={{ color: '#aeaebd', fontSize: 14 }}>{strings.FORGOT_YOUR_PASSWORD_CLI} <TouchableOpacity onPress={() => setVisibleEmail(true)}><Text style={{ color: '#69c2d1', borderBottomWidth: 1, top: 4, borderColor: '#69c2d1', fontSize: 14 }}>{strings.CLICK_HERE}</Text></TouchableOpacity></Text>
            </View>
            <View style={{ width: 400 }}>
              <CheckBox
                center
                backgroundColor='transparent'
                containerStyle={{ backgroundColor: 'transparent', borderWidth: 0 }}
                title={<Text style={{ color: '#aeaebd', left: 10, fontSize: 14 }}>{strings.REMEMBER_ME_FOR_NEXT_LOGI}</Text>}
                checkedColor={'#69c2d1'}
                onIconPress={() => setCheck(!check)}
                checked={check}
              />
            </View>
            <TouchableOpacity
              style={styles.buttonStyle}
              activeOpacity={0.5}
              onPress={handleSubmitPress}>
              <Text style={styles.buttonTextStyle}>{strings.LOGIN}</Text>
            </TouchableOpacity>
            {/* Social media buttons start */}
            <View>
              <Text style={{color:'#81889f', fontSize:12, fontFamily:'HelveticaNeue-Medium', textAlign:'center'}}>Or login using your social media account</Text>
              <View style={{marginTop:20, flexDirection:'row', alignSelf:'center'}}>
                <TouchableOpacity onPress={googleLogin}>
                    <Image style={{width:80, height:80}} source={require('../../Image/google.png')} />
                </TouchableOpacity>
                  {/* <TouchableOpacity style={{height:50, paddingHorizontal:8, backgroundColor:"pink", width:200}} onPress={googleLogin}>
                    <Text>Google Login</Text>
                  </TouchableOpacity> */}
                {/* </View>
                <View style={{marginTop:20}}> */}
                  {/* <TouchableOpacity style={{height:50, paddingHorizontal:8, backgroundColor:"pink", width:200}} onPress={onFbLogin}>
                    <Text>FB Login</Text>
                  </TouchableOpacity> */}
                  <TouchableOpacity onPress={onFbLogin}>
                    <Image style={{width:80, height:80}} source={require('../../Image/facebook.png')} />
                  </TouchableOpacity>
                {/* <View style={{marginTop:20}}>
                  <TouchableOpacity style={{height:50, paddingHorizontal:8, backgroundColor:"pink", width:200}} onPress={appleLogin}>
                    <Text>Apple Login</Text>
                  </TouchableOpacity>
                </View> */}
                <TouchableOpacity onPress={appleLogin}>
                    <Image style={{marginLeft:10, marginTop:10}} source={require('../../Image/white-logo-masked-circular.png')} />
                </TouchableOpacity>
              </View>
            </View>
            {/* Social media buttons end */}
          </KeyboardAvoidingView>
        </View>
        </KeyboardAwareScrollView>
      {/* </ScrollView> */}
      <Provider>
        <Portal>
          <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
            {/* <View style={{alignSelf:'center'}}> */}
            <View style={{ alignSelf: 'flex-end', marginBottom: 50 }}>
              <TouchableOpacity onPress={hideModal}>
                <Icon name="close" color="grey" size={20} />
              </TouchableOpacity>
            </View>
            <Text style={{ color: 'white', fontSize: 16, fontWeight: '600', textAlign: 'center', bottom: 10 }}>{ModalErr}</Text>
            <TouchableOpacity
              style={[styles.buttonStyle, { right: 70, marginBottom: 20 }]}
              activeOpacity={0.5}
              onPress={hideModal}>
              <Text style={styles.buttonTextStyle}>Ok</Text>
            </TouchableOpacity>
          </Modal>
        </Portal>
      </Provider>
      <Provider>
        <Portal>
          <Modal visible={visibleEmailModal} onDismiss={hideModalEmail} contentContainerStyle={containerStyle1}>
            {/* <View style={{alignSelf:'center'}}> */}
            <View style={{ alignSelf: 'flex-end', marginTop: 20 }}>
              <TouchableOpacity onPress={hideModalEmail}>
                <Icon name="close" color="grey" size={20} />
              </TouchableOpacity>
            </View>
            <Text style={{ fontWeight: '700', fontSize: 16, color: 'white' }}>{strings.RECOVER_PASSWORD}</Text>
            <View style={styles.labelSectionStyle}>
              <Text style={{ color: 'white', right: 136 }}>
                {strings.EMAIL_ADDRESS}
               </Text>
            </View>

            <View style={[styles.SectionStyle, { width: 350 },{ borderBottomColor: emailErr!=''&& userEmail!='' ? '#D26C67': '#dadae8' }]}>
              <TextInput
                style={styles.inputStyle}
                onChangeText={(UserEmail) =>
                  setEmail(UserEmail)
                }
                placeholder={strings.PLEASE_ENTER_EMAIL} //dummy@abc.com
                placeholderTextColor="#8b9cb5"
                autoCapitalize="none"
                value={userEmail}
                keyboardType="email-address"
                returnKeyType="next"
                onSubmitEditing={() =>
                  passwordInputRef.current &&
                  passwordInputRef.current.focus()
                }
                underlineColorAndroid="#f000"
                blurOnSubmit={false}
              />
              {emailErr=='' && userEmail!='' ? 
              <Icon style={{marginRight:10}} name="check" size={22} color="#4AC47F" /> : null }
              {userEmail!=='' ? 
              <TouchableOpacity onPress={()=>clearEmailAddress()}>
                <Icon name="close" size={22} color={emailErr != '' ? "#D26C67" : "grey"} />
              </TouchableOpacity>:null}
            </View>
            <TouchableOpacity
              style={[styles.buttonStyle, { right: 70, marginBottom: 30 }]}
              activeOpacity={0.5}
              onPress={handleForgotPassPress}>
              <Text style={styles.buttonTextStyle}>{strings.OK}</Text>
            </TouchableOpacity>
          </Modal>
        </Portal>
      </Provider>
    </View>
  );
};
export default LoginScreen;

const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: '#2b3249',
    alignContent: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    marginTop: 20,
    margin: 10,
    width: 428,
    justifyContent: 'center',
    textAlign: 'center',
    borderBottomWidth: 1,
    //borderBottomColor: '#dadae8'
  },
  labelSectionStyle: {
    flexDirection: 'row',
    top: 20,
    marginLeft: 14,
    fontSize: 18,
    width: 450,
    justifyContent: 'center',
    textAlign: 'center'
  },
  buttonStyle: {
    backgroundColor: '#69c2d1',
    borderWidth: 0,
    color: '#FFFFFF',
    borderColor: '#69c2d1',
    height: 40,
    width: 150,
    alignItems: 'center',
    borderRadius: 30,
    marginLeft: 155,
    marginTop: 20,
    marginBottom: 20,
  },
  buttonTextStyle: {
    color: '#FFFFFF',
    paddingVertical: 10,
    fontSize: 16,
  },
  inputStyle: {
    flex: 1,
    color: 'white',
    flexDirection: 'row',
  },
  registerTextStyle: {
    color: 'grey',
    textAlign: 'center',
    fontSize: 16,
    alignSelf: 'center',
    padding: 10,
  },
  errorTextStyle: {
    color: '#fb7e7e',
    textAlign: 'left',
    fontSize: 14,
    left: 12
  },
  alertbox: {
    backgroundColor: '#000',
  },
  button: {
    backgroundColor: '#4ba37b',
    width: 100,
    borderRadius: 50,
    alignItems: 'center',
    marginTop: 100
  },
  label: {
    fontFamily: 'HelveticaNeue-Light',
    fontSize: 18,
    color: '#fff',
    flex: 1,
    flexDirection: 'row',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "#2b3249",
    borderRadius: 10,
    padding: 15,
    height: 100,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 20,
      height: 20
    },
    shadowOpacity: 0.35,
    shadowRadius: 3.84,
    elevation: 5
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginTop: 15,
    marginBottom: 15,
    textAlign: "center",
    color: 'white',
  },
  socialMediaBtn: {
    // width:64,
    // height:64,
  },
});
