import * as types from './constant'
import AsyncStorage from '@react-native-community/async-storage';
const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}
export function storeUserLogin(loginData) {
    return (dispatch) => {
        if (loginData) {
            dispatch({ type: types.USERLOGIN, data: loginData });
        }
        else {
            AsyncStorage.getItem('user', (err, result) => {
                dispatch({ type: types.USERLOGIN, data: result });
            })
        }
    }
}
export function checkDependency(data) {
    console.log("aaaaaaaaa:",data)
    return (dispatch) => {
        dispatch({ type: types.DEPENDENCY, data: data });
    }
}

export function setRouteBack(data) {
    console.log("aaaaaaaaa:",data)
    return (dispatch) => {
        dispatch({ type: types.BACK, data: data });
    }
}

export function CheckRemember(data) {
    return (dispatch) => {
        if (data) {
            dispatch({ type: types.REMEMBER, data: data });
        }
        else {
            AsyncStorage.getItem('remember', (err, result) => {
                dispatch({ type: types.REMEMBER, data: result });
            })
        }
    }
}
export function setSession(data) {
    return (dispatch) => {
        dispatch({ type: types.requestedUser, data: data });
    }
}

export function setModal(data) {
    return (dispatch) => {
        dispatch({ type: types.DATEMODAL, data: data });
    }
}

export function setDate(data) {
    return (dispatch) => {
        dispatch({ type: types.SETDATE, data: data });

    }
}

export function medicineLoading(data) {
    return (dispatch) => {
        dispatch({ type: types.MEDICINE_LOADING, data: data });
    }
}

export function activityLoading(data) {
    return (dispatch) => {
        dispatch({ type: types.ACTIVITY_LOADING, data: data });
    }
}

export function wellBeingLoading(data) {

    return (dispatch) => {
        dispatch({ type: types.WELLBEING_LOADING, data: data });
    }
}


export function medicineDataSource(data) {
    console.log("MedicnedataaRedux :::>",data)
    return (dispatch) => {
        dispatch({ type: types.MEDICINE_DATA, data: data });
    }
}


export function activityDataSource(data) {
    return (dispatch) => {
        dispatch({ type: types.ACTIVITY_DATA, data: data });
    }
}


export function wellBeingDataSource(data) {
    return (dispatch) => {
        dispatch({ type: types.WELLBEING_DATA, data: data });
    }
}

export function allDataNull() {
    return (dispatch) => {
        dispatch({ type: types.ALLDATA_NULL, data: [] });
        // setTimeout(() => {
        //     dispatch({ type: types.ALLDATA_NULL, data: [] });
        // }, 1000)
    }
}



export function medicineOffset(data) {
    return (dispatch) => {
        dispatch({ type: types.MEDICINE_OFFSET, data: data });
    }
}
export function updateWheel(data) {
    console.log("update wheel data",data)
    return (dispatch) => {
        dispatch({ type: types.UPDATEWHEEL, data: data });
    }
}


export function activityOffset(data) {
    return (dispatch) => {
        dispatch({ type: types.ACTIVITY_OFFSET, data: data });
    }
}


export function wellBeingOffset(data) {
    return (dispatch) => {
        dispatch({ type: types.WELLBEING_OFFSET, data: data });
    }
}


export function Logout(data) {
    return (dispatch) => {
        dispatch({ type: types.LOGOUT, data: data });
    }
}

export function setToken(data) {
    return (dispatch) => {
        dispatch({ type: types.FCMTOKEN, data: data });
    }
}