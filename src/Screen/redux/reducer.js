import { cos } from 'react-native-reanimated';
import * as types from './constant'

const initialSettings = {
    userLogin: null,
    userid: null,
    dependancy: null,
    rememberData: null,
    requestedUser: {},
    currentDate: new Date(),
    openDatePicker: false,
    medicineListLoading: false,
    activityListLoading: false,
    wellBeingListLoading: false,
    medicineDataSource: [],
    activityDataSource: [],
    wellBeingDataSource: [],
    moffset: 0,
    aoffset: 0,
    woffset: 0,
    fcmToken: '',
    updateWheel: false,
    goBack: ''

};

const settings = (state = initialSettings, action) => {
    console.log("action::", action)
    switch (action.type) {
        case types.USERLOGIN:
            const data1 = action.data
            const userLogin = action.data ? JSON.parse(data1) : null;
            const userId = action.data ? JSON.parse(data1).id : null;
            return {
                ...state,
                userLogin: userLogin,
                userId: userId
            };
        case types.REMEMBER:
            const data = action.data;
            console.log("data123", data)
            return {
                ...state,
                rememberData: data ? JSON.parse(data) : '',
            };
        case types.DEPENDENCY:
            console.log("dependency=>>>", action.data)
            return {
                ...state,
                dependancy: action,
            };
        case types.requestedUser:
            // console.log("data45668", action.data)
            return {
                ...state,
                requestedUser: action.data,
            };
        case types.UPDATEWHEEL:
            // console.log("data45668", action.data)
            return {
                ...state,
                updateWheel: !state.updateWheel,
            };
        case types.SETDATE:
            // console.log("data45668", action.data)
            return {
                ...state,
                currentDate: action.data && action.data !== '' ? action.data : new Date(),
            };
        case types.DATEMODAL:
            return {
                ...state,
                openDatePicker: !state.openDatePicker,
            };
        case types.MEDICINE_LOADING:
            console.log("acada", action)
            return {
                ...state,
                medicineListLoading: action.data,
            };
        case types.ACTIVITY_LOADING:
            return {
                ...state,
                activityListLoading: action.data,
            };
        case types.WELLBEING_LOADING:
            return {
                ...state,
                wellBeingListLoading: action.data,
            };

        case types.LOGOUT:
            return {
                state: {},
            };

        case types.FCMTOKEN:
            return {
                fcmToken: action.data,
            };

        case types.MEDICINE_DATA:
            return {
                ...state,
                medicineDataSource: state.medicineDataSource.concat(action.data)
            };
        case types.ACTIVITY_DATA:
            return {
                ...state,
                activityDataSource: state.activityDataSource.concat(action.data)
            };
        case types.WELLBEING_DATA:
            return {
                ...state,
                wellBeingDataSource: state.wellBeingDataSource.concat(action.data)
            };
        case types.BACK:
            return {
                ...state,
                goBack: action.data
            };
        case types.ALLDATA_NULL:
            return {
                ...state,
                wellBeingDataSource: [],
                activityDataSource: [],
                medicineDataSource: [],
                moffset: 0,
                aoffset: 0,
                woffset: 0,
                medicineListLoading: false,
                activityListLoading: false,
                wellbeingListLoading: false,
            };

        case types.MEDICINE_OFFSET:
            return {
                ...state,
                moffset: action.data
            };
        case types.ACTIVITY_OFFSET:
            return {
                ...state,
                aoffset: action.data
            };
        case types.WELLBEING_OFFSET:
            return {
                ...state,
                woffset: action.data
            };

        default:
            return state;
    }
};
export default settings
