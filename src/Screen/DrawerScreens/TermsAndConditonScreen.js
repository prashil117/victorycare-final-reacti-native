import React from 'react';
import { StyleSheet, ScrollView, Text, View, Linking } from 'react-native';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const TermsScreen = ({ navigation }) => {
    return (
        <View style={styles.mainBody}>
            <ScrollView showsVerticalScrollIndicator={false} >
                <View style={{ alignItems: 'flex-start', marginLeft: 40, marginRight: 40, marginTop: 20 }}>
                    <Text style={styles.headerText}>
                        1. {strings.ONE}
                    </Text>
                    <Text style={styles.subHeader}>
                        1.1
                    </Text>
                    <Text style={styles.subText}>
                        {strings.ONE_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        1.2
                    </Text>
                    <Text style={styles.subText}>
                        {strings.ONE_2}
                    </Text>
                    {/* <Text style={styles.subText}>
                        Please visit our website at victory.care to contact us.
                    </Text> */}
                    <Text style={styles.subHeader}>
                        1.3
                    </Text>
                    <Text style={styles.subText}>
                        {strings.ONE_3}
                    </Text>
                    <Text style={styles.subHeader}>
                        1.4
                    </Text>
                    <Text style={styles.subText}>
                        {strings.ONE_4}
                    </Text>
                    <Text style={styles.subHeader}>
                        1.5
                    </Text>
                    <Text style={styles.subText}>
                        {strings.ONE_5}
                    </Text>
                </View>
                <View style={{ alignItems: 'flex-start', marginLeft: 40, marginRight: 40, marginTop: 20 }}>
                    <Text style={{ color: '#4fcad4', fontWeight: '600', fontSize: 20, paddingBottom: 10 }}>
                        2. {strings.TWO}
                    </Text>
                    <Text style={styles.subText}>
                        {strings.TWO_1_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        2.1
                    </Text>
                    <Text style={styles.subText}>
                        {strings.TWO_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        2.2
                    </Text>
                    <Text style={styles.subText}>
                        {strings.TWO_2}
                    </Text>
                    <Text style={styles.subHeader}>
                        2.3
                    </Text>
                    <Text style={styles.subText}>
                        {strings.TWO_3}
                    </Text>
                    <Text style={styles.subHeader}>
                        2.4
                    </Text>
                    <Text style={styles.subText}>
                        {strings.TWO_4}
                    </Text>
                </View>
                <View style={{ alignItems: 'flex-start', marginLeft: 40, marginRight: 40, marginTop: 20 }}>
                    <Text style={{ color: '#4fcad4', fontWeight: '600', fontSize: 20, paddingBottom: 10 }}>
                        3. {strings.THREE}
                    </Text>
                    <Text style={styles.subText}>
                        {strings.THREE_1_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        3.1
                    </Text>
                    <Text style={styles.subText}>
                        {strings.THREE_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        3.2
                    </Text>
                    <Text style={styles.subText}>
                        {strings.THREE_2}
                    </Text>
                    <Text style={styles.subHeader}>
                        3.3
                    </Text>
                    <Text style={styles.subText}>
                        {strings.THREE_3}
                    </Text>
                    <Text style={styles.subHeader}>
                        3.4
                    </Text>
                    <Text style={styles.subText}>
                        {strings.THREE_4}
                    </Text>
                </View>
                <View style={{ alignItems: 'flex-start', marginLeft: 40, marginRight: 40, marginTop: 20 }}>
                    <Text style={{ color: '#4fcad4', fontWeight: '600', fontSize: 20, paddingBottom: 10 }}>
                        4. {strings.FOUR}
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FOUR_1_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        4.1
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FOUR_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        4.2
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FOUR_2}
                    </Text>
                    <Text style={styles.subHeader}>
                        4.3
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FOUR_3}
                    </Text>
                    <Text style={styles.subHeader}>
                        4.4
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FOUR_4}
                    </Text>
                    <Text style={styles.subHeader}>
                        4.5
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FOUR_5}
                    </Text>
                    <Text style={styles.subHeader}>
                        4.6
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FOUR_6}
                    </Text>
                </View>
                <View style={{ alignItems: 'flex-start', marginLeft: 40, marginRight: 40, marginTop: 20 }}>
                    <Text style={{ color: '#4fcad4', fontWeight: '600', fontSize: 20, paddingBottom: 10 }}>
                        5. {strings.FIVE}
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FIVE_1_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        5.1
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FIVE_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        5.2
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FIVE_2}
                    </Text>
                    <Text style={styles.subHeader}>
                        5.3
                    </Text>
                    <Text style={styles.subText}>
                        {strings.FIVE_3}
                    </Text>
                </View>
                <View style={{ alignItems: 'flex-start', marginLeft: 40, marginRight: 40, marginTop: 20 }}>
                    <Text style={{ color: '#4fcad4', fontWeight: '600', fontSize: 20, paddingBottom: 10 }}>
                        6. {strings.SIX}
                    </Text>
                    <Text style={styles.subText}>
                        {strings.SIX_1_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        6.1
                    </Text>
                    <Text style={styles.subText}>
                        {strings.SIX_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        6.2
                    </Text>
                    <Text style={styles.subText}>
                        {strings.SIX_2}
                    </Text>
                    <Text style={styles.subHeader}>
                        6.3
                    </Text>
                    <Text style={styles.subText}>
                        {strings.SIX_3}
                    </Text>
                    <Text style={styles.subHeader}>
                        6.4
                    </Text>
                    <Text style={styles.subText}>
                        {strings.SIX_4}
                    </Text>
                </View>
                <View style={{ alignItems: 'flex-start', marginLeft: 40, marginRight: 40, marginTop: 20 }}>
                    <Text style={{ color: '#4fcad4', fontWeight: '600', fontSize: 20, paddingBottom: 10 }}>
                    7. {strings.SEVEN}
                    </Text>
                    <Text style={styles.subText}>
                        {strings.SEVEN_1_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        7.1
                    </Text>
                    <Text style={styles.subText}>
                        {strings.SEVEN_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        7.2
                    </Text>
                    <Text style={styles.subText}>
                        {strings.SEVEN_2}
                    </Text>
                    <Text style={styles.subHeader}>
                        7.3
                    </Text>
                    <Text style={styles.subText}>
                        {strings.SEVEN_3}
                    </Text>
                </View>
                <View style={{ alignItems: 'flex-start', marginLeft: 40, marginRight: 40, marginTop: 20 }}>
                    <Text style={{ color: '#4fcad4', fontWeight: '600', fontSize: 20, paddingBottom: 10 }}>
                        8. {strings.EIGHT}
                    </Text>
                    <Text style={styles.subText}>
                        {strings.EIGHT_1_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        8.1
                    </Text>
                    <Text style={styles.subText}>
                        {strings.EIGHT_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        8.2
                    </Text>
                    <Text style={styles.subText}>
                        {strings.EIGHT_2}
                    </Text>
                </View>
                <View style={{ alignItems: 'flex-start', marginLeft: 40, marginRight: 40, marginTop: 20 }}>
                    <Text style={{ color: '#4fcad4', fontWeight: '600', fontSize: 20, paddingBottom: 10 }}>
                        9. {strings.NINE}
                    </Text>
                    <Text style={styles.subText}>
                        {strings.NINE_1_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        9.1
                    </Text>
                    <Text style={styles.subText}>
                        {strings.NINE_1}
                    </Text>
                    <Text style={styles.subHeader}>
                        9.2
                    </Text>
                    <Text style={styles.subText}>
                        {strings.NINE_2}
                    </Text>
                    <Text style={styles.subHeader}>
                        9.3
                    </Text>
                    <Text style={styles.subText}>
                        {strings.NINE_3}
                    </Text>
                    <Text style={styles.subHeader}>
                        9.4
                    </Text>
                    <Text style={styles.subText}>
                        {strings.NINE_4}
                    </Text>
                </View>
            </ScrollView>
        </View>
    );
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        // marginTop: -180,
        backgroundColor: '#2b3249',
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    subHeader: {
        fontFamily: 'HelveticaNeue-Bold',
        color: 'white',
        fontWeight: '500',
        fontSize: 16,
        paddingBottom: 10
    },
    subText: {
        fontFamily: 'HelveticaNeue-Light',
        color: 'white',
        paddingBottom: 10,
        lineHeight: 20
    },
    headerText: {
        fontFamily: 'HelveticaNeue-Bold',
        color: '#4fcad4',
        fontWeight: '600',
        fontSize: 20,
        paddingBottom: 10
    },


});


export default TermsScreen;
