import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    Text,
    StyleSheet,
    RefreshControl,
    Alert,
    FlatList,
    ScrollView,
    Button,
    ActivityIndicator,
    Keyboard
} from 'react-native';
import moment from "moment";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import DropDownPicker from 'react-native-dropdown-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Modal, Portal, Provider, FAB } from 'react-native-paper';
import deletebtn from '../../../Image/delete-btn.png'
import editbtn from '../../../Image/edit-btn.png'
import notificationbtn from '../../../Image/notification-btn.png'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';


const MedicineNotificationScreen = ({ navigation }) => {
    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const [refreshing, setRefreshing] = React.useState(false);
    const [loading, setLoading] = useState(false);
    const [dataSource, setDataSource] = useState([]);
    const [keyword, setKeyword] = useState('');
    const [searchData, setsearchData] = useState('');
    const [offset, setOffset] = useState(0);
    const [dropValues, setDropDownList] = useState([]);
    const [check, setCheck] = useState('start');
    const [isDatePickerVisible1, setDatePickerVisibility1] = useState(false);
    const [type, setType] = useState('');
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const [isListEnd, setIsListEnd] = useState(false);
    const [visible1, setVisible1] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [isAscending, setIsAscending] = useState(true);
    const [isAscNotification, setisAscNotification] = useState(true);
    const [isAscStartdate, setisAscStartdate] = useState(true);
    const [isAscEnddate, setisAscEnddate] = useState(true);
    const showAlert = (id, index) => {
      setAwesomeAlert(true);
      setPopupId(id)
      setPopupIndex(index)
    };

    const hideAlert = () => {
      setAwesomeAlert(false);
    };
    const [time, setTime] = React.useState([]);
    const onDismissSnackBar = () => setVisibleSnack(false);
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const containerStyle = { backgroundColor: '#2b3249', marginBottom: 25, paddingTop: 50, alignItems: 'center', height: 500, width: '70%', alignSelf: 'center', borderRadius: 8, width:'80%', height:'100%', zIndex:4999 };
    const containerStyle1 = { backgroundColor: '#2b3249', marginBottom: 25, padding: 10, alignItems: 'center', height: '90%', width: '80%', alignSelf: 'center', borderRadius: 10 };
    
    useEffect(() => getMedicineData(), [sessionUser]);
    const [allValuesNotification, SetallNotificationValues] = useState({
        id: '',
        preenddate: '',
        prestartdate: '',
        medicine: '',
        enablePrescriptionNotification: false,
        areaexpertise: '',
        prescriptionaddress: '',
    });

    const showModal1 = () => {
        setVisible1(true);
        setType('add');
    }
    const hideModal1 = (key) => {
        for (key in allValuesNotification) {
            if (allValuesNotification.hasOwnProperty(key)) {
                allValuesNotification[key] = null;
            }
        }
        setVisible1(false)
        setTime([])

    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
        setDatePickerVisibility1(false);
    };

    const handleConfirm = (date) => {
        console.log('allValuesNotification.prestartdate:',date)
        if (check === "start") {
            SetallNotificationValues({ ...allValuesNotification, ['prestartdate']: moment(date).format('YYYY-MM-DD'), ['startDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "end") {
            SetallNotificationValues({ ...allValuesNotification, ['preenddate']: moment(date).format('YYYY-MM-DD'), ['endDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check.includes("mul")) {
            let index = check.split(" ")[1];
            const list = [...time];
            list[index]['time'] = moment(date).format('HH:mm')
            setTime(list);
        }

        hideDatePicker();
    };

     // handle click event of the Remove button
     const handleRemoveClick = index => {
        const list = [...time];
        list.splice(index, 1);
        setTime(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setTime([...time, { time: "" }]);
    };

    const searchKeyword = async (value) => {
        console.log("value", value)
        await setKeyword(value);
        await setsearchData(value);
        await setIsListEnd(false);
        await setLoading(false);
        await setDataSource([])
        await setOffset(0)
    }

    useEffect(() => {
        getMedicineDataSearch()
    }, [keyword]);


    const getMedicineDataSearch = () => {

        console.log("result.data=>", "Search...keyword.."+keyword+"offset"+offset)
        if(keyword==''){
            Keyboard.dismiss()
        }
        if (!loading && !isListEnd) {
            setLoading(true);
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                appsecret: config.appsecret,
                hcpid: 0,
                action: 'getpatientmedicinedata',
                keyword: keyword,
                offset: 0
            };
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                console.log("result.data=>", "Search..."+result)
                if (result.data.data !== null) {
                    setLoading(false);
                    setDataSource(result.data.data);
                    setOffset(offset + 10)
                }
                else {
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
        else {
            setLoading(false);
        }
    }
    
    useEffect(() => {
        getDropDownList();
    },[sessionUser]);
    const getDropDownList = () => {
        var data = {
            action: 'getmedicinemasterdatafulllist',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "getdata.php", data).then(result => {
            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            value: val.id,
                            icon: () => <View style={{width:20, height:20, borderRadius:10, backgroundColor:(val.color && val.color != null ? val.color : '#fff')}} />,
                            label: val.name,
                        }
                        arr.push(ob)
                    })
                    setDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }

    const getMedicineData = () => {

        console.log("result.data=>", "..isListEnd..."+isListEnd+"..offset.."+offset)

        if (!loading && !isListEnd) {
            console.log('getData');
            setLoading(true);
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                appsecret: config.appsecret,
                action: 'getpatientmedicinedata',
                keyword: '',
                offset: offset
            };
            console.log("dataToSend:", dataToSend)
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                console.log("result.data=>", result)
                if (result.data.status === "success") {
                  console.log("resule data:",result.data.data);
                    if (result.data.data !== null) {
                        setLoading(false);
                        setDataSource([...dataSource, ...result.data.data]);
                        setOffset(offset + 10)
                    }
                    else {
                        setIsListEnd(true);
                        setLoading(false);
                    }
                }
                else {
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
    };

    const DeleteAlert = (id, index) => {
        showAlert(id, index)
        // Alert.alert(
        //     'Delete',
        //     'Are your sure you want to delete?',
        //     [
        //         {
        //             text: 'No',
        //             onPress: () => { },
        //             style: 'cancel',
        //         },
        //         {
        //             text: 'Yes',
        //             onPress: () => DeleteMedicineNotification(id, index)
        //         },
        //     ],
        //     { cancelable: false },
        // );
    }

    const DeleteMedicineNotification = (id, index) => {
        setLoading(true)
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
        data.appsecret = config.appsecret;
        data.id = id;
        data.siblinguserid = sessionUser && sessionUser.userid ? currentUser.id : '';
        data.action = 'deletepatientmedicine';
        axios.post(config.apiUrl + 'deletedata.php', data).then(result => {
            setLoading(false);
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                dataSource.splice(index, 1)
                setsnackMsg(strings.NOTIFICATION_DELETED_SUCC)
                setVisibleSnack(true)
            }
            else {
                setVisible1(false)
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }
        })
    }


    const AddUpdateMedicineNotification = () => {
        setLoading(true)
        var data = allValuesNotification;
        data.patientid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        data.siblinguserid = sessionUser && sessionUser.userid ? currentUser.id : '';
        data.addedBy = currentUser.id;
        data.hcpid = 0;
        data.doctor = currentUser.id;
        data.appsecret = config.appsecret;
        data.createDate = moment(new Date()).format('YYYY-MM-DD');
        data.action = type === "add" ? 'addpatientmedicine' : 'updatepatientsmedicine';
        file = type === "add" ? 'adddata.php' : 'updatedata.php';
        data.time = time.map(x => x.time).join(',');
        console.log("Data::", data.time)
        axios.post(config.apiUrl + file, data).then(result => {
            setLoading(false);
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                hideModal1();
                setVisible1(false);
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC)
                setVisibleSnack(true)
                setType('add')
                onRefresh();
            }
            else {
                setType('add')
                setVisible1(false)
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }
        })
    }

    const onClickHeaderAscDesc = (type) => {
        // alert(type)
        var sorted;
        // console.log("setDataSource::", dataSource); return false;
        if (isAscending) {
            sorted = [...dataSource].sort((a, b) => {
                // return (a === b)? 0 : a? -1 : 1;
                // return b.time - a.time
                return b.type < a.type ? 1 : -1;
            });
            setIsAscending(false)
        } else {
            setIsAscending(true)
            sorted = [...dataSource].sort((a, b) => {
                // // return (a === b)? a : 0? -1 : 1;
                // return b.time - a.time
                return b.type > a.type ? 1 : -1;
            });
        }
        
        // return false;
        setDataSource(sorted)

        setisAscNotification(false)
        setisAscStartdate(false)
        setisAscEnddate(false)

        if (type === 'enablepresnotification') {
            if (isAscNotification) {
                setisAscNotification(false)
            } else {
                setisAscNotification(true)
            }
        }
        if (type === 'startdate') {
            if (isAscStartdate) {
                setisAscStartdate(false)
            } else {
                setisAscStartdate(true)
            }
        } else if (type === 'enddate') {
            if (isAscEnddate) {
                setisAscEnddate(false)
            } else {
                setisAscEnddate(true)
            }
        }
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setsearchData('')
        setKeyword('')
        wait(2000).then(() => {
            setRefreshing(false);
            setOffset(0);
            setIsListEnd(false)
            setDataSource([]);
            getMedicineData();
        });
    }, []);

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="white"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };
    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{marginTop:20}}>
                <Text style={{color:'#ccc', fontSize:18, fontWeight:'bold' , textAlign:'center'}}>{strings.NO_ITEMS_FOUND}</Text>
            </View>
        );
    };
    const ItemView = ({ item, index }) => {
        return (
            // Flat List Item
            <View key={item.id} style={styles.medicine}>
                <View style={[styles.medicineColor, { backgroundColor: item.color }]}>
                </View>
                <Text style={styles.medicineText}>{item.name}</Text>
                <Switch style={[styles.headerText, { left: -50, marginTop:10 }]}
                    trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                    borderColor={'#81889f'}
                    borderWidth={1}
                    value={item.enablepresnotification && item.enablepresnotification == true ? true : false}
                    onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['enablePrescriptionNotification']: val })}
                    borderRadius={16}
                    thumbColor={"#fff"}
                />
                <View style={{width:200, left:-30}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{ fontSize: 16, width: 100, fontWeight: '500', marginTop: 12, color: 'white' }} >{moment(item.prestartdate).format('DD.MM.YYYY')}</Text>
                        <Text style={{ fontSize: 16, width: 100, fontWeight: '500', marginTop: 12, color: 'white' }}>{moment(item.preenddate).format('DD.MM.YYYY')}</Text>
                    </View>
                    <View style={{width:200}}>
                        <Text style={{color:'#fff'}}>Time Slots:{item.time}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: "row", right: 15, flexWrap: "wrap" }}>
                    <TouchableOpacity onPress={() => { DeleteAlert(item.id, index) }}>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onEditNotification(item)}>
                        <Image style={styles.icons} height={35} width={35} source={editbtn} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };


    const onEditNotification = (data) => {
        console.log("data::>", data)
        allValuesNotification.medicineid = data.id;
        allValuesNotification.medicine = data.medicinemasterid;
        allValuesNotification.areaexpertise = data.areaexpertise
        // allValuesNotification.enablePrescriptionNotification = data.enablePrescriptionNotification;
        allValuesNotification.enablePrescriptionNotification = (data.enablepresnotification === "1" ? true : false);
        allValuesNotification.prescriptionaddress = data.prescaddress;
        allValuesNotification.notification = (data.notification === "1" ? true : false);
        allValuesNotification.selecttime = data.selecttime;
        allValuesNotification.prestartdate = moment(data.prestartdate).format('YYYY-MM-DD');
        allValuesNotification.preenddate = moment(data.preenddate).format('YYYY-MM-DD');
        let arr = data.time.split(',');
        let arr1=[]
        for (var i in arr) {
            arr1.push({time:arr[i]})
        }
        setTime(arr1)
        setVisible1(true);
        setType('edit');
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.mainBody}>
                <AwesomeAlert
                  contentContainerStyle={{
                      backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                      shadowOffset: {
                          width: 0,
                          height: 12,
                      },
                      shadowOpacity: 0.58,
                      shadowRadius: 16.00,

                      elevation: 24
                  }}
                  show={awesomeAlert}
                  showProgress={false}
                  title={strings.DELETE_RECORD}
                  titleStyle={{ color: 'white' }}
                  message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                  messageStyle={{ color: 'white' }}
                  closeOnTouchOutside={true}
                  closeOnHardwareBackPress={false}
                  showCancelButton={true}
                  showConfirmButton={true}
                  cancelText={strings.NO_CANCEL}
                  confirmText={strings.YES_DELETE}
                  cancelButtonColor="#516b93"
                  confirmButtonColor="#69c2d1"
                  confirmButtonBorderRadius="0"
                  confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                  cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                  cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                  confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                  onCancelPressed={() => {
                    setAwesomeAlert(false);
                  }}
                  onConfirmPressed={(id, ) => {
                    DeleteMedicineNotification(popupId, popupIndex);
                  }}
                />
                <View style={styles.main} >
                    <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                        <View style={{ ...styles.SectionStyle, top: 60, alignSelf: 'flex-end' }}>
                            <TextInput
                                style={styles.inputStyle}
                                placeholder={strings.TYPE_TO_SEARCH_HERE}
                                placeholderTextColor="#81889f"
                                autoCapitalize="none"
                                underlineColorAndroid="#81889f"
                                blurOnSubmit={false}
                                value={searchData}
                                onChangeText={(text) => { searchKeyword(text) }}
                            />
                            <Icon style={styles.searchIcon} name="search" size={22} color="#81889f" />
                        </View>
                        <View style={{ paddingTop: 45, left: 120, flexDirection: "row", flexWrap: "wrap" }}>
                            <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }} onPress={() => onClickHeaderAscDesc('enablepresnotification')}>
                                <Text style={styles.headerText}>{strings.NOTIFICATIONS}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }} onPress={() => onClickHeaderAscDesc('startdate')}>
                                <Text style={styles.headerText}>{strings.START_DATE}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }} onPress={() => onClickHeaderAscDesc('enddate')}>
                                <Text style={styles.headerText}>{strings.END_DATE}</Text>
                            </TouchableOpacity>
                            <Text style={styles.headerText}>{strings.DELETE}</Text>
                            <Text style={styles.headerText}>{strings.EDIT}</Text>
                        </View>
                    </View>

                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={dataSource}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={ItemView}
                        ListEmptyComponent={NoDataFound}
                        ListFooterComponent={renderFooter}
                        onEndReached={getMedicineData}
                        onEndReachedThreshold={0.5}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                            />}
                    />
                </View>
                <Provider>
                    <Portal>
                        <Modal visible={visible1} onDismiss={hideModal1} contentContainerStyle={containerStyle1}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                <Text style={{ fontFamily:'HelveticaNeue-Bold', color: 'white', fontWeight: '700', fontSize:22, marginTop:50, }}>{type == 'add' ? strings.ADD_MEDICINE_NOTIFICATION : strings.EDIT_MEDICINE_NOTIFICATION}</Text>
                                <View style={{ left: 250 }}>
                                    <TouchableOpacity onPress={hideModal1}>
                                        <Icon name="close" color="#81889f" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <KeyboardAwareScrollView enableOnAndroid={true}>
                                <View style={{ alignItems: 'center', flexWrap: 'wrap', flexDirection: 'row', marginTop: 50 }}>
                                    <View style={{ height: 500 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{type == 'add' ? strings.ADD_MEDICINE_NOTIFICATION : strings.EDIT_MEDICINE_NOTIFICATION} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                        <DropDownPicker
                                            items={dropValues}
                                            placeholder={strings.PLEASE_SELECT_MEDICINE}
                                            labelStyle={{ fontFamily:'HelveticaNeue-Light', fontSize: 14, textAlign: 'left', color: 'white', zIndex:9969, borderBottomWidth:2, padding:0, margin:0, width:'100%' }}
                                            defaultValue={allValuesNotification.medicine ? allValuesNotification.medicine : null}
                                            containerStyle={{ height: 40, color: 'white', borderWidth: 0, zIndex:9969 }}
                                            style={{ fontFamily:'HelveticaNeue-Light', backgroundColor: '#2b3249', color: '#81889f', borderWidth: 0, borderBottomWidth: 2, borderBottomColor:'#81889f', }}
                                            itemStyle={{
                                                justifyContent: 'flex-start', color: 'white',zIndex:9970, padding:0, margin:0,
                                            }}
                                            dropDownStyle={{ backgroundColor: '#2b3249', color: 'white' }}
                                            onChangeItem={item => SetallNotificationValues({ ...allValuesNotification, ['medicine']: item.value })}
                                        />
                                        <View style={{ marginTop: 40, flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between' }}>
                                            <View>
                                                <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.START_DATE}<Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                                <TextInput
                                                    style={{ width: 135, color: '#81889f', fontSize:16 }}
                                                    placeholder={strings.SELECT_START_TIME}
                                                    placeholderTextColor={'#81889f'}
                                                    editable={false}
                                                    onTouchStart={() => { setDatePickerVisibility1(true); setCheck('start') }}
                                                    value={allValuesNotification.prestartdate}
                                                    borderBottomWidth={2}
                                                    borderColor={'#81889f'}
                                                />
                                            </View>
                                            <DateTimePickerModal
                                                style={{ backgroundColor: '#2b3249', }}
                                                headerTextIOS={strings.SELECT_TIME}
                                                pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                                                modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                                                textColor="white"
                                                onTouchStart={() => { setDatePickerVisibility1(true) }}
                                                isVisible={isDatePickerVisible1}
                                                mode="date"
                                                onConfirm={handleConfirm}
                                                onCancel={hideDatePicker}
                                            />
                                            <View style={{ marginLeft: 30 }}>
                                                <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.END_DATE}<Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                                <TextInput
                                                    style={{ width: 135, color: '#81889f', fontSize:16 }}
                                                    placeholder={strings.SELECT_END_DATE}
                                                    placeholderTextColor={'#81889f'}
                                                    editable={false}
                                                    onTouchStart={() => { setDatePickerVisibility1(true); setCheck('end') }}
                                                    value={allValuesNotification.preenddate}
                                                    borderBottomWidth={2}
                                                    borderColor={'#81889f'}
                                                />
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 40, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                            <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', width: 135, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATIONS}</Text>
                                            <Switch style={[styles.headerText, { left: 30 }]}
                                                trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                borderColor={'#81889f'}
                                                borderWidth={1}
                                                value={allValuesNotification.enablePrescriptionNotification}
                                                onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['enablePrescriptionNotification']: val })}
                                                borderRadius={16}
                                                thumbColor={"#fff"}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ marginRight: 5, marginLeft: 50, height: 500 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.AREA_OF_EXPERTISE}</Text>
                                        <TextInput
                                            style={{ fontFamily:'HelveticaNeue-Light', width: 320, color: 'white', marginTop:20, fontSize:16 }}
                                            placeholder={strings.TYPE_HERE_TEXT}
                                            placeholderTextColor={'#81889f'}
                                            value={allValuesNotification.areaexpertise}
                                            onChangeText={val => SetallNotificationValues({ ...allValuesNotification, ['areaexpertise']: val })}
                                            borderBottomWidth={2}
                                            borderColor={'#81889f'}
                                        />
                                        <View style={{ marginTop: 40 }}>
                                            <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.ADDRESS}</Text>
                                            <TextInput
                                                style={{ fontFamily:'HelveticaNeue-Light', width: 320, color: '#81889f', fontSize:16 }}
                                                placeholder={strings.TYPE_HERE_ADDRESS}
                                                placeholderTextColor={'#81889f'}
                                                value={allValuesNotification.prescriptionaddress}
                                                onChangeText={val => SetallNotificationValues({ ...allValuesNotification, ['prescriptionaddress']: val })}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                        {/* <View> */}
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', fontSize: 14, marginTop: 20 }}>
                                            {strings.TIME}
                                                </Text>
                                                {console.log("Timemmm:", time)}
                                        {time.map((x, i) => {
                                            return (
                                                <View key={i} style={styles.SectionStyle}>
                                                    <TextInput
                                                        style={styles.inputStyle}
                                                        // name="diagnosis"
                                                        placeholder={strings.ADD_TIME}
                                                        editable={false}
                                                        value={x.time}
                                                        onTouchStart={() => { setDatePickerVisibility(true); setCheck(`mul ${i}`) }}
                                                        placeholderTextColor="#81889f"
                                                        underlineColorAndroid="#81889f"
                                                    />
                                                    <View>
                                                        {time.length !== 0 && <TouchableOpacity onPress={() => handleRemoveClick(i)} ><Icon name="close" color="#81889f" size={16} /></TouchableOpacity>}
                                                    </View>
                                                </View>
                                            )
                                        })}
                                        <Button style={{ color: 'white' }} onPress={handleAddClick} title={"+ " + strings.ADD_MEDICINE_TAKEN_TIME}> +{strings.ADD_MEDICINE_TAKEN_TIME}</Button>
                                        {/* </View> */}
                                        {type == "add" ? <TouchableOpacity onPress={() => AddUpdateMedicineNotification()} style={[styles.buttonStyle2, { top: 30 }]} activeOpacity={0.5}>
                                            <Text style={styles.buttonTextStyle}>{strings.SAVE}</Text>
                                        </TouchableOpacity> :
                                        <TouchableOpacity onPress={() => AddUpdateMedicineNotification()} style={[styles.buttonStyle2, { top: 30 }]} activeOpacity={0.5}>
                                            <Text style={styles.buttonTextStyle}>{strings.UPDATE}</Text>
                                        </TouchableOpacity>}
                                    </View>
                                </View>
                                </KeyboardAwareScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                <DateTimePickerModal
                    style={{ backgroundColor: '#2b3249', }}
                    headerTextIOS={'Select Time'}
                    pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                    modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                    textColor="white"
                    headerTextIOS={'Select Time'}
                    locale="en_GB"
                    textColor="white"
                    isVisible={isDatePickerVisible}
                    mode="time"
                    onConfirm={handleConfirm}
                    onCancel={hideDatePicker}
                />
                <FAB
                    style={styles.floatBtn}
                    fabStyle={{ height: 100 }}
                    color={'white'}
                    theme={{ colors: { accent: '#eb8682' } }}
                    icon="plus"
                    onPress={showModal1}
                >
                </FAB>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    main: {
        marginTop: 50
    },
    headerText: {
        fontFamily:'HelveticaNeue-Medium',
        fontSize:12,
        color: '#81889f',
        marginLeft: 40,
    },
    icons: {
        marginLeft: 15,
        marginTop: 4,
        width:35,
        height:35
    },
    searchIcon: {
        paddingTop: 11,

    },
    medicine: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: 820,
        height: 54,
        borderRadius: 15,
        marginBottom: 17,
    },
    medicineText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    medicineColor: {
        backgroundColor: '#e0675c',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    buttonStyle2: {
        fontFamily:'HelveticaNeue-Light', 
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        // marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
        backgroundColor: '#69c2d1'
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#81889f',
        textAlign: 'center'
    },
    nameText: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        right: 35,
        width: 200,
        justifyContent: 'center',
        textAlign: 'center'
    },
    inputStyle: {
        fontFamily:'HelveticaNeue-Light',
        fontSize:16,
        color:'#81889f',
        flex: 1,
        flexDirection: 'row',
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 80,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    }
});

export default MedicineNotificationScreen;
