import React from 'react';
import { StyleSheet, View, Text, Image, } from 'react-native';
import HowWorks from '../../../Image/how-it-work.png';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const HowWorkScreen = ({ navigation }) => {
    return (
        <View style={styles.mainBody}>
            <Image source={HowWorks} />
            <Text style={styles.menuText}>
            {strings.YOU_CAN_RECORD_AND_SHARE}
                </Text>
        </View >
    );
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        marginTop: -180,
        backgroundColor: '#2b3249',
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuText: {
        marginTop:100,
        fontSize: 14,
        fontWeight: '600',
        color: '#AEAEBD',
        marginLeft: 30
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        alignSelf: 'flex-end',
        height: 40,
        width: 130,
        marginRight: 50,
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
        fontWeight: '600'
    },

});


export default HowWorkScreen;
