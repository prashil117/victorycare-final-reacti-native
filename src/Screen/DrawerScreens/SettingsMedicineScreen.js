import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    Text,
    StyleSheet,
    FlatList,
    ActivityIndicator,
} from 'react-native';


import Icon from 'react-native-vector-icons/MaterialIcons';

import deletebtn from '../../../Image/delete-btn.png'
import editbtn from '../../../Image/edit-btn.png'
import notificationbtn from '../../../Image/notification-btn.png'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';



const SettingmedicineMaster = ({ navigation }) => {
    const currentUser = useSelector(state => state.auth.userLogin)
    const [loading, setLoading] = useState(false);
    const [dataSource, setDataSource] = useState([]);
    const [offset, setOffset] = useState(0);
    const [isListEnd, setIsListEnd] = useState(false);

    useEffect(() => getmedicineData(), []);

    const getmedicineData = () => {
        console.log(offset);
        if (!loading && !isListEnd) {
            console.log('getData');
            setLoading(true);
            var dataToSend = {
                userid: currentUser.id,
                appsecret: config.appsecret,
                action: 'getmedicinemasterdata',
                offset: offset
            };
            console.log("offset")
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                console.log("result.data", result.data.data0)
                if (result.data.data0 !== null) {
                    setLoading(false);
                    setDataSource([...dataSource, ...result.data.data0]);
                    setOffset(offset + 10)
                }
                else {
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
    };

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="white"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };

    const ItemView = ({ item }) => {
        return (
            // Flat List Item
            <View key={item.id} style={styles.medicine}>
                <View style={[styles.medicineColor, { backgroundColor: item.color }]}>
                </View>
                <Text style={styles.medicineText}>{item.name}</Text>
                <View style={{ paddingTop: 10, left: 140, flexDirection: "row", flexWrap: "wrap" }}>
                    <TouchableOpacity>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image style={styles.icons} height={35} width={35} source={editbtn} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.mainBody}>
                {/* <Loader loading={loading} /> */}

                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={dataSource}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={ItemView}
                    ListFooterComponent={renderFooter}
                    onEndReached={getmedicineData}
                    onEndReachedThreshold={0.5}
                />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerText: {
        color: 'white',
        marginLeft: 20,
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        width:35,
        height:35,
    },
    medicine: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: 700,
        height: 50,
        borderRadius: 15,
        marginBottom: 17,

    },
    medicineText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    medicineColor: {
        backgroundColor: '#fb7e7e',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    buttonStyle: {
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 50,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    }

});

export default SettingmedicineMaster;
