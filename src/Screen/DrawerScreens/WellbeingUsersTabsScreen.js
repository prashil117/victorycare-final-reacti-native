import React, { useState, useEffect } from 'react';
import {
    TouchableOpacity,
    SafeAreaView,
    View,
    Text,
    StyleSheet,
} from 'react-native';
import PatientsWellbeingScreen from './PatientsWellbeingMasterScreen';
import MyWellbeingScreen from './MyWellbeingMasterScreen';
import { bindActionCreators } from "redux";
import { wellBeingLoading } from '../redux/action'
import AsyncStorage from '@react-native-community/async-storage';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';
import { Snackbar } from 'react-native-paper';
import { useSelector, connect, useDispatch } from "react-redux";
const WellbeingUsersMaster = ({ navigation }) => {
    const [tab, setTab] = useState(1);
    const dependency = useSelector(state => state.auth.dependancy)
    const [dependency1, setDependency] = useState({});
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const dispatch = useDispatch();
    useEffect(() => {
        async function getDependency() {
            AsyncStorage.getItem('dependency', (err, result) => {
                setDependency(result);
            })
        }
        getDependency();
    }, []);
    const onDismissSnackBar = () => setVisibleSnack(false);
    console.log("dependency1:", dependency1)
    const tab1Title = (dependency1 && dependency1 == 'dependent' ? strings.MY_WELLBEING : strings.PATIENTS_WELLBEING);
    const tab2Title = (dependency1 && dependency1 == 'dependent' ? strings.HCPS_WELLBEING : strings.MY_WELLBEING);
    useEffect(() => {
        dispatch(wellBeingLoading(false))
    }, [])

    const setTabfromMaster = () => {
        setTab(1);
    }

    const setMsg = (msg) => {
        console.log("msg")
        setVisibleSnack(true)
        setsnackMsg("WellBeing added successfully")
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.mainBody}>

                {/*Tab Container Start*/}
                <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', textAlign: 'center', position: 'absolute', width: '100%', height: 80, top: 0 }}>
                    <View style={tab === 1 ? styles.tabContainer : styles.tabContainer1}>
                        {/*Tabs Start*/}
                        <View style={tab === 1 ? styles.weekTab : styles.monthTab}>
                            <TouchableOpacity onPress={() => setTab(1)} style={{ right: 0, top: 0 }} activeOpacity={0.5}>
                                <Text style={tab === 1 ? styles.weekText : styles.monthText}>{tab1Title}</Text>
                            </TouchableOpacity>
                        </View>

                        <View style={tab === 2 ? styles.weekTab : styles.monthTab}>
                            <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setTab(2)}>
                                <Text style={tab === 1 ? styles.weekText : styles.monthText}>{tab2Title}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>

                    {tab === 1 && <PatientsWellbeingScreen {...navigation} />}
                    {tab === 2 && <MyWellbeingScreen {...navigation} setSnackMsg={setMsg} setTab={setTabfromMaster} />}
                </View>
                <Snackbar
                    style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                    visible={visibleSnackbar}
                    duration={3000}
                    onDismiss={onDismissSnackBar}
                >
                    {snackMsg}
                </Snackbar>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    tabContainer: {
        // width: 400,
        height: 50,
        flexDirection: 'row',
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        top: 10,
        left: '25%',
        borderRadius: 50,
        backgroundColor: '#474f69',
        alignSelf: 'center',
        alignContent: 'center',
    },
    tabContainer1: {
        width: 400,
        height: 50,
        flexDirection: 'row',
        // borderWidth:1,
        marginLeft: 160,
        marginRight: 160,
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        top: 10,
        left: '14%',
        borderRadius: 50,
        backgroundColor: '#474f69',
    },
    tabContainer2: {
        width: 300,
        height: 50,
        flexDirection: 'row',
        // borderWidth:1,
        marginLeft: 160,
        marginRight: 160,
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        top: 10,
        left: '16%',
        borderRadius: 50,
        backgroundColor: '#474f69',
    },
    weekTab: {
        // width: 150,
        // height:50,
        backgroundColor: '#69c2d1',
        fontSize: 18,
        borderRadius: 50,
        paddingHorizontal: 20,
    },
    Date: {
        width: 240,
        height: 45,
        backgroundColor: '#464e6a',
        margin: 5,
        borderRadius: 10,
        flexDirection: 'row'
    },
    weekText: {
        color: '#fff',
        fontSize: 20,
        textAlign: 'center',
        paddingTop: 10,
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    monthTab: {
        // width: 150,
        // height:50,
        backgroundColor: '#474f69',
        fontSize: 18,
        borderRadius: 50,
        paddingHorizontal: 20,
    },
    monthText: {
        color: '#fff',
        fontSize: 20,
        textAlign: 'center',
        paddingTop: 10,
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    headerText: {
        color: 'white',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    medicine: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: 700,
        height: 50,
        borderRadius: 15,
        marginBottom: 17,
    },
    medicineText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    medicineColor: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        // position: 'absolute',
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8

    },
    imageBackStyle: {
        alignSelf: 'center',
        backgroundColor: '#39415b',
        borderRadius: 10,
        height: 100,
        left: 100,
        alignItems: 'center',
        width: 220

    },
    medicineColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        width: 18
    },
    // imageBackStyle: {
    //     // alignSelf: 'center',
    //     backgroundColor: '#39415b',
    //     borderRadius: 10,
    //     height: 100,
    //     alignItems: 'center',
    //     width: 200,
    //     marginLeft: 100

    // },

});


function mapStateToProps(state) {

    return {
        sessionUser: state.auth.requestedUser,
    };
}
// @ts-ignore
function matchDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(WellbeingUsersMaster);
