import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import {
  TextInput,
  Image,
  TouchableOpacity,
  Switch,
  SafeAreaView,
  View,
  Text,
  Alert,
  StyleSheet,
  RefreshControl,
  Button,
  ScrollView,
  Dimensions,
} from 'react-native';


import Icon from 'react-native-vector-icons/MaterialIcons';
import { setSession, medicineLoading, activityLoading, wellBeingLoading, allDataNull } from '../redux/action'
import Loader from '../Components/Loader';

import axios from 'axios'

import config from '../appconfig/config';
import { DrawerContentScrollView } from '@react-navigation/drawer';
import { Props } from 'react-native-image-zoom-viewer/built/image-viewer.type';
import { Snackbar } from 'react-native-paper';
import AwesomeAlert from 'react-native-awesome-alerts';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';


const Requests = ({ navigation }) => {
  const currentUser = useSelector(state => state.auth.userLogin)
  const sessionUser = useSelector(state => state.auth.requestedUser)
  const dispatch = useDispatch();
  const [requestData, setData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [visibleSnackbar, setVisibleSnack] = React.useState(false);
  const onDismissSnackBar = () => setVisibleSnack(false);
  const [awesomeAlert, setAwesomeAlert] = useState(false)
  const [addedbyId, setaddedbyId] = useState(false)
  const [requestId, setrequestId] = useState(false)
  const [popupIndex, setPopupIndex] = useState(false)
  const [snackMsg, setsnackMsg] = useState('');

  const fetchRequest = () => {
    setLoading(true)
    var data = {
      appsecret: config.appsecret,
      invitedId: currentUser.id,
      action: 'getRequest',
    }
    // console.log("Data", data)

    axios.post(config.apiUrl + 'getdata.php', data).then(result => {
      console.log("data:", result.data.data)
      setLoading(false)
      if (result.data.status === 'success') {
        setData(result.data.data)
      }
    })
  }
  useEffect(fetchRequest, []);

  const startSession = (data) => {
    console.log("data", data.userid);
    dispatch(setSession(data))
    dispatch(medicineLoading(false));
    dispatch(activityLoading(false));
    dispatch(wellBeingLoading(false));
    dispatch(allDataNull([]));

    // console.log("navigations",navigation)
    setTimeout(() => {
      navigation.navigate('medicineScreenStack')
    }, 1500)

  }

  const accept = (val, id) => {
    var data = {
      appsecret: config.appsecret,
      id: id,
      action: 'updateRequest',
      status: val
    }
    axios.post(config.apiUrl + 'updatedata.php', data).then(result => {
      if (result.data.status === 'success') {
        setData(result.data.data)
        fetchRequest();
        setsnackMsg(strings.REQUEST_UPDATED_SUCC)
        setVisibleSnack(true)
      }else{
        fetchRequest();
        setsnackMsg(strings.SOMETHING_WENT_WRONG)
        setVisibleSnack(true)
      }
    })
  }
  const showAlert = (addedbyId, requestId) => {
      setAwesomeAlert(true);
      setaddedbyId(addedbyId)
      setrequestId(requestId)
  };
  const hideAlert = () => {
      setAwesomeAlert(false);
  };

  const onRefresh = React.useCallback(() => {
      // setRefreshing(true);
      wait(2000).then(() => {
          // setRefreshing(false);
          fetchRequest();
      });
  }, []);

  const DeleteAlert = (addedbyId, requestId) => {
      showAlert(addedbyId, requestId)
  }
  const DeleteRequest = (addedbyId, requestId) => {
      setLoading(true)
      var data = {};
      data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
      data.appsecret = config.appsecret;
      data.addedBy = addedbyId;
      data.requestId = requestId;
      data.action = 'deleterequest'
      console.log("data:", data)
      axios.post(config.apiUrl + 'deletedata.php', data).then(result => {
          setLoading(false)
          if (result.data.status == 'success') {
              // setVisible(false)
              setsnackMsg(strings.REQUEST_DELETED_SUCC)
              setAwesomeAlert(false)
              setVisibleSnack(true)
              onRefresh();
          }
          else {
              // setVisible(false)
              setsnackMsg(strings.SOMETHING_WENT_WRONG)
              setAwesomeAlert(false)
              setVisibleSnack(true)
              onRefresh();
          }
      })
  }
  return (
    <View style={styles.container}>
      <Loader loading={loading} />
      <Text style={styles.headerText}>{strings.PENDING_REQUEST_S_TO_ALLO}</Text>
      {
        requestData && requestData.length > 0 && requestData.map((data) => {
          if (data.status === 'pending') {
            return (
              <View style={styles.requestsDiv}>
                <Text style={styles.requestsText}>{data.email}</Text>
                <TouchableOpacity style={styles.icons} onPress={() => { accept('approve', data.id) }}>
                  <Icon name="check" color="#fff" size={50} />
                </TouchableOpacity>
                <TouchableOpacity style={styles.icons} onPress={() => { accept('reject', data.id) }}>
                  <Icon name="close" color="#fff" size={50} />
                </TouchableOpacity>
              </View>
            )
          }
        })
      }
      <View style={styles.gridContainer}>
        <View style={styles.innerHeader}>
          <Text style={{ fontSize: 28, fontWeight: 'bold', color: '#fff', textAlign:'center', width:'100%' }}>{strings.APPROVED_REQUESTS}</Text>
        </View>
        {requestData && requestData.length > 0 && requestData.map((data) => {
          if (data.status === 'approve') {
            return (
              <View key={data.id} style={styles.innerContainer}>
                <View style={styles.row1}>
                  <Image style={{ width: 70, height: 70, position: "relative", right: 0, top: 0, borderRadius: 40, fontFamily: 'HelveticaNeue-Light', }} source={{ uri: data.profilepic }} />
                  <Text style={{ paddingLeft: 10, top: 15, color: '#fff', width: 170, fontWeight: 'bold', fontSize: 20, fontFamily: 'HelveticaNeue-Light', }}>{data.firstname + ' ' + data.lastname}</Text>
                  <TouchableOpacity onPress={() => { DeleteAlert(data.addedBy, data.requestId) }}>
                    <Image style={{ width: 30, height: 30, position: "relative", top: 5, fontFamily: 'HelveticaNeue-Light', }} source={require('../../../Image/delete-btn.png')} />
                  </TouchableOpacity>
                </View>
                <View style={styles.row2}>
                  <Text style={{ paddingLeft: 10, color: '#fff', fontWeight: 'bold', fontSize: 20, textAlign: 'center', fontFamily: 'HelveticaNeue-Light', }}>Email:{'\n'} {data.email}</Text>
                </View>
                <View style={styles.row3}>
                  <TouchableOpacity onPress={() => startSession(data)} style={styles.viewBtn}>
                    <Text style={{ color: 'white', fontSize: 24, fontWeight:'bold', marginTop:3, fontFamily: 'HelveticaNeue-Light', }}>{strings.VIEW}</Text>
                  </TouchableOpacity>
                </View>
              </View>
            )
          }
        })
      }
      {requestData.length == 0 && <>
        <View style={styles.noDataFound}>
          <Text style={{fontSize:20, color:'#81889f', fontFamily: 'HelveticaNeue-Light',}}>No data found</Text>
        </View>
        </>
      }
      <AwesomeAlert
        contentContainerStyle={{
            backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
            shadowOffset: {
                width: 0,
                height: 12,
            },
            shadowOpacity: 0.58,
            shadowRadius: 16.00,

            elevation: 24
        }}
        show={awesomeAlert}
        showProgress={false}
        title={strings.DELETE_RECORD}
        titleStyle={{ color: 'white' }}
        message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
        messageStyle={{ color: 'white' }}
        closeOnTouchOutside={true}
        closeOnHardwareBackPress={false}
        showCancelButton={true}
        showConfirmButton={true}
        cancelText={strings.NO_CANCEL}
        confirmText={strings.YES_DELETE}
        cancelButtonColor="#516b93"
        confirmButtonColor="#69c2d1"
        confirmButtonBorderRadius="0"
        confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
        cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
        cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
        confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
          onCancelPressed={() => {
              setAwesomeAlert(false);
          }}
          onConfirmPressed={() => {
              DeleteRequest(addedbyId, requestId);
          }}
      />
      <View style={{ width: 350, alignSelf: 'center', alignItems: 'center', zIndex: 9985 }}>
          <Snackbar
              style={{ width: 350, alignSelf: 'center', alignItems: 'center', }}
              visible={visibleSnackbar}
              duration={2000}
              onDismiss={onDismissSnackBar}
          >
              {snackMsg}
          </Snackbar>
      </View>
      </View>
    </View >
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#2b3249',
    padding: 10,
    alignContent: 'center',
    flexDirection: 'column',
    height: Dimensions.get('window').height,
  },
  footer: {
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
  },
  requestsDiv: {
    width: '100%',
    marginTop: 40,
    flexDirection: 'row',
  },
  requestsText: {
    width: '80%',
    color: 'white',
    fontSize: 24,
    alignSelf: 'center',
    fontWeight: 'bold',
    alignSelf: 'flex-start',
    marginTop: 10
  },
  mainBody: {
    flex: 1,
    backgroundColor: '#2b3249',
    padding: 10,
    alignContent: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  headerText: {
    fontFamily: 'HelveticaNeue-Light',
    color: 'white',
    marginLeft: 20,
    fontSize: 20,
    alignSelf: 'center',
  },
  icons: {
    height: 50,
    width: 50,
    marginLeft: 20,
  },
  searchIcon: {
    paddingTop: 11,
  },
  gridContainer: {
    width: '100%',
    marginTop: 30,
    padding: 10,
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.32,
    elevation: 5,
    flexDirection: 'row',
    flexWrap: 'wrap'
  },
  innerHeader: {
    fontFamily: 'HelveticaNeue-Light',
    paddingBottom: 10,
    paddingLeft: 10,
    paddingRight: 10,
    borderBottomWidth: 2,
    borderBottomColor: '#f3f3f3',
    flexDirection:'row',
    textAlign:'center',
    // flex:1,
    width:'100%'
  },
  innerContainer: {
    fontFamily: 'HelveticaNeue-Light',
    width: '30%',
    marginTop: 10,
    marginLeft: 10,
    // borderWidth:1,
    flexDirection: 'column',
    backgroundColor: '#39415b',
    borderRadius: 10,
    padding: 10,
  },
  noDataFound: {
    width: '100%',
    marginTop: 10,
    marginLeft: 10,
    // borderWidth:1,
    flexDirection: 'column',
    backgroundColor: 'transparent',
    alignItems:'center',
    padding: 10,
  },
  row1: {
    width: '100%',
    color: '#fff',
    fontSize: 20,
    flexDirection: 'row',
  },
  row2: {
    width: '100%',
    height:60,
    marginTop: 20
  },
  row3: {
    width: '100%',
    marginTop: 20,
    marginBottom: 20
  },
  viewBtn: {
    backgroundColor: '#69c2d1',
    color: '#fff',
    fontSize: 22,
    fontWeight: '700',
    width: 200,
    height:40,
    alignSelf: 'center',
    alignItems: 'center',
    borderRadius: 30,
    textAlign: 'center',
  }

});

export default Requests;
