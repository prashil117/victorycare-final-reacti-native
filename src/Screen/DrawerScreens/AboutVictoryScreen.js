import React from 'react';
import { StyleSheet, Linking, View, Text, Image, } from 'react-native';
import Logo from '../../../Image/logo-victory.png';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const AboutScreen = ({ navigation }) => {
    return (
        <View style={styles.mainBody}>
            <View style={{ alignItems: 'center' }}>
                <Image style={{ height: 200, width: 200 }} source={Logo} />
                <Text style={styles.logText}>Victor(y) Care</Text>
                <Text style={styles.menuText}>
                    {strings.TRACK_YOUR_HEALTH_COMMUNI}
                </Text>
                <Text style={styles.appversionStyle}>
                    {strings.APP_VERSION_NUMBER_0_2_0}
                </Text>
            </View>
            <View style={{ alignSelf: 'center', flexDirection: 'row', flexWrap: 'wrap', top: 50,left:80 }}>
                {/* <View style={{ padding: 100 }}> */}
                <View style={{ alignItems: 'flex-start', marginRight: 100 }}>
                    <Text style={styles.headerText}>{strings.SUPPORT_FEEDBACK}</Text>
                    <Text style={{ color: '#69c2d1', fontSize: 14 }} onPress={() => Linking.openURL('mailto:hello@victory.care')}>hello@victory.care</Text>
                </View>
                <View style={{ alignItems: 'flex-start', marginRight: 100 }}>
                    <Text style={[styles.headerText,{width:300}]}>{strings.VICTOR_Y_CARE_ENABLES_IN}</Text>
                                <View style={{ alignSelf: 'flex-start',alignItems:'flex-start', flexDirection: 'row', flexWrap: 'wrap',}}>
                        <Text style={{ color: 'white', fontSize: 14, fontWeight: '700' }}>{strings.VISIT_US_AT_WWW_VICTORY} </Text>
                        <Text style={{ color: '#69c2d1', fontWeight: '500' }} onPress={() => Linking.openURL('https://www.victory.care')}>{strings.VISIT_US_AT_WWW_VICTORY}</Text>
                    </View>
                </View>
                <View style={{ alignItems: 'flex-start',marginRight: 100 }}>
                    <Text style={styles.headerText}>{strings.CONCEPT_DESIGN_AND_DEVEL}</Text>
                    <Text style={{ color: '#f4f4f4', fontWeight: '500' }}>{strings.VICTOR_Y_CARE_AS}</Text>
                    {/* <Text style={{ color: '#f4f4f4', fontWeight: '500' }}>Norway</Text> */}
                </View>
                {/* </View> */}
            </View>
        </View>
    );
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        // marginTop: -180,
        backgroundColor: '#2b3249',
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuText: {
        marginTop: 20,
        fontSize: 24,
        fontWeight: '600',
        color: '#ffffff',
    },
    logText: {
        width: 300,
        fontSize: 44,
        fontWeight: '600',
        fontStyle: 'normal',
        textAlign: 'center',
        color: '#ffffff'
    },
    headerText: {
        color: "white",
        marginBottom: 10,
        fontWeight: '300',
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        alignSelf: 'flex-end',
        height: 40,
        width: 130,
        marginRight: 50,
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
        fontWeight: '600'
    },
    appversionStyle: {
        fontSize: 20,
        fontWeight: '300',
        textAlign: 'center',
        color: '#81889f',
        marginTop: 100
    }

});


export default AboutScreen;
