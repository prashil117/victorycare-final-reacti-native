import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
} from 'react-native';
import Info from '../../../Image/settingsicon/info.png'
import notification from '../../../Image/settingsicon/notification.png'
import Privacy from '../../../Image/settingsicon/privacy-icn.png'
import Profile from '../../../Image/settingsicon/profile-icn.png'
import Que from '../../../Image/settingsicon/que.png'
import Wheel from '../../../Image/settingsicon/wheel.png'
import Terms from '../../../Image/settingsicon/terms.png'
import Requests from '../../../Image/settingsicon/send-request.png'
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const settingScreen = ({ navigation }) => {

  // useEffect(() => {
  //   selectedLng()
  // }, [])
  //
  // const selectedLng = async () => {
  //   const lngData = await getLng()
  //
  //   if (!!lngData || lngData != null) {
  //     strings.setLanguage(lngData)
  //   }
  //   console.log("selected Language data next file==>>>", lngData)
  // }
  // useEffect(() => {
  //   getLanguageData();
  // })
  return (
    <View style={styles.mainBody}>
      <ScrollView showsVerticalScrollIndicator={false}>
        {/*
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('MainDashboardWeekScreen')}} >
          <Image source={Terms} style={styles.img} />
          <Text style={styles.menuText}>Main Dashboard</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('WellbeingDashboardWeekScreen')}} >
          <Image source={Terms} style={styles.img} />
          <Text style={styles.menuText}>Wellbeing Dashboard</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('TestScreen')}} >
          <Image source={Terms} style={styles.img} />
          <Text style={styles.menuText}>Test Wheel</Text>
        </TouchableOpacity>*/}
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('ProfileScreen')}}  >
          <Image source={Profile} style={styles.img} />
          <Text style={styles.menuText}>{strings.PROFILE_SETTINGS}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('PrivacyScreen')}} >
          <Image source={Privacy} style={styles.img} />
          <Text style={styles.menuText}>{strings.PRIVACY}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={() => navigation.navigate('NotificationScreen')} >
          <Image source={notification} style={styles.img} />
          <Text style={styles.menuText}>{strings.NOTIFICATIONS}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('HowWorkScreen')}} >
          <Image source={Que} style={styles.img} />
          <Text style={styles.menuText}>{strings.HOW_DOES_IT_WORK}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('TimelClockSettingsScreen')}} >
          <Image source={Wheel} style={styles.img} />
          <Text style={styles.menuText}>{strings.TWENTYFOUR_HOUR_CLOCK_SETTINGS}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('RequestsScreen')}} >
          <Image source={Requests} style={styles.img} />
          <Text style={styles.menuText}>{strings.REQUEST_TO_ACCESS_PROFILE}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('AboutScreen')}} >
          <Image source={Info} style={styles.img} />
          <Text style={styles.menuText}>{strings.ABOUT_VICTOR_Y_CARE}</Text>
        </TouchableOpacity>
        <TouchableOpacity style={styles.menu} onPress={()=>{navigation.navigate('TermsAndConditonScreen')}} >
          <Image source={Terms} style={styles.img} />
          <Text style={styles.menuText}>{strings.TERMS_CONDITONS_AND_PR}</Text>
        </TouchableOpacity>
      </ScrollView>
    </View >
  );
};

const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: '#2b3249',
    padding: 10,
    alignContent: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  menu: {
    marginTop: 20,
    borderRadius: 15,
    marginBottom: 10,
    backgroundColor: '#39415b',
    height: 60,
    flexDirection: 'row',
    flexWrap: 'wrap',
    width: 700,
    padding: 20,
  },
  menuText: {
    fontSize: 20,
    fontWeight: '600',
    color: 'white',
    marginLeft: 30,
  },
  img:{
    width:25,
    height:25,
  }
});


export default settingScreen;
