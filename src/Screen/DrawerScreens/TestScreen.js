/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

 import React, { useState, useEffect, useRef } from 'react';
 import {
   SafeAreaView,
   StyleSheet,
   ScrollView,
   View,
   Text,
   TextInput,
   TouchableOpacity,
   AppState,
   TouchableHighlight,
 } from 'react-native';
 import SearchDropDown from '../Components/SearchDropDown'
 import Autocomplete from 'react-native-autocomplete-input';
 import config from '../appconfig/config';
 import axios from 'axios'
 import { useSelector } from 'react-redux'
 import { LoginManager, GraphRequest, GraphRequestManager } from "react-native-fbsdk";
 import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-google-signin/google-signin';
 import { appleAuth } from '@invertase/react-native-apple-authentication';
 import {Stopwatch, Timer} from 'react-native-stopwatch-timer';
 import moment from "moment";

  const TestScreen = ({ navigation }) => {
    const currTime = moment().format('HH:mm');
    // alert(currTime);
    const [isTimerStart, setIsTimerStart] = useState(false);
    const [isStopwatchStart, setIsStopwatchStart] = useState(false);
    const [timerDuration, setTimerDuration] = useState(180000);
    const [resetTimer, setResetTimer] = useState(false);
    const [showStartBtn, setShowStartBtn] = useState(false);
    const [resetStopwatch, setResetStopwatch] = useState(false);
    // const [stopwatchTime, setStopwatchTime] = useState('00:00:00');
    const [stopwatchTime, setStopwatchTime] = useState(moment().format('hh:mm:ss'));
    const [currentTime, setCurrentTime] = useState(moment().format('hh:mm:ss'));
    async function appleLogin() {
      // performs login request
      const authRes = await appleAuth.performRequest({
        requestedOperation: appleAuth.Operation.LOGIN,
        requestedScopes: [appleAuth.Scope.EMAIL, appleAuth.Scope.FULL_NAME],
      });
      console.log("Apple Auth Response:", authRes)
    }
    const googleLogin = async() => {
      try{
        await GoogleSignin.hasPlayServices();
        const userInfo = await GoogleSignin.signIn()
        console.log("user info:", userInfo)
      }catch (error) {
        if(error.code === statusCodes.SIGN_IN_CANCELLED){
          console.log(error)
        }else if(error.code === statusCodes.IN_PROGRESS){
          console.log(error)
        }else if(error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE){
          console.log(error)
        }else{
          console.log(error)
        }
      }
    };
    const fbLogin = (restCallback) => {
      LoginManager.logOut();
      return LoginManager.logInWithPermissions(['email', 'public_profile']).then(
        result => {
          console.log("result==>>", result)
          if(result.declinedPermissions && result.declinedPermissions.includes("email")){
            restCallback({message:"Email is required"})
          }
          if(result.isCancelled){
            console.log("Error")
          }else{
            const infoRequest = new GraphRequest(
              '/me?fields=email,name,picture',
              null,
              restCallback
            );
            new GraphRequestManager().addRequest(infoRequest).start()
          }
        },
        function (error){
          console.log("Login failed with error:"+error)
        }
      )
    }

    const onFbLogin = async() => {
      try{
        await fbLogin(_responseInfoCallback)
      }catch(error){
        console.log("error raised", error)
      }
    }
    const _responseInfoCallback = async(error, result) => {
      if(error){
        console.log("error top", error)
      }else{
        const userData = result;
        console.log("fb userData:", userData)
      }
    }
    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const [allValuesNotification, SetallNotificationValues] = useState({
        id: '',
        title: '',
    });
    const [MainJSON, setMainJSON] = useState([]);
 
    // Used to set Filter JSON Data.
    const [FilterData, setFilterData] = useState([]);
    
    // Used to set Selected Item in State.
    const [selectedItem, setselectedItem] = useState({});
    
    // Auditlog Start
    const appState = useRef(AppState.currentState)
    const [appStateVisible, setAppStateVisible] = useState(appState.current)
    useEffect(() => {
        GoogleSignin.configure()
        AppState.addEventListener("change", _handleAppStateChange)
        return () => {
          AppState.removeEventListener("change", _handleAppStateChange)
        }
    }, []);
    
    const _handleAppStateChange = (nextAppState) => {
      if(appState.current.match(/inactive|background/) && nextAppState === "active"){
        console.log("App has come to the foreground:")
      }
      appState.current = nextAppState
      setAppStateVisible(appState.current)

      console.log("App state:", appState.current)
      addAuditLog(appState.current);
    }

    const addAuditLog = (activity) => {
      // alert(activity); return false;
      var dataToSend = {
          action: 'insertlog',
          userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
          page:'testscreen',
          activity: activity,
          appsecret: config.appsecret,
      }
      axios.post(config.apiUrl + "auditlogs.php", dataToSend).then(result => {
          console.log("get audit logs:", result.data.data)
          if (result.data.data == 'success') {
              console.log("Log entered successfully!")
          }
      })
    }
    // Auditlog End
    useEffect(() => {
      // fetch('https://jsonplaceholder.typicode.com/todos/')
      //   .then((res) => res.json())
      //   .then((json) => {
      //     console.log('result.data.dataq::', json)
      //     setMainJSON(json);
      //   })
      //   .catch((e) => {
      //     alert(e);
      //   });

      var data = {
        userid: currentUser && currentUser.id ? currentUser.id : '',
        usertype: currentUser && currentUser.usertype ? currentUser.usertype : '',
        appsecret: config.appsecret,
        action: 'getmedicinemasterdatafulllist',
      }
      // dataSourceForList[index].addtowheel = !dataSourceForList[index].addtowheel;
      axios.post(config.apiUrl + "getdata.php", data).then(result => {
          // setLoading(false)
          console.log('result.data.data::', result.data.data)
          if (result.data.data) {
            setMainJSON(result.data.data);
          }
        }).catch(err => {
          // setLoading(false);
          console.log("error:::->", err)
        })
      }, []);
   
    const SearchDataFromJSON = (query) => {
      console.log("Querry:", MainJSON)
      if (query) {
        //Making the Search as Case Insensitive.
        const regex = new RegExp(`${query.trim()}`, 'i');
        setFilterData(
          MainJSON.filter((data) => data.name.search(regex) >= 0)
        );
      } else {
        setFilterData([]);
      }
    };
    const showTimer = (status) => {
      if(status){
          setIsStopwatchStart(false);
          setResetStopwatch(true);
      }
    }
    const getLatestTime = (time) => {
      // var ltime = time;
      var date = moment().format('hh:mm:ss');
      // setCurrentTime(date);
      // console.log("curr time:", currentTime)
      // alert(isStopwatchStart)
      if(isStopwatchStart){
        setShowStartBtn(false)
        var shours =  stopwatchTime.split(':')[0];
        var smins =  stopwatchTime.split(':')[1];
        var ssecs =  stopwatchTime.split(':')[2];
        console.log("hr,mins,sec:", shours+'-'+smins+'-'+ssecs);
        // var date = moment()
        // .add(smins, 'minutes')
        // .add(ssecs, 'seconds')
        // .add(shours, 'hours')
        // .format('hh:mm:ss');
        
        console.log("end time:", date)
      }else{
          setShowStartBtn(true)
          console.log("curr time:", date)
          setIsStopwatchStart(false);
          setResetStopwatch(true);
      }
    }
  
    return (
      <View style={styles.MainContainer}>
        <Text style={styles.title}>
          Example of React Native Timer and Stopwatch
        </Text>
        <View style={styles.sectionStyle}>
          <Stopwatch
            laps
            // msecs
            start={isStopwatchStart}
            // To start
            reset={resetStopwatch}
            // To reset
            options={showStartBtn ? options : ''}
            // Options for the styling
            getTime={(time) => {
              setTimeout(() => {
                // console.log(time)
                setStopwatchTime(time);
            }, 100);
            }}
          />
          <TouchableHighlight
            onPress={() => {
              getLatestTime(isStopwatchStart);
              setIsStopwatchStart(!isStopwatchStart);
              setResetStopwatch(false);
              showTimer(isStopwatchStart)
            }}>
            <Text style={styles.buttonText}>
              {!isStopwatchStart ? 'START' : 'STOP'}
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => {
              setIsStopwatchStart(false);
              setResetStopwatch(true);
            }}>
            <Text style={styles.buttonText}>RESET</Text>
          </TouchableHighlight>
        </View>
        {/* <View style={styles.sectionStyle}>
          <Timer
            totalDuration={timerDuration}
            // msecs
            // Time Duration
            start={isTimerStart}
            // To start
            reset={resetTimer}
            // To reset
            options={options}
            // Options for the styling
            handleFinish={() => {
              alert('Custom Completion Function');
            }}
            // Can call a function On finish of the time
            getTime={(time) => {
              console.log(time);
            }}
          />
          <TouchableHighlight
            onPress={() => {
              setIsTimerStart(!isTimerStart);
              setResetTimer(false);
            }}>
            <Text style={styles.buttonText}>
              {!isTimerStart ? 'START' : 'STOP'}
            </Text>
          </TouchableHighlight>
          <TouchableHighlight
            onPress={() => {
              setIsTimerStart(false);
              setResetTimer(true);
            }}>
            <Text style={styles.buttonText}>RESET</Text>
          </TouchableHighlight>
        </View> */}
        {/* <Autocomplete
          autoCapitalize="none"
          autoCorrect={false}
          containerStyle={styles.AutocompleteStyle}
          data={FilterData}
          defaultValue={
            JSON.stringify(selectedItem) === '{}' ?
            '' :
            selectedItem.name
          }
          keyExtractor={(item, i) => i.toString()}
          onChangeText={(text) => SearchDataFromJSON(text)}
          placeholder="Type The Search Keyword..."
          renderItem={({item}) => (
            <TouchableOpacity
              onPress={() => {
                setselectedItem(item);
                setFilterData([]);
                // alert(selectedItem.name)
                // SetallNotificationValues({ ...allValuesNotification, ['title']: selectedItem.name });
              }}>
              <Text style={styles.SearchBoxTextItem}>
                  {item.name}
              </Text>
            </TouchableOpacity>
          )}
        /> */}
 
        
        <View style={{marginTop:100}}>
        <TouchableOpacity style={{height:50, paddingHorizontal:8, backgroundColor:"pink", width:200}} onPress={googleLogin}>
            <Text>Google Login</Text>
          </TouchableOpacity>
        </View>
        <View style={{marginTop:20}}>
          <TouchableOpacity style={{height:50, paddingHorizontal:8, backgroundColor:"pink", width:200}} onPress={onFbLogin}>
            <Text>FB Login</Text>
          </TouchableOpacity>
        </View>
        <View style={{marginTop:20}}>
          <TouchableOpacity style={{height:50, paddingHorizontal:8, backgroundColor:"pink", width:200}} onPress={appleLogin}>
            <Text>Apple Login</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
 };
 
 const styles = StyleSheet.create({
  MainContainer: {
    backgroundColor: '#FAFAFA',
    flex: 1,
    padding: 12,
  },
  AutocompleteStyle: {
    flex: 1,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
    zIndex: 1,
   borderWidth:1
  },
  SearchBoxTextItem: {
    margin: 5,
    fontSize: 16,
    paddingTop: 4,
  },
  selectedTextContainer: {
    top:-200,
    flex: 1,
    justifyContent: 'center',
  },
  selectedTextStyle: {
    textAlign: 'center',
    fontSize: 18,
  },
  buttonText: {
    fontSize: 20,
    marginTop: 10,
  },
 });
 const options = {
  container: {
    backgroundColor: '#FF0000',
    padding: 5,
    borderRadius: 5,
    width: 200,
    alignItems: 'center',
    // display:'none'
  },
  text: {
    fontSize: 25,
    color: '#FFF',
    marginLeft: 7,
  },
};

 export default TestScreen;