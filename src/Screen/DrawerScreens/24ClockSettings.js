import React, { useState, useEffect} from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    Picker,
    I18nManager,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import RNRestart from 'react-native-restart';

import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng } from '../../helper/changeLng';
 
const ClockSettings = ({ navigation }) => {
  const [selectedValue, setSelectedValue] = useState('');
  // const [dropValues, setDropDownList] = useState([]);

  const dropValues = [{label:"English", value:"en"}, {label:"Norwegian", value:"no"}, {label:"Danish", value:"da"}, {label:"Spanish", value:"es"}, {label:"French", value:"fr"}];
  // const dropValues = ["en", "no", "da", "es", "fr"];
  console.log("dropvalues:",dropValues)
    useEffect(() => {
      // alert(Platform.OS)
      selectedLng()
    }, [])

    const selectedLng = async () => {
      const lngData = await getLng()

      if (!!lngData || lngData != null) {
        strings.setLanguage(lngData)
        setSelectedValue(lngData)
      }
      console.log("selected Language data==>>>", lngData)
    }

    const onChangeLng = async (lng) => {
      console.log("Language:",lng)
      await I18nManager.forceRTL(false)
      setLng(lng)
      RNRestart.Restart()
      return;
    }
    return (
        <View style={styles.mainBody}>
            <ScrollView>
            {Platform.OS === 'ios' &&
            <DropDownPicker
                zIndex={20}
                items={dropValues}
                placeholder={'Select Language'}
                labelStyle={{ fontSize: 14, textAlign: 'left', color: 'white', borderBottomWidth:1, borderBottomColor:'#f2f2f2', padding:0, margin:0, width:300,fontFamily: 'HelveticaNeue-Light',  }}
                defaultValue={selectedValue}
                containerStyle={{ height: 40, color: 'white', width:200, left:400, top:50, fontFamily: 'HelveticaNeue-Light', }}
                style={{ backgroundColor: '#39415b', color: 'white',borderWidth:0, fontFamily: 'HelveticaNeue-Light',}}
                itemStyle={{
                    justifyContent: 'flex-start', color: 'white',padding:0, margin:0,fontFamily: 'HelveticaNeue-Light',
                }}
                dropDownStyle={{ backgroundColor: '#2b3249', color: 'white', width:250,fontFamily: 'HelveticaNeue-Light', }}
                onChangeItem={item => onChangeLng(item.value)}
              />}
                <TouchableOpacity style={styles.menu} >
                    <Text style={styles.menuText}>{strings.LANGUAGE}</Text>
                    
                    <View style={{position:'relative', zIndex:9000}}>
                      {/* {Platform.OS === 'ios' &&
                          <DropDownPicker
                            zIndex={20}
                            items={dropValues}
                            placeholder={'Select Language'}
                            labelStyle={{ fontSize: 14, textAlign: 'left', color: 'white', borderBottomWidth:1, borderBottomColor:'#f2f2f2', padding:0, margin:0, width:300,  }}
                            defaultValue={selectedValue}
                            containerStyle={{ height: 40, color: 'white', width:200, left:400, marginTop:-30 }}
                            style={{ backgroundColor: '#39415b', color: 'white',borderWidth:0}}
                            itemStyle={{
                                justifyContent: 'flex-start', color: 'white',padding:0, margin:0,
                            }}
                            dropDownStyle={{ backgroundColor: '#2b3249', color: 'white', width:250, }}
                            onChangeItem={item => onChangeLng(item.value)}
                          />
                      } */}
                      {Platform.OS === 'android' &&
                        <Picker
                          selectedValue={selectedValue}
                          style={{ height: 50, width: 150, borderWidth:1, marginLeft:400, top:-10, backgroundColor: '#2b3249', color: '#fff', fontSize:25 }}
                          onValueChange={(itemValue, itemIndex) => onChangeLng(itemValue)}
                          itemStyle={{ backgroundColor: "white", color: "#000"}}
                          >
                          <Picker.Item label="English" value="en" style={{fontSize:22}} />
                          <Picker.Item label="Norwegian" value="no" />
                          <Picker.Item label="Danish" value="da" />
                          <Picker.Item label="Spanish" value="es" />
                          <Picker.Item label="French" value="fr" />
                        </Picker>
                      }
                    </View>
                </TouchableOpacity>
                  <TouchableOpacity style={styles.menu} onPress={() => { navigation.navigate('SettingsWellbeingScreen') }} >
                      <Text style={styles.menuText}>{strings.EDIT_WELLBEING}</Text>
                  </TouchableOpacity>
                <TouchableOpacity style={styles.menu} onPress={() => { navigation.navigate('SettingsMedicineScreen') }} >
                    <Text style={styles.menuText}>{strings.EDIT_MEDICINE}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.menu} onPress={() => { navigation.navigate('SettingsActivityScreen') }} >
                    <Text style={styles.menuText}>{strings.EDIT_ACTIVITY}</Text>
                </TouchableOpacity>
            </ScrollView>
        </View >
    );
};

const styles = StyleSheet.create({
    mainBody: {
        fontFamily: 'HelveticaNeue-Light',
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menu: {
        fontFamily: 'HelveticaNeue-Light',
        // marginTop: -15,
        borderRadius: 15,
        marginBottom: 10,
        backgroundColor: '#39415b',
        height: 56,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: 700,
        padding: 15,
        // borderWidth:1,
    },
    menuWrapper:{
      fontFamily: 'HelveticaNeue-Light',
      backgroundColor: '#39415b',
      height: 56,
      flexDirection: 'row',
      flexWrap: 'wrap',
      justifyContent: 'space-between',
      width:700,
      // borderWidth:1,
      marginTop: 20,
      borderRadius: 15,
      marginBottom: 10,
      padding: 15,      
    },
    menuText: {
        fontFamily: 'HelveticaNeue-Light',
        fontSize: 20,
        fontWeight: '600',
        color: 'white',
        marginLeft: 30
    }
});

export default ClockSettings;