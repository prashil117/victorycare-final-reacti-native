import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import {
  TextInput,
  Image,
  TouchableOpacity,
  Switch,
  SafeAreaView,
  View,
  Text,
  Alert,
  StyleSheet,
  RefreshControl,
  Button,
  FlatList,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import moment from "moment";
import config from '../appconfig/config';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Loader from '../Components/Loader';
import { VictoryPie, VictoryChart, VictoryLabel, VictoryTheme, VictoryBar, Circle } from 'victory-native';
import Svg from 'react-native-svg';
import axios from 'axios';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const height = Dimensions.get('window').height;
const WellbeingDashboard = (props) => {
  const currentUser = useSelector(state => state.auth.userLogin)
  const sessionUser = useSelector(state => state.auth.requestedUser)
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = React.useState(true);
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [AsleepBarChartArray, setAsleepBarChartArray] = useState([]);
  const [AwakeBarChartArray, setAwakeBarChartArray] = useState([]);
  const [PainBarChartArray, setPainBarChartArray] = useState([]);
  const [UncomfortBarChartArray, setUncomfortBarChartArray] = useState([]);
  const [DistressDoughnut, setDistressDoughnut] = useState(0);
  const [SleepDoughnut, setSleepDoughnut] = useState(0);
  const [ComfortableDoughnut, setComfortableDoughnut] = useState(0);
  const [UncomfortDoughnut, setUnomfortableDoughnut] = useState(0);
  const [weeklyHour] = useState(168);
  const [TotalSleep, setTotalSleep] = useState(0);
  const [TotalDistress, setTotalDistress] = useState(0);
  const [TotalComfort, setTotalComfort] = useState(0);
  const [TotalUncomfort, setTotalUncomfort] = useState(0);
  const [SleepHours, setSleepHours] = useState("0:0");
  const [DistressHours, setDistressHours] = useState("0:0");
  const [ComfortHours, setComfortHours] = useState("0:0");
  const [UncomfortHours, setUncomfortHours] = useState("0:0");
  const week = ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']

  const hours = (hh) => {
    if (hh !== undefined)
      return hh.split(":")[0];
  }
  const minutes = (mm) => {
    if (mm !== undefined)
      return mm.split(":")[1];
  }


  const avg = (hh) => {
    if (hh !== undefined)
      return Math.ceil(hh.split(":")[0] / 7);

  }

  const showModal = () => {
    setVisible(true)
  };
  const hideModal = () => {
    setVisible(false)
  };

  useEffect(() => {
    appIntialize();
  }, [props.selectedDate])



  const calulateTotalTime = (timeArray) => {
    const sum = timeArray.reduce((acc, time) => acc.add(moment.duration(time)), moment.duration());
    var abc = [Math.floor(sum.asHours()), sum.minutes()].join(':');
    return [Math.floor(sum.asHours()), sum.minutes()].join(':')
  }


  const appIntialize = (date) => {
    var startOfWeek = moment(props.selectedDate).startOf('week').format('YYYY/MM/DD');
    var endOfWeek = moment(props.selectedDate).endOf('week').format('YYYY/MM/DD')
    console.log("startOfWeek:", startOfWeek)
    setAsleepBarChartArray([]);
    setAwakeBarChartArray([]);
    setPainBarChartArray([]);
    setUncomfortBarChartArray([]);
    setSleepHours("0:0");
    setDistressHours("0:0");
    setComfortHours("0:0");
    setUncomfortHours("0:0");
    // getDates(startOfWeek, endOfWeek)
    setLoading(true)
    var data = {};
    data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
    data.appsecret = config.appsecret;
    data.startOfWeek = startOfWeek;
    data.endOfWeek = endOfWeek;
    data.action = 'getweeklygraphdata';
    axios.post(config.apiUrl + 'getgraphs.php', data).then(result => {
      setLoading(false)
      console.log('result:',result.data)
      var data1 = result && result.data && result.data ? result.data : null;
      var chartData = data1.graphSumData ? data1.graphSumData : [];
      if (data1.status == "success") {
        if (data1.graphData !== null || data1.graphData !== undefined) {
          console.log("asleep1:", data1.graphData)
          var newdata = data1.graphData;
          if (newdata.asleep && newdata.asleep.length > 0) {
            setTotalSleep(newdata.asleep.length)
            var sleepArray = [];
            for (var i in newdata.asleep) {
              var startTime = moment(newdata.asleep[i].starttime).format("HH:mm")
              var endTime = moment(newdata.asleep[i].endtime).format("HH:mm")
              var diff1 = diff(startTime, endTime);
              sleepArray.push(diff1);
            }
            var sleep = calulateTotalTime(sleepArray);
            setSleepHours(sleep);
            setSleepDoughnut(parseInt(sleep.split(":")[0]))
          }
          else {
            setSleepHours('0:0');
            setSleepDoughnut(0);

          }
          if (newdata.pain && newdata.pain.length > 0) {
            var DistressArray = [];
            setTotalDistress(newdata.pain.length)
            for (var i in newdata.pain) {
              var startTime = moment(newdata.pain[i].starttime).format("HH:mm")
              var endTime = moment(newdata.pain[i].endtime).format("HH:mm")
              var diff1 = diff(startTime, endTime)
              DistressArray.push(diff1)
            }
            var pain = (calulateTotalTime(DistressArray));
            setDistressHours(pain)
            setDistressDoughnut(parseInt(pain.split(":")[0]))
          }
          else {
            setDistressHours("0:0");
            setDistressDoughnut(0);
          }
          if (newdata.awake && newdata.awake.length > 0) {
            setTotalComfort(newdata.awake.length)
            var chartArray = [];
            for (var i in newdata.awake) {
              var startTime = moment(newdata.awake[i].starttime).format("HH:mm");
              var endTime = moment(newdata.awake[i].endtime).format("HH:mm");
              var diff1 = diff(startTime, endTime);
              chartArray.push(diff1);
            }
            var comfort = (calulateTotalTime(chartArray));
            setComfortHours(comfort);
            setComfortableDoughnut(parseInt(comfort.split(":")[0]));
          }
          else {
            setComfortHours("0:0");
            setComfortableDoughnut(0)
          }
          if (newdata.uncomfortable && newdata.uncomfortable.length > 0) {
            setTotalUncomfort(newdata.uncomfortable.length)
            var UncomfortArray = [];
            for (var i in newdata.uncomfortable) {
              var startTime = moment(newdata.uncomfortable[i].starttime).format("HH:mm");
              var endTime = moment(newdata.uncomfortable[i].endtime).format("HH:mm");
              var diff1 = diff(startTime, endTime);
              UncomfortArray.push(diff1)
            }
            var uncomfort = (calulateTotalTime(UncomfortArray));
            setUncomfortHours(uncomfort);
            setUnomfortableDoughnut(parseInt(uncomfort.split(":")[0]))
          }
          else {
            setUncomfortHours("0:0");
            setUnomfortableDoughnut(0)
          }
        }
        if (chartData && chartData.length > 0) {
          var AwakeArray = [];
          var AsleepArray = [];
          var PainArray = [];
          var UncomfortArray = [];
          // getDiffer(chartData);
          for (var i in chartData) {
            if (chartData[i].name == "Awake") {
              AwakeArray.push(chartData[i]);
            }
            if (chartData[i].name == "Asleep") {
              AsleepArray.push(chartData[i])
            }
            if (chartData[i].name == "Pain") {
              PainArray.push(chartData[i])
            }
            if (chartData[i].name == "Uncomfortable") {
              UncomfortArray.push(chartData[i]);
            }
          }
          var startOfWeek1 = moment(props.selectedDate).startOf('week').format('YYYY/MM/DD').toString();
          var startOfWeek2 = moment(props.selectedDate).startOf('week').format('YYYY/MM/DD').toString();
          var startOfWeek3 = moment(props.selectedDate).startOf('week').format('YYYY/MM/DD').toString();
          var startOfWeek4 = moment(props.selectedDate).startOf('week').format('YYYY/MM/DD').toString();
          console.log("tmpValue", startOfWeek2)
          var Aindex = 0
          var Pindex = 0
          var Uindex = 0
          var Sindex = 0
          for (let i = 0; i <= 7; i++) {
            if (AsleepArray[Sindex] !== undefined) {
              if (moment(AsleepArray[Sindex].createddate).format('YYYY/MM/DD') === startOfWeek1) {
                var sec = (AsleepArray[Sindex].endtimeseconds - AsleepArray[Sindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                setAsleepBarChartArray(oldArray => [...oldArray, { x: week[i], y: tmpvalue }])
                Sindex = Sindex + 1;
                startOfWeek1 = moment(startOfWeek1).add(1, 'days').format('YYYY/MM/DD').toString();
              }
              else {
                setAsleepBarChartArray(oldArray => [...oldArray, { x: week[i], y: 0 }])
                startOfWeek1 = moment(startOfWeek1).add(1, 'days').format('YYYY/MM/DD').toString();
              }

            }
            else {
              setAsleepBarChartArray(oldArray => [...oldArray, { x: week[i], y: 0 }])
            }
            if (PainArray[Pindex] !== undefined) {

              if (moment(PainArray[Pindex].createddate).format('YYYY/MM/DD') === startOfWeek2) {
                var sec = (PainArray[Pindex].endtimeseconds - PainArray[Pindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                setPainBarChartArray(oldArray => [...oldArray, { x: week[i], y: tmpvalue }])

                Pindex = Pindex + 1;
                startOfWeek2 = moment(startOfWeek2).add(1, 'days').format('YYYY/MM/DD').toString();;
              }
              else {
                setPainBarChartArray(oldArray => [...oldArray, { x: week[i], y: 0 }])
                startOfWeek2 = moment(startOfWeek2).add(1, 'days').format('YYYY/MM/DD').toString();;
              }
            }
            else {
              setPainBarChartArray(oldArray => [...oldArray, { x: week[i], y: 0 }])
            }
            if (AwakeArray[Aindex] !== undefined) {
              if (moment(AwakeArray[Aindex].createddate).format('YYYY/MM/DD') === startOfWeek3) {
                var sec = (AwakeArray[Aindex].endtimeseconds - AwakeArray[Aindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                setAwakeBarChartArray(oldArray => [...oldArray, { x: week[i], y: tmpvalue }])
                Aindex = Aindex + 1;
                startOfWeek3 = moment(startOfWeek3).add(1, 'days').format('YYYY/MM/DD').toString();;
              }
              else {
                setAwakeBarChartArray(oldArray => [...oldArray, { x: week[i], y: 0 }])
                startOfWeek3 = moment(startOfWeek3).add(1, 'days').format('YYYY/MM/DD').toString();;
              }
            }
            else {
              setAwakeBarChartArray(oldArray => [...oldArray, { x: week[i], y: 0 }])
            }
            if (UncomfortArray[Uindex] !== undefined) {
              if (moment(UncomfortArray[Uindex].createddate).format('YYYY/MM/DD') === startOfWeek4) {
                var sec = (UncomfortArray[Uindex].endtimeseconds - UncomfortArray[Uindex].starttimeseconds) * 1000;
                var tmpvalue = Math.round(moment.duration(sec).asHours())
                setUncomfortBarChartArray(oldArray => [...oldArray, { x: week[i], y: tmpvalue }])
                Uindex = Uindex + 1;
                startOfWeek4 = moment(startOfWeek4).add(1, 'days').format('YYYY/MM/DD').toString();;
              }
              else {

                setUncomfortBarChartArray(oldArray => [...oldArray, { x: week[i], y: 0 }])
                startOfWeek4 = moment(startOfWeek4).add(1, 'days').format('YYYY/MM/DD').toString();;
              }
            }
            else {
              setUncomfortBarChartArray(oldArray => [...oldArray, { x: week[i], y: 0 }])
            }
          }

        }
        else {
          setAwakeBarChartArray([{ x: week[0], y: 0 }, { x: week[1], y: 0 }, { x: week[2], y: 0 }, { x: week[3], y: 0 }, { x: week[4], y: 0 }, { x: week[5], y: 0 }, { x: week[6], y: 0 }]);
          setPainBarChartArray([{ x: week[0], y: 0 }, { x: week[1], y: 0 }, { x: week[2], y: 0 }, { x: week[3], y: 0 }, { x: week[4], y: 0 }, { x: week[5], y: 0 }, { x: week[6], y: 0 }]);
          setAsleepBarChartArray([{ x: week[0], y: 0 }, { x: week[1], y: 0 }, { x: week[2], y: 0 }, { x: week[3], y: 0 }, { x: week[4], y: 0 }, { x: week[5], y: 0 }, { x: week[6], y: 0 }]);
          setUncomfortBarChartArray([{ x: week[0], y: 0 }, { x: week[1], y: 0 }, { x: week[2], y: 0 }, { x: week[3], y: 0 }, { x: week[4], y: 0 }, { x: week[5], y: 0 }, { x: week[6], y: 0 }]);
        }

      }
    }, err => {
      loadingService.hide();
      var a = abc(a, b);
    });
  }

  const diff = (start, end) => {
    start = start.split(":");
    end = end.split(":");
    var startDate = new Date(0, 0, 0, start[0], start[1], 0);
    var endDate = new Date(0, 0, 0, end[0], end[1], 0);
    var diff1 = endDate.getTime() - startDate.getTime();
    var hours = Math.floor(diff1 / 1000 / 60 / 60);
    diff1 -= hours * 1000 * 60 * 60;
    var minutes = Math.floor(diff1 / 1000 / 60);
    if (hours < 0)
      hours = hours + 24;

    return (hours <= 9 ? "0" : "") + hours + ":" + (minutes <= 9 ? "0" : "") + minutes;
  }

  const sampleData = [
    { x: 'Mo', y: 13 },
    { x: 'Tu', y: 15 },
    { x: 'We', y: 14 },
    { x: 'Th', y: 16 },
    { x: 'Fr', y: 10 },
    { x: 'Sa', y: 11 },
    { x: 'Su', y: 12 },
  ];
  const donutData = [
    { y: 25, label: " " },
    { y: 15, label: " " },
  ];

  const graphRadius = 10;
  const domainPaddingX = 30;
  const chartWidth = 250;
  const chartHeight = 130;
  const dataWidth = 20;
  return (
    <View style={{ marginTop: 85 }}>
      <Loader loading={loading} />
      <View style={{ flexDirection: 'row', marginBottom: 100, width: '100%', position: 'absolute', marginLeft: 25 }}>
        <View style={styles.registration}>
          <View style={{ borderBottomWidth: 5, borderBottomColor: '#6bb1d6', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.TOTAL}</Text>
          </View>
          <View style={{ width: 180, alignItems: 'center', position: 'relative', top: 10, right: 0, }}>
            <Text style={styles.h1}>{TotalSleep} </Text>
            <Text style={styles.h5}>{strings.REGISTRATIONS} </Text>
          </View>
        </View>
        <View style={styles.hours}>
          <View style={{ borderBottomWidth: 2, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.SLEEP_HOURS}</Text>
          </View>
          <View style={{ flexDirection: 'row', width: 250, alignItems: 'center', position: 'relative', top: 20, left: 30 }}>
            <Text style={styles.h1}>{hours(SleepHours)} </Text>
            <Text style={styles.hrs}>{strings.HRS.substring(0,3)} </Text>
            <Text style={styles.h1}>{minutes(SleepHours)} </Text>
            <Text style={styles.hrs}>{strings.MIN.substring(0,3)} </Text>
          </View>
        </View>
        <View style={{ width: 150, height: 120, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
            <Svg>
            <VictoryPie
              data={[{ y: weeklyHour, label: "168" }, { y: SleepDoughnut, label: " " }]}
              labels={(datum) => `${datum.y}`}
              radius={40}
              innerRadius={20}
              width={110}
              height={110}
              colorScale={['#6bb1d6', '#3d7c9e']}
              events={[{
                target: "data",
                eventHandlers: {
                  onPress: () => {
                    return [
                      {
                        target: "data",
                        mutation: ({ style }) => {
                          return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                        }
                      }, {
                        target: "labels",
                        mutation: ({ text }) => {
                          return text === "clicked" ? null : { text: "clicked" };
                        }
                      }
                    ];
                  }

                }
              }]}
            />
            {/*<Circle cx="55" cy="55" r="20" fill="#ff0000" stroke="#2b3249" strokeWidth={2} />*/}
            <VictoryLabel
              textAnchor="middle"
              style={[{ fill: "#fff", fontSize: 14, fontWeight:'bold' }]}
              x={55} y={55}
              text={SleepDoughnut+'/\n'+weeklyHour}
            />
            </Svg>
            <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="circle" size={16} color="#6bb1d6" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.TOTAL}</Text>
              <Icon name="circle" size={16} color="#3d7c9e" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.CONSUME}</Text>
            </View>
          </View>
        </View>
        <View style={styles.average}>
          <View style={{ width: 150, height: 50, alignItems: 'center', position: 'relative', top: 20, right: 0, borderBottomWidth: 2, height: 90, borderBottomColor: '#81889f' }}>
            <Text style={styles.h1}>{avg(SleepHours)} </Text>
          </View>
          <View style={{ width: 150, alignItems: 'center', position: 'relative', top: 25, right: 0, }}>
            <Text style={styles.h5}>{strings.AVERAGE} </Text>
          </View>
        </View>
        <View style={styles.innerGraph}>
          <VictoryChart
            theme={VictoryTheme.material}
            domainPadding={{ x: domainPaddingX }}
            width={chartWidth} height={chartHeight}
            padding={{ bottom: 30, }}

          >
            <VictoryBar
              cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
              style={{
                data: {
                  fill: "#7aecff",
                  width: dataWidth
                },
                labels: {
                  fontSize: 15,
                  fill: ({ datum }) => "#fff"
                }
              }}
              labelComponent={<VictoryLabel dy={(datum)=>datum.y == 0 ? 15 : 0}/>}
              labels={({ datum }) => datum.y != 0 ? datum.y : ''}
              data={AsleepBarChartArray}
            />
          </VictoryChart>
        </View>
      </View>
      <View style={{ flexDirection: 'row', marginTop: 150, width: '100%', position: 'absolute', marginLeft: 25 }}>
        <View style={styles.registration}>
          <View style={{ borderBottomWidth: 5, borderBottomColor: '#fad000', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.TOTAL}</Text>
          </View>
          <View style={{ width: 180, alignItems: 'center', position: 'relative', top: 10, right: 0, }}>
            <Text style={styles.h1}>{TotalDistress} </Text>
            <Text style={styles.h5}>{strings.REGISTRATIONS} </Text>
          </View>
        </View>
        <View style={styles.hours}>
          <View style={{ borderBottomWidth: 2, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.DISTRESS_HOURS}</Text>
          </View>
          <View style={{ flexDirection: 'row', width: 250, alignItems: 'center', position: 'relative', top: 20, left: 30 }}>
            <Text style={styles.h1}>{hours(DistressHours)} </Text>
            <Text style={styles.hrs}>{strings.HRS.substring(0,3)} </Text>
            <Text style={styles.h1}>{minutes(DistressHours)}</Text>
            <Text style={styles.hrs}>{strings.MIN.substring(0,3)} </Text>
          </View>
        </View>
        <View style={{ width: 150, height: 120, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
          <Svg>
            <VictoryPie
              data={[{ y: weeklyHour, label: " " }, { y: DistressDoughnut, label: " " }]}
              labels={(datum) => `${datum.y}`}
              radius={40}
              innerRadius={20}
              width={110}
              height={110}
              colorScale={['#fad000', '#967f0e']}
              events={[{
                target: "data",
                eventHandlers: {
                  onPress: () => {
                    return [
                      {
                        target: "data",
                        mutation: ({ style }) => {
                          return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                        }
                      }, {
                        target: "labels",
                        mutation: ({ text }) => {
                          return text === "clicked" ? null : { text: "clicked" };
                        }
                      }
                    ];
                  }

                }
              }]}
            />
            <VictoryLabel
              textAnchor="middle"
              style={[{ fill: "#fff", fontSize: 14, fontWeight:'bold' }]}
              x={55} y={55}
              text={DistressDoughnut+'/\n'+weeklyHour}
            />
            </Svg>
            <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="circle" size={16} color="#fad000" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.TOTAL}</Text>
              <Icon name="circle" size={16} color="#967f0e" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.CONSUME}</Text>
            </View>
          </View>
        </View>
        <View style={styles.average}>
          <View style={{ width: 150, height: 50, alignItems: 'center', position: 'relative', top: 20, right: 0, borderBottomWidth: 2, height: 90, borderBottomColor: '#81889f' }}>
            <Text style={styles.h1}>{avg(DistressHours)} </Text>
          </View>
          <View style={{ width: 150, alignItems: 'center', position: 'relative', top: 25, right: 0, }}>
            <Text style={styles.h5}>{strings.AVERAGE} </Text>
          </View>
        </View>
        <View style={styles.innerGraph}>
          <VictoryChart
            theme={VictoryTheme.material}
            domainPadding={{ x: domainPaddingX }}
            width={chartWidth} height={chartHeight}
            padding={{ bottom: 30, }}

          >
            <VictoryBar
              cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
              style={{
                data: {
                  fill: "#fad000",
                  width: dataWidth
                },
                labels: {
                  fontSize: 15,
                  fill: ({ datum }) => "#fff"
                }
              }}
              data={PainBarChartArray}
              labelComponent={<VictoryLabel dy={(datum)=>datum.y == 0 ? 15 : 0}/>}
              labels={({ datum }) => datum.y != 0 ? datum.y : ''}
            />
          </VictoryChart>
        </View>
      </View>
      <View style={{ flexDirection: 'row', marginTop: 300, width: '100%', position: 'absolute', marginLeft: 25 }}>
        <View style={styles.registration}>
          <View style={{ borderBottomWidth: 5, borderBottomColor: '#afdb07', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.TOTAL}</Text>
          </View>
          <View style={{ width: 180, alignItems: 'center', position: 'relative', top: 10, right: 0, }}>
            <Text style={styles.h1}>{TotalUncomfort} </Text>
            <Text style={styles.h5}>{strings.REGISTRATIONS} </Text>
          </View>
        </View>
        <View style={styles.hours}>
          <View style={{ borderBottomWidth: 2, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.UNCOMFORTABLE_HOURS}</Text>
          </View>
          <View style={{ flexDirection: 'row', width: 250, alignItems: 'center', position: 'relative', top: 20, left: 30 }}>
            <Text style={styles.h1}>{hours(UncomfortHours)}</Text>
            <Text style={styles.hrs}>{strings.HRS.substring(0,3)} </Text>
            <Text style={styles.h1}>{minutes(UncomfortHours)}</Text>
            <Text style={styles.hrs}>{strings.MIN.substring(0,3)} </Text>
          </View>
        </View>
        <View style={{ width: 150, height: 120, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
          <Svg>
            <VictoryPie
              data={[{ y: weeklyHour, label: " " }, { y: UncomfortDoughnut, label: " " }]}
              labels={(datum) => `${datum.y}`}
              radius={40}
              innerRadius={20}
              width={110}
              height={110}
              colorScale={['#afdb07', '#728d0a']}
              events={[{
                target: "data",
                eventHandlers: {
                  onPress: () => {
                    return [
                      {
                        target: "data",
                        mutation: ({ style }) => {
                          return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                        }
                      }, {
                        target: "labels",
                        mutation: ({ text }) => {
                          return text === "clicked" ? null : { text: "clicked" };
                        }
                      }
                    ];
                  }

                }
              }]}
            />
            <VictoryLabel
              textAnchor="middle"
              style={[{ fill: "#fff", fontSize: 14, fontWeight:'bold' }]}
              x={55} y={55}
              text={UncomfortDoughnut+'/\n'+weeklyHour}
            />
            </Svg>
            <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="circle" size={16} color="#afdb07" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.TOTAL}</Text>
              <Icon name="circle" size={16} color="#728d0a" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.CONSUME}</Text>
            </View>
          </View>
        </View>
        <View style={styles.average}>
          <View style={{ width: 150, height: 50, alignItems: 'center', position: 'relative', top: 20, right: 0, height: 90, borderBottomWidth: 2, borderBottomColor: '#81889f' }}>
            <Text style={styles.h1}>{avg(UncomfortHours)} </Text>
          </View>
          <View style={{ width: 150, alignItems: 'center', position: 'relative', top: 25, right: 0, }}>
            <Text style={styles.h5}>{strings.AVERAGE} </Text>
          </View>
        </View>
        <View style={styles.innerGraph}>
          <VictoryChart
            theme={VictoryTheme.material}
            domainPadding={{ x: domainPaddingX }}
            width={chartWidth} height={chartHeight}
            padding={{ bottom: 30, }}

          >
            <VictoryBar
              cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
              style={{
                data: {
                  fill: "#afdb07",
                  width: dataWidth
                },
                labels: {
                  fontSize: 15,
                  fill: ({ datum }) => "#fff"
                }
              }}
              data={UncomfortBarChartArray}
              labelComponent={<VictoryLabel dy={(datum)=>datum.y == 0 ? 15 : 0}/>}
              labels={({ datum }) => datum.y != 0 ? datum.y : ''}
            />
          </VictoryChart>
        </View>
      </View>
      <View style={{ flexDirection: 'row', marginTop: 450, width: '100%', position: 'absolute', marginLeft: 25 }}>
        <View style={styles.registration}>
          <View style={{ borderBottomWidth: 5, borderBottomColor: '#e26060', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.TOTAL}</Text>
          </View>
          <View style={{ width: 180, alignItems: 'center', position: 'relative', top: 10, right: 0, }}>
            <Text style={styles.h1}>{TotalComfort} </Text>
            <Text style={styles.h5}>{strings.REGISTRATIONS} </Text>
          </View>
        </View>
        <View style={styles.hours}>
          <View style={{ borderBottomWidth: 2, borderBottomColor: '#81889f', paddingTop: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14 }}>COMFORTABLE HOURS</Text>
          </View>
          <View style={{ flexDirection: 'row', width: 250, alignItems: 'center', position: 'relative', top: 20, left: 30 }}>
            <Text style={styles.h1}>{hours(ComfortHours)}</Text>
            <Text style={styles.hrs}>{strings.HRS.substring(0,3)} </Text>
            <Text style={styles.h1}>{minutes(ComfortHours)}</Text>
            <Text style={styles.hrs}>{strings.MIN.substring(0,3)} </Text>
          </View>
        </View>
        <View style={{ width: 150, height: 120, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
          <Svg>
            <VictoryPie
              data={[{ y: weeklyHour, label: " " }, { y: ComfortableDoughnut, label: " " }]}
              labels={(datum) => `${datum.y}`}
              radius={40}
              innerRadius={20}
              width={110}
              height={110}
              colorScale={['#e26060', '#793e3e']}
              events={[{
                target: "data",
                eventHandlers: {
                  onPress: () => {
                    return [
                      {
                        target: "data",
                        mutation: ({ style }) => {
                          return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                        }
                      }, {
                        target: "labels",
                        mutation: ({ text }) => {
                          return text === "clicked" ? null : { text: "clicked" };
                        }
                      }
                    ];
                  }

                }
              }]}
            />
            <VictoryLabel
              textAnchor="middle"
              style={[{ fill: "#fff", fontSize: 14, fontWeight:'bold' }]}
              x={55} y={55}
              text={ComfortableDoughnut+'/\n'+weeklyHour}
            />
            </Svg>
            <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="circle" size={16} color="#e26060" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.TOTAL}</Text>
              <Icon name="circle" size={16} color="#793e3e" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.CONSUME}</Text>
            </View>
          </View>
        </View>
        <View style={styles.average}>
          <View style={{ width: 150, height: 50, alignItems: 'center', position: 'relative', height: 90, top: 20, right: 0, borderBottomWidth: 2, borderBottomColor: '#81889f' }}>
            <Text style={styles.h1}>{avg(ComfortHours)} </Text>
          </View>
          <View style={{ width: 150, alignItems: 'center', position: 'relative', top: 25, right: 0, }}>
            <Text style={styles.h5}>{strings.AVERAGE} </Text>
          </View>
        </View>
        <View style={styles.innerGraph}>
          <VictoryChart
            theme={VictoryTheme.material}
            domainPadding={{ x: domainPaddingX }}
            width={chartWidth} height={chartHeight}
            padding={{ bottom: 30, }}

          >
            <VictoryBar
              cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
              style={{
                data: {
                  fill: "#e26060",
                  width: dataWidth
                },
                labels: {
                  fontSize: 15,
                  fill: ({ datum }) => "#fff"
                }
              }}
              data={AwakeBarChartArray}
              labelComponent={<VictoryLabel dy={(datum)=>datum.y == 0 ? 15 : 0}/>}
              labels={({ datum }) => datum.y != 0 ? datum.y : ''}
            />
          </VictoryChart>
        </View>
      </View>
      <View style={{ flexDirection: 'row', position: 'absolute', top: height - 160, left: 250 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={{ color: '#fff', paddingRight: 15 }}>{strings.STATUS_OF_WELLBEING}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Icon name="circle" size={18} color="#6bb1d6" />
          <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.SLEEP}</Text>
        </View>
        <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
          <Icon name="circle" size={18} color="#afdb07" />
          <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.AWAKE_COMFORTABLE}</Text>
        </View>
        <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
          <Icon name="circle" size={18} color="#fad000" />
          <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.AWAKE_UNCOMFORTABLE}</Text>
        </View>
        <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
          <Icon name="circle" size={22} color="#e26060" />
          <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.AWAKE_DISTRESS}</Text>
        </View>
      </View>
    </View >
  );
};
const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: '#2b3249',
    padding: 10,
    alignContent: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabContainer: {
    width: 300,
    height: 50,
    flexDirection: 'row',
    // borderWidth:1,
    position: 'absolute',
    top: 10,
    left: 50,
    borderRadius: 50,
    backgroundColor: '#474f69',
  },
  weekTab: {
    width: 150,
    // height:50,
    backgroundColor: '#69c2d1',
    fontSize: 14,
    borderRadius: 50
  },
  weekText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 10,
    fontWeight: 'bold'
  },
  monthTab: {
    width: 150,
    // height:50,
    backgroundColor: '#474f69',
    fontSize: 14,
    borderRadius: 50
  },
  monthText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 10,
    fontWeight: 'bold',
  },
  middleDiv: {
    width: 400,
  },
  titleText: {
    color: '#fff',
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  rightDiv: {
    width: 300, flexDirection: "row", flexWrap: "wrap", position: 'absolute', top: 0, right: 0
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    marginTop: 10,
    margin: 10,
    width: 200,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#dadae8',
    textAlign: 'center'
  },
  inputStyle: {
    flex: 1,
    color: 'white',
    flexDirection: 'row',
    fontSize: 14,
    left: 8,
    paddingTop: 7
  },
  searchIcon: {
    paddingTop: 11,

  },
  container: {
    flexDirection: 'row', position: 'relative', top: 0, left: 0, alignItems: 'center', padding: 0, margin: 0, backgroundColor: 'transparent', width: '100%',
  },
  innerGraph: {
    width: 250, height: 120, position: 'relative', alignItems: 'center', marginTop: 0
  },
  registration: { width: 170, height: 140, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' },
  hours: { width: 200, height: 140, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' },
  average: { width: 150, height: 140, borderRadius: 20, backgroundColor: '#3a4159' },
  h1: { fontSize: 40, fontWeight: 'bold', color: '#fff', alignItems: 'center' },
  h5: { fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 0, fontSize: 14, textTransform:'uppercase' },
  hrs: { fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20, fontSize: 14 },
});
export default WellbeingDashboard;
