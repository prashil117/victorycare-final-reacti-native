import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    Text,
    StyleSheet,
    FlatList,
    ActivityIndicator,
} from 'react-native';

import DateTimePickerModal from "react-native-modal-datetime-picker";
import { ColorPicker, fromHsv } from 'react-native-color-picker'
import moment from "moment";
import { Modal, Portal, Provider, FAB } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';

import deletebtn from '../../../Image/delete-btn.png'
import editbtn from '../../../Image/edit-btn.png'
import notificationbtn from '../../../Image/notification-btn.png'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';


const SettingEventMaster = ({ navigation }) => {
    const currentUser = useSelector(state => state.auth.userLogin)
    const [loading, setLoading] = useState(false);
    const [dataSource, setDataSource] = useState([]);
    const [offset, setOffset] = useState(0);
    const [isListEnd, setIsListEnd] = useState(false);
    const [title, setTitle] = useState(strings.ADD_WELLBEING);
    const [visible, setVisible] = React.useState(false);
    const [allValues, SetallValues] = useState({
        id: '',
        name: '',
        color: '',
        description: '',
        notification: false,
        recurring: false,
        addtowheel: false,
        selecttime: ''
    });

    const showModal = () => setVisible(true);
    const hideModal = () => {
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setVisible(false)
        setTitle('Add WellBeing')

    };
    const [visibleColor, setVisibleColor] = React.useState(false);
    const showModalColor = () => setVisibleColor(true);
    const hideModalColor = () => setVisibleColor(false);
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const containerStyle = { backgroundColor: '#2b3249', marginBottom: 25, padding: 10, alignItems: 'center', width: '50%', alignSelf: 'center', borderRadius: 8 };
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };
    const handleConfirm = (date) => {
        SetallValues({ ...allValues, ['selecttime']: moment(date).format('HH:mm') })
        hideDatePicker();
    };

    const onEditWellBeing = (data) => {
        console.log("data", data)
        allValues.id = data.id;
        allValues.name = data.name;
        allValues.color = data.color;
        allValues.description = data.description;
        allValues.recurring = data.recurring == "1" ? true : false;
        allValues.addtowheel = data.addtowheel == "1" ? true : false;
        allValues.notification = data.notification == "1" ? true : false;
        allValues.selecttime = data.selecttime;
        setTitle('Edit WellBeing');
        setVisible(true);
    }

    useEffect(() => geteventData(), []);

    const geteventData = () => {
        console.log(offset);
        if (!loading && !isListEnd) {
            console.log('getData');
            setLoading(true);
            var dataToSend = {
                userid: currentUser.id,
                appsecret: config.appsecret,
                action: 'geteventmasterdata',
                offset: offset
            };
            console.log("offset")
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                console.log("result.data", result.data.data0)
                if (result.data.data0 !== null) {
                    setLoading(false);
                    setDataSource([...dataSource, ...result.data.data0]);
                    setOffset(offset + 10)
                }
                else {
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
    };

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="white"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };

    const ItemView = ({ item }) => {
        return (
            // Flat List Item
            <View key={item.id} style={styles.event}>
                <View style={[styles.eventColor, { backgroundColor: item.color }]}>
                </View>
                <Text style={styles.eventText}>{item.name}</Text>
                <View style={{ paddingTop: 10, left: 140, flexDirection: "row", flexWrap: "wrap" }}>
                    <TouchableOpacity>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onEditWellBeing(item)}>
                        <Image style={styles.icons} height={35} width={35} source={editbtn} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.mainBody}>
                {/* <Loader loading={loading} /> */}

                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={dataSource}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={ItemView}
                    ListFooterComponent={renderFooter}
                    onEndReached={geteventData}
                    onEndReachedThreshold={0.5}
                />
                <Provider>
                    <Portal>
                        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                <Text style={{ color: 'white', fontWeight: '700' }}>{title}</Text>
                                <View style={{ left: 190 }}>
                                    <TouchableOpacity onPress={hideModal}>
                                        <Icon name="close" color="#81889f" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                       <KeyboardAwareScrollView enableOnAndroid={true}>
                            <View style={{ marginTop: 20, alignItems: 'center' }}>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.NAME_OF_WELLBEING}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.MEDICINE_SEIZURE_HEADACHE}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.name}
                                        onChangeText={val => SetallValues({ ...allValues, ['name']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20, left: allValues.color ? 10 : 0 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.CHOOSE_COLOR}</Text>
                                    <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                                        <View style={[allValues.color ? styles.eventColorForm : '', { backgroundColor: allValues.color, marginBottom: 8 }]}>
                                        </View>
                                        <TextInput
                                            style={{ width: 460, right: allValues.color ? 18 : 0 }}
                                            placeholder={allValues.color ? '' : 'Color'}
                                            placeholderTextColor={'#81889f'}
                                            editable={false}
                                            onTouchStart={showModalColor}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                </View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.DESCRIPTION}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_DESCRIPTION_HERE}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.description}
                                        onChangeText={val => SetallValues({ ...allValues, ['description']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap" }}>
                                    <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.RECURRING_EVERYDAY}</Text>
                                    <Switch style={[styles.headerText, { left: 30 }]}
                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                        borderColor={'#81889f'}
                                        borderWidth={1}
                                        value={allValues.recurring ? true : false}
                                        onValueChange={val => SetallValues({ ...allValues, ['recurring']: val })}
                                        borderRadius={16}
                                        thumbColor={"#fff"}
                                    />
                                    <TextInput
                                        marginLeft={120}
                                        width={150}
                                        placeholder={strings.SELECT_TIME}
                                        placeholderTextColor={allValues.recurring ? 'white' : '#81889f'}
                                        editable={false}
                                        style={{ color: allValues.recurring ? 'white' : '#81889f', fontSize: 14 }}
                                        value={allValues.selecttime}
                                        onTouchStart={() => { allValues.recurring ? showDatePicker(true) : '' }}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                    <View style={{ width: 200 }}>
                                        <DateTimePickerModal
                                            style={{ backgroundColor: '#2b3249', }}
                                            headerTextIOS={strings.SELECT_TIME}
                                            locale="en_GB"
                                            textColor="white"
                                            isVisible={isDatePickerVisible}
                                            mode="time"
                                            onConfirm={handleConfirm}
                                            onCancel={hideDatePicker}
                                        />

                                    </View>
                                </View>
                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                    <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.ADD_TO_STANDARD_WHEEL}</Text>
                                    <Switch style={[styles.headerText, { left: 30 }]}
                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                        borderColor={'#81889f'}
                                        borderWidth={1}
                                        borderRadius={16}
                                        thumbColor={"#fff"}
                                        value={allValues.addtowheel}
                                        onValueChange={val => SetallValues({ ...allValues, ['addtowheel']: val })}
                                    />
                                </View>
                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                    <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATION}</Text>
                                    <Switch style={[styles.headerText, { left: 30 }]}
                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                        borderColor={'#81889f'}
                                        borderWidth={1}
                                        value={allValues.notification}
                                        onValueChange={val => SetallValues({ ...allValues, ['notification']: val })}
                                        borderRadius={16}
                                        thumbColor={"#fff"}
                                    />
                                </View>
                                <TouchableOpacity
                                    style={[styles.buttonStyle, { right: 80 }]}
                                    activeOpacity={0.5}
                                >
                                    <Text style={styles.buttonTextStyle}>{strings.SAVE}</Text>
                                </TouchableOpacity>
                            </View>
                            </KeyboardAwareScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                <Provider>
                    <Portal>
                        <Modal visible={visibleColor} onDismiss={hideModalColor} contentContainerStyle={{ alignSelf: 'center', marginBottom: 100 }}>
                            <View style={{ marginTop: 20 }}>
                                <ColorPicker
                                    hideSliders={true}
                                    color={allValues.color ? allValues.color : '#ff0058'}
                                    pickerSize={10}
                                    onColorSelected={color => { SetallValues({ ...allValues, ['color']: fromHsv(color) }) }, hideModalColor}
                                    onColorChange={color => SetallValues({ ...allValues, ['color']: fromHsv(color) })}
                                    style={{ width: 100, height: 100 }}
                                />
                            </View>
                        </Modal>
                    </Portal>
                </Provider>
                <FAB
                    style={styles.floatBtn}
                    fabStyle={{ height: 100 }}
                    color={'white'}
                    theme={{ colors: { accent: '#eb8682' } }}
                    icon="plus"
                    onPress={showModal}
                >
                </FAB>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    headerText: {
        color: 'white',
        marginLeft: 20,
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        width:35,
        height:35,
    },
    event: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: 700,
        height: 50,
        borderRadius: 15,
        marginBottom: 17,

    },
    eventText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    eventColor: {
        backgroundColor: '#fb7e7e',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    eventColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        width: 18
    },
    buttonStyle: {
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 50,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    }

});

export default SettingEventMaster;
