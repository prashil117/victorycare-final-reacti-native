import React, { useState, useEffect } from 'react';
import { useSelector,useDispatch } from 'react-redux'

import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    StyleSheet,
    Alert,
    RefreshControl,
    FlatList,
    Button,
    ScrollView,
    ActivityIndicator,
    Keyboard
} from 'react-native';
import moment from "moment";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Icon from 'react-native-vector-icons/MaterialIcons';
import usericn from '../../../Image/nopreview.jpg';
import { Modal, Portal, Text, Provider, FAB, Snackbar } from 'react-native-paper';
import deletebtn from '../../../Image/delete-btn.png'
import downArrow from '../../../Image/down_arrow.png'
import editbtn from '../../../Image/edit-btn.png'
import notificationbtn from '../../../Image/notification-btn.png'
import { setSession, medicineLoading, activityLoading, wellBeingLoading, allDataNull } from '../redux/action'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';
import SearchableDropdown from 'react-native-searchable-dropdown';

const AllPatientScreen = ( navigation ) => {
    console.log("navigation+navigation::7",navigation)
    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const dispatch = useDispatch();
    const [refreshing, setRefreshing] = React.useState(false);
    const [loading, setLoading] = useState(false);
    const [dataSource, setDataSource] = useState([]);
    const [offset, setOffset] = useState(0);
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const [dropValues, setDropDownList] = useState([]);
    const [hcpDropDownList, setHcpDropDownList] = useState([]);
    const [doctorDropDownList, setDoctorDropDownList] = useState([]);
    const [nurseDropDownList, setNurseDropDownList] = useState([]);
    const [state1, setState] = useState([]);
    const [type, setType] = useState('');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isDatePickerVisible1, setDatePickerVisibility1] = useState(false);
    const onDismissSnackBar = () => setVisibleSnack(false);
    const [keyword, setKeyword] = useState('');
    const [searchData, setsearchData] = useState('');
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [isListEnd, setIsListEnd] = useState(false);
    const [visible1, setVisible1] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [hostId, setHostId] = useState(false)
    const [guestId, setGuestId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [doctorId, setDoctorId] = useState('');
    const [hcpId, setHcpId] = useState('');
    const [nurseId, setNursetId] = useState('');
    const showAlert = (hostid, guestid, index) => {
        setAwesomeAlert(true);
        setHostId(hostid)
        setGuestId(guestid)
        setPopupIndex(index)
    };

    const hideAlert = () => {
        setAwesomeAlert(false);
    };
    useEffect(() => getPatientsData(), [sessionUser]);
    const [time, setTime] = React.useState([]);
    const [check, setCheck] = useState('start');
    const [allValuesNotification, SetallNotificationValues] = useState({
        id: '',
        prestartdate: '',
        preenddate: '',
        startDate: '',
        endDate: '',
        addtostandardwheel: false,
        activity: null,
        recurringDaily: false,
        startDate: '',
        endDate: ''
    });

    const DeleteUser = (hostId, guestId) => {
        // alert(hostId+"-"+guestId); return false;
        setLoading(true)
        var data = {};
        data.userid = navigation.userid && navigation.userid != '' ? navigation.userid : currentUser.id;
        data.appsecret = config.appsecret;
        data.page = 'allpatientsubscreen';
        data.activity = 'unlinkpatient';
        if (currentUser.usertype === 'admin') {
            data.adminid = currentUser.id;
            data.hostType = 'adminids';
        }
        if (currentUser.usertype === 'healthcareprovider') {
            data.adminid = currentUser.id;
            data.hostType = 'hcpids';
        }
        if (currentUser.usertype === 'patient') {
            data.patientid = currentUser.id;
            data.hostType = 'patientids';
        }
        if (currentUser.usertype === 'doctor') {
            data.doctorid = currentUser.id;
            data.hostType = 'doctorids';
        }
        if (currentUser.usertype === 'nurse') {
            data.nurseid = currentUser.id;
            data.hostType = 'nurseids';
        }
        data.guestId = guestId;
        data.hostId = hostId;
        data.guestType = 'patientids';

        data.action = 'unlinkuser';
        axios.post(config.apiUrl + 'removeuser.php', data).then(result => {
            setLoading(false);
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                dataSource.splice(index, 1)
                addAuditLog(guestId, hostId, 'user linked');
                setsnackMsg(strings.USER_UNLINKED_SUCCESSFULL)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
            else if(result.data.status == 'errorGuest'){
                setVisible1(false)
                addAuditLog(guestId, hostId, 'problem updating guest');
                setsnackMsg(strings.PROBLEM_UPDATING_GUEST)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }else if(result.data.status == 'errorHost'){
                setVisible1(false)
                addAuditLog(guestId, hostId, 'problem updating host');
                setsnackMsg(strings.PROBLEM_UPDATING_HOST)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }else{
                setVisible1(false)
                addAuditLog(guestId, hostId, 'problem updating data');
                setsnackMsg(strings.PROBLEM_ADDING_DATA)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
        })
    }

    const showModal1 = () => {
        setVisible1(true);
        setType('add')
    }
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
        setDatePickerVisibility1(false);
    };
    const handleConfirm = (date) => {
        // if(type == 'add'){
        if (check === "start") {
            SetallNotificationValues({ ...allValuesNotification, ['prestartdate']: moment(date).format('YYYY-MM-DD'), ['startDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "end") {
            SetallNotificationValues({ ...allValuesNotification, ['preenddate']: moment(date).format('YYYY-MM-DD'), ['endDate ']: moment(date).format('YYYY-MM-DD') })
        }
        if (check.includes("mul")) {
            let index = check.split(" ")[1];
            console.log("check", date)
            console.log("check", index)
            const list = [...time];
            list[index]['time'] = moment(date).format('HH:mm')
            setTime(list);
        }
        hideDatePicker();
    };


    const searchKeyword = async (value, hId, dId, nId) => {
        console.log("value", value)
        await setKeyword(value);
        await setsearchData(value);
        await setIsListEnd(false);
        await setLoading(false);
        await setDoctorId(dId);
        await setHcpId(hId);
        await setNursetId(nId);
        await setDataSource([])
        await setOffset(0)
    }

    useEffect(() => {
        getPatientsDataSearch()
    }, [keyword, hcpId, doctorId, nurseId]);


    const startSession = (data) => {
        setLoading(true)
        data.userid = data.uid;
        dispatch(setSession(data))
        dispatch(medicineLoading(false));
        dispatch(activityLoading(false));
        dispatch(wellBeingLoading(false));
        dispatch(allDataNull([]));
        setTimeout(() => {
            navigation.navigate('medicineScreenStack')
        }, 1500)

    }

    const getPatientsDataSearch = () => {
        // alert(navigation.userid)
        if (keyword == '') {
            Keyboard.dismiss()
        }
        if (!loading && !isListEnd) {
            setLoading(true);
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                mutualuserid:navigation.userid && navigation.userid != '' ? navigation.userid : '',
                subuserids : 'patientids',
                appsecret: config.appsecret,
                action: 'getallpatients',
                keyword: keyword,
                hcpid: hcpId ? hcpId : '',
                doctorid: doctorId ? doctorId : '',
                nurseid: nurseId ? nurseId : '',
                offset: 0
            };

            if (currentUser.usertype === 'admin') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : "")))
                dataToSend.adminid = currentUser.id;
            }
            if (currentUser.usertype === 'healthcareprovider') {
                (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
                dataToSend.hcpid = currentUser.id;
            }
            if (currentUser.usertype === 'patient') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
                dataToSend.doctorid = currentUser.id;
            }
            if (currentUser.usertype === 'doctor') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
                dataToSend.doctorid = currentUser.id;
            }
            if (currentUser.usertype === 'nurse') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : '')))
                dataToSend.nurseid = currentUser.id;
            }
            
            // console.log("dataToSend----->",dataToSend); return false;
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                if (result.data.data !== null) {
                    setLoading(false);
                    setDataSource(result.data.data);
                    setOffset(offset + 10)
                }
                else {
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
        else {
            setLoading(false);
        }
    }


    useEffect(() => {
        getDropDownList();
    }, [sessionUser]);
    const getDropDownList = () => {
        var dataToSend = {
            action: 'getallpatients',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        if (currentUser.usertype === 'admin') {
                dataToSend.adminid = currentUser.id;
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : "")))
            }
            if (currentUser.usertype === 'healthcareprovider') {
                (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
                dataToSend.hcpid = currentUser.id;
            }
            if (currentUser.usertype === 'patient') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
                dataToSend.doctorid = currentUser.id;
            }
            if (currentUser.usertype === 'doctor') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
                dataToSend.doctorid = currentUser.id;
            }
            if (currentUser.usertype === 'nurse') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : '')))
                dataToSend.nurseid = currentUser.id;
            }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
            console.log("result.data123", result)
            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            value: val.id,
                            icon: () => <View style={{ width: 20, height: 20, borderRadius: 10, backgroundColor: val.color }} />,
                            label: val.name,
                        }
                        console.log("result.data123123", ob)
                        arr.push(ob)
                    })
                    setDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }

    const getPatientsData = () => {
        console.log("offset", offset,);
        if (!loading && !isListEnd) {
            console.log("offset123", offset,);
            console.log('getData');
            setLoading(true);
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                mutualuserid:navigation.userid && navigation.userid != '' ? navigation.userid : '',
                subuserids : 'patientids',
                appsecret: config.appsecret,
                action: 'getallpatients',
                keyword: keyword,
                offset: offset
            };
            if (currentUser.usertype === 'admin') {
                dataToSend.adminid = currentUser.id;
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : "")))
            }
            if (currentUser.usertype === 'healthcareprovider') {
                (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
                dataToSend.hcpid = currentUser.id;
            }
            if (currentUser.usertype === 'patient') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
                dataToSend.doctorid = currentUser.id;
            }
            if (currentUser.usertype === 'doctor') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
                dataToSend.doctorid = currentUser.id;
            }
            if (currentUser.usertype === 'nurse') {
                (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : '')))
                dataToSend.nurseid = currentUser.id;
            }
            console.log("offset")
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                // alert("Asdf");
                console.log("result.data:", result)
                if (result.data.status === "success") {
                    if (result.data.data !== null) {
                        setLoading(false);
                        setDataSource([...dataSource, ...result.data.data]);
                        setOffset(offset + 10)
                    }
                    else {
                        setIsListEnd(true);
                        setLoading(false);
                    }
                }
                else {
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
    };

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setOffset(0);
        setKeyword('')
        setsearchData('')
        wait(2000).then(() => {
            setRefreshing(false);
            setIsListEnd(false)
            setDataSource([]);
            getPatientsData();
        });
    }, []);

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="white"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };
    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{ marginTop: 20 }}>
                <Text style={{ color: '#ccc', fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>{strings.NO_RESULT_FOUND}</Text>
            </View>
        );
    };
    const ItemView = ({ item, index }) => {
        console.log("index", index)
        return (
            // Flat List Item
            <View key={item.id} style={styles.activity}>
                <TouchableOpacity onPress={() => startSession(data)}>
                    <View style={[styles.activityColor, { backgroundColor: "#49526e" }]}>

                        {!item.profilepic ?
                            <Image
                                source={usericn}
                                style={{ width: 40, height: 40, borderRadius: 30, left: 3, top: 2 }}
                            />
                            :
                            <Image
                                source={{ uri: config.apiUrl+item.profilepic }}
                                style={{ width: 45, height: 45, borderRadius: 30, left: 0, top: 0 }}
                            />
                        }
                    </View>
                </TouchableOpacity>
                <Text style={styles.activityText} >
                    { ((item.firstname +" "+ item.lastname).length > 25) ? 
                    (((item.firstname +" "+ item.lastname).substring(0,22)) + '...') : 
                    item.firstname +" "+ item.lastname }
                    {/* {item.firstname +" "+ item.lastname} */}
                </Text>
                <View style={{ paddingTop: 10, flexDirection: "row", flexWrap: "wrap", }}>
                    <Text style={{ marginLeft: 0, fontSize: 18, color: "white", width: 150, textAlign: 'right' }}>{(item.phone ? item.phone : 'Not Available')}</Text>
                    <Text style={{ marginLeft: 45, fontSize: 18, color: "white", width: 200, }}>{(item.city ? item.city : 'Not Available')}</Text>

                    <TouchableOpacity onPress={() => onEditActivity(item)}>
                        <Image style={styles.icons} height={35} width={35} source={editbtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => DeleteAlert(item.id, item.uid, index)}>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    const DeleteAlert = (hostid, guestid, index) => {
        // alert(hostid+"-"+guestid+"-"+index);return false;
        showAlert(hostid, guestid, index);
    }


    useEffect(() => {
        getHcpDropDownList();
        getdoctorDropDownList();
        getnurseDropDownList();
    }, [sessionUser]);
    const getHcpDropDownList = () => {
        var dataToSend = {
            action: 'getallhcps',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        if (currentUser.usertype === 'admin') {
            dataToSend.adminid = currentUser.id;
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : "")))
        }
        if (currentUser.usertype === 'patient') {
            (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.hcpid = currentUser.id;
        }
        if (currentUser.usertype === 'doctor') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.doctorid = currentUser.id;
        }
        if (currentUser.usertype === 'nurse') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : '')))
            dataToSend.nurseid = currentUser.id;
        }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            id: val.uid,
                            // icon: () => <View style={{ width: 20, height: 20, borderRadius: 10, backgroundColor: "ff0000" }} />,
                            // name: <View style={{flexDirection:'row'}}><Image source={usericn} style={{ width: 40, height: 40, borderRadius: 30, borderWidth:1 }} /><Text>{val.firstname} {val.lastname}</Text></View>,
                            name: val.firstname + ' ' + val.lastname,
                        }
                        console.log("result.data12312345", ob)
                        arr.push(ob)
                    })
                    setHcpDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }
    const getdoctorDropDownList = () => {
        var dataToSend = {
            action: 'getalldoctors',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        if (currentUser.usertype === 'admin') {
            dataToSend.adminid = currentUser.id;
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : "")))
        }
        if (currentUser.usertype === 'healthcareprovider') {
            (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.hcpid = currentUser.id;
        }
        if (currentUser.usertype === 'patient') {
            // alert(navigation.currusertype)
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.doctorid = currentUser.id;
        }
        if (currentUser.usertype === 'nurse') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : '')))
            dataToSend.nurseid = currentUser.id;
        }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            id: val.uid,
                            name: val.firstname + ' ' + val.lastname,
                        }
                        console.log("result.data123123", ob)
                        arr.push(ob)
                    })
                    setDoctorDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }
    const getnurseDropDownList = () => {
        var dataToSend = {
            action: 'getallnurses',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        if (currentUser.usertype === 'admin') {
            dataToSend.adminid = currentUser.id;
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : "")))
        }
        if (currentUser.usertype === 'healthcareprovider') {
            (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.hcpid = currentUser.id;
        }
        if (currentUser.usertype === 'patient') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.doctorid = currentUser.id;
        }
        if (currentUser.usertype === 'doctor') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.doctorid = currentUser.id;
        }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            id: val.uid,
                            name: val.firstname + ' ' + val.lastname,
                        }
                        console.log("result.data123123", ob)
                        arr.push(ob)
                    })
                    setNurseDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }
    return (
        <SafeAreaView style={{
            flex: 1, marginTop: 100, justifyContent: 'center',
            alignItems: 'center', height: 600,
        }}>
            <AwesomeAlert
                contentContainerStyle={{
                    backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 12,
                    },
                    shadowOpacity: 0.58,
                    shadowRadius: 16.00,

                    elevation: 24
                }}
                show={awesomeAlert}
                showProgress={false}
                title={strings.DELETE_RECORD}
                titleStyle={{ color: 'white' }}
                message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                messageStyle={{ color: 'white' }}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={strings.NO_CANCEL}
                confirmText={strings.YES_DELETE}
                cancelButtonColor="#516b93"
                confirmButtonColor="#69c2d1"
                confirmButtonBorderRadius="0"
                confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                onCancelPressed={() => {
                    setAwesomeAlert(false);
                }}
                onConfirmPressed={() => {
                    DeleteUser(navigation.userid, guestId);
                }}
            />
            {/* <View style={{ flexDirection: 'row', zIndex: 800, position: 'absolute', top: 0 }}>
                <View style={{ flexDirection: 'row', alignSelf: 'flex-end', position: 'absolute', top: 0, left: -350 }}>
                    <SearchableDropdown
                        multi={false}
                        onTextChange={text => console.log(text)}
                        // onItemSelect={item => console.log(JSON.stringify(item.name))}
                        selectedItems={state1}
                        onItemSelect={(item) => {
                            // alert(item.name)
                            setIsListEnd(false)
                            searchKeyword(keyword, item.id, doctorId, nurseId)
                            wait(2000).then(() => {
                                const items = [];//state.selectedItems;
                                items.push(item)
                                // setState({ selectedItems: items });
                                setState(items);
                            });
                        }}
                        containerStyle={{ padding: 5 }}
                        itemStyle={{
                            padding: 10,
                            marginTop: 2,
                            backgroundColor: '#ddd',
                            borderColor: '#474F69',
                            borderWidth: 1,
                            borderRadius: 5,
                        }}
                        textInputStyle={{
                            padding: 12,
                            borderWidth: 1,
                            borderColor: '#ccc',
                            backgroundColor: '#FAF7F6',
                            width: 200,
                        }}
                        itemTextStyle={{ color: '#222' }}
                        itemsContainerStyle={{ maxHeight: 340 }}
                        items={hcpDropDownList}
                        // items={items}
                        defaultIndex={2}
                        placeholder="Filter HCP"
                        underlineColorAndroid="transparent"
                        resetValue={false}
                    >
                    </SearchableDropdown>
                    <Image style={{ flexDirection: 'row', left: -40, top: 15 }} height={25} width={25} source={downArrow} />
                </View>
                <View style={{ flexDirection: 'row', alignSelf: 'flex-start', position: 'absolute', top: 0, left: -100 }}>
                    <SearchableDropdown
                        multi={false}
                        onTextChange={text => console.log(text)}
                        // onItemSelect={item => console.log(JSON.stringify(item.name))}
                        selectedItems={state1}
                        onItemSelect={(item) => {
                            // alert(item.name)
                            setIsListEnd(false)
                            searchKeyword(keyword, hcpId, item.id, nurseId)
                            wait(2000).then(() => {
                                const items = [];//state.selectedItems;
                                items.push(item)
                                // setState({ selectedItems: items });
                                setState(items);
                            });
                        }}
                        containerStyle={{ padding: 5 }}
                        itemStyle={{
                            padding: 10,
                            marginTop: 2,
                            backgroundColor: '#ddd',
                            borderColor: '#474F69',
                            borderWidth: 1,
                            borderRadius: 5,
                        }}
                        textInputStyle={{
                            padding: 12,
                            borderWidth: 1,
                            borderColor: '#ccc',
                            backgroundColor: '#FAF7F6',
                            width: 200,
                        }}
                        itemTextStyle={{ color: '#222' }}
                        itemsContainerStyle={{ maxHeight: 340 }}
                        items={doctorDropDownList}
                        // items={items}
                        defaultIndex={2}
                        placeholder="Filter Doctor"
                        underlineColorAndroid="transparent"
                        resetValue={false}
                    >
                    </SearchableDropdown>
                    <Image style={{ flexDirection: 'row', left: -40, top: 15 }} height={25} width={25} source={downArrow} />
                </View>
                <View style={{ flexDirection: 'row', alignSelf: 'flex-start', position: 'absolute', top: 0, left: 150 }}>
                    <SearchableDropdown
                        multi={false}
                        onTextChange={text => console.log(text)}
                        // onItemSelect={item => console.log(JSON.stringify(item.name))}
                        selectedItems={state1}
                        onItemSelect={(item) => {
                            // alert(item.name)
                            setIsListEnd(false)
                            searchKeyword(keyword, hcpId, doctorId, item.id)
                            wait(2000).then(() => {
                                const items = [];//state.selectedItems;
                                items.push(item)
                                // setState({ selectedItems: items });
                                setState(items);
                            });
                        }}
                        containerStyle={{ padding: 5 }}
                        itemStyle={{
                            padding: 10,
                            marginTop: 2,
                            backgroundColor: '#ddd',
                            borderColor: '#474F69',
                            borderWidth: 1,
                            borderRadius: 5,
                        }}
                        textInputStyle={{
                            padding: 12,
                            borderWidth: 1,
                            borderColor: '#ccc',
                            backgroundColor: '#FAF7F6',
                            width: 200,
                        }}
                        itemTextStyle={{ color: '#222' }}
                        itemsContainerStyle={{ maxHeight: 340 }}
                        items={nurseDropDownList}
                        // items={items}
                        defaultIndex={2}
                        placeholder="Filter Nurse"
                        underlineColorAndroid="transparent"
                        resetValue={false}
                    >
                    </SearchableDropdown>
                    <Image style={{ flexDirection: 'row', left: -40, top: 15 }} height={25} width={25} source={downArrow} />
                </View>
            </View> */}
            <View><Text style={{color:'#fff', fontSize:18, fontFamily:'HelveticaNeue-Light'}}><Text style={{color:"#fff",fontWeight:'bold', fontSize:22}}>Your</Text> and <Text style={{color:"#fff",fontWeight:'bold', fontSize:22}}>{navigation.userfirstname} {navigation.userlastname}</Text>'s Mutual Patients</Text></View>
            <View style={{ right: 170, top: 0, flexDirection: "row", flexWrap: "wrap" }}>
                <View style={styles.SectionStyle}>
                    <Icon style={styles.searchIcon} name="search" size={22} color="#81889f" />
                    <TextInput
                        style={styles.inputStyle}
                        placeholder={strings.TYPE_TO_SEARCH_HERE}
                        placeholderTextColor="#8b9cb5"
                        autoCapitalize="none"
                        underlineColorAndroid="#f000"
                        onChangeText={(text) => { searchKeyword(text) }}
                        blurOnSubmit={false}
                        value={searchData}
                    />
                </View>
                <View style={{ paddingTop: 45, left: 100, flexDirection: "row", flexWrap: "wrap" }}>
                    <Text style={[styles.headerText, {}]}>{strings.PHONE}</Text>
                    <Text style={[styles.headerText, { left: 50 }]}>{strings.ADDRESS}</Text>
                    <Text style={[styles.headerText, { left: 210 }]}>{strings.EDIT}</Text>
                    <Text style={[styles.headerText, { left: 220 }]}>{strings.DELETE}</Text>
                </View>
            </View>
            <FlatList
                style={{ width: '80%', top: 0, }}
                showsVerticalScrollIndicator={false}
                data={dataSource}
                keyExtractor={(item, index) => index.toString()}
                renderItem={ItemView}
                ListEmptyComponent={NoDataFound}
                ListFooterComponent={renderFooter}
                onEndReached={getPatientsData}
                onEndReachedThreshold={0.5}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />}
            />
            <DateTimePickerModal
                style={{ backgroundColor: '#2b3249', }}
                headerTextIOS={strings.SELECT_TIME}
                locale="en_GB"
                textColor="white"
                isVisible={isDatePickerVisible}
                pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                mode="time"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
            />
            <FAB
                style={styles.floatBtn}
                fabStyle={{ height: 100 }}
                color={'white'}
                theme={{ colors: { accent: '#eb8682' } }}
                icon="plus"
                onPress={showModal1}
            >
            </FAB>
            <Snackbar
                style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                visible={visibleSnackbar}
                duration={3000}
                onDismiss={onDismissSnackBar}
            >
                {snackMsg}
            </Snackbar>
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 50,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    headerText: {
        color: 'white',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    activity: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: '100%',
        height: 50,
        borderRadius: 15,
        marginBottom: 17,
    },
    activityText: {
        fontFamily:'HelveticaNeue-Bold',
        flexDirection: 'row',
        fontSize: 18,
        width: 220,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    activityColor: {
        borderColor: 'black',
        // borderWidth: 1,
        borderRadius: 30,
        width: 45,
        height: 45,
        left: 15,
        marginTop: 2,
    },
    activityColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        fontFamily:'HelveticaNeue-Medium',
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        // position: 'absolute',
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8

    },
    scrollView: {
        height: '80%',
        width: '100%',
        // margin: 20,
        alignSelf: 'center',
        // padding: 20,
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 250,
    }
});

export default AllPatientScreen;
