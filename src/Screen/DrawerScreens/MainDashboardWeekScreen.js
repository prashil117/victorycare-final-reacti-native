import React, { useState, useEffect } from 'react';
import {
    TextInput,
    Image,
    TouchableOpacity,
    View,
    Text,
    Alert,
    StyleSheet,
} from 'react-native';
import MainDashboardWeek from './mainDashboardWeekScreen1'
import MainDashboardMonth from './MainDashboardMonthScreen'
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import { VictoryPie, VictoryChart, VictoryTheme, VictoryBar } from 'victory-native';
import Svg from 'react-native-svg';
import userImg from '../../../Image/victor.png';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const MainDashboard = ({ navigation }) => {
    const [visible, setVisible] = React.useState(true);
    const [selected, setSelected] = React.useState('week')


    return (
        <View style={styles.mainBody}>
            {/*Tab Container Start*/}
            <View style={{ flexDirection: 'row', position: 'absolute', width: '100%', height: 80, top: 0, left: 0 }}>
                <View style={styles.tabContainer}>
                    {/*Tabs Start*/}
                    <View style={selected === 'week' ? styles.selectTab : styles.unselectTab}>
                        <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setSelected('week')} >
                            <Text style={styles.weekText}>{strings.WEEKLY}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={selected === 'month' ? styles.selectTab : styles.unselectTab}>
                        <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setSelected('month')}>
                            <Text style={styles.monthText}>{strings.MONTHLY}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={[styles.middleDiv, { borderWidth: 0 }]}>
                        <Text style={styles.titleText}>{strings.PATIENTS_LIST}</Text>
                    </View>
                    {/*Tabs End*/}
                </View>
                <View style={[styles.rightDiv, { borderWidth: 0 }]}>
                    <View style={[styles.SectionStyle, { borderWidth: 0 }]}>
                        <Icon style={styles.searchIcon} name="search" size={22} color="#81889f" />
                        <TextInput
                            style={styles.inputStyle}
                            placeholder={strings.TYPE_TO_SEARCH_HERE}
                            placeholderTextColor="#8b9cb5"
                            autoCapitalize="none"
                            onChangeText={(text) => { searchKeyword(text) }}
                            underlineColorAndroid="#f000"
                            blurOnSubmit={false}
                        />
                    </View>
                </View>
                {selected === 'week' &&
                    <View style={{ marginTop: 70, left: 10 }}>
                        <MainDashboardWeek />
                    </View>}
                {selected !== 'week' &&
                    <View style={{ marginTop: 70, left: 10 }}>
                        <MainDashboardMonth />
                    </View>}
            </View>
        </View>
    )
}
const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    tabContainer: {
        width: 300,
        height: 50,
        flexDirection: 'row',
        // borderWidth:1,
        position: 'absolute',
        top: 10,
        left: 50,
        borderRadius: 50,
        backgroundColor: '#474f69',
    },
    selectTab: {
        width: 150,
        // height:50,
        backgroundColor: '#69c2d1',
        fontSize: 18,
        borderRadius: 50
    },
    weekText: {
        color: '#fff',
        fontSize: 20,
        textAlign: 'center',
        paddingTop: 10,
        fontWeight: 'bold'
    },
    unselectTab: {
        width: 150,
        // height:50,
        backgroundColor: '#474f69',
        fontSize: 18,
        borderRadius: 50
    },
    monthText: {
        color: '#fff',
        fontSize: 20,
        textAlign: 'center',
        paddingTop: 10,
        fontWeight: 'bold',
    },
    middleDiv: {
        width: 400,
    },
    titleText: {
        color: '#fff',
        fontSize: 26,
        fontWeight: 'bold',
        textAlign: 'center',
    },
    rightDiv: {
        width: 300, flexDirection: "row", flexWrap: "wrap", position: 'absolute', top: 0, right: 0
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 10,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    searchIcon: {
        paddingTop: 11,

    },
    container: {
        flexDirection: 'row', position: 'relative', top: 0, left: 0, alignItems: 'center', padding: 0, margin: 0, backgroundColor: '#22293e', width: '100%',
    },
    innerGraph: {
        width: 250, position: 'relative', alignItems: 'center', marginTop: 50
    }
});
export default MainDashboard;
