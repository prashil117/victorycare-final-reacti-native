import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import DropDownPicker from 'react-native-dropdown-picker';
import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    StyleSheet,
    Alert,
    RefreshControl,
    FlatList,
    Button,
    ScrollView,
    ActivityIndicator,
    Keyboard
} from 'react-native';
import moment from "moment";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Modal, Portal, Text, Provider, FAB, Snackbar } from 'react-native-paper';
import deletebtn from '../../../Image/delete-btn.png'
import editbtn from '../../../Image/edit-btn.png'
import notificationbtn from '../../../Image/notification-btn.png'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const ActivityNotificationScreen = ({ navigation }) => {
    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const [refreshing, setRefreshing] = React.useState(false);
    const [loading, setLoading] = useState(false);
    const [dataSource, setDataSource] = useState([]);
    const [offset, setOffset] = useState(0);
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const [dropValues, setDropDownList] = useState([]);
    const [type, setType] = useState('');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isDatePickerVisible1, setDatePickerVisibility1] = useState(false);
    const onDismissSnackBar = () => setVisibleSnack(false);
    const [keyword, setKeyword] = useState('');
    const [searchData, setsearchData] = useState('');
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [isListEnd, setIsListEnd] = useState(false);
    const [visible1, setVisible1] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [isAscending, setIsAscending] = useState(true);
    const [isAscNotification, setisAscNotification] = useState(true);
    const [isAscStartdate, setisAscStartdate] = useState(true);
    const [isAscEnddate, setisAscEnddate] = useState(true);
    const showAlert = (id, index) => {
      setAwesomeAlert(true);
      setPopupId(id)
      setPopupIndex(index)
    };

    const hideAlert = () => {
      setAwesomeAlert(false);
    };
    const containerStyle = { backgroundColor: '#2b3249', marginBottom: 25, paddingTop:50, padding: 10, alignItems: 'center', width: '50%', alignSelf: 'center', borderRadius: 8, width:'80%', height:'100%' };
    const containerStyle1 = { backgroundColor: '#2b3249', marginBottom: 25, padding: 10, alignItems: 'center', height: '90%', width: '80%', alignSelf: 'center', borderRadius: 10 };
    
    useEffect(() => getActivityData(), [sessionUser]);
    const [time, setTime] = React.useState([]);
    const [check, setCheck] = useState('start');
    const [allValuesNotification, SetallNotificationValues] = useState({
        id: '',
        prestartdate: '',
        preenddate: '',
        startDate: '',
        endDate: '',
        enablePrescriptionNotification: false,
        addtostandardwheel: false,
        activity: null,
        recurringDaily: false,
        startDate: '',
        endDate: ''
    });

    const showModal1 = () => {
      setVisible1(true);
      setType('add')
    }
    const hideModal1 = (key) => {
        for (key in allValuesNotification) {
            if (allValuesNotification.hasOwnProperty(key)) {
                allValuesNotification[key] = null;
            }
        }
        setVisible1(false)
        setTime([])
        setType('add')

    };
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
        setDatePickerVisibility1(false);
    };
    const handleConfirm = (date) => {
      // if(type == 'add'){
        if (check === "start") {
            SetallNotificationValues({ ...allValuesNotification, ['prestartdate']: moment(date).format('YYYY-MM-DD'), ['startDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "end") {
            SetallNotificationValues({ ...allValuesNotification, ['preenddate']: moment(date).format('YYYY-MM-DD'), ['endDate ']: moment(date).format('YYYY-MM-DD') })
        }
        if (check.includes("mul")) {
            let index = check.split(" ")[1];
            console.log("check", date)
            console.log("check", index)
            const list = [...time];
            list[index]['time'] = moment(date).format('HH:mm')
            setTime(list);
        }
      // }else{
      //
      // }

        hideDatePicker();
    };

    const handleInputChange = (value, index) => {
        const list = [...time];
        list[index]['time'] = value;
        setTime(list);
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...time];
        list.splice(index, 1);
        setTime(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setTime([...time, { time: "" }]);
    };


    const searchKeyword = async (value) => {
        console.log("value", value)
        await setKeyword(value);
        await setsearchData(value);
        await setIsListEnd(false);
        await setLoading(false);
        await setDataSource([])
        await setOffset(0)
    }

    useEffect(() => {
        getActivityDataSearch()
    }, [keyword]);


    const getActivityDataSearch = () => {

        if(keyword==''){
            Keyboard.dismiss()
        }
        if (!loading && !isListEnd) {
            setLoading(true);
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                appsecret: config.appsecret,
                hcpid: 0,
                action: 'getpatientactivitydata',
                keyword: keyword,
                offset: 0
            };
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                if (result.data.data !== null) {
                    setLoading(false);
                    setDataSource(result.data.data);
                    setOffset(offset + 10)
                }
                else {
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
        else {
            setLoading(false);
        }
    }


    useEffect(() => {
        getDropDownList();
    },[sessionUser]);
    const getDropDownList = () => {
        var data = {
            action: 'getactivitymasterdatafulllist',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "getdata.php", data).then(result => {

            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            value: val.id, 
                            icon: () => <View style={{width:20, height:20, borderRadius:10, backgroundColor:val.color}} />,
                            label: val.name,
                        }
                        console.log("result.data123123", ob)
                        arr.push(ob)
                    })
                    setDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }

    const getActivityData = () => {
        console.log("offset", offset,);
        if (!loading && !isListEnd) {
            console.log("offset123", offset,);
            console.log('getData');
            setLoading(true);
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                appsecret: config.appsecret,
                action: 'getpatientactivitydata',
                keyword: keyword,
                offset: offset
            };
            console.log("offset")
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                console.log("result.data", result.data.data)
                if (result.data.status === "success") {
                    if (result.data.data !== null) {
                        setLoading(false);
                        setDataSource([...dataSource, ...result.data.data]);
                        setOffset(offset + 10)
                    }
                    else {
                        setIsListEnd(true);
                        setLoading(false);
                    }
                }
                else {
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
    };

    const AddNotification = () => {
        setLoading(true)
        var data = allValuesNotification;
        data.userid= sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
        data.hcpid = 0;
        data.addedBy = currentUser.id;
        data.preenddate = data.startDate;
        data.prestartdate = data.endDate;
        data.appsecret = config.appsecret;
        data.starttime = time.map(x => x.time).join(',');
        data.action = 'addpatientactivity';
        console.log("notification", data)
        axios.post(config.apiUrl + 'adddata.php', data).then(result => {
            setLoading(false)
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                hideModal1()
                setVisible1(false);
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC);
                setVisibleSnack(true)
                getActivityData()
            }
            else {
                // setType('add');
                setVisible1(false);
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
                setVisible1(false);
                hideModal1()
            }
        })
    }

    const updateNotification = () => {
        setLoading(true)
        var data = allValuesNotification;
        data.uid= sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
        data.hcpid = 0;
        data.eventmasterid = data.id;
        data.addedBy = currentUser.id;
        data.appsecret = config.appsecret;
        data.activitymasterid = data.activity;
        data.addtostandardwheel = data.addtostandardwheel
        data['presenddate'] = data.endDate ? data.endDate : data.preenddate;
        data.presstartdate = data.startDate ? data.startDate : data.prestartdate;
        data.siblinguserid = sessionUser && sessionUser.userid ? currentUser.id : '';
        data.starttime = time.map(x => x.time).join(',');
        data.action = 'updatepatientsactivity';
        console.log("notification", data)
        axios.post(config.apiUrl + 'updatedata.php', data).then(result => {
            setLoading(false)
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                setOffset(0);
                setLoading(false);
                setVisible1(false)
                setsnackMsg(strings.NOTIFICATION_UPDATED_SUCC);
                setVisibleSnack(true)
                hideModal1()
                onRefresh()
            }
            else {
                // setType('add');
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
                setVisible1(false);
                hideModal1()
            }
        })
    }

    const onEditNotification = (data) => {
        console.log("data++", data)
        allValuesNotification.id = data.id;
        allValuesNotification.activity = data.activitymasterid;
        allValuesNotification.recurringDaily = data.selecttime !== '' ? true : false;
        allValuesNotification.addtostandardwheel = data.addtostandardwheel = "1" ? true : false;
        allValuesNotification.enablePrescriptionNotification = (data.enablepresnotification === "1" ? true : false);
        allValuesNotification.selecttime = data.selecttime;
        allValuesNotification.prestartdate = moment(data.presstartdate).format('YYYY-MM-DD');
        allValuesNotification.preenddate = moment(data.presenddate).format('YYYY-MM-DD');
        let arr = data.starttime.split(',');
        // for (var i in arr) {
        //     setTime([...time, { time: arr[i] }]);
        // }
        let arr1=[]
        for (var i in arr) {
            arr1.push({time:arr[i]})
        }
        setTime(arr1)
        setVisible1(true);
        setType('edit');
    }

    const onClickHeaderAscDesc = (type) => {
        // alert(type)
        var sorted;
        // console.log("setDataSource::", dataSource); return false;
        if (isAscending) {
            sorted = [...dataSource].sort((a, b) => {
                // return (a === b)? 0 : a? -1 : 1;
                // return b.time - a.time
                return b.type < a.type ? 1 : -1;
            });
            setIsAscending(false)
        } else {
            setIsAscending(true)
            sorted = [...dataSource].sort((a, b) => {
                // // return (a === b)? a : 0? -1 : 1;
                // return b.time - a.time
                return b.type > a.type ? 1 : -1;
            });
        }
        
        // return false;
        setDataSource(sorted)

        setisAscNotification(false)
        setisAscStartdate(false)
        setisAscEnddate(false)

        if (type === 'enablepresnotification') {
            if (isAscNotification) {
                setisAscNotification(false)
            } else {
                setisAscNotification(true)
            }
        }
        if (type === 'startdate') {
            if (isAscStartdate) {
                setisAscStartdate(false)
            } else {
                setisAscStartdate(true)
            }
        } else if (type === 'enddate') {
            if (isAscEnddate) {
                setisAscEnddate(false)
            } else {
                setisAscEnddate(true)
            }
        }
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setOffset(0);
        setKeyword('')
        setsearchData('')
        wait(2000).then(() => {
            setRefreshing(false);
            setIsListEnd(false)
            setDataSource([]);
            getActivityData();
        });
    }, []);

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="white"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };
    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{marginTop:20}}>
                <Text style={{color:'#ccc', fontSize:18, fontWeight:'bold', textAlign:'center'}}>{strings.NO_RESULT_FOUND}</Text>
            </View>
        );
    };
    const ItemView = ({ item, index }) => {
        return (
            // Flat List Item
            <View key={item.id} style={styles.activity}>
                <View style={[styles.activityColor, { backgroundColor: item.color }]}>
                </View>
                <Text style={styles.activityText}>{item.name}</Text>
                <Switch style={[styles.headerText, { left: -50, marginTop:10 }]}
                    trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                    borderColor={'#81889f'}
                    borderWidth={1}
                    value={item.enablepresnotification && item.enablepresnotification == true ? true : false}
                    onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['enablePrescriptionNotification']: val })}
                    borderRadius={16}
                    thumbColor={"#fff"}
                />
                <View style={{width:200, left:-30}}>
                    <View style={{flexDirection:'row'}}>
                        <Text style={{ fontSize: 16, width: 100, fontWeight: '500', marginTop: 12, color: 'white' }} >{moment(item.presstartdate).format('DD.MM.YYYY')}</Text>
                        <Text style={{ fontSize: 16, width: 100, fontWeight: '500', marginTop: 12, color: 'white' }}>{moment(item.presenddate).format('DD.MM.YYYY')}</Text>
                    </View>
                    <View style={{width:200}}>
                        <Text style={{color:'#fff'}}>Time Slots:{item.starttime}</Text>
                    </View>
                </View>
                <View style={{ flexDirection: "row", right: 15, flexWrap: "wrap" }}>
                    <TouchableOpacity onPress={() => { DeleteAlert(item.id, index) }}>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onEditNotification(item)}>
                        <Image style={styles.icons} height={35} width={35} source={editbtn} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    const DeleteAlert = (id, index) => {
        showAlert(id, index)
        // Alert.alert(
        //     'Delete',
        //     'Are your sure you want to delete?',
        //     [
        //         {
        //             text: 'No',
        //             onPress: () => { },
        //             style: 'cancel',
        //         },
        //         {
        //             text: 'Yes',
        //             onPress: () => DeleteActivityNotification(id, index)
        //         },
        //     ],
        //     { cancelable: false },
        // );
    }

    const DeleteActivityNotification = (id, index) => {
        setLoading(true)
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        data.appsecret = config.appsecret;
        data.id = id;
        data.action = 'deletepatientactivity';
        data.siblinguserid = sessionUser && sessionUser.userid ? currentUser.id : '';
        axios.post(config.apiUrl + 'deletedata.php', data).then(result => {
            setLoading(false);
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                dataSource.splice(index, 1)
                setsnackMsg(strings.NOTIFICATION_DELETED_SUCC)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
            else {
                setVisible1(false)
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
        })
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.mainBody}>
              <AwesomeAlert
                contentContainerStyle={{
                    backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 12,
                    },
                    shadowOpacity: 0.58,
                    shadowRadius: 16.00,

                    elevation: 24
                }}
                show={awesomeAlert}
                showProgress={false}
                title={strings.DELETE_RECORD}
                titleStyle={{ color: 'white' }}
                message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                messageStyle={{ color: 'white' }}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={strings.NO_CANCEL}
                confirmText={strings.YES_DELETE}
                cancelButtonColor="#516b93"
                confirmButtonColor="#69c2d1"
                confirmButtonBorderRadius="0"
                confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                onCancelPressed={() => {
                  setAwesomeAlert(false);
                }}
                onConfirmPressed={() => {
                  DeleteActivityNotification(popupId, popupIndex);
                }}
              />
                {/* <Loader loading={loading} /> */}
                <View style={styles.main} >
                    <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                        <View style={{ ...styles.SectionStyle, alignSelf: 'flex-end', top: 80 }}>
                            <TextInput
                                style={styles.inputStyle}
                                placeholder={strings.TYPE_TO_SEARCH_HERE}
                                placeholderTextColor="#81889f"
                                autoCapitalize="none"
                                value={searchData}
                                underlineColorAndroid="#81889f"
                                onChangeText={(text) => { searchKeyword(text) }}
                                blurOnSubmit={false}
                            />
                            <Icon style={styles.searchIcon} name="search" size={22} color="#81889f" />
                        </View>
                        {/* <View style={styles.nameText}>
                            <Text style={{ color: 'white', top: 25 }}>{strings.NAME}</Text>
                        </View> */}
                        <View style={{ paddingTop: 45, left: 120, flexDirection: "row", flexWrap: "wrap" }}>
                            <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }} onPress={() => onClickHeaderAscDesc('enablepresnotification')}>
                                <Text style={styles.headerText}>{strings.NOTIFICATIONS}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }} onPress={() => onClickHeaderAscDesc('startdate')}>
                                <Text style={styles.headerText}>{strings.START_DATE}</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }} onPress={() => onClickHeaderAscDesc('enddate')}>
                                <Text style={styles.headerText}>{strings.END_DATE}</Text>
                            </TouchableOpacity>
                            <Text style={styles.headerText}>{strings.DELETE}</Text>
                            <Text style={styles.headerText}>{strings.EDIT}</Text>
                        </View>
                    </View>
                    <FlatList
                        showsVerticalScrollIndicator={false}
                        data={dataSource}
                        keyExtractor={(item, index) => index.toString()}
                        renderItem={ItemView}
                        ListEmptyComponent={NoDataFound}
                        ListFooterComponent={renderFooter}
                        onEndReached={getActivityData}
                        onEndReachedThreshold={0.5}
                        refreshControl={
                            <RefreshControl
                                refreshing={refreshing}
                                onRefresh={onRefresh}
                            />}
                    />
                </View>
                <Provider>
                    <Portal>
                        <Modal visible={visible1} onDismiss={hideModal1} contentContainerStyle={containerStyle1}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                                <Text style={{ fontFamily:'HelveticaNeue-Bold', color: 'white', fontWeight: '700', fontSize:22, marginTop:50, }}>{type == 'add' ? strings.ADD_ACTIVITY_NOTIFICATION : strings.EDIT_ACTIVITY_NOTIFICATION}</Text>
                                <View style={{ left: 250, }}>
                                    <TouchableOpacity onPress={hideModal1}>
                                        <Icon name="close" color="#81889f" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}
                                contentContainerStyle={styles.contentContainer}>
                                <View style={{ height: 400, marginTop: 40, width:'60%' }}>
                                    <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8, left: 4 }}>{strings.NAME_OF_ACTIVITY} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                    <DropDownPicker
                                        items={dropValues}
                                        placeholder={strings.SELECT_ACTIVITY}
                                        labelStyle={{ fontFamily:'HelveticaNeue-Light', fontSize: 14, textAlign: 'left', color: 'white' }}
                                        defaultValue={allValuesNotification.activity ? allValuesNotification.activity : null}
                                        containerStyle={{ height: 40, color: 'white', borderWidth: 0, zIndex:9970 }}
                                        style={{ fontFamily:'HelveticaNeue-Light', backgroundColor: '#2b3249', color: '#81889f', borderWidth: 0, borderBottomWidth: 2, borderBottomColor:'#81889f',  }}
                                        itemStyle={{
                                            justifyContent: 'flex-start', color: 'white'
                                        }}
                                        dropDownStyle={{ backgroundColor: '#2b3249', color: 'white' }}
                                        onChangeItem={item => SetallNotificationValues({ ...allValuesNotification, ['activity']: item.value })}
                                    />
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between' }}>
                                        <View>
                                            <Text style={{ color: 'white', marginBottom: 8 }}>{strings.START_DATE} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                            <TextInput
                                                style={{ width: 200, color: '#81889f', fontSize:16 }}
                                                placeholder={strings.SELECT_START_DATE}
                                                placeholderTextColor={'#81889f'}
                                                editable={false}
                                                onTouchStart={() => { setDatePickerVisibility1(true); setCheck('start') }}
                                                value={allValuesNotification.prestartdate}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                          </View>
                                          <DateTimePickerModal
                                                style={{ backgroundColor: '#2b3249', }}
                                                headerTextIOS={strings.SELECT_TIME}
                                                pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                                                modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                                                textColor="white"
                                                onTouchStart={() => { setDatePickerVisibility1(true) }}
                                                isVisible={isDatePickerVisible1}
                                                mode="date"
                                                onConfirm={handleConfirm}
                                                onCancel={hideDatePicker}
                                            />
                                        <View style={{ marginLeft: 30 }}>
                                            <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.END_DATE} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                            <TextInput
                                                style={{ width: 200, color: '#81889f', fontSize:16 }}
                                                placeholder={strings.SELECT_END_DATE}
                                                placeholderTextColor={'#81889f'}
                                                editable={false}
                                                onTouchStart={() => { setDatePickerVisibility1(true); setCheck('end') }}
                                                value={allValuesNotification.preenddate}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                    </View>
                                    <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', fontSize: 16, marginTop: 40 }}>
                                        {strings.TIME}
                                    </Text>
                                    {time.map((x, i) => {
                                        return (
                                            <View key={i} style={styles.SectionStyle}>
                                                <TextInput
                                                    style={styles.inputStyle}
                                                    name="time"
                                                    placeholder={strings.SELECT_TIME}
                                                    editable={false}
                                                    onTouchStart={() => { setDatePickerVisibility(true); setCheck(`mul ${i}`) }}
                                                    value={x.time}
                                                    placeholderTextColor="#81889f"
                                                    underlineColorAndroid="#81889f"
                                                />
                                                <View>
                                                    {time.length !== 0 && <TouchableOpacity onPress={() => handleRemoveClick(i)} ><Icon name="close" color="#81889f" size={16} /></TouchableOpacity>}
                                                </View>
                                            </View>
                                        )
                                    })}
                                    <Button style={{ color: '#81889f' }} onPress={handleAddClick} title={"+ " + strings.PLEASE_ENTER_ACTIVITY_TAKEN_TIME}> +{strings.PLEASE_ENTER_ACTIVITY_TAKEN_TIME}</Button>
                                    <View style={{ fontFamily:'HelveticaNeue-Light', marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                        <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.ADD_TO_STANDARD_WHEEL}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={2}
                                            value={allValuesNotification.addtostandardwheel}
                                            onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['addtostandardwheel']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                    </View>
                                    <View style={{ fontFamily:'HelveticaNeue-Light', marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                        <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.RECURRING_DAILY}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={2}
                                            value={allValuesNotification.recurringDaily}
                                            onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['recurringDaily']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATIONS}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={1}
                                            value={allValuesNotification.enablePrescriptionNotification}
                                            onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['enablePrescriptionNotification']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                    </View>
                                    {type == "add" ? <TouchableOpacity onPress={() => AddNotification()} style={[styles.buttonStyle, { top: 30 }]} activeOpacity={0.5}>
                                        <Text style={styles.buttonTextStyle}>{strings.SAVE}</Text>
                                    </TouchableOpacity> :
                                        <TouchableOpacity onPress={() => updateNotification()} style={[styles.buttonStyle ]} activeOpacity={0.5}>
                                            <Text style={styles.buttonTextStyle}>{strings.UPDATE}</Text>
                                        </TouchableOpacity>}
                                </View>

                            </ScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                <View style={{ width: 200 }}>
                    <DateTimePickerModal
                        style={{ backgroundColor: '#2b3249', }}
                        headerTextIOS={strings.SELECT_TIME}
                        pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                        modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                        textColor="white"
                        // headerTextIOS={'Select Time'}
                        locale="en_GB"
                        textColor="white"
                        isVisible={isDatePickerVisible}
                        mode="time"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />
                </View>
                <Snackbar
                    style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                    visible={visibleSnackbar}
                    duration={3000}
                    onDismiss={onDismissSnackBar}
                >
                    {snackMsg}
                </Snackbar>
                <FAB
                    style={styles.floatBtn}
                    fabStyle={{ height: 100 }}
                    color={'white'}
                    theme={{ colors: { accent: '#eb8682' } }}
                    icon="plus"
                    onPress={showModal1}
                >
                </FAB>

            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    main: {
        marginTop: 50
    },
    headerText: {
        fontFamily:'HelveticaNeue-Medium',
        fontSize:12,
        color: '#81889f',
        marginLeft: 40,
    },
    icons: {
        marginLeft: 15,
        marginTop: 4,
        width:35,
        height:35
    },
    searchIcon: {
        paddingTop: 11,

    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 120,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    activity: {
        color: '#81889f',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 2,
        borderColor: '#474f69',
        width: 820,
        height: 54,
        borderRadius: 15,
        marginBottom: 17,
    },
    activityText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    activityColor: {
        backgroundColor: '#e0675c',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    buttonStyle3: {
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        fontFamily:'HelveticaNeue-Light', 
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#81889f',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        fontFamily:'HelveticaNeue-Light',
        fontSize:16,
        color:'#81889f',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    nameText: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        right: 35,
        width: 200,
        justifyContent: 'center',
        textAlign: 'center'
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 70,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,
    },
    scrollView: {
        height: '63%',
        width: '100%',
        // margin: 20,
        alignSelf: 'center',
        // padding: 20,
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 250,
    }
});

export default ActivityNotificationScreen;
