import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
    TextInput,
    Image,
    TouchableOpacity,    
    SafeAreaView,
    View,
    StyleSheet,
    RefreshControl,
    FlatList,
    ActivityIndicator,
    Keyboard
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import usericn from '../../../Image/nopreview.jpg';
import { Text, FAB, Snackbar } from 'react-native-paper';
import deletebtn from '../../../Image/delete-btn.png'
import downArrow from '../../../Image/up_arrow.png'
import editbtn from '../../../Image/edit-btn.png'
import infobtn from '../../../Image/info-btn.png'
import { setSession, medicineLoading, activityLoading, wellBeingLoading, allDataNull } from '../redux/action'
import axios from 'axios'
import config from '../appconfig/config';
import AwesomeAlert from 'react-native-awesome-alerts';
import strings from '../../constants/lng/LocalizedStrings';
import SearchableDropdown from 'react-native-searchable-dropdown';

const AllDoctorScreen = ( navigation ) => {
    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const dispatch = useDispatch();
    const editRight = currentUser && (currentUser.usertype == 'superadmin' || currentUser.usertype == 'admin' || currentUser.usertype == 'healthcareprovider') ? true : false
    const [refreshing, setRefreshing] = React.useState(false);
    const [loading, setLoading] = useState(false);
    const [dataSource, setDataSource] = useState([]);
    const [subuserids, setSubuserids] = useState('');
    const [doctorSelect, setDoctorSelect] = useState('');
    const [nurseSelect, setNurseSelect] = useState('');
    const [offset, setOffset] = useState(0);
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const [dropValues, setDropDownList] = useState([]);
    const [hcpDropDownList, setHcpDropDownList] = useState([]);
    const [patientDropDownList, setPatientDropDownList] = useState([]);
    const [nurseDropDownList, setNurseDropDownList] = useState([]);
    const [state1, setState] = useState([]);
    const onDismissSnackBar = () => setVisibleSnack(false);
    const [keyword, setKeyword] = useState('');
    const [searchData, setsearchData] = useState('');
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [isListEnd, setIsListEnd] = useState(false);
    const [visible1, setVisible1] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [hostId, setHostId] = useState(false)
    const [guestId, setGuestId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [filterSelected, setFilterSelected] = useState(false)
    const [patientId, setPatientId] = useState('');
    const [hcpId, setHcpId] = useState('');
    const [nurseId, setNursetId] = useState('');
    const [dIds1, setDids1] = useState(undefined);
    const [dIds2, setDids2] = useState(undefined);
    const showAlert = (hostid, guestid, index) => {
      setAwesomeAlert(true);
      setHostId(hostid)
      setGuestId(guestid)
      setPopupIndex(index)
    };
    
    useEffect(() => getDoctorsData(), [sessionUser]);
    const addAuditLog = (activity) => {
        // alert(activity); return false;
        var dataToSend = {
            action: 'insertlog',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            siblinguserid: currentUser && currentUser.id ? currentUser.id : '',
            page:'alldoctorscreen',
            activity: activity,
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "auditlogs.php", dataToSend).then(result => {
            if (result.data.status == 'success') {
                console.log("Log entered successfully!")
            }
        })
    }
    
    const searchKeyword = async (search, value, pId, dId) => {
        getDoctorsDataSearch(subuserids, pId, dId)
        await setKeyword(search);
        await setsearchData(value);
        await setIsListEnd(false);
        await setLoading(false);
        await setDataSource([])
        await setOffset(0);
    }

    useEffect(() => {
        getDoctorsDataSearch()
    }, [keyword]);

    const startSession = (data) => {
        setLoading(true)
        data.userid = data.uid;
        // dispatch(setSession(data))
        // dispatch(medicineLoading(false));
        // dispatch(activityLoading(false));
        // dispatch(wellBeingLoading(false));
        // dispatch(allDataNull([]));
        setTimeout(() => {
            // alert(currentUser.usertype)
            navigation.navigate('AllUsersSubScreen', { userid: data.uid, userfirstname:data.firstname, userlastname:data.lastname, currusertype:data.usertype })
        }, 1500)

    }

    const onViewUser = (data, editable) => {
        // alert(editable); return false;
        setLoading(true)
        navigation.navigate('ViewEditProfileScreen', { userid: data.uid, userfirstname:data.firstname, userlastname:data.lastname, currusertype:data.usertype, editable:editable })
    }

    const getDoctorsDataSearch = (subuserids = '', doc, nur) => {
        // alert(subuserids+'-'+doc+'-'+nur)
        if(subuserids != ''){
            array1 = subuserids.split(',');
            if(doc !== '' && nur === ''){
                // alert("in")
                var docSel = doc;
                var nurSel = '';
                if(nurseSelect !== null){
                    array2 = doc.split(',');
                    // alert("in d-"+nurseSelect)
                    array3 = nurseSelect.split(',');
                    
                    var filteredArray = array1.filter(value => array2.includes(value));
                    filteredArray = filteredArray.filter(value => array3.includes(value));
                    filteredArrayFinal = filteredArray.length > 0 ? filteredArray : 'nofilter';
                }else{
                    // alert("out d")
                    array3 = doc.split(',');
                    var filteredArray = array1.filter(value => array3.includes(value))
                    filteredArrayFinal = filteredArray.length > 0 ? filteredArray : 'nofilter';
                }
            }else if(doc == '' && nur != ''){
                var docSel = '';
                var nurSel = nur;
                if(doctorSelect != null){
                    // alert("in n")
                    array2 = doctorSelect.split(',');
                    array3 = nur.split(',');
                    var filteredArray = array1.filter(value => array2.includes(value))
                    filteredArray = filteredArray.filter(value => array3.includes(value))
                    filteredArrayFinal = filteredArray.length > 0 ? filteredArray : 'nofilter';
                }else{
                    // alert("out n")
                    array3 = nur.split(',');
                    var filteredArray = array1.filter(value => array3.includes(value))
                    filteredArrayFinal = filteredArray.length > 0 ? filteredArray : 'nofilter';
                }
            }
        }else{
            filteredArrayFinal = '';
        }
        if (keyword == '') {
            Keyboard.dismiss()
        }
        if ((!loading && !isListEnd) || (doc != undefined || nur != undefined)) {
            // alert(dIds1)
            setLoading(true);
            var dataToSend = {
                userid: currentUser.id,
                subuserids: 'doctorids',
                docSec:docSel,
                nurSec:nurSel,
                usertype: sessionUser && sessionUser.usertype ? 'patient' : currentUser.usertype,
                appsecret: config.appsecret,
                arrayids: filteredArrayFinal,
                hcpid: '',
                action: 'getallhcps',
                keyword: keyword,
                offset: 0
            };
            console.log("dataToSend:->>", dataToSend)
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                console.log("data after search doctor::",result.data)
                if (result.data.data !== null) {
                    setIsListEnd(false);
                    setLoading(false);
                    setDataSource(result.data.data);
                    setSubuserids(result.data.subuserids);
                    setDoctorSelect(result.data.doctorSelect);
                    setNurseSelect(result.data.nurseSelect);
                    setOffset(offset + 10)
                }
                else {
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
        else {
            setLoading(false);
        }
    }


    useEffect(() => {
        getDropDownList();
    },[sessionUser]);
    const getDropDownList = () => {
        var dataToSend = {
            action: 'getalldoctors',
            userid: currentUser.id,
            subuserids: 'doctorids',
            appsecret: config.appsecret,
        }
        // if (currentUser.usertype === 'admin') {
        //     dataToSend.adminid = currentUser.id;
        // }
        // if (currentUser.usertype === 'patient') {
        //     dataToSend.patientid = currentUser.id;
        // }
        // if (currentUser.usertype === 'healthcareprovider') {
        //     dataToSend.hcpid = currentUser.id;
        // }
        // if (currentUser.usertype === 'doctor') {
        //     dataToSend.doctorid = currentUser.id;
        // }
        // if (currentUser.usertype === 'nurse') {
        //     dataToSend.nurseid = currentUser.id;
        // }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
            console.log("result.data123", result)
            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [{id:'', name:'--'+strings.SELECT_ALL+'--'}];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            value: val.id, 
                            icon: () => <View style={{width:20, height:20, borderRadius:10, backgroundColor:val.color}} />,
                            label: val.name,
                        }
                        console.log("result.data123123", ob)
                        arr.push(ob)
                    })
                    setDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }

    const getDoctorsData = (arrayids = '') => {
        console.log("offset", offset,);
        if ((!loading && !isListEnd)) {
            console.log("offset123", offset,);
            console.log('getData');
            setLoading(true);
            var dataToSend = {
                userid: currentUser.id,
                appsecret: config.appsecret,
                action: 'getalldoctors',
                arrayids: (arrayids && arrayids !== null) ? arrayids : '',
                subuserids: 'doctorids',
                keyword: keyword,
                offset: offset
            };
            console.log("offset")
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                // alert("Asdf");
                console.log("result.data:", result)
                if (result.data.status === "success") {
                    if (result.data.data !== null) {
                        setLoading(false);
                        setDataSource([...dataSource, ...result.data.data]);
                        setOffset(offset + 10)
                    }
                    else {
                        setIsListEnd(true);
                        setLoading(false);
                    }
                }
                else {
                    // alert(isListEnd)
                    setIsListEnd(true);
                    setLoading(false);
                }
            })
        }
    };


    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setOffset(0);
        setKeyword('')
        setsearchData('')
        wait(2000).then(() => {
            setRefreshing(false);
            setIsListEnd(false)
            setDataSource([]);
            getDoctorsData();
        });
    }, []);

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="white"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };
    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{marginTop:20}}>
                <Text style={{color:'#ccc', fontSize:18, fontWeight:'bold', textAlign:'center'}}>{strings.NO_RESULT_FOUND}</Text>
            </View>
        );
    };
    const ItemView = ({ item, index }) => {
        console.log("index", index)
        return (
            // Flat List Item
            <View key={item.id} style={styles.activity}>
                <View style={[styles.activityColor, { backgroundColor: "#49526e" }]}>
                
                {!item.profilepic ?
                    <Image
                        source={usericn}
                        style={{ width:40, height:40, borderRadius:30, left: 3, top:2 }}
                    /> 
                    :
                    <Image
                        source={{ uri: config.apiUrl+item.profilepic }}
                        style={{ width: 45, height: 45, borderRadius: 30, left: 0, top:0  }}
                    />
                }
                </View>
                <TouchableOpacity onPress={() => startSession(item)}>
                    <Text style={styles.activityText}>{item.firstname} {item.lastname}</Text>
                </TouchableOpacity>
                <View style={{ paddingTop: 10, flexDirection: "row", flexWrap: "wrap", }}>
                    <Text style={{marginLeft:0, fontSize:18, color:"white", width:150, textAlign:'right'}}>{(item.phone ? item.phone : strings.NOT_AVAILABLE)}</Text>
                    <Text style={{marginLeft:45, fontSize:18, color:"white", width:200,}}>{(item.city ? item.city : strings.NOT_AVAILABLE)}</Text>
                    {/* <TouchableOpacity style={{ width: 34, marginLeft:50 }} onPress={() => showModal1(item.id)}>
                        <Image style={{ marginTop: -3, height: 34, width: 34 }} height={34} width={34} source={notificationbtn} />
                    </TouchableOpacity> */}
                    {editRight &&
                    <>
                    <TouchableOpacity onPress={() => onViewUser(item, true)}>
                        <Image style={styles.icons} height={35} width={35} source={ editbtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => DeleteAlert(item.id, item.uid, index)}>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                    </>}
                    {!editRight &&
                    <>
                    <TouchableOpacity onPress={() => onViewUser(item, false)}>
                        <Image style={styles.icons} height={35} width={35} source={ infobtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => DeleteAlert(item.id, item.uid, index)}>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                    </>}
                </View>
            </View>
        );
    };

    const DeleteAlert = (hostid, guestid, index) => {
        showAlert(hostid, guestid, index)
        // Alert.alert(
        //     'Delete',
        //     'Are your sure you want to delete?',
        //     [
        //         {
        //             text: 'No',
        //             onPress: () => { },
        //             style: 'cancel',
        //         },
        //         {
        //             text: 'Yes',
        //             onPress: () => DeleteActivityNotification(id, index)
        //         },
        //     ],
        //     { cancelable: false },
        // );
    }

    const DeleteUser = (hostId, guestId, index) => {
        // alert(index); return false;
        setLoading(true)
        var data = {};
        data.userid = currentUser.id;
        data.appsecret = config.appsecret;
        data.page = 'alldoctorscreen';
        data.activity = 'unlinkdoctor';
        if (currentUser.usertype === 'admin') {
            data.adminid = currentUser.id;
            data.hostType = 'adminids';
        }
        if (currentUser.usertype === 'healthcareprovider') {
            data.adminid = currentUser.id;
            data.hostType = 'hcpids';
        }
        if (currentUser.usertype === 'patient') {
            data.patientid = currentUser.id;
            data.hostType = 'patientids';
        }
        if (currentUser.usertype === 'doctor') {
            data.doctorid = currentUser.id;
            data.hostType = 'doctorids';
        }
        if (currentUser.usertype === 'nurse') {
            data.nurseid = currentUser.id;
            data.hostType = 'nurseids';
        }
        data.guestId = guestId;
        data.hostId = hostId;
        data.guestType = 'doctorids';

        // data.action = editRight ? 'deleteuser' : 'unlinkuser';
        data.action = 'unlinkuser';
        axios.post(config.apiUrl + 'removeuser.php', data).then(result => {
            setLoading(false);
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                dataSource.splice(index, 1)
                addAuditLog('unlink user');
                setsnackMsg(strings.USER_UNLINKED_SUCCESSFULL)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
            else if(result.data.status == 'errorGuest'){
                setVisible1(false)
                addAuditLog('problem updating guest data');
                setsnackMsg(strings.PROBLEM_UPDATING_GUEST)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }else if(result.data.status == 'errorHost'){
                setVisible1(false)
                addAuditLog('problem updating host data');
                setsnackMsg(strings.PROBLEM_UPDATING_HOST)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }else{
                setVisible1(false)
                addAuditLog('problem updating data');
                setsnackMsg(strings.PROBLEM_ADDING_DATA)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
        })
    }
    const showModal1 = () => {
        // setVisible1(true);
        // setHideZindex(true);
        navigation.navigate('AddUserScreen', { toBeAddedUsertype: 'doctor' })
    }
    const selectedItems = [{setState}];
    // const [serverData, setServerData] = useState([]);
    // useEffect(() => {
    //     fetch('https://aboutreact.herokuapp.com/demosearchables.php')
    //     .then((response) => response.json())
    //     .then((responseJson) => {
    //     //Successful response from the API Call
    //     setServerData(responseJson.results);
    //     })
    //     .catch((error) => {
    //     console.error(error);
    //     });
    // }, []);
    
    useEffect(() => {
        getHcpDropDownList();
        getpatientDropDownList();
        getnurseDropDownList();
    },[sessionUser]);
    const getHcpDropDownList = () => {
        var dataToSend = {
            action: 'getallhcps',
            userid: currentUser.id,
            subuserids: 'hcpids',
            appsecret: config.appsecret,
        }
        // if (currentUser.usertype === 'admin') {
        //     dataToSend.adminid = currentUser.id;
        // }
        // if (currentUser.usertype === 'patient') {
        //     dataToSend.patientid = currentUser.id;
        // }
        // if (currentUser.usertype === 'doctor') {
        //     dataToSend.doctorid = currentUser.id;
        // }
        // if (currentUser.usertype === 'nurse') {
        //     dataToSend.nurseid = currentUser.id;
        // }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
            console.log('dropValues---',result.data.data)
            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [{id:'', name:'--'+strings.SELECT_ALL+'--'}];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            id: val.uid, 
                            name: val.firstname+' '+val.lastname,
                            doctorids:val.doctorids
                        }
                        console.log("result.data12312345 hcps:", ob)
                        arr.push(ob)
                    })
                    setHcpDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }
    const getpatientDropDownList = () => {
        var dataToSend = {
            action: 'getallpatients',
            userid: currentUser.id,
            subuserids: 'patientids',
            appsecret: config.appsecret,
        }
        // if (currentUser.usertype === 'admin') {
        //     dataToSend.adminid = currentUser.id;
        // }
        // if (currentUser.usertype === 'patient') {
        //     dataToSend.patientid = currentUser.id;
        // }
        // if (currentUser.usertype === 'doctor') {
        //     dataToSend.doctorid = currentUser.id;
        // }
        // if (currentUser.usertype === 'nurse') {
        //     dataToSend.nurseid = currentUser.id;
        // }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [{id:'', name:'--'+strings.SELECT_ALL+'--'}];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            id: val.uid, 
                            name: val.firstname+' '+val.lastname,
                        }
                        console.log("result.data123123patients:", ob)
                        arr.push(ob)
                    })
                    setPatientDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }
    const getnurseDropDownList = () => {
        var dataToSend = {
            action: 'getallnurses',
            userid:currentUser.id,
            subuserids: 'nurseids',
            appsecret: config.appsecret,
        }
        // if (currentUser.usertype === 'admin') {
        //     dataToSend.adminid = currentUser.id;
        // }
        // if (currentUser.usertype === 'patient') {
        //     dataToSend.patientid = currentUser.id;
        // }
        // if (currentUser.usertype === 'doctor') {
        //     dataToSend.doctorid = currentUser.id;
        // }
        // if (currentUser.usertype === 'nurse') {
        //     dataToSend.nurseid = currentUser.id;
        // }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [{id:'', name:'--'+strings.SELECT_ALL+'--'}];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            id: val.uid, 
                            name: val.firstname+' '+val.lastname,
                            doctorids:val.doctorids
                        }
                        console.log("result.data123123:nurse::", ob)
                        arr.push(ob)
                    })
                    setNurseDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }
    return (
        <SafeAreaView style={{ flex: 1, marginTop:100, justifyContent: 'center',
        alignItems: 'center', height:600, }}>
                <AwesomeAlert
                  contentContainerStyle={{
                      backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                      shadowOffset: {
                          width: 0,
                          height: 12,
                      },
                      shadowOpacity: 0.58,
                      shadowRadius: 16.00,
    
                      elevation: 24
                  }}
                  show={awesomeAlert}
                  showProgress={false}
                  title={editRight ? strings.DELETE_RECORD : strings.UNLINK_RECORD}
                  titleStyle={{ color: 'white' }}
                  message={editRight ? strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE : strings.ARE_YOU_SURE_YOU_WANT_TO_UNLINK}
                  messageStyle={{ color: 'white' }}
                  closeOnTouchOutside={true}
                  closeOnHardwareBackPress={false}
                  showCancelButton={true}
                  showConfirmButton={true}
                  cancelText={strings.NO_CANCEL}
                  confirmText={strings.YES_DELETE}
                  cancelButtonColor="#516b93"
                  confirmButtonColor="#69c2d1"
                  confirmButtonBorderRadius="0"
                  confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                  cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                  cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                  confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                    onCancelPressed={() => {
                        setAwesomeAlert(false);
                    }}
                    onConfirmPressed={() => {
                        DeleteUser(currentUser.id, guestId, popupIndex);
                    }}
                />
                <View style={{flexDirection:'row', zIndex:800, position:'absolute', top:0}}>
                    {currentUser.usertype !== 'patient' && <>
                    <View style={{flexDirection:'row',}}>
                        <SearchableDropdown
                            multi={false}
                            onTextChange={text => console.log(text)}
                            // onItemSelect={item => console.log(JSON.stringify(item.name))}
                            selectedItems={state1}
                            onItemSelect={(item) => {
                                searchKeyword('',subuserids, item.doctorids, '');
                                setTimeout(async () => {
                                    const items = [];
                                    items.push(item)
                                    await setDids1(item.doctorids)
                                    await setState(items);
                                    await setIsListEnd(false)
                                    // await getHCPDataSearch(subuserids, 'arr1,'+item.hcpids)
                                }, 1000)
                            }}
                            containerStyle={{ padding: 5 }}
                            itemStyle={styles.itemStyle}
                            textInputStyle={styles.textInputStyle}
                            itemTextStyle={{ color: '#fff' }}
                            itemsContainerStyle={{ maxHeight: 300 }}
                            items={patientDropDownList}
                            // items={items}
                            defaultIndex={2}
                            placeholder={strings.FILTER_PATIENTS}
                            placeholderTextColor="#fff"
                            underlineColorAndroid="transparent"
                            resetValue={false}
                        >
                        </SearchableDropdown>
                        <Image style={{ flexDirection: 'row', left: -30, top: 20 }} height={8} width={14} source={downArrow} />
                    </View>
                    </>
                    }
                    <View style={{flexDirection:'row', }}>
                        <SearchableDropdown
                            multi={false}
                            onTextChange={text => console.log(text)}
                            selectedItems={state1}
                            onItemSelect={(item) => {
                                searchKeyword('',subuserids, item.doctorids, '');
                                setTimeout(async () => {
                                    const items = [];
                                    items.push(item)
                                    await setDids1(item.doctorids)
                                    await setState(items);
                                    await setIsListEnd(false)
                                    // await getHCPDataSearch(subuserids, 'arr1,'+item.hcpids)
                                }, 1000)
                            }}
                            containerStyle={{ padding: 5 }}
                            itemStyle={styles.itemStyle}
                            textInputStyle={styles.textInputStyle}
                            itemTextStyle={{ color: '#fff' }}
                            itemsContainerStyle={{ maxHeight: 340 }}
                            items={nurseDropDownList}
                            // items={items}
                            defaultIndex={2}
                            placeholder={strings.FILTER_NURSES}
                            placeholderTextColor="#fff"
                            underlineColorAndroid="transparent"
                            resetValue={false}
                        >
                        </SearchableDropdown>
                        <Image style={{ flexDirection: 'row', left: -30, top: 20 }} height={8} width={14} source={downArrow} />
                    </View>
                    {currentUser.usertype !== 'healthcareprovider' && <>
                    <View style={{flexDirection:'row', }}>
                        <SearchableDropdown
                            multi={false}
                            onTextChange={text => console.log(text)}
                            // onItemSelect={item => console.log(JSON.stringify(item.name))}
                            selectedItems={state1}
                            onItemSelect={(item) => {
                                searchKeyword('',subuserids, '', item.doctorids);
                                setTimeout(async () => {
                                    const items = [];
                                    items.push(item)
                                    await setDids1(item.doctorids)
                                    await setState(items);
                                    await setIsListEnd(false)
                                    // await getHCPDataSearch(subuserids, 'arr1,'+item.hcpids)
                                }, 1000)
                            }}
                            containerStyle={{ padding: 5 }}
                            itemStyle={styles.itemStyle}
                            textInputStyle={styles.textInputStyle}
                            itemTextStyle={{ color: '#fff' }}
                            itemsContainerStyle={{ maxHeight: 340 }}
                            items={hcpDropDownList}
                            // items={items}
                            defaultIndex={2}
                            placeholder={strings.FILTER_HCPS}
                            placeholderTextColor="#fff"
                            underlineColorAndroid="transparent"
                            resetValue={false}
                        >
                        </SearchableDropdown>
                        <Image style={{ flexDirection: 'row', left: -30, top: 20 }} height={8} width={14} source={downArrow} />
                    </View>
                    </>
                    }
                </View>
                {/* <Loader loading={loading} /> */}
                {/* <SearchableDropdown
                    onTextChange={text => console.log(text)}
                    onItemSelect={item => console.log(JSON.stringify(item.name))}
                    containerStyle={{ padding: 5 }}
                    itemStyle={{
                        padding: 10,
                        marginTop: 2,
                        backgroundColor: '#ddd',
                        borderColor: '#474F69',
                        borderWidth: 1,
                        borderRadius: 5,
                    }}
                    textInputStyle={{
                        padding: 12,
                        borderWidth: 1,
                        borderColor: '#ccc',
                        backgroundColor: '#FAF7F6',
                        width:300,
                    }}
                    itemStyle={{
                        padding: 10,
                        marginTop: 2,
                        backgroundColor: '#FAF9F8',
                        borderColor: '#474F69',
                        borderWidth: 1,
                    }}
                    itemTextStyle={{ color: '#fff' }}
                    itemsContainerStyle={{ maxHeight: 340 }}
                    items={serverData}
                    defaultIndex={2}
                    placeholder="Select User"
                    underlineColorAndroid="transparent"
                /> */}

                <View style={{ right: 170, top:50, flexDirection: "row", flexWrap: "wrap" }}>
                    <View style={styles.SectionStyle}>
                        <Icon style={styles.searchIcon} name="search" size={22} color="#81889f" />
                        <TextInput
                            style={styles.inputStyle}
                            placeholder={strings.TYPE_TO_SEARCH_HERE}
                            placeholderTextColor="#8b9cb5"
                            autoCapitalize="none"
                            underlineColorAndroid="#f000"
                            onChangeText={(text) => { searchKeyword(text, '', '', '') }}
                            blurOnSubmit={false}
                            value={searchData}
                        />
                    </View>
                    <View style={{ paddingTop: 45, left: 100, flexDirection: "row", flexWrap: "wrap" }}>
                        <Text style={[styles.headerText,{}]}>{strings.PHONE}</Text>
                        <Text style={[styles.headerText,{left:50}]}>{strings.ADDRESS}</Text>
                        <Text style={[styles.headerText, {left:210}]}>{editRight ? strings.EDIT : 'Info' }</Text>
                        <Text style={[styles.headerText, {left:220}]}>{editRight ? strings.DELETE : 'Unlink'}</Text>
                    </View>
                </View>
                <FlatList
                    style={{width:'80%', top:50,}}
                    showsVerticalScrollIndicator={false}
                    data={dataSource}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={ItemView}
                    ListEmptyComponent={NoDataFound}
                    ListFooterComponent={renderFooter}
                    onEndReached={getDoctorsData}
                    onEndReachedThreshold={0.5}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />}
                />
                <FAB
                    style={styles.floatBtn}
                    fabStyle={{ height: 100 }}
                    color={'white'}
                    theme={{ colors: { accent: '#eb8682' } }}
                    icon="plus"
                    onPress={showModal1}
                >
                </FAB>
                <Snackbar
                    style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                    visible={visibleSnackbar}
                    duration={3000}
                    onDismiss={onDismissSnackBar}
                >
                    {snackMsg}
                </Snackbar>
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 50,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    headerText: {
        color: 'white',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    activity: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: '100%',
        height: 50,
        borderRadius: 15,
        marginBottom: 17,
    },
    activityText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 220,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    activityColor: {
        borderColor: 'black',
        // borderWidth: 1,
        borderRadius: 30,
        width: 45,
        height: 45,
        left: 15,
        marginTop: 2,
    },
    activityColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        // position: 'absolute',
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8

    },
    scrollView: {
        height: '80%',
        width: '100%',
        // margin: 20,
        alignSelf: 'center',
        // padding: 20,
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 250,
    },
    itemStyle:{
        padding: 10,
        marginTop: 2,
        backgroundColor: '#474F69',
        borderColor: '#474F69',
        borderWidth: 1,
        borderRadius: 5,
        color:'#fff'
    },
    textInputStyle:{
        padding: 8,
        borderWidth: 1,
        borderColor: 'transparent',
        backgroundColor: '#474F69',
        borderRadius:5,
        color:'#fff',
        width:200
    }
});

export default AllDoctorScreen;
