import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    Text,
    Alert,
    StyleSheet,
    RefreshControl,
    Button,
    FlatList,
    ActivityIndicator,
    I18nManager
} from 'react-native';
import RNRestart from 'react-native-restart';
import DropDownPicker from 'react-native-dropdown-picker';
import ImagePicker from 'react-native-image-crop-picker';
import AsyncStorage from '@react-native-community/async-storage';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { storeUserLogin } from '../redux/action'
import { ColorPicker, fromHsv } from 'react-native-color-picker'
import upload from '../../../Image/upload-medicine.png'
import Language from '../../../Image/language.png'
import moment from "moment";
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';

import { Modal, Portal, Provider, FAB, Snackbar } from 'react-native-paper';

import deletebtn from '../../../Image/delete-btn.png'

import editbtn from '../../../Image/edit-btn.png'

import notificationbtn from '../../../Image/notification-btn.png'

import Loader from '../Components/Loader';

import axios from 'axios'

import config from '../appconfig/config';
import { ScrollView } from 'react-native-gesture-handler';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';


const Onboard = ({ navigation }) => {
    const dispatch = useDispatch();
    const currentUser = useSelector(state => state.auth.userLogin)
    const [refreshing, setRefreshing] = React.useState(false);
    const [loadingM, setLoadingM] = useState(false);
    const [loadingA, setLoadingA] = useState(false);
    const [loadingE, setLoadingE] = useState(false);
    const [dataSourceM, setDataSourceM] = useState([]);
    const [dataSourceA, setDataSourceA] = useState([]);
    const [dataSourceE, setDataSourceE] = useState([]);
    const [offsetM, setOffsetM] = useState(0);
    const [offsetA, setOffsetA] = useState(0);
    const [offsetE, setOffsetE] = useState(0);
    const [isListEndA, setIsListEndA] = useState(false);
    const [isListEndM, setIsListEndM] = useState(false);
    const [isListEndE, setIsListEndE] = useState(false);
    const [title, setTitle] = useState('Add Medicine');
    const [visibleM, setVisibleM] = React.useState(false);
    const [visibleA, setVisibleA] = React.useState(false);
    const [visibleE, setVisibleE] = React.useState(false);
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [type, setType] = useState('');
    const [display1, setDisplay1] = React.useState(true);
    const [display2, setDisplay2] = React.useState(false);
    const [display3, setDisplay3] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [isLangModalVisible, setVisibleLanguage] = useState(false)
    const [allValuesM, SetallValuesM] = useState({
        id: '',
        name: null,
        color: '',
        description: '',
        enablenotification: false,
        dose: null,
        timesperday: null,
        company: '',
        areaofuse: '',
        notes: '',
        activeingredients: '',
        recurring: false,
        enableStandardWheel: false,
        selecttime: '',
        image: '',
        newimage: '',
        filename: '',
        filetype:'',
        sourceURL:'',
        filesize:'',
        allimageData: '',
    });
    const [allValuesA, SetallValuesA] = useState({
        id: '',
        name: '',
        color: '',
        description: '',
        notification: false,
        recurring: false,
        addtowheel: false,
        selecttime: ''
    });
    const [allValuesE, SetallValuesE] = useState({
        id: '',
        name: '',
        color: '',
        description: '',
        notification: false,
        recurring: false,
        addtowheel: false,
        selecttime: ''
    });
    const showAlert = (id, index) => {
        setAwesomeAlert(true);
        setPopupId(id)
        setPopupIndex(index)
    };
    const [selectedValue, setSelectedValue] = useState('');
    useEffect(() => {
        selectedLng()
    }, [])

    const selectedLng = async () => {
        const lngData = await getLng()

        if (!!lngData || lngData != null) {
            console.log("lanf", lngData)
            strings.setLanguage(lngData)
            setSelectedValue(lngData)
        }
        console.log("selected Language data==>>>", lngData)
    }

    const onChangeLng = async (lng) => {
        setVisibleLanguage(false)
        console.log(lng)
        await I18nManager.forceRTL(false)
        setLng(lng)
        RNRestart.Restart()
        return;
    }


    const [dropValues] = useState([
        { value: 'en', label: 'English' },
        { value: 'no', label: 'Norwegian' },
        { value: 'da', label: 'Danish' },
        { value: 'sv', label: 'Swedish' },
        { value: 'de', label: 'German' },
        { value: 'es', label: 'Spanish' },
        { value: 'fr', label: 'French' },
        { value: 'hi', label: 'Hindi' }
    ]);

    const hideAlert = () => {
        setAwesomeAlert(false);
    };

    const handleChooseLibrary = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            // multiple: true,
        }).then(image => {
            console.log("cropped image::",image);
            SetallValuesM({ ...allValuesM, filename: image.filename, filetype: image.mime, newimage: image.path, image: '', sourceURL:image.sourceURL, filesize:image.size, allimageData:image });
        });
    }

    const handleChooseCamera = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };
        ImagePicker.openCamera({
            width: 300,
            height: 300,
            cropping: true,
        }).then(image => {
            console.log("cropped camera image::",image);
            SetallValuesM({ ...allValuesM, filename: image.filename, filetype: image.mime, newimage: image.path, image:'', sourceURL:image.sourceURL, filesize:image.size, allimageData:image })
        });
    }

    const chooseOption = () => {
        Alert.alert(
            "",
            strings.UPLOAD_IMAGE,
            [
                { text: strings.CHOOSE_FROM_LIBRARY, onPress: () => handleChooseLibrary() },
                { text: strings.TAKE_PICTURE, onPress: () => handleChooseCamera() },
                { text: strings.CANCEL, onPress: () => console.log("OK Pressed"), style: "cancel" }
            ]
        );
    }
    const goToHome = () => {
        var data1 = {
            action: "setloginfirsttime",
            userid: currentUser.id,
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "login.php", data1).then(async (result) => {
            console.log("result.data", result.data)
            if (result.data.status == 'success') {

                await AsyncStorage.getItem('user').then(async (value) => {
                    var newValue = JSON.parse(value);
                    newValue.firsttimelogin = 0;
                    console.log("Datalogin", newValue.firsttimelogin)
                    await AsyncStorage.setItem('user', JSON.stringify(newValue));
                    dispatch(storeUserLogin(value))
                    navigation.replace('DrawerNavigationRoutes');
                })
            }
        })
    }

    const onDismissSnackBar = () => setVisibleSnack(false);

    const showTab1 = () => {
        setDisplay1(true);
        setDisplay2(false);
        setDisplay3(false);
    };

    const showTab2 = () => {
        setDisplay1(false);
        setDisplay2(true);
        setDisplay3(false);
    };

    const showTab3 = () => {
        setDisplay1(false);
        setDisplay2(false);
        setDisplay3(true);
    };

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }

    const showModalM = () => {
        setTitle('Add Medicine')
        setType('Add')
        setVisibleM(true)
    };

    const hideModalM = () => {
        for (key in allValuesM) {
            if (allValuesM.hasOwnProperty(key)) {
                allValuesM[key] = null;
            }
        }
        setVisibleM(false)
    };

    const showModalA = () => {
        setType('Add')
        setTitle('Add Activity')
        setVisibleA(true)
    };

    const hideModalA = () => {
        for (key in allValuesM) {
            if (allValuesA.hasOwnProperty(key)) {
                allValuesA[key] = null;
            }
        }
        setVisibleA(false)
    };

    const showModalE = () => {
        setVisibleE(true)
        setTitle('Add WellBeing')
        setType('Add')
    };

    const hideModalE = () => {
        for (key in allValuesE) {
            if (allValuesE.hasOwnProperty(key)) {
                allValuesE[key] = null;
            }
        }
        setVisibleE(false)
    };



    const [visibleColor, setVisibleColor] = React.useState(false);

    const showModalColor = () => setVisibleColor(true);
    const hideModalColor = () => setVisibleColor(false);

    const [index, setIndex] = useState();

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

    const containerStyle = { backgroundColor: '#2b3249', marginBottom: 25, padding: 10, alignItems: 'center', height: '90%', width: '90%', alignSelf: 'center', borderRadius: 8, zIndex: 9700 };
    const containerStyleL = { backgroundColor: '#2b3249', marginBottom: 25, padding: 25, alignItems: 'center', height: 200, width: '25%', alignSelf: 'center', borderRadius: 8, zIndex: 9700 };

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };


    const onEditMedicine = (data, index) => {
        setIndex(index);
        console.log("data", data)
        allValuesM.id = data.id;
        allValuesM.name = data.name;
        allValuesM.color = data.color;
        allValuesM.activeingredients = data.activeingredients;
        allValuesM.recurring = data.selecttime ? true : false;
        allValuesM.addtowheel = data.addtowheel == "1" ? true : false;
        allValuesM.notification = data.notification == "1" ? true : false;
        allValuesM.selecttime = data.selecttime;
        allValuesM.dose = data.dose;
        allValuesM.timesperday = data.timesperday;
        allValuesM.company = data.company;
        allValuesM.areaofuse = data.areaofuse;
        allValuesM.notes = data.notes;
        setTitle(strings.EDIT_MEDICINE);
        setType('edit');
        setVisibleM(true);
    }

    const handleConfirm = (date) => {
        if (display1)
            SetallValuesE({ ...allValuesE, ['selecttime']: moment(date).format('HH:mm') })
        if (display2)
            SetallValuesM({ ...allValuesM, ['selecttime']: moment(date).format('HH:mm') })
        if (display3)
            SetallValuesA({ ...allValuesA, ['selecttime']: moment(date).format('HH:mm') })
        hideDatePicker();
    };

    const onEditWellBeing = (data) => {
        allValuesE.id = data.id;
        allValuesE.name = data.name;
        allValuesE.color = data.color;
        allValuesE.description = data.description;
        allValuesE.recurring = data.selecttime !== "" ? true : false;
        allValuesE.addtowheel = data.addtowheel == "1" ? true : false;
        allValuesE.notification = data.notification == "1" ? true : false;
        allValuesE.selecttime = data.selecttime;
        setTitle('Edit WellBeing');
        setVisibleE(true);
        setType('edit')
    }

    const onEditActivity = (data) => {
        console.log("data", data)
        allValuesA.id = data.id;
        allValuesA.name = data.name;
        allValuesA.color = data.color;
        allValuesA.description = data.description;
        allValuesA.recurring = data.selecttime !== '' ? true : false;
        allValuesA.addtowheel = data.addtowheel == "1" ? true : false;;
        allValuesA.notification = data.notification = "1" ? true : false;
        allValuesA.selecttime = data.selecttime;
        setTitle(strings.EDIT_ACTIVITY)
        setVisibleA(true);
        setType('edit');
    }
    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        wait(2000).then(() => {
            setRefreshing(false);
            setOffsetA(0);
            setOffsetM(0);
            setOffsetE(0);
            setVisibleA(false)
            setVisibleM(false)
            setVisibleE(false)
            setIsListEndA(false)
            setIsListEndM(false)
            setIsListEndE(false)
            setDataSourceA([]);
            setDataSourceM([]);
            setDataSourceE([]);
            getMedicineData();
            getActivityData();
            getWellBeingData();
        });
    }, []);

    const getMedicineData = () => {
        if (!loadingM && !isListEndM) {
            console.log('getData');
            setLoadingM(true);
            var dataToSend = {
                userid: currentUser.id,
                appsecret: config.appsecret,
                action: 'getmedicinemasterdata',
                offset: offsetM
            };
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                if (result.data.data0 !== null) {
                    console.log("res", result.data.data0)
                    setLoadingM(false);
                    setDataSourceM([...dataSourceM, ...result.data.data0]);
                    setOffsetM(offsetM + 10)
                }
                else {
                    setIsListEndM(true);
                    setLoadingM(false);
                }
            })
        }
    };

    const getWellBeingData = () => {
        if (!loadingE && !isListEndE) {
            setLoadingE(true);
            var dataToSend = {
                userid: currentUser.id,
                appsecret: config.appsecret,
                action: 'geteventmasterdata',
                offset: offsetE
            };
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                if (result.data.data0 !== null) {
                    setLoadingE(false);
                    setDataSourceE([...dataSourceE, ...result.data.data0]);
                    setOffsetE(offsetE + 10)
                }
                else {
                    setIsListEndE(true);
                    setLoadingE(false);
                }
            })
        }
    };

    const getActivityData = () => {
        if (!loadingA && !isListEndA) {
            setLoadingA(true);
            var dataToSend = {
                userid: currentUser.id,
                appsecret: config.appsecret,
                action: 'getactivitymasterdata',
                offset: offsetA
            };

            console.log("offset")
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                setLoadingA(false);
                if (result.data.data0 !== null) {
                    setDataSourceA([...dataSourceA, ...result.data.data0]);
                    setOffsetA(offsetA + 10)
                }
                else {
                    setIsListEndA(true);
                    setLoadingA(false);
                }
            })
        }
    };

    const AddUpdateActivity = () => {
        setLoadingA(true)
        var data = allValuesA;
        data.userid = currentUser.id;
        data.appsecret = config.appsecret;
        data.preenddate = moment(data.preenddate).format('YYYY-MM-DD')
        data.prestartdate = moment(data.prestartdate).format('YYYY-MM-DD')
        data.hcpid = 0;
        data.action = type == "edit" ? 'updateactivity' : 'addactivity';
        var filename = type == "edit" ? 'updatedata.php' : 'adddata.php';
        console.log("daa", data)
        console.log("daa", filename)
        axios.post(config.apiUrl + filename, data).then(result => {
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                console.log("result", result.data)
                setLoadingA(false);
                setVisibleA(false)
                hideModalA();
                setsnackMsg(type == "Add" ? strings.ACTIVITY_CREATED_SUCC : strings.ACTIVITY_UPDATED_SUCC)
                setVisibleSnack(true)
                onRefresh();
            }
            else {
                setVisibleA(false)
                setLoadingA(false);
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }
        })
    }

    const AddUpdateMedicine = () => {
        if (allValuesM.name === '' || allValuesM.name === null) {
            alert(strings.PLEASE_ENTER_NAME_OF_MEDICINE)
            return
        }
        if (allValuesM.dose === '' || allValuesM.dose === null) {
            alert(strings.PLEASE_ENTER_DOSE)
            return
        }
        if (allValuesM.timesperday === '' || allValuesM.timesperday === null) {
            alert(strings.PLEASE_ENTER_TIMES_PER_DAY)
            return
        }
        setLoadingM(true)
        // var data = allValuesM;
        // data.userid = currentUser.id;
        // data.appsecret = config.appsecret;
        // data.doses = data.dose;
        // data.action = type == "edit" ? 'updatemedicine' : 'addmedicine';
        // var filename = type == "edit" ? 'updatedata.php' : 'adddata.php';
        var filename = 'updatedata.php';
        // console.log("old data:", data); return false;
        var data = new FormData();
        data.append('id', allValuesM.id);
        data.append('name', allValuesM.name);
        data.append('color', allValuesM.color);
        data.append('selecttime', allValuesM.selecttime);
        data.append('company', allValuesM.company);
        data.append('activeingredients', allValuesM.activeingredients);
        data.append('areaofuse', allValuesM.areaofuse);
        data.append('description', allValuesM.description);
        data.append('enablenotification', allValuesM.enablenotification);
        data.append('enableStandardWheel', allValuesM.enableStandardWheel);
        data.append('notes', allValuesM.notes);
        data.append('dose', allValuesM.dose);
        data.append('timesperday', allValuesM.timesperday);
        data.append('userid',currentUser && currentUser.id ? currentUser.id : '');
        data.append('appsecret',config.appsecret);
        data.append('action',(type == "edit") ? 'updatemedicine' : 'addmedicine');
        data.append('base_url', config.apiUrl);
        // alert(allValuesM.image); return false;
        if(allValuesM.newimage != ''){
            data.append('image',
            {
                uri:allValuesM.newimage,
                name:allValuesM.newimage,
            });
        }else if(allValuesM.image != ''){
            data.append('image',allValuesM.image);
        }else{
            data.append('image','');
        }
        console.log("data", data)
        axios.post(config.apiUrl + filename, data, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(result => {
            setLoadingM(false)
            // console.log("result.data", result.data)
            if (result.data.status == 'success') {
                setLoadingM(false);
                hideModalM();
                setsnackMsg(type == "Add" ? strings.MEDICINE_CREATE_SUCC : strings.MEDICINE_UPDATED_SUCC)
                setVisibleSnack(true)
                setVisibleM(false)
                onRefresh();
            }
            else {

                setVisibleM(false)
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }

        })
    }

    const AddUpdateWellBeing = () => {
        setLoadingE(true)
        var data = allValuesE;
        data.userid = currentUser.id;
        data.appsecret = config.appsecret;
        data.action = type == "edit" ? 'updateevent' : 'addevent';
        var filename = type == "edit" ? 'updatedata.php' : 'adddata.php';
        axios.post(config.apiUrl + filename, data).then(result => {
            if (result.data.status == 'success') {
                setLoadingE(false);
                setVisibleE(false);
                hideModalE();
                onRefresh();
                setsnackMsg(type == "Add" ? strings.WELLBEING_CREATED_SUCC : strings.WELLBEING_UPDATED_SUCC)
                setVisibleSnack(true)
            }
            else {
                setVisibleE(false)
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
                hideModalE();
            }
        })
    }
    var count = 0
    const handleNext = () => {
        count = count + 1;
        console.log("count", count)
        if (display1) {
            showTab2();
        }
        if (display2) {
            showTab3();
        }
        if (display3) {
            goToHome();
        }
    }

    useEffect(() => getWellBeingData(), getMedicineData(), getActivityData(), []);

    const DeleteAlert = (id, index) => {
        showAlert(id, index)
        // Alert.alert(
        //     'Delete',
        //     'Are your sure you want to delete?',
        //     [
        //         {
        //             text: 'No',
        //             onPress: () => { },
        //             style: 'cancel',
        //         },
        //         {
        //             text: 'Yes',
        //             onPress: () => {
        //                 DeleteMedicineActivityEvent(id, index);
        //             }
        //         },
        //     ],
        //     { cancelable: false },
        // );
    }

    const DeleteMedicineActivityEvent = (id, index) => {
        setLoadingM(true)
        var data = {};
        data.userid = currentUser.id;
        data.appsecret = config.appsecret;
        data.id = id;
        data.action = display1 ? 'deleteevent' : display2 ? 'deletemedicine' : 'deleteActivity'
        axios.post(config.apiUrl + 'deletedata.php', data).then(result => {
            setLoadingM(false)
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                if (display1)
                    dataSourceE.splice(index, 1);
                if (display2)
                    dataSourceM.splice(index, 1);
                if (display3)
                    dataSourceA.splice(index, 1);
                setsnackMsg(strings.RECORD_DELETED_SUCC)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
            else {
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
        })
    }

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loadingA ? (
                    <ActivityIndicator
                        color="black"
                        style={{ margin: 15, flexDirection:'row', position:'absolute', top:30, left:0, width:'100%', alignSelf:'flex-end' }} />
                ) : loadingM ? (
                    <ActivityIndicator
                        color="black"
                        style={{ margin: 15, flexDirection:'row', position:'absolute', top:30, left:-200, width:'100%', alignSelf:'flex-end' }} />
                ) : loadingE ? (
                    <ActivityIndicator
                        color="black"
                        style={{ margin: 15, flexDirection:'row', position:'absolute', top:30, left:-200, width:'100%', alignSelf:'flex-end' }} />
                ) : null}
            </View>
        );
    };


    const handleModal = () => {
        if (display1) {
            showModalE();
        }
        if (display2) {
            showModalM();
        }
        if (display3) {
            showModalA();
        }
    }

    const handleEditModal = (data, index) => {
        if (display1) {
            onEditWellBeing(data);
        }
        if (display2) {
            onEditMedicine(data, index);
        }
        if (display3) {
            onEditActivity(data);
        }
    }

    const setColor = (color) => {
        if (display2) {
            console.log("color2", color)
            SetallValuesM({ ...allValuesM, ['color']: color });
        }
        if (display1) {
            console.log("color1", color)
            SetallValuesE({ ...allValuesE, ['color']: color });
        }
        if (display3) {
            console.log("color3", color)
            SetallValuesA({ ...allValuesA, ['color']: color });
        }
    }

    console.log("abcactiivty", allValuesA)

    const ItemView = ({ item, index }) => {
        return (
            // Flat List Item
            <View style={styles.medicine}>
                <View style={[styles.medicineColor, { backgroundColor: item.color }]}>
                </View>
                <Text style={styles.medicineText}>{item.name}</Text>
                <View style={{ paddingTop: 10, flexDirection: "row", flexWrap: "wrap" }}>
                    <TouchableOpacity onPress={() => DeleteAlert(item, index)}>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => handleEditModal(item, index)}>
                        <Image style={styles.icons} height={35} width={35} source={editbtn} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    const openLanguage = () => {
        console.log("rrey")
        setVisibleLanguage(true);
    }

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <AwesomeAlert
                contentContainerStyle={{
                    backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 12,
                    },
                    shadowOpacity: 0.58,
                    shadowRadius: 16.00,

                    elevation: 24
                }}
                show={awesomeAlert}
                showProgress={false}
                title={strings.DELETE_RECORD}
                titleStyle={{ color: 'white' }}
                message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                messageStyle={{ color: 'white' }}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={strings.NO_CANCEL}
                confirmText={strings.YES_DELETE}
                cancelButtonColor="#516b93"
                confirmButtonColor="#69c2d1"
                confirmButtonBorderRadius="0"
                confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                onCancelPressed={() => {
                    setAwesomeAlert(false);
                }}
                onConfirmPressed={() => {
                    DeleteMedicineActivityEvent(popupId, popupIndex);
                }}
            />
            <View style={styles.mainBody}>
                <TouchableOpacity onPress={() => openLanguage()} style={{
                    position: 'absolute',
                    right: 15,
                    top: 15,
                }} >
                    <Image
                        source={Language}
                        style={{ width: 40, height: 40 }}
                    />
                </TouchableOpacity>
                {/* <Loader loading={loadingA || loadingM || loadingE} /> */}
                <View style={{ flexDirection: "row", flexWrap: "wrap", backgroundColor: '#474f69', borderRadius: 50 }}>
                    <View style={{ flexDirection: "row", flexWrap: "wrap", }}>
                        <TouchableOpacity onPress={showTab1}>
                            <View style={display1 ? styles.onBoardTabActive : styles.onBoardTab}>
                                <Text style={styles.tabText}>{strings.ADD_WELLBEING_STATUS}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={showTab2}>
                            <View style={display2 ? styles.onBoardTabActive : styles.onBoardTab}>
                                <Text style={styles.tabText}>{strings.ADD_MEDICINE}</Text>
                            </View>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={showTab3}>
                            <View style={display3 ? styles.onBoardTabActive : styles.onBoardTab}>
                                <Text style={styles.tabText} >{strings.ADD_ACTIVITY}</Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>
                {display1 &&
                    <View style={{ marginTop: 20 }}>
                        <View style={{ height: 400, marginBottom: 30 }}>
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                data={dataSourceE}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={ItemView}
                                ListFooterComponent={renderFooter}
                                onEndReached={getWellBeingData}
                                onEndReachedThreshold={0.5}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={refreshing}
                                        onRefresh={onRefresh}
                                    />}
                            />
                        </View>
                        <View >
                            <Text style={{ color: '#aeaebd', fontSize: 16, fontWeight: '600', textAlign: 'center' }}>{strings.HAVE_YOU_HELPED_GETTING}{'\n'}
                                {strings.FEEL_FREE_TO_ADD_MORE}</Text>
                        </View>
                    </View>
                }
                {display2 &&
                    <View style={{ marginTop: 20 }}>
                        <View style={{ height: 381, left: 190, marginBottom: 30 }}>
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                data={dataSourceM}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={ItemView}
                                ListFooterComponent={renderFooter}
                                onEndReached={getMedicineData}
                                onEndReachedThreshold={0.5}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={refreshing}
                                        onRefresh={onRefresh}
                                    />}
                            />
                        </View>
                        <View>
                            <Text style={{ color: '#aeaebd', fontSize: 16, fontWeight: '600', textAlign: 'center' }}>{strings.THE_MEDICINES_YOU_USE_REGULARELY}{'\n'}
                                {strings.YOU_CAN_USE_BUIL_IN_NOTIFICATION}{'\n'}
                                {strings.FEEL_FREE_TO_ADD_MORE}</Text>
                        </View>
                    </View>
                }
                {display3 &&
                    <View style={{ marginTop: 20 }}>
                        <View style={{ height: 400, left: 45, marginBottom: 30 }}>
                            <FlatList
                                showsVerticalScrollIndicator={false}
                                data={dataSourceA}
                                keyExtractor={(item, index) => index.toString()}
                                renderItem={ItemView}
                                ListFooterComponent={renderFooter}
                                onEndReached={getActivityData}
                                onEndReachedThreshold={0.5}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={refreshing}
                                        onRefresh={onRefresh}
                                    />}
                            />
                        </View>
                        <View >
                            <Text style={{ color: '#aeaebd', fontSize: 16, fontWeight: '600', textAlign: 'center' }}>{strings.ENTER_PLANNED_AND_UNPLANNED_ACTIVITIES}{'\n'}
                                {strings.FEEL_FREE_TO_ADD_MORE_PRESCRIBED}
                            </Text>
                        </View>
                    </View>
                }
                <View style={styles.Next}>
                    <FAB
                        style={styles.floatBtn}
                        color={'white'}
                        theme={{ colors: { accent: '#eb8682' } }}
                        icon="plus"
                        onPress={handleModal}
                    >
                    </FAB>
                    <FAB
                        style={styles.nextBtn}
                        fabStyle={{ height: 100 }}
                        color={'white'}
                        label={'NEXT'}
                        theme={{ colors: { accent: '#69c2d1' } }}
                        onPress={handleNext}
                    >
                    </FAB>
                    <View style={{ position: 'absolute', bottom: -120, left: 230, flexDirection: "row" }}>
                        <Text style={{ color: '#81889f', fontSize: 18, }}>{strings.OR_FILL_IT_LATER} </Text>
                        <TouchableOpacity onPress={goToHome}>
                            <Text style={{ color: '#69c2d1', fontSize: 18 }}>{strings.SKIP}</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                {/* Add update medicines start */}
                <Provider>
                    <Portal>
                        <Modal visible={visibleM} onDismiss={hideModalM} contentContainerStyle={containerStyle}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                                <Text style={{ color: 'white', fontWeight: '700', fontSize:22 }}>{title}</Text>
                                <View style={{ left: 350 }}>
                                    <TouchableOpacity onPress={hideModalM}>
                                        <Icon name="close" color="#81889f" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <KeyboardAwareScrollView enableOnAndroid={true} style={{ width: '100%' }}>
                                <View style={{ alignItems: 'flex-start', justifyContent: 'center', alignSelf: 'center', flexWrap: 'wrap', flexDirection: 'row', marginTop: 30, }}>
                                    <View style={{ padding: 0, width: '45%' }}>
                                        <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', fontSize: 14, top: 40, width: 200, }}>{strings.ADD_PICTURE_OF_MEDICINE_PACKAGE}</Text>
                                        {!allValuesM.image && !allValuesM.newimage ?
                                            <TouchableOpacity onPress={chooseOption} style={[styles.imageBackStyle, { alignSelf: 'center' }]} >
                                                <Image
                                                    source={upload}
                                                    style={{ width: 78, height: 78, top: 30 }}
                                                />
                                            </TouchableOpacity> :
                                            <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center', top: -20 }]}>
                                                <Image
                                                    source={{ uri: allValuesM.image != '' ? config.apiUrl+allValuesM.image : allValuesM.newimage }}
                                                    style={{ width: 256, height: 146, borderRadius: 10 }}
                                                />
                                                <TouchableOpacity onPress={() => SetallValues({ ...allValues, image: '' })} ><Icon name="close" color="#81889f" size={22} /></TouchableOpacity>
                                            </TouchableOpacity>}

                                        <View style={{ marginTop: 20, }}>
                                            <Text style={{ flexDirection: 'row' }}>
                                                <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.MEDICINE_NAME}</Text>
                                                <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text>
                                            </Text>
                                            <TextInput
                                                style={{ color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16, paddingVertical: 5, }}
                                                placeholder={strings.TYPE_NAME_OF_MEDICINE_HER}
                                                placeholderTextColor={'#81889f'}
                                                value={allValuesM.name}
                                                onChangeText={val => SetallValuesM({ ...allValuesM, ['name']: val })}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                            <Text style={{ color: '#e0675c' }}>{allValuesM.name === '' ? strings.PLEASE_ENTER_NAME_OF_MEDICINE : ''}</Text>
                                        </View>

                                        <View style={{ marginTop: 20, left: 0 }}>
                                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.CHOOSE_COLOR}</Text>
                                            <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                                                <View style={[styles.medicineColorForm, { backgroundColor: allValuesM.color, marginBottom: 8 }]}>
                                                </View>
                                                <TextInput
                                                    style={{ width: allValuesM.color ? '100%' : '100%', marginTop: -30 }}
                                                    placeholder={'       ' + strings.CHOOSE_COLOR}
                                                    placeholderTextColor={'#81889f'}
                                                    editable={false}
                                                    onTouchStart={showModalColor}
                                                    borderBottomWidth={2}
                                                    borderColor={'#81889f'}
                                                />
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                            <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.ADD_TO_STANDARD_WHEEL}</Text>
                                            <Switch style={[styles.headerText, { left: 30 }]}
                                                trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                borderColor={'#81889f'}
                                                borderWidth={1}
                                                borderRadius={16}
                                                thumbColor={"#fff"}
                                                value={allValuesM.addtowheel}
                                                onValueChange={val => SetallValuesM({ ...allValuesM, ['addtowheel']: val })}
                                            />
                                        </View>
                                        <View style={{ marginTop: 20 }}>
                                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.ACTIVE_INGREDIENTS}</Text>
                                            <TextInput
                                                style={{ width: '100%', color: 'white', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                                placeholder={strings.TYPE_ACTIVE_INGREDIENTS_H}
                                                placeholderTextColor={'#81889f'}
                                                value={allValuesM.activeingredients}
                                                onChangeText={val => SetallValuesM({ ...allValuesM, ['activeingredients']: val })}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ marginRight: 5, marginLeft: 50, marginTop: 15, width: '45%' }}>
                                        <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between' }}>
                                            <View style={{ width: '45%' }}>
                                                <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.DOSE}</Text>
                                                <TextInput
                                                    style={{ fontFamily: 'HelveticaNeue-Light', fontSize: 16, color: '#81889f' }}
                                                    placeholder={strings.TYPE_DOSAGE}
                                                    placeholderTextColor={'#81889f'}
                                                    value={allValuesM.dose}
                                                    onChangeText={val => SetallValuesM({ ...allValuesM, ['dose']: val })}
                                                    borderBottomWidth={2}
                                                    borderColor={'#81889f'}
                                                />
                                                <Text style={{ color: '#e0675c' }}>{allValuesM.dose === '' ? strings.PLEASE_ENTER_DOSE : ''}</Text>
                                            </View>
                                            <View style={{ marginLeft: 30, width: '45%' }}>
                                                <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.TIMES_PER_DAY}</Text>
                                                <TextInput
                                                    style={{ color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                                    placeholder={strings.ZERO0_DAY}
                                                    placeholderTextColor={'#81889f'}
                                                    value={allValuesM.timesperday}
                                                    onChangeText={val => SetallValuesM({ ...allValuesM, ['timesperday']: val })}
                                                    borderBottomWidth={2}
                                                    borderColor={'#81889f'}
                                                />
                                                <Text style={{ color: '#e0675c' }}>{allValuesM.timesperday === '' ? strings.PLEASE_ENTER_TIMES_PER_DAY : ''}</Text>
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 20 }}>
                                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.COMPANY}</Text>
                                            <TextInput
                                                style={{ width: '100%', color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                                placeholder={strings.TYPE_COMPANY_HERE}
                                                placeholderTextColor={'#81889f'}
                                                value={allValuesM.company}
                                                onChangeText={val => SetallValuesM({ ...allValuesM, ['company']: val })}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                        <View style={{ marginTop: 40 }}>
                                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.AREA_OF_USE}</Text>
                                            <TextInput
                                                style={{ width: '100%', color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                                placeholder={strings.TYPE_AREA_OF_USE_HERE}
                                                placeholderTextColor={'#81889f'}
                                                value={allValuesM.areaofuse}
                                                onChangeText={val => SetallValuesM({ ...allValuesM, ['areaofuse']: val })}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                        <View style={{ marginTop: 40, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                            <Text style={{ color: 'white', width: 135, alignSelf: 'center', left: 30, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>Add to Notification</Text>
                                            <Switch style={[styles.headerText, { left: 30 }]}
                                                trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                borderColor={'#81889f'}
                                                borderWidth={1}
                                                value={allValuesM.notification}
                                                onValueChange={val => SetallValuesM({ ...allValuesM, ['notification']: val })}
                                                borderRadius={16}
                                                thumbColor={"#fff"}
                                            />
                                        </View>
                                        <View style={{ marginTop: 40 }}>
                                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.DESCRIPTION}</Text>
                                            <TextInput
                                                style={{ width: '100%', color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                                placeholder={strings.TYPE_NOTES_HERE}
                                                placeholderTextColor={'#81889f'}
                                                value={allValuesM.description}
                                                onChangeText={val => SetallValuesM({ ...allValuesM, ['description']: val })}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                        <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", width: '100%' }}>
                                            <Text style={{ color: 'white', alignSelf: 'center', fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.RECURRING_DAILY}</Text>
                                            <Switch style={[styles.headerText, {}]}
                                                trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                borderColor={'#81889f'}
                                                borderWidth={1}
                                                value={allValuesM.recurring ? true : false}
                                                onValueChange={val => SetallValuesM({ ...allValuesM, ['recurring']: val })}
                                                borderRadius={16}
                                                thumbColor={"#fff"}
                                            />
                                            <TextInput

                                                placeholder={strings.SELECT_TIME}
                                                placeholderTextColor={allValuesM.recurring ? 'white' : '#81889f'}
                                                editable={false}
                                                style={{ width: '60%', color: allValuesM.recurring ? 'white' : '#81889f', fontSize: 16, fontFamily: 'HelveticaNeue-Light', left: 15 }}
                                                value={allValuesM.selecttime}
                                                onTouchStart={() => { allValuesM.recurring ? () => { showDatePicker(true), setCheck('time') } : '' }}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ width: '100%', alignSelf: 'flex-end', alignItems: 'flex-end', justifyContent: 'flex-end' }}>

                                        <TouchableOpacity onPress={AddUpdateMedicine} style={[styles.buttonStyle, { right: 40, top: 10, marginBottom: 40, fontFamily: 'HelveticaNeue-Light' }]} activeOpacity={0.5}>
                                            <Text style={styles.buttonTextStyle}>{type == "Add" ? strings.SAVE : strings.UPDATE}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    {/* <Text style={{ marginBottom: 10 }}>
                                        <Text style={{ color: 'white' }}>is this a prescripted medicine. </Text>
                                        <TouchableOpacity>
                                            <Text style={{ color: '#69c2d1', textDecorationLine: 'underline' }}>click here to add more details</Text>
                                        </TouchableOpacity>
                                    </Text> */}
                                </View>
                            </KeyboardAwareScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                {/* Add update medicines end */}
                {/* Add update activity start */}
                <Provider>
                    <Portal>
                        <Modal visible={visibleA} onDismiss={hideModalA} contentContainerStyle={containerStyle}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop:20 }}>
                                <Text style={{ color: 'white', fontWeight: '700', fontSize:22 }}>{title}</Text>
                                <View style={{ left: 350 }}>
                                    <TouchableOpacity onPress={hideModalA}>
                                        <Icon name="close" color="#81889f" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <KeyboardAwareScrollView
                                contentContainerStyle={{ flexGrow: 1, flexDirection: 'row' }}
                                enableOnAndroid={true} scrollEnabled>
                                <View style={{ marginTop: 20, alignItems: 'center' }}>
                                    <View style={{ marginTop: 20 }}>
                                        <Text style={{ color: 'white', marginBottom: 8 }}>{strings.NAME_OF_ACTIVITY}</Text>
                                        <TextInput
                                            style={{ width: 460, color: 'white' }}
                                            placeholder={strings.MEDICINE_SEIZURE_HEADACHE}
                                            placeholderTextColor={'#81889f'}
                                            value={allValuesA.name}
                                            onChangeText={val => SetallValuesA({ ...allValuesA, ['name']: val })}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />

                                    </View>
                                    <View style={{ marginTop: 20, left: allValuesA.color ? 10 : 0 }}>
                                        <Text style={{ color: 'white', marginBottom: 8 }}>{strings.CHOOSE_COLOR}</Text>
                                        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                                            <View style={[allValuesA.color ? styles.activityColorForm : '', { backgroundColor: allValuesA.color, marginBottom: 8 }]}>
                                            </View>
                                            <TextInput
                                                style={{ width: 460, right: allValuesA.color ? 18 : 0 }}
                                                placeholder={allValuesA.color ? '' : 'Color'}
                                                placeholderTextColor={'#81889f'}
                                                editable={false}
                                                onTouchStart={showModalColor}
                                                borderBottomWidth={1}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ marginTop: 20 }}>
                                        <Text style={{ color: 'white', marginBottom: 8 }}>{strings.DESCRIPTION}</Text>
                                        <TextInput
                                            style={{ width: 460, color: 'white' }}
                                            placeholder={strings.TYPE_DESCRIPTION_HERE}
                                            placeholderTextColor={'#81889f'}
                                            value={allValuesA.description}
                                            onChangeText={val => SetallValuesA({ ...allValuesA, ['description']: val })}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap" }}>
                                        <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 5 }}>{strings.RECURRING_EVERYDAY}</Text>
                                        <Switch style={[styles.headerText, { left: 5 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={1}
                                            value={allValuesA.recurring ? true : false}
                                            onValueChange={val => SetallValuesA({ ...allValuesA, ['recurring']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                        <TextInput
                                            marginLeft={100}
                                            width={150}
                                            placeholder={strings.SELECT_TIME}
                                            placeholderTextColor={allValuesA.recurring ? 'white' : '#81889f'}
                                            editable={false}
                                            style={{ color: allValuesA.recurring ? 'white' : '#81889f', fontSize: 14 }}
                                            value={allValuesA.selecttime}
                                            onTouchStart={allValuesA.recurring ? () => { showDatePicker(true) } : ''}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                        <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.ADD_TO_STANDARD_WHEEL}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={1}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                            value={allValuesA.addtowheel}
                                            onValueChange={val => SetallValuesA({ ...allValuesA, ['addtowheel']: val })}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                        <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATION}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={1}
                                            value={allValuesA.notification}
                                            onValueChange={val => SetallValuesA({ ...allValuesA, ['notification']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={AddUpdateActivity}
                                        style={[styles.buttonStyle, { right: 80 }]}
                                        activeOpacity={0.5}
                                    >
                                        <Text style={styles.buttonTextStyle}>{type == "Add" ? strings.SAVE : strings.UPDATE}</Text>
                                    </TouchableOpacity>
                                </View>
                            </KeyboardAwareScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                {/* Add update activity end */}
                {/* Add update wellbeing start */}
                <Provider>
                    <Portal>
                        <Modal visible={visibleE} onDismiss={hideModalE} contentContainerStyle={containerStyle}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                                <Text style={{ color: 'white', fontWeight: '700', fontSize: 22, }}>{title}</Text>
                                <View style={{ left: 350 }}>
                                    <TouchableOpacity onPress={hideModalE}>
                                        <Icon name="close" color="#81889f" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <KeyboardAwareScrollView
                                contentContainerStyle={{ flexGrow: 1, flexDirection: 'row' }}
                                enableOnAndroid={true} scrollEnabled>
                                <View style={{ marginTop: 20, alignItems: 'center' }}>
                                    <View style={{ marginTop: 20 }}>
                                        <Text style={{ color: 'white', marginBottom: 8 }}>{strings.NAME_OF_WELLBEING}</Text>
                                        <TextInput
                                            style={{ width: 460, color: 'white' }}
                                            placeholder={strings.PLEASE_ENTER_WELLBEING}
                                            placeholderTextColor={'#81889f'}
                                            value={allValuesE.name}
                                            onChangeText={val => SetallValuesE({ ...allValuesE, ['name']: val })}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, left: 0 }}>
                                        <Text style={{ color: 'white', marginBottom: 8 }}>{strings.CHOOSE_COLOR}</Text>
                                        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                                            <View style={[allValuesE.color ? styles.eventColorForm : '', { backgroundColor: allValuesE.color, marginBottom: 8 }]}>
                                            </View>
                                            <TextInput
                                                style={{ width: 460, right: allValuesE.color ? 18 : 0 }}
                                                placeholder={allValuesE.color ? '' : 'Color'}
                                                placeholderTextColor={'#81889f'}
                                                editable={false}
                                                onTouchStart={showModalColor}
                                                borderBottomWidth={1}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ marginTop: 20 }}>
                                        <Text style={{ color: 'white', marginBottom: 8 }}>{strings.DESCRIPTION}</Text>
                                        <TextInput
                                            style={{ width: 460, color: 'white' }}
                                            placeholder={strings.TYPE_DESCRIPTION_HERE}
                                            placeholderTextColor={'#81889f'}
                                            value={allValuesE.description}
                                            onChangeText={val => SetallValuesE({ ...allValuesE, ['description']: val })}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap" }}>
                                        <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 5 }}>{strings.RECURRING_DAILY}</Text>
                                        <Switch style={[styles.headerText, { left: 5 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={1}
                                            value={allValuesE.recurring ? true : false}
                                            onValueChange={val => SetallValuesE({ ...allValuesE, ['recurring']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                        <TextInput
                                            marginLeft={100}
                                            width={150}
                                            placeholder={strings.SELECT_TIME}
                                            placeholderTextColor={allValuesE.recurring ? 'white' : '#81889f'}
                                            editable={false}
                                            style={{ color: allValuesE.recurring ? 'white' : '#81889f', fontSize: 14 }}
                                            value={allValuesE.selecttime}
                                            onTouchStart={allValuesE.recurring ? () => { showDatePicker(true) } : ''}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                        <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.ADD_TO_WHEEL}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={1}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                            value={allValuesE.addtowheel}
                                            onValueChange={val => SetallValuesE({ ...allValuesE, ['addtowheel']: val })}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                        <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATION}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={1}
                                            value={allValuesE.notification}
                                            onValueChange={val => SetallValuesE({ ...allValuesE, ['notification']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                    </View>
                                    <TouchableOpacity
                                        onPress={AddUpdateWellBeing}
                                        style={[styles.buttonStyle, { right: 80, top:50 }]}
                                        activeOpacity={0.5}
                                    >
                                        <Text style={styles.buttonTextStyle}>{type == "Add" ? strings.SAVE : strings.UPDATE}</Text>
                                    </TouchableOpacity>
                                </View>
                            </KeyboardAwareScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                {/* Add update wellbeing end */}
                <Provider>
                    <Portal>
                        <Modal visible={visibleColor} onDismiss={hideModalColor} contentContainerStyle={{ alignSelf: 'center', marginBottom: 100 }}>
                            <View style={{ marginTop: 20 }}>
                                <ColorPicker
                                    hideSliders={true}
                                    color={
                                        display1 && allValuesE.color ? allValuesE.color :
                                            display2 && allValuesM.color ? allValuesM.color :
                                                display3 && allValuesA.color ? allValuesA.color : '#ff0058'
                                    }
                                    pickerSize={10}
                                    onColorSelected={color => { setColor(fromHsv(color)) }, hideModalColor}
                                    onColorChange={color => { setColor(fromHsv(color)) }}
                                    style={{ width: 100, height: 100 }}
                                />
                            </View>
                        </Modal>
                    </Portal>
                </Provider>
                <Snackbar
                    style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                    visible={visibleSnackbar}
                    duration={3000}
                    onDismiss={onDismissSnackBar}
                >
                    {snackMsg}
                </Snackbar>
                <View style={{ width: 150 }}>
                    <DateTimePickerModal
                        style={{ backgroundColor: '#2b3249', }}
                        headerTextIOS={strings.SELECT_TIME}
                        locale="en_GB"
                        textColor="white"
                        isVisible={isDatePickerVisible}
                        pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                        modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                        mode="time"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />

                </View>
                <Provider>
                    <Portal>
                        <Modal visible={isLangModalVisible} onDismiss={() => { setVisibleLanguage(false) }} contentContainerStyle={containerStyleL}>
                            {/* <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}> */}
                            <View style={{ left: 100, top: -30 }}>
                                <TouchableOpacity onPress={() => { setVisibleLanguage(false) }}>
                                    <Icon name="close" color="#81889f" size={20} />
                                </TouchableOpacity>
                            </View>
                            {/* </View> */}
                            <View>
                                <Text style={{ color: 'white', fontWeight: '700', top: -20, marginBottom: 10, right: 30 }}>{'Select Langauge'}</Text>
                            </View>
                            <TouchableOpacity xstyle={styles.menu}>
                                <DropDownPicker
                                    items={dropValues}
                                    placeholder={'select Language'}
                                    labelStyle={{ fontSize: 14, textAlign: 'left', color: 'white' }}
                                    defaultValue={selectedValue}
                                    containerStyle={{ height: 56, top: -15, borderWidth: 0, }}
                                    style={{ backgroundColor: '#2b3249', width: 200, borderWidth: 1, }}
                                    dropDownStyle={{ backgroundColor: '#2b3249', height: 132, color: 'white', }}
                                    onChangeItem={(item) => { onChangeLng(item.value) }}
                                />
                            </TouchableOpacity>
                        </Modal>
                    </Portal>
                </Provider>
            </View>
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    headerText: {
        color: 'white',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    medicine: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: 550,
        height: 50,
        borderRadius: 15,
        marginBottom: 17,
    },
    medicineText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    medicineColor: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        position: 'relative',
        height: 90,
        width: 90,
        borderRadius: 100,
        paddingTop: 17,
        paddingLeft: 16,
        top: 70,
        shadowColor: '#955555',
        shadowOpacity: 0.8,
        zIndex: 9000
    },
    nextBtn: {
        position: 'absolute',
        height: 50,
        width: 200,
        borderRadius: 100,
        fontSize: 28,
        fontWeight: 'bold',
        bottom: -90,
        left: 340,
        marginRight: '10%',
        shadowColor: '#955555',
        shadowOpacity: 0.8,
        textAlign: 'center'
    },
    medicineColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        width: 18
    },
    activityColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        width: 18
    },
    eventColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        width: 18
    },
    imageBackStyle: {
        // alignSelf: 'center',
        backgroundColor: '#39415b',
        borderRadius: 10,
        height: 146,
        left: 100,
        alignItems: 'center',
        width: 256
    },
    onBoardTabActive: {
        width: 220, height: 55,
        borderRadius: 100,
        paddingTop: 15,
        backgroundColor: '#fb7e7e',
    },
    onBoardTab: {
        width: 240,
        color: 'white',
        paddingTop: 15,
        fontSize: 20,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    tabText: {
        fontSize: 18,
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
    },
    Next: {
        marginLeft: '50%',
        marginRight: '50%',
        marginTop: -50
    }

});

export default Onboard;
