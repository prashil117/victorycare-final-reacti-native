import React, { useState, useRef, useEffect } from "react";
import { useDispatch, useSelector } from 'react-redux'
import {
    TextInput,
    Switch,
    StyleSheet,
    ScrollView,
    SafeAreaView,
    View,
    Alert,
    Image,
    Text,
    TouchableOpacity,
    RefreshControl,
    AppState,
    TouchableHighlight,
} from 'react-native';
import { Tooltip } from 'react-native-elements'
// import Modal from "react-native-modal";
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

// import DropDownPicker from 'react-native-dropdown-picker';
import { VictoryPie, VictoryLabel, VictoryGroup, VictoryScatter, Circle } from 'victory-native';
import DateTime from 'react-native-customize-selected-date'
import DateTimePickerModal from "react-native-modal-datetime-picker";
// import Icon from 'react-native-vector-icons/Feather';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Timeline from 'react-native-timeline-flatlist'; // use npm install react-native-timeline-flatlist --save
import { Portal, Provider, Modal, FAB, Snackbar } from 'react-native-paper';
import { setModal, setDate, wellBeingLoading } from '../redux/action'
import moment from "moment";
import Loader from '../Components/Loader';
import Svg from 'react-native-svg';
import { COLORS, FONTS, SIZES, icons, images } from '../../constants';
import AwesomeAlert from 'react-native-awesome-alerts';
import config from '../appconfig/config';
import axios from 'axios'
import TimelineDisplay from '../Components/timeline'
import {Stopwatch, Timer} from 'react-native-stopwatch-timer';
import MAW from '../Components/maw'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import ImagePicker from 'react-native-image-crop-picker';
import usericn from '../../../Image/nopreview.jpg';
import Video from 'react-native-video';
import upload from '../../../Image/upload-profile.png'
import ImageViewer from 'react-native-image-zoom-viewer';

const TimelineScreen = ({ navigation }) => {
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const currentUser = useSelector(state => state.auth.userLogin)
    const updateWheel = useSelector(state => state.auth.updateWheel)
    const newcurrentDate = useSelector(state => state.auth.currentDate);
    const [count, setCount] = useState(0);
    const [imageVisible, setIsVisible] = useState(false);

    const [isStopwatchStart, setIsStopwatchStart] = useState(false);
    const [resetStopwatch, setResetStopwatch] = useState(false);
    const [showStartBtn, setShowStartBtn] = useState(false);
    const [stopwatchTime, setStopwatchTime] = useState(moment().format('HH:mm'));
    const dateModal = useSelector(state => state.auth.openDatePicker);
    const dispatch = useDispatch();
    const renderChildDay = (day) => {
        if ((['2018-12-20'], day)) {
            return <Text></Text>
        }
        if ((['2018-12-18'], day)) {
            return <Text></Text>
        }
    }
    const showDatePicker = (value) => {
        setDatePickerVisibility(true);
        setStart(value)
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };
    
    const onChangeDate = (date) => {
        dispatch(setModal(false))
        dispatch(setDate(date))
    }
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [descriptionTop, setDescriptionTop] = useState('30')
    const [awesomeAlert1, setAwesomeAlert1] = useState(false)
    const [callActivityOne, setCallActivityOne] = useState(false)
    const showAlert = (id, index, source) => {
        setAwesomeAlert(true);
    };

    const hideAlert = () => {
        setAwesomeAlert(false);
    };
    const showAlert1 = () => {
        setAwesomeAlert1(true);
    };

    const hideAlert1 = () => {
        setAwesomeAlert1(false);
    };
    const innerClockRadius = 150;
    const wellbeingRadius = 200;
    const wellbeingInnerRadius = 215;
    const medicineRadius = 190;
    const medicineInnerRadius = 223;
    const medicineCapsuleInnerRadius = 235;
    const activityRadius = 265;
    const activityStartRadius = 278;
    const activityEndRadius = 272;
    const [zindex3, setZindex3] = React.useState(false);
    const [zindexpopup, setZindexpopup] = React.useState(false);
    const [visiblepopup, setVisiblepopup] = React.useState(false);
    const [popupId, setPopupId] = useState('');
    const [popupIndex, setPopupIndex] = useState('');
    const [popupSource, setPopupSource] = useState('');
    const [popupColor, setPopupColor] = useState('');
    const [ModalErr, setModalErr] = useState(false);
    const [visible, setVisible] = React.useState(false);
    const [visible1, setVisible1] = React.useState(false);
    const [visible2, setVisible2] = React.useState(false);
    const [visible3, setVisible3] = React.useState(false);
    const [visible4, setVisible4] = React.useState([]);
    // const [Images1, setImages] = useState([]);
    const [title, setTitle] = useState(strings.ADD_MEDICINE);
    const [loadingText, setLoadingText] = useState('');
    const [type, setType] = useState('');
    const [type1, setType1] = useState('');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [keyword, setKeyword] = useState('');
    const [refreshing, setRefreshing] = React.useState(false);
    const [loading, setLoading] = useState(false);
    const [border, setBorder] = useState(0);
    const [offset, setOffset] = useState(0);
    const [isListEnd, setIsListEnd] = useState(false);
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [activitydataSource, setActivityDataSource] = useState([]);
    const [medicinedataSource, setMedicineDataSource] = useState([]);
    const [wellbeingdataSource, setWellbeingDataSource] = useState([]);
    const [activityWheelList, setActivityWheelList] = useState([]);
    const [medicineWheelList, setMedicineWheelList] = useState([]);
    const [wellbeingWheelList, setWellbeingWheelList] = useState([]);
    const [isStart, setStart] = useState(true);
    const addSome = strings.ADD_SOME_ACTIVITY;
    const [addActivityPressButton, setAddActivityPressButton] = useState(false);
    const NOW = strings.NOW;

    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const [allValues, SetallValues] = useState({
        activitymasterid: '',
        medicinemasterid: '',
        eventmasterid: '',
        id: '',
        name: '',
        color: '',
        description: '',
        start: '',
        end: '',
        enableCurrentTime: '',
        image: '',
        newimage: '',
        filename: '',
        filetype:'',
        inputType:'',
        sourceURL:'',
        filesize:'',
        allimageData: '',
    });

    const showTimer = (status) => {
        if(status){
            setIsStopwatchStart(false);
            setResetStopwatch(true);
        }
    }
    const handleChooseLibrary = (value) => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };

        ImagePicker.openPicker({
            width: value == 'image' ? 300 : 'auto',
            height: value == 'image' ? 300 : 'auto',
            cropping: value == 'image' ? true : false,
            mediaType: value,
            // multiple: true,
        }).then(image => {
            console.log("cropped "+value+"::",image);
            // alert(image.sourceURL)
            SetallValues({ ...allValues, filename: image.filename, filetype: image.mime, newimage: image.path, image:'', sourceURL:image.sourceURL, filesize:image.size, allimageData:image, inputType:value });
            showModal2()
        });
    }
    const videoBuffer = (isBuffer) => {
        console.log(isBuffer)
    }
    const handleChooseCamera = (value) => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };
        ImagePicker.openCamera({
            width: value == 'image' ? 300 : 'auto',
            height: value == 'image' ? 300 : 'auto',
            cropping: value == 'image' ? true : false,
            mediaType: value,
        }).then(image => {
            console.log("cropped camera image::",image);
            SetallValues({ ...allValues, filename: image.filename, filetype: image.mime, newimage: image.path, image:'', sourceURL:image.sourceURL, filesize:image.size, allimageData:image, inputType:value });
            showModal2()
        });
    }
    const chooseImage = () => {
        Alert.alert(
            "",
            "Upload Image",
            [
                { text: strings.CHOOSE_FROM_LIBRARY, onPress: () => handleChooseLibrary('image') },
                { text: strings.TAKE_PICTURE, onPress: () => handleChooseCamera('image') },
                { text: strings.CANCEL, onPress: () => console.log("OK Pressed"), style: "cancel" }
            ]
        );
    }
    const chooseVideo = () => {
        // return false;
        Alert.alert(
            "",
            "Upload Image",
            [
                { text: strings.CHOOSE_FROM_LIBRARY, onPress: () => handleChooseLibrary('video') },
                { text: strings.TAKE_VIDEO, onPress: () => handleChooseCamera('video') },
                { text: strings.CANCEL, onPress: () => console.log("OK Pressed"), style: "cancel" }
            ]
        );
    }
    const getLatestTime = (time) => {
        // var ltime = time;
        var date = moment().format('HH:mm:ss');
        // setCurrentTime(date);
        // console.log("curr time:", currentTime)
        // alert(isStopwatchStart)
        if(isStopwatchStart){
          setShowStartBtn(false)
          var shours =  stopwatchTime.split(':')[0];
          var smins =  stopwatchTime.split(':')[1];
          var ssecs =  stopwatchTime.split(':')[2];
          SetallValues({ ...allValues, ['end']: moment().format('HH:mm:ss') })
          console.log("hr,mins,sec:", shours+'-'+smins+'-'+ssecs);
          showModal2()
          // var date = moment()
          // .add(smins, 'minutes')
          // .add(ssecs, 'seconds')
          // .add(shours, 'hours')
          // .format('HH:mm');
          
          console.log("end time:", date)
        }else{
            setShowStartBtn(true)
            SetallValues({ ...allValues, ['start']: moment().format('HH:mm:ss') })
            console.log("curr time:", date)
            setIsStopwatchStart(false);
            setResetStopwatch(true);
        }
    }
      
    const state = {
        country: 'uk'
    }
    const [startTimeErr, setStartTimeErr] = useState((false));
    const [endTimeErr, setEndTimeErr] = useState((false));
    const handleConfirm = (date) => {
        console.log("fata", date)
        console.log("data", allValues.end)
        if (isStart) {
            if (allValues.end === '' || allValues.end === null) {
                SetallValues({ ...allValues, ['start']: moment(date).format('HH:mm') })
                setStartTimeErr(false);
            }
            else {
                var data = moment(date, 'HH:mm').isBefore(moment(allValues.end, 'HH:mm'));
                console.log("start time:", data)
                if (data || title == strings.EDIT_MEDICINE) {
                    SetallValues({ ...allValues, ['start']: moment(date).format('HH:mm') })
                    setStartTimeErr(false);
                }
                else {
                    setStartTimeErr(true);
                }
            }
        }
        else {
            // alert(allValues.start)
            if (allValues.start === '' || allValues.start === null) {
                SetallValues({ ...allValues, ['end']: moment(date).format('HH:mm') })
                setEndTimeErr(false);
            }
            else {
                var data = moment(moment(date).format('HH:mm'), 'HH:mm').isAfter(moment(allValues.start, 'HH:mm'));
                if (data) {
                    SetallValues({ ...allValues, ['end']: moment(date).format('HH:mm') })
                    setEndTimeErr(false);
                }
                else {
                    setEndTimeErr(true);
                }
            }
            // SetallValues({ ...allValues, ['end']: moment(date).format('HH:mm') })
        }
        hideDatePicker();
    };
    // Auditlog Start
    const appState = useRef(AppState.currentState)
    const [appStateVisible, setAppStateVisible] = useState(appState.current)
    useEffect(() => {
        AppState.addEventListener("change", _handleAppStateChange)
        return () => {
          AppState.removeEventListener("change", _handleAppStateChange)
        }
    }, []);
    
    const _handleAppStateChange = (nextAppState) => {
      if(appState.current.match(/inactive|background/) && nextAppState === "active"){
        console.log("App has come to the foreground:")
      }
      appState.current = nextAppState
      setAppStateVisible(appState.current)

      console.log("App state:", appState.current)
      addAuditLog(appState.current);
    }

    const addAuditLog = (activity) => {
    //   alert(activity); return false;
      var dataToSend = {
          action: 'insertlog',
          userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
          siblinguserid: currentUser && currentUser.id ? currentUser.id : '',
          page:'timelinescreen',
          activity: activity,
          appsecret: config.appsecret,
      }
      axios.post(config.apiUrl + "auditlogs.php", dataToSend).then(result => {
          console.log("get audit logs:", result.data)
          if (result.data.data == 'success') {
              console.log("Log entered successfully!")
          }
      })
    }
    // Auditlog End
    const [index, setIndex] = useState();
    const DeleteAlert = (id, index, color, source, masterid) => {
        console.log("color:", color)
        // setModalErr('Are your sure you want to delete ' + source + '?');
        // setVisiblepopup(true, id);
        // setZindexpopup(true);
        setPopupId(id);
        setPopupIndex(index);
        setPopupSource(source);
        setPopupColor(color);
        showAlert(id, index, source)


        // Alert.alert(
        //     'Delete',
        //     'Are your sure you want to delete '+source+'?',
        //     [
        //         {
        //             text: 'No',
        //             onPress: () => { },
        //             style: 'cancel',
        //         },
        //         {
        //             text: 'Yes',
        //             onPress: () => DeleteRow(id, index, source),
        //         },
        //     ],
        //     { cancelable: false },
        // );
    }

    const DeleteRow = (id, index, source) => {
        console.log(id + '-' + index + '-' + source)
        setLoading(true)
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            data.appsecret = config.appsecret;
        data.id = id;
        if (source == 'medicine')
            data.action = 'deletemedicinewheeldata'
        else if (source == 'activity')
            data.action = 'deleteactivitywheeldata'
        else if (source == 'wellbeing')
            data.action = 'deletestatuswheeldata'
        console.log("data:=>", data)
        // return false;
        axios.post(config.apiUrl + 'wheeldata.php', data).then(result => {
            console.log("asdf", result.data)
            setLoading(false)
            if (result.data.status == 'success') {
                sortedActivities.splice(index, 1);
                addAuditLog(data.action);
                setsnackMsg('Record deleted successfully')
                setVisibleSnack(true)
                setAwesomeAlert(false)
                onRefresh();
                onChangeDate(new Date())
            }
            else {
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }
        })
    }
    const onImageClick = (data, index) => {
        // console.log("index:", index)
        return false;
        var dataArr = [];
        if (data)
            dataArr.push({ url: config.apiUrl+data })
        console.log("Image Data before:", dataArr);
        setVisible4(dataArr);
        wait(4000).then(async () => {
            setIsVisible(true)
            console.log("Image Data:", visible4)
        });
    }
    const onEdit = (data, index, source, name, color) => {
        console.log("Medicine data:", data);
        // time.split(":")[1];
        var inputType = data.image.split('.').pop() == 'mp4' || data.image.split('.').pop() == 'mov' ? 'video' : 'image';
        var description = data.description
        var start = data.start;
        var end = data.end;
        var name = data.name;
        
        // var data = allValues;
        // data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
        // data.id = allValues.activitymasterid;
        // data.activityname = allValues.name;
        // data.start = "15:00";
        // data.end = "17:00";
        setBorder(index)
        SetallValues({ ...allValues, ['description']: description, ['name']: name, ['image']: data.image, ['inputType']: inputType, ['start']: start, ['end']: end, ['id']: data.id, ['activitymasterid']: data.activitymasterid, ['eventmasterid']: data.eventmasterid, ['medicinemasterid']: data.medicinemasterid, ['color']: data.color, ['enableCurrentTime'] : data.enableCurrentTime })

        if (source == 'medicine') {
            // console.log("allvalues::=>", data)
            data.action = 'deletemedicine'
            // allValues.id = data.id;
            // allValues.medicinemasterid = data.medicinemasterid;
            // allValues.name = data.name;
            // allValues.color = data.color;
            // allValues.description = data.description;
            // allValues.start = data.start;
            // allValues.end = data.end;
            // allValues.enableCurrentTime = data.enableCurrentTime;

            setTitle(strings.EDIT_MEDICINE);
            setType('edit');
            setVisible1(true);
            setZindex3(true);
            setVisible(false)
            setVisible2(false)
            setVisible3(false)
        }
        else if (source == 'activity') {
            // SetallValues({ ...allValues, ['id']: data.id, ['activitymasterid']: data.activitymasterid, ['color']: data.color, ['enableCurrentTime'] : data.enableCurrentTime})
            data.action = 'editactivity'
            // allValues.id = data.id;
            // allValues.activitymasterid = data.activitymasterid;
            // allValues.name = data.name;
            // allValues.color = data.color;
            // allValues.description = data.description;
            // allValues.start = data.start;
            // allValues.end = data.end;
            // allValues.enableCurrentTime = data.enableCurrentTime;
            // console.log("allvalues::=>", allValues); return false;
            setTitle(strings.EDIT_ACTIVITY);
            setType('edit');
            setVisible2(true);
            setZindex3(true);
            setVisible(false)
            setVisible1(false)
            setVisible3(false)
        }
        else if (source == 'wellbeing') {
            // console.log("allvalues::=>", allValues)
            data.action = 'editwellbeing'
            // allValues.id = data.id;
            // allValues.eventmasterid = data.eventmasterid;
            // allValues.name = data.name;
            // allValues.color = data.color;
            // allValues.description = data.description;
            // allValues.start = data.start;
            // allValues.end = data.end;
            // allValues.enableCurrentTime = data.enableCurrentTime;

            setTitle(strings.EDIT_WELLBEING);
            setType('edit');
            setVisible3(true);
            setZindex3(true);
            setVisible(false)
            setVisible2(false)
        }
        setIndex(index);

    }
    const onDismissSnackBar = () => setVisibleSnack(false);
    const showModal = () => setVisible(val => !val);

    const showModal1 = () => {
        setVisible1(true)
        setZindex3(true);
        setVisible(false)
        setVisible2(false)
        setVisible3(false)
        setTitle(strings.ADD_MEDICINE)
        setType('add')
    };
    const hideModal1 = (key) => {
        SetallValues({ ...allValues, ['start']: '', ['end']: '' })
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setLoadingText('')
        setVisible1(false)
        setTitle(strings.ADD_MEDICINE)
        setType('add')
        setZindex3(false);
        setBorder(0)
    };
    const showModal2 = () => {
        setVisible2(true)
        setZindex3(true);
        setVisible(false)
        setVisible1(false)
        setVisible3(false)
        setType('add')
        setTitle(strings.ADD_ACTIVITY)
    };
    const hideModal2 = (key) => {
        SetallValues({ ...allValues, ['start']: '', ['end']: '' })
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setLoadingText('')
        SetallValues({ ...allValues, newimage: '' })
        setVisible2(false)
        setZindex3(false);
        setBorder(0)
    };
    const showModal3 = () => {
        setVisible3(true)
        setZindex3(true);
        setVisible(false)
        setVisible1(false)
        setVisible2(false)
        setTitle(strings.ADD_WELLBEING)
        setType('add')
    };
    const hideModal3 = (key) => {
        SetallValues({ ...allValues, ['start']: '', ['end']: '' })
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setLoadingText('')
        setVisible3(false)
        setZindex3(false);
        setBorder(0)
    };
    const hideModal4 = (key) => {
        setIsVisible(false)
        setLoadingText('')    
    };

    const AddUpdateActivity = () => {
        if (allValues.name === '' || allValues.name === null) {
            alert(strings.PLEASE_SELECT_ACTIVITY)
            return
        }
        if (allValues.start === '' || allValues.start === null) {
            alert(strings.SELECT_START_TIME)
            return
        }
        if (allValues.end === '' || allValues.end === null) {
            alert("Please select End time")
            return
        }
        setLoadingText('Loading...')
        console.log("allValues:", allValues)
        var data = new FormData();
        data.append('userid', sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id);
        data.append('activityid', allValues.activitymasterid);
        data.append('activityname', allValues.name);
        data.append('appsecret', config.appsecret);
        data.append('hcpid', 0);
        data.append('action', type == "edit" ? 'updateactivitywheeldata' : 'addactivitywheeldata');
        data.append('id', type == "edit" ? allValues.id : '');
        data.append('currentDate', newcurrentDate);
        data.append('start', allValues.enableCurrentTime ? moment().format('HH:mm') : allValues.start);
        data.append('end', allValues.enableCurrentTime ? moment().format('HH:mm') : allValues.end);
        data.append('description', allValues.description);
        if(allValues.image){
            data.append('image',allValues.image);
        }else if(allValues.newimage){
            data.append('image',
            {
                uri:allValues.newimage,
                name:allValues.newimage,
            });
        }else{
            data.append('image','');
        }
        // var data = allValues;
        // data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        // data.activityid = allValues.activitymasterid;
        // data.activityname = allValues.name;
        // data.appsecret = config.appsecret;
        // data.hcpid = 0;
        // data.action = type == "edit" ? 'updateactivitywheeldata' : 'addactivitywheeldata';
        // data.id = type == "edit" ? allValues.id : '';
        // data.currentDate = newcurrentDate
        // data.start = allValues.enableCurrentTime ? moment().format('HH:mm') : allValues.start;
        // data.end = allValues.enableCurrentTime ? moment().format('HH:mm') : allValues.end;
        console.log("activity data::", data);
        setLoading(true)
        // addAuditLog(type == "add" ? 'create activity' : 'update activity'); return false;
        axios.post(config.apiUrl + 'wheeldata.php', data, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(result => {
            setLoading(false)
            setLoadingText('')
            console.log("result:",result.data);
            if (result.data.status == 'success') {
                setLoading(false);
                addAuditLog(type == "add" ? 'create activity' : 'update activity');
                hideModal2();
                setType('add');
                setsnackMsg(type == "add" ? strings.ACTIVITY_CREATED_SUCC : strings.ACTIVITY_UPDATED_SUCC)
                setVisibleSnack(true)
                setVisible(false)
                SetallValues({ ...allValues, ['start']: '', ['end']: '' })
                getActivityWheelData();
                // onRefresh();
            }
            else {
                setType('add');
                setVisible(false)
                hideModal2();
                SetallValues({ ...allValues, ['start']: '', ['end']: '' })
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }
        })
    }

    const AddUpdateMedicine = () => {
        if (allValues.name === '' || allValues.name === null) {
            alert(strings.PLEASE_SELECT_MEDICINE)
            return
        }
        setLoadingText('Loading...')
        setLoading(true)
        var data = allValues;
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        data.appsecret = config.appsecret;
        data.medicineid = allValues.medicinemasterid;
        data.medicinename = allValues.name;
        data.hcpid = 0;
        data.currentDate = newcurrentDate
        data.start = allValues.enableCurrentTime ? moment().format('HH:mm') : allValues.start;
        data.end = allValues.enableCurrentTime ? moment().format('HH:mm') : allValues.end;
        // return false;
        data.action = type == "edit" ? 'updatemedicinewheeldata' : 'addmedicinewheeldata';
        data.id = type == "edit" ? allValues.id : '';
        // console.log("data:", data);return false;
        axios.post(config.apiUrl + 'wheeldata.php', data).then(result => {
            setLoadingText('')
            setLoading(false)
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                setLoading(false);
                addAuditLog(type == "add" ? 'create medicine' : 'update medicine');
                hideModal1();
                setType('add');
                setsnackMsg(type == "add" ? strings.MEDICINE_CREATE_SUCC : strings.MEDICINE_UPDATED_SUCC)
                setVisibleSnack(true)
                setVisible(false)
                SetallValues({ ...allValues, ['start']: '', ['end']: '' })
                getMedicineWheelData();
            }
            else {
                setType('add');
                setVisible(false)
                hideModal1();
                SetallValues({ ...allValues, ['start']: '', ['end']: '' })
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }

        })
    }

    const AddUpdateWellbeing = () => {
        if (allValues.name === '' || allValues.name === null) {
            alert(strings.PLEASE_SELECT_WELLBEING)
            return
        }
        setLoadingText('Loading...')
        setLoading(true)
        var data = allValues;
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        data.eventid = allValues.eventmasterid;
        data.eventname = allValues.name;
        data.appsecret = config.appsecret;
        data.hcpid = 0;
        data.currentDate = newcurrentDate
        data.start = allValues.enableCurrentTime ? moment().format('HH:mm') : allValues.start;
        data.end = allValues.enableCurrentTime ? moment().format('HH:mm') : allValues.end;
        // data.start = "18:34";
        // data.end = "20:46";
        console.log("data", data)
        data.action = type == "edit" ? 'updatestatuswheeldata' : 'addstatuswheeldata';
        data.id = type == "edit" ? allValues.id : '';
        console.log("allValuesDAta:", data);
        axios.post(config.apiUrl + 'wheeldata.php', data).then(result => {
            setLoading(false)
            setLoadingText('')
            // console.log("result.data", result.data)
            if (result.data.status == 'success') {
                setLoading(false);
                addAuditLog(type == "add" ? 'create wellbeing' : 'update wellbeing');
                hideModal3();
                setType('add');
                setsnackMsg(type == "add" ? strings.WELLBEING_CREATED_SUCC : strings.WELLBEING_UPDATED_SUCC)
                setVisibleSnack(true)
                setVisible(false)
                SetallValues({ ...allValues, ['start']: '', ['end']: '' })
                getWellbeingWheelData();
            }
            else {
                setType('add');
                setVisible(false)
                hideModal3();
                SetallValues({ ...allValues, ['start']: '', ['end']: '' })
                setsnackMsg('Something went wrong, Please try again')
                setVisibleSnack(true)
            }

        })
    }

    function selectActivity(id, name, color) {
        if (border == 0) {
            setBorder(id)
            SetallValues({ ...allValues, ['activitymasterid']: id, ['name']: name, ['color']: color })
        } else {
            setBorder(id)
            SetallValues({ ...allValues, ['activitymasterid']: id, ['name']: name, ['color']: color })
        }
    }

    function selectMedicine(id, name, color) {
        if (border == 0) {
            setBorder(id)
            SetallValues({ ...allValues, ['medicinemasterid']: id, ['name']: name, ['color']: color })
        } else {
            setBorder(id)
            SetallValues({ ...allValues, ['medicinemasterid']: id, ['name']: name, ['color']: color })
        }
    }

    function selectWellbeing(id, name, color) {
        if (border == 0) {
            setBorder(id)
            SetallValues({ ...allValues, ['eventmasterid']: id, ['name']: name, ['color']: color })
        } else {
            setBorder(id)
            SetallValues({ ...allValues, ['eventmasterid']: id, ['name']: name, ['color']: color })
        }
    }
    const fetchActivityWheelList = () => {
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;

        data.appsecret = config.appsecret;
        data.action = 'getactivitymasterforwheel';
        var filename = 'getdata.php';
        console.log("datadata Medicine Master", data)
        axios.post(config.apiUrl + filename, data).then(result => {
            if (result.data.status == 'success') {
                setActivityWheelList(result.data.data)
            }
        })
    }
    useEffect(() => { fetchActivityWheelList() }, [])

    const fetchMedicineWheelList = () => {
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            data.appsecret = config.appsecret;

        data.action = 'getmedicinemasterforwheel';
        var filename = 'getdata.php';
        console.log("datafetchmedicine", data)
        axios.post(config.apiUrl + filename, data).then(result => {
            if (result.data.status == 'success') {

                setMedicineWheelList(result.data.data)
            }
        })
    }
    useEffect(() => { fetchMedicineWheelList() }, [])

    const fetchWellbeingWheelList = () => {
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            data.appsecret = config.appsecret;
        data.action = 'geteventmasterforwheel';
        var filename = 'getdata.php';
        axios.post(config.apiUrl + filename, data).then(result => {
            if (result.data.status == 'success') {
                setWellbeingWheelList(result.data.data)
            }
        })
    }
    useEffect(() => { fetchWellbeingWheelList() }, [])
    const getActivitylist = () => {
        let oldVar = "";
        if (activityWheelList.length > 0) {
            return activityWheelList.map((item, index) => {
                // console.log("activitydataSource.data2::",item)
                // if(oldVar !=  item.id){
                //   oldVar = item.id
                return (
                    <TouchableOpacity key={index} style={[styles.labelCSS, { justifyContent: 'center', backgroundColor: item.color, borderWidth: border == item.id ? 3 : 0, borderColor: border == item.id ? "#fff" : "transparent" }]} onPress={() => selectActivity(item.id, item.name, item.color)}>
                        <Text flex left style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center' }}>{item.name}
                        </Text>
                    </TouchableOpacity>
                );
                // }
            });
        } else {
            return (
                <View style={{ flex: 1, flexDirection: 'row', marginTop: 50, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { navigation.navigate('activityScreenStack', { open: true }); setVisible2(false) }} style={[styles.labelCSS, { justifyContent: 'center', backgroundColor: "#516b93", alignSelf: 'center', bottom:30 }]}>
                        <Text flex left style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', alignSelf: 'center' }} >{strings.ADD_NEW_ACTIVITY}
                        </Text>
                    </TouchableOpacity>
                </View>
            )
        }
    };

    const getMedicinelist = () => {
        if (medicineWheelList.length > 0) {
            return medicineWheelList.map((item, index) => {
                return (
                    <TouchableOpacity key={index} style={[styles.labelCSS, { justifyContent: 'center', backgroundColor: item.color, borderWidth: border == item.id ? 3 : 0, borderColor: border == item.id ? "#fff" : "transparent" }]} onPress={() => selectMedicine(item.id, item.name, item.color)}>
                        <Text flex left style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center' }}>{item.name}
                        </Text>
                    </TouchableOpacity>
                );
            });
        } else {
            return (
                <View style={{ flex: 1, flexDirection: 'row', marginTop: 50, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { navigation.navigate({ name: 'medicineScreenStack', params: { open: true } }); setVisible1(false) }} style={[styles.labelCSS, { justifyContent: 'center', backgroundColor: "#516b93", alignSelf: 'center', bottom:30 }]}>
                        <Text flex left style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', alignSelf: 'center' }} >{strings.ADD_NEW_MEDICINE}
                  </Text>
                    </TouchableOpacity>
                </View>
            )
        }
    };

    const getWellbeinglist = () => {
        // if (wellbeingWheelList !== undefined || wellbeingWheelList !== null) {
        if (wellbeingWheelList.length > 0) {
            // console.log("wellbeingdataSource.length:",wellbeingdataSource.data1)
            return wellbeingWheelList.map((item, index) => {
                return (
                    <TouchableOpacity key={index} style={[styles.labelCSS, { justifyContent: 'center', backgroundColor: item.color, borderWidth: border == item.id ? 3 : 0, borderColor: border == item.id ? "#fff" : "transparent" }]} onPress={() => selectWellbeing(item.id, item.name, item.color)}>
                        <Text flex left style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center' }} >{item.name}
                        </Text>
                    </TouchableOpacity>
                );
            });
        } else {
            return (
                <View style={{ flex: 1, flexDirection: 'row', marginTop: 50, alignItems: 'center', justifyContent: 'center' }}>
                    <TouchableOpacity onPress={() => { navigation.navigate('wellbeingScreenStack', { open: true }); setVisible3(false) }} style={[styles.labelCSS, { justifyContent: 'center', backgroundColor: "#516b93", alignSelf: 'center', bottom:30 }]}>
                        <Text flex left style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', alignSelf: 'center' }} >{strings.ADD_NEW_WELLBEING}
                </Text>
                    </TouchableOpacity>
                </View>
            )
        }
    };
    const containerStyle1 = {
        backgroundColor: '#2b3249', shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 10, paddingLeft: 10, paddingRight: 10, alignItems: 'center', width: '90%', height: '100%', alignSelf: 'center', borderRadius: 8,
    };
    const containerStyle2 = {
        backgroundColor: '#2b3249', shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 3
        },
        zindex:9992,
        position:'absolute',
        borderWidth:1,
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 10, paddingLeft: 10, paddingRight: 10, alignItems: 'center', width: '100%', height: 768, alignSelf: 'center', borderRadius: 8, top:-50,
    };
    const renderDetail = (rowData, sectionID, rowID) => {
        console.log("rrowData:",rowData)
        let title = <Text style={[styles.rowTitle]} >{rowData.name}</Text>;
        // let deleteBtn = "";
        // let editBtn= ""
        // if(rowData.lineColor == "#474f69"){
        let attachment = rowData.image ? rowData.image.split('.').pop() : '';
        
        var attachment1 = (attachment === 'mp4' || attachment === 'mov') ? 'Video' : ((attachment === 'jpg' || attachment === 'jpeg' || attachment === 'png' || attachment == 'gif') ? 'Photo' : '');
        
        // (rowData.image && (rowData.image.split('.').pop() == 'mp4' || rowData.image.split('.').pop() == 'mov')) ? 'Video' : (rowData.image && (rowData.image.split('.').pop() == 'jpeg' || rowData.image.split('.').pop() == 'mov') 'Photo');
        let deleteBtn = <TouchableOpacity style={[styles.rowActionBtn, { right: 60, top: -20 }]} onPress={() => ((rowData.source == 'medicine') ? DeleteAlert(rowData.id, sectionID, rowData.color, rowData.source, rowData.medicinemasterid) : (rowData.source == 'activity' ? DeleteAlert(rowData.id, sectionID, rowData.color, rowData.source, rowData.activitymasterid) : DeleteAlert(rowData.id, sectionID, rowData.color, rowData.source, rowData.eventmasterid)))}>
            <View>
                {rowData.lineColor == "#474f69" ? (
                    null
                ) : <Image resizeMode="contain" style={{ width: 30, height: 30, top: -15, justifyContent: 'center' }} source={require('../../../Image/delete-btn.png')} />}
            </View>
        </TouchableOpacity>;
        let editBtn = <TouchableOpacity style={[styles.rowActionBtn, { right: 20, top: -50 }]} onPress={() => ((rowData.source == 'medicine') ? onEdit(rowData, rowData.medicinemasterid, rowData.source, rowData.name, rowData.color) : (rowData.source == 'activity' ? onEdit(rowData, rowData.activitymasterid, rowData.source, rowData.name, rowData.color) : onEdit(rowData, rowData.eventmasterid, rowData.source, rowData.name, rowData.color)))}>
            <View>
                {rowData.lineColor == "#474f69" ? (
                    null
                ) :
                    <Image resizeMode="contain" style={{ width: 30, height: 30, top: -15, justifyContent: 'center' }} source={require('../../../Image/edit-btn.png')} />
                }
            </View>
        </TouchableOpacity>;
        var desc = null;
        if (rowData.description)
            desc = (
                <View style={[styles.descriptionContainer,{top: rowData.description == strings.ADD_SOME_ACTIVITY ? 0 : -40}]}>
                    <Text style={[styles.textDescriptionStyle]}>
                        {rowData.description+"\n"}
                        <TouchableOpacity  onPress={() => { onImageClick(rowData.image, sectionID)}}>
                            <View style={{color:"#74c2ce", flexDirection:'row', paddingHorizontal:5}}>
                                <Icon style={{color:"#74c2ce", top:20, transform: [{ rotate: "135deg" }], display: attachment1 ? 'flex' : 'none'}} name="attach-file" color="white" size={20} />
                                <Text style={{color:"#74c2ce", top:20,}}>{" "+attachment1 +" "+ (rowData.createddate ? moment(rowData.createddate).format('DD.MM.YYYY') : '')}</Text>
                            </View>
                        </TouchableOpacity>
                    </Text>
                </View>
            );

        return (
            <View style={{ flex: 1 }}>
                {title}
                {deleteBtn}
                {editBtn}
                {desc}
            </View>
        );
    };
    const clockHours = ['00:00', '00:15', '00:30', '00:45', '01:00', '01:15', '01:30', '01:45', '02:00', '02:15', '02:30', '02:45', '03:00', '03:15', '03:30', '03:45', '04:00', '04:15', '04:30', '04:45', '05:00', '05:15', '05:30', '05:45', '06:00', '06:15', '06:30', '06:45', '07:00', '07:15', '07:30', '07:45', '08:00', '08:15', '08:30', '08:45', '09:00', '09:15', '09:30', '09:45', '10:00', '10:15', '10:30', '10:45', '11:00', '11:15', '11:30', '11:45', '12:00', '12:15', '12:30', '12:45', '13:00', '13:15', '13:30', '13:45', '14:00', '14:15', '14:30', '14:45', '15:00', '15:15', '15:30', '15:45', '16:00', '16:15', '16:30', '16:45', '17:00', '17:15', '17:30', '17:45', '18:00', '18:15', '18:30', '18:45', '19:00', '19:15', '19:30', '19:45', '20:00', '20:15', '20:30', '20:45', '21:00', '21:15', '21:30', '21:45', '22:00', '22:15', '22:30', '22:45', '23:00', '23:15', '23:30', '23:45'];

    var wheelData = []
    var activityData = []
    var activityStartData = []
    var activityEndData = []
    // var activityTime = [{ startTime: '02:45', endTime: '08:00', color: '#fff000', source: 'activity' }, { startTime: '06:00', endTime: '21:00', color: '#00ff00', source: 'activity' }];
    var wellbeingData1 = [];
    // var wellbeingTime = [{ startTime: '00:15', endTime: '04:15', color: '#ffff00', source: 'wellbeing' }, { startTime: '15:00', endTime: '20:00', color: '#00ffff', source: 'wellbeing' }];
    // var medicineData1 = []
    var medicineColor = []
    var activityColor = []
    var activityStartColor = []
    var activityEndColor = []
    var wellbeingColor = []
    var medicineTime = [];
    var activityTimetest = [];
    var wellbeingTimetest = [];
    for (let i = 0; i < 96; i++) {
        if (i % 8 == 0) {
            wheelData.push({
                id: i,
                name: " ",
                icon: "../../../Image/healthcare_icon.png",
                color: '#fff000',
                y: 10,
            })
        } else if (i % 8 == 2) {
            wheelData.push({
                id: i,
                name: " ",
                color: '#ff00ff',
                y: 10,
            })
        } else {
            wheelData.push({
                id: i,
                name: " ",
                color: '#f2f2f2',
                y: 10,
            })
        }
    }
    const getRandom = (time) => {
        // Getting minutes
        if (time !== undefined) {
            var mins = time.split(":")[1];

            // Getting hours
            var hrs = time.split(":")[0];
            var m = (Math.round(mins / 15) * 15) % 60;

            // Converting '09:0' to '09:00'
            m = m < 10 ? '0' + m : m;
            var h = mins > 52 ? (hrs === 23 ? 0 : ++hrs) : hrs;

            // Converting '9:00' to '09:00'
            // h = h < 10 ? '0' + h : h;
            let final = h + ":" + m;
            // console.log("Final time1:",final)
            return ((final == '24:00') ? '00:00' : final);
        } else {
            return null;
        }
    }
    const getActivityWheelData = () => {
        console.log("newCurrentDatewerwerwe", newcurrentDate)
        setLoading(true);
        var dataToSend = {
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            currentdate: moment(newcurrentDate).format("YYYY-MM-DD"),
            hcpid: 0,
            appsecret: config.appsecret,
            action: 'getactivitywheeldatareact',
        };
        setEndActivityData([]);

        axios.post(config.apiUrl + "wheeldata.php", dataToSend).then(result => {
            const data = JSON.parse(JSON.stringify(result))
            if (result.data.data !== null) {
                console.log("activity dta:",result.data)
                stopLoading();
                function activityEndDataFfun() {
                    setEndActivityData(activityEndData);
                    for (let i = 0; i < clockHours.length; i++) {
                        var arr = clockHours[i].toString().split(',')
                        const getMedArr = (result.data.data !== undefined) ? result.data.data.find(x => getRandom(x.start) === arr[0]) : ""
                        if (getMedArr !== undefined && getMedArr.start) {
                            var getStartTime = getMedArr.start
                            var getActivityColor = getMedArr.color
                            var getActivityName = getMedArr.name
                        }
                        if (getMedArr !== undefined && getMedArr.end) {
                            var getEndTime = getMedArr.end
                            // var getActivityColor = getMedArr.color
                            // var getActivityName = getMedArr.name
                        }
                        // if(getMedArr != undefined){
                        // console.log("activity start time:", getMedArr)
                        if (clockHours[i] == getRandom(getStartTime)) {
                            activityStartColor.push(getMedArr.color)
                            activityStartData.push({
                                id: i,
                                name: getActivityName,
                                color: getActivityColor,
                                y: Number(activityStartRadius),
                                x: 10,
                                label: " ",
                                starttime: getStartTime,
                                endtime: getEndTime,
                            })
                        } else {
                            activityStartColor.push("transparent")
                            activityStartData.push({
                                id: i,
                                name: " ",
                                color: "transparent",
                                y: Number(activityRadius),
                                x: 1,
                                label: " ",
                                starttime: " ",
                                endtime: " ",
                            })
                        }

                        if (clockHours[i] == getRandom(getEndTime)) {
                            activityEndColor.push(getActivityColor)
                            activityEndData.push({
                                id: i,
                                name: getActivityName,
                                color: getActivityColor,
                                y: Number(activityEndRadius),
                                x:10,
                                label: " ",
                                starttime: getStartTime,
                                endtime: getEndTime,
                            })
                        } else {
                            activityEndColor.push("transparent")
                            activityEndData.push({
                                id: i,
                                name: " ",
                                color: "transparent",
                                y: Number(activityRadius),
                                x:1,
                                label: " ",
                                starttime: " ",
                                endtime: " ",
                            })
                        }

                        if (clockHours[i] >= getStartTime && clockHours[i] <= getEndTime) {
                            activityColor.push(getActivityColor)
                            activityData.push({
                                id: i,
                                name: getActivityName,
                                color: getActivityColor,
                                y: Number(activityRadius),
                                x:10,
                                starttime: getStartTime,
                                endtime: getEndTime,
                                label: " ",
                            })
                        } else {
                            activityColor.push("#fff")
                            activityData.push({
                                id: i,
                                name: " ",
                                color: "#fff",
                                y: Number(activityRadius),
                                x:1,
                                starttime: " ",
                                endtime: " ",
                                label: " ",
                            })
                        }
                    }
                    // setActivityStartdata(activityData)
                    // setActivity(activityData)
                    let testTimeline3 = []
                    if (result.data.data !== undefined) {
                        testTimeline3 = result.data.data.map((item) => {
                            console.log('testmedicinedata:',item)
                            return {
                                id: item.id,
                                activitymasterid: item.activitymasterid,
                                circleColor: item.color,
                                color: item.color,
                                description: item.description,
                                icon: require('../../../Image/activity-mask.png'),
                                key: item.key,
                                lineColor: item.color,
                                name: item.name,
                                source: item.source,
                                start: item.start,
                                end: item.end,
                                time: item.start,
                                image:item.image,
                                createddate:item.createddate,
                                starttime1: new Date(item.starttime.replace(/\s/, 'T')),
                                starttime: item.starttime,
                                endtime: item.endtime,
                                title: item.name,
                            }
                        })
                    }
                    // else {
                    //     testTimeline3 = ([{ time: '06:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '07:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '08:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '09:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '10:00', title: '', circleColor: '#474f69', lineColor: '#474f69' }])
                    // }
                    return {
                        data1: activityData, activityStartColorScales: activityStartColor, activityEndColorScales: activityEndColor, data2: testTimeline3, activityColorScales: activityColor
                    }
                };
                let activityDataFfun = activityEndDataFfun();
                setActivityDataSource(activityDataFfun)
                // console.log("test1234:",activitydataSource)
            }
            else {
                setLoading(false);
            }
        })
        // }
    };
    // useEffect(() => getMedicineWheelData(), []);
    const getMedicineWheelData = () => {
        // alert(config.appsecret)
        // if (!loading && !isListEnd) {
        setLoading(true);
        var dataToSend = {
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            // currentdate: "2020-11-04",
            currentdate: moment(newcurrentDate).format("YYYY-MM-DD"),
            hcpid: 0,
            appsecret: config.appsecret,
            action: 'getmedicinewheeldatareact',
        };
        axios.post(config.apiUrl + "wheeldata.php", dataToSend).then(result => {

            // console.log("dataTosend",result.data.data)
            if (result.data.data !== null) {
                stopLoading();
                // setMedicineDataSource(result.data.data);
                // medicineEndDataFfun(result.data.data)
                // const test = medicineEndDataFfun(result.data.data)
                function medicineEndDataFfun() {
                    var medicineData1 = [];
                    for (let i = 0; i < clockHours.length; i++) {
                        var arr = clockHours[i].toString().split(',')
                        const res = (result.data.data !== undefined) ? result.data.data.find(x => getRandom(x.start) === arr[0]) : "";
                        // console.log("medicine res:",res)
                        if (res != undefined) {
                            if (arr[0] === getRandom(res.start)) {
                                medicineColor.push(res.color)
                                medicineData1.push({
                                    id: i,
                                    name: res.name,
                                    color: res.color,
                                    y: Number(medicineRadius),
                                    x:10,
                                    cornerRadius: 10,
                                    starttime: res.start,
                                    label: " ",
                                })
                            }
                        } else {
                            medicineColor.push("transparent")
                            medicineData1.push({
                                id: i,
                                name: " ",
                                color: 'transparent',
                                y: Number(medicineRadius),
                                x:1,
                                cornerRadius: 10,
                                starttime: null,
                                label: " ",
                            })
                        }
                    }
                    setMedicineData(medicineData1)
                    let testTimeline2 = []
                    if (result.data.data !== undefined) {
                        testTimeline2 = result.data.data.map((item) => {
                            // var date1 = new Date(item.starttime.replace(/\s/, 'T'));
                            // console.log('testmedicinedata2:',date1);
                            return {
                                medicinemasterid: item.medicinemasterid,
                                circleColor: item.color,
                                color: item.color,
                                description: item.description,
                                icon: require('../../../Image/capsule-mask.png'),
                                id: item.id,
                                key: item.key,
                                lineColor: item.color,
                                name: item.name,
                                source: item.source,
                                start: item.start,
                                end: item.end,
                                starttime1: new Date(item.starttime.replace(/\s/, 'T')),
                                starttime: item.starttime,
                                endtime: item.endtime,
                                time: item.start,
                                title: item.medicinename,
                            }
                        })
                    }
                    //  else {
                    //     let currentDateTime= moment(new Date()).format('HH:mm');
                    //     testTimeline2 = ([{ time: currentDateTime, title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: currentDateTime, title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '03:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '04:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '05:00', title: '', circleColor: '#474f69', lineColor: '#474f69' }])
                    // }
                    return {
                        data1: medicineData1, data2: testTimeline2, medColorScales: medicineColor
                    }
                };
                let medicineDataFfun = medicineEndDataFfun();
                setMedicineDataSource(medicineDataFfun)
                // setMedicineDataSource({data1:test1234, data2:"test123123123"})
                // let test1234 = test123()
                // alert("Adf")
                console.log("test123 medic:",medicinedataSource)
            }
            else {
                setLoading(false);
            }
        })
        // }
    };

    // useEffect(() => getWellbeingWheelData(), []);
    const getWellbeingWheelData = () => {
        // alert(config.appsecret)
        // if (!loading) {
        setLoading(true);
        setWellBeingData([]);
        var dataToSend = {
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            currentdate: moment(newcurrentDate).format("YYYY-MM-DD"),
            hcpid: 0,
            appsecret: config.appsecret,
            action: 'geteventwheeldatareact',
        };
        axios.post(config.apiUrl + "wheeldata.php", dataToSend).then(result => {
            stopLoading();
            if (result.data.data !== null) {
                // setWellbeingDataSource(result.data.data);
                // eventEndDataFfun(result.data.data)

                function eventEndDataFfun() {

                    var wellbeingData1 = [];
                    for (let i = 0; i < clockHours.length; i++) {
                        var arr = clockHours[i].toString().split(',')
                        const getMedArr = (result.data.data !== undefined) ? result.data.data.find(x => getRandom(x.start) === arr[0]) : ""

                        if (getMedArr != undefined && getMedArr.start && getMedArr.start) {
                            var getStartTime = getMedArr.start
                            var getEndTime = getMedArr.end
                            var getWellbeingColor = getMedArr.color
                            var getWellbeingName = getMedArr.name
                        }


                        // if(getMedArr != undefined){
                        if (clockHours[i] >= getStartTime && clockHours[i] <= getEndTime) {
                            wellbeingColor.push(getWellbeingColor)
                            wellbeingData1.push({
                                id: i,
                                name: getWellbeingName,
                                color: getWellbeingColor,
                                y: Number(wellbeingRadius),
                                x:10,
                                cornerRadius: 10,
                                starttime: getStartTime,
                                endtime: getEndTime,
                                label: " ",
                            })
                        } else {
                            wellbeingColor.push("#fff")
                            wellbeingData1.push({
                                id: i,
                                name: " ",
                                color: '#fff',
                                y: Number(wellbeingRadius),
                                x:1,
                                cornerRadius: 10,
                                starttime: null,
                                endtime: null,
                                label: " ",
                            })
                        }
                    }
                    setWellBeingData(wellbeingData1)
                    let testTimeline1 = []
                    // console.log("wellbeingColor=>", wellbeingColor)
                    if (result.data.data !== undefined) {
                        testTimeline1 = result.data.data.map((item) => {
                            // console.log('new date:',new Date(item.starttime))
                            return {
                                eventmasterid: item.eventmasterid,
                                circleColor: item.circleColor,
                                color: item.color,
                                source: item.source,
                                description: item.description,
                                icon: require('../../../Image/wellbeing-mask.png'),
                                id: item.id,
                                key: item.key,
                                lineColor: item.lineColor,
                                name: item.name,
                                start: item.start,
                                end: item.end,
                                starttime1: new Date(item.starttime.replace(/\s/, 'T')),
                                starttime: item.starttime,
                                endtime: item.endtime,
                                time: item.time,
                                title: item.title,
                            }
                        })
                    }
                    // else {
                    //     testTimeline1 = ([{ time: '11:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '12:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '13:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '14:00', title: '', circleColor: '#474f69', lineColor: '#474f69' },
                    //     { time: '15:00', title: '', circleColor: '#474f69', lineColor: '#474f69' }])
                    // }
                    // const timelineData1 = [testTimeline1];
                    return {
                        data1: wellbeingData1, data2: testTimeline1, wellbeingColorScales: wellbeingColor
                    }
                };
                let wellbeingDataFfun = eventEndDataFfun();
                setWellbeingDataSource(wellbeingDataFfun)
            }
            else {
                setLoading(false);
            }
        })
        // }
    };

    const onRefresh = () => {
        // alert("asdf")
        setRefreshing(false);
        setVisible(false)
        // setMedicineData([]);
        // setWellBeingData([]);
        // setActivity([]);
        // setActivityStartdata([]);
        // setEndActivityData([]);
        // setActivityWheelList([]);
        // setMedicineWheelList([]);
        // setWellbeingWheelList([]);
        getMedicineWheelData();
        getWellbeingWheelData();
        getActivityWheelData();
        stopLoading();
        fetchActivityWheelList()
        fetchWellbeingWheelList()
        fetchMedicineWheelList()
        setLoadingText('')
    }



    const stopLoading = () => {
        wait(2500).then(() => {
            setLoading(false)
        });
    }

    function timelineData3() {

        if (activitydataSource.data2 !== undefined) {
            // console.log("wellbeingdataSource.length:",wellbeingdataSource.data1)
            return activitydataSource.data2
        } else {
            return []
            // return ([{ time: "01:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            //     { time: "02:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            //     { time: "03:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            //     { time: "04:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            //     { time: "05:00", title: '', circleColor: '#474f69', lineColor: '#474f69' }])
        }
    };
    function timelineData2() {
        if (medicinedataSource.data2 !== undefined) {
            // console.log("wellbeingdataSource.length:",wellbeingdataSource.data1)
            return medicinedataSource.data2
        } else {
            return []
            // return ([{ time: "06:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            // { time: "07:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            // { time: "08:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            // { time: "09:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            // { time: "10:00", title: '', circleColor: '#474f69', lineColor: '#474f69' }])
        }
    };
    function timelineData1() {
        if (wellbeingdataSource.data2 !== undefined) {
            // console.log("wellbeingdataSource.length:",wellbeingdataSource.data1)
            return wellbeingdataSource.data2
        } else {
            return []
            // return ([{ time:  "11:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            // { time: "12:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            // { time: "13:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            // { time: "14:00", title: '', circleColor: '#474f69', lineColor: '#474f69' },
            // { time: "15:00", title: '', circleColor: '#474f69', lineColor: '#474f69' }])
        }
    };
    let td1 = timelineData1()
    let td2 = timelineData2()
    let td3 = timelineData3()

    // const timelineData123 = [...td1, ...td2, ...td3];
    const timelineData123 = [].concat(td1, td2, td3)
    let sortedActivities = [];
    const getSenAct = () => {
        timelineData123.length > 0 ? setAddActivityPressButton(false) : setAddActivityPressButton(true);
    }
    if (timelineData123.length > 0) {
        // getSenAct()
        sortedActivities = timelineData123.slice().sort((b, a) => b.starttime1 - a.starttime1)
    } else {
        // getSenAct()
        var getTime = getcurrenttime(10)
        // setAddActivityPressButton(true);
        sortedActivities = ([{ time: getTime[0], name: NOW, circleColor: '#6AB2D3', lineColor: '#474f69', image:'', description: addSome, icon: require('../../../Image/activity-mask.png') },
        { time: getTime[1], name: '', circleColor: '#474f69', lineColor: '#474f69', image:'',  },
        { time: getTime[2], title: '', circleColor: '#474f69', lineColor: '#474f69', image:'',  },
        { time: getTime[3], title: '', circleColor: '#474f69', lineColor: '#474f69', image:'', },
        { time: getTime[4], title: '', circleColor: '#474f69', lineColor: '#474f69', image:'', },
        { time: getTime[5], title: '', circleColor: '#474f69', lineColor: '#474f69', image:'', },
        { time: getTime[6], title: '', circleColor: '#474f69', lineColor: '#474f69', image:'', },
        { time: getTime[7], title: '', circleColor: '#474f69', lineColor: '#474f69', image:'', },
        { time: getTime[8], title: '', circleColor: '#474f69', lineColor: '#474f69', image:'', },
        { time: getTime[9], title: '', circleColor: '#474f69', lineColor: '#474f69', image:'', }])
    }

    // var innerClockData = []
    // let j = 0;
    // for (let i = 0; i < 96; i++) {
    //     if (i % 4 == 0) {
    //         innerClockData.push({
    //             id: i,
    //             name: ((j == 0) ? '24/0' : j),
    //             color: '#2b3249',
    //             y: 10,
    //         })
    //         j++;
    //     } else {
    //         innerClockData.push({
    //             id: i,
    //             name: " ",
    //             color: '#666',
    //             y: 10,
    //         })
    //     }
    // }

    function getcurrenttime(allValues) {
        let Time;
        var Time24 = 0;
        let date = new Date();
        let hours = date.getHours();
        var totalTime = [];
        // if(value==0){
        //     Time=moment(new Date()).format('HH:mm');
        //     return Time;
        // }else{

        //     Time=parseInt(hours) + value
        //     if(Time>24){
        //       return parseInt(value)-1
        //     }else{
        //         return Time;
        //     }
        // }

        for (let i = 0; i < allValues; i++) {

            if (i == 0) {
                totalTime[i] = moment(new Date()).format('HH:mm');
                hours = parseInt(hours) + 1
            } else if (hours > 24) {
                Time24 = parseInt(Time24) + 1

                if (Time24.toString().length == 1) {
                    totalTime[i] = "0" + Time24 + ":00"
                } else {
                    totalTime[i] = Time24 + ":00"
                }

            } else {
                totalTime[i] = hours + ":00";
                hours = parseInt(hours) + 1
            }
        }
        return totalTime;
    }

    
    const [categories] = React.useState(wheelData)
    const [activity, setActivity] = React.useState([])
    const [activityStart, setActivityStartdata] = React.useState([])
    const [activityEnd, setEndActivityData] = React.useState([])
    const [medicine, setMedicineData] = React.useState([])
    const [wellbeing, setWellBeingData] = React.useState([])
    // const [innerClock, setinnerClock] = React.useState(innerClockData)


    const switchCurrentTime = (val) => {
        debugger

        if (val == true) {
            var dateCurrent = moment(new Date()).format('HH:mm');
            SetallValues({ ...allValues, ['enableCurrentTime']: val, ['start']: dateCurrent, ['end']: dateCurrent })
        } else {
            SetallValues({ ...allValues, ['enableCurrentTime']: val, ['start']: '', ['end']: '' })
        }
    }

    const renderChart = () => {
        let wellbeingColorScales = wellbeingdataSource.wellbeingColorScales
        let activityColorScales = activitydataSource.activityColorScales
        let activityStartColorScales = activitydataSource.activityStartColorScales
        let activityEndColorScales = activitydataSource.activityEndColorScales
        let medColorScales = medicinedataSource.medColorScales
        // debugger;
        return (
            <View>
                
                  <Loader loading={loading} />
               
                {/* Left Panel Start */}
                {/* <View style={{ alignItems: 'center', justifyContent: 'center', width: 370, height: 750, position: "absolute", top: -10, left: -20, fontFamily: 'HelveticaNeue', zIndex: 9979 }}>
                    <TouchableOpacity onPress={onRefresh}>
                        <Text style={styles.title}>TIMELINE</Text>
                    </TouchableOpacity>
                    <ScrollView showsVerticalScrollIndicator={false} contentContainerStyle={{ paddingBottom: 60, width: '100%' }}>
                        <View style={styles.container}>
                            <Timeline
                                data={sortedActivities}
                                circleSize={30}
                                lineColor="#fff"
                                timeContainerStyle={{ minWidth: 52, minHeight: 70, marginTop: -5, color: "#fff", showsVerticalScrollIndicator: false }}
                                timeStyle={{
                                    textAlign: 'center',
                                    backgroundColor: 'transparent',
                                    fontWeight: 'bold',
                                    color: '#fff',
                                    padding: 5,
                                    borderRadius: 13,
                                }}
                                titleStyle={{ color: '#fff' }}
                                descriptionStyle={{ color: '#fff', width: 200 }}
                                options={{
                                    style: { paddingTop: 5, color: "#fff" },
                                }}
                                innerCircle={'icon'}
                                renderDetail={renderDetail}
                            />
                        </View>
                    </ScrollView>
                </View> */}
                <TimelineDisplay onRefresh={onRefresh} sortedActivities={sortedActivities} renderDetail={renderDetail} />
                {/* Left Panel End */}
                {/* Right Panel Start */}
                {/* <ScrollView style={{ height: 890, width: 680, position: 'relative', top: -120, left: 330, zIndex: 9978 }}> */}
                {/* <ScrollView
                    contentContainerStyle={styles.scrollView}
                    refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />
                    }
                >
                    <Text>Pull down to see RefreshControl indicator</Text>
                </ScrollView> */}
                <View style={{
                    height: 680, width: 680, position: 'absolute', top: -120, left: 330,
                }}>
                    {/* Activity Start */}
                    <View style={{ alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 134, left: 112, zIndex: 9973, width: 535, height: 535, borderRadius: 535, }}>
                        <Svg width={SIZES.width} height={SIZES.width} style={{ width: "100%", height: "auto", position: 'relative', top: 25, left: 25, }}>
                            <VictoryPie
                                standalone={false} // Android workaround
                                data={activitydataSource.data1}
                                radius={({ datum }) => `${datum.y}`}
                                innerRadius={({ datum }) => `${datum.y - 3}`}
                                cornerRadius={({ datum }) => `${datum.cornerRadius}`}
                                padAngle={({ datum }) => 0}

                                labelRadius={({ innerRadius }) => (SIZES.width * 14 + innerRadius) / 2.1}
                                style={{
                                    labels: { fill: "transparent", fontSize: 0, color: "transparent" },
                                }}
                                width={SIZES.width * 0.95}
                                height={SIZES.width * 0.95}
                                colorScale={activityColorScales}
                                events={[{
                                    target: "data",
                                    eventHandlers: {
                                        onPress: () => {
                                            return [
                                                {
                                                    target: "labels",
                                                    mutation: (props) => {
                                                        // mutation: ({style}) => {
                                                        console.log("style1:", activitydataSource.data1[props.index].name)
                                                        // let categoryName = style.fill
                                                        if (activitydataSource.data1[props.index].starttime || activitydataSource.data1[props.index].endtime) {
                                                            let sentence = strings.START_TIME+":" + activitydataSource.data1[props.index].starttime + '\n' + strings.STOP_TIME+":" + activitydataSource.data1[props.index].endtime
                                                            Alert.alert(
                                                                activitydataSource.data1[props.index].name,
                                                                sentence,
                                                                [
                                                                    {
                                                                        text: 'Dismiss',
                                                                        onPress: () => {
                                                                            return null;
                                                                        },
                                                                    },
                                                                ],
                                                                { cancelable: false },
                                                            );
                                                        }
                                                    }
                                                }
                                            ];
                                        }
                                    }
                                }]}
                            />
                        </Svg>
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 130, left: 108, zIndex: 9973, width: 545, height: 545, borderRadius: 545, }}>
                        <Svg width={SIZES.width} height={SIZES.width} style={{ width: "100%", zIndex: 9999, height: "auto", position: 'relative', top: 23, left: 23 }}>
                            <VictoryPie
                                standalone={false}
                                data={activitydataSource.data1}
                                radius={({ datum }) => `${datum.y + 8}`}
                                innerRadius={({ datum }) => `${datum.y - 12}`}
                                cornerRadius={30}
                                // labelRadius={({ innerRadius }) => (SIZES.width * 14 + innerRadius) / 2.1}
                                // padAngle={({ datum }) => 0.6}
                                style={{
                                    labels: { fill: "transparent", fontSize: 0, color: "transparent" },
                                }}
                                width={SIZES.width * 0.95}
                                height={SIZES.width * 0.95}
                                colorScale={activityStartColorScales}
                                events={[{
                                    target: "data",
                                    eventHandlers: {
                                        onPress: () => {
                                            return [
                                                {
                                                    target: "labels",
                                                    mutation: (props) => {
                                                        // mutation: ({style}) => {
                                                        console.log("style2:", activitydataSource.data1[props.index].starttime)
                                                        // let categoryName = style.fill
                                                        if (activitydataSource.data1[props.index].starttime != " " || activitydataSource.data1[props.index].endtime != " ") {
                                                            let sentence = strings.START_TIME+":" + activitydataSource.data1[props.index].starttime + '\n' + strings.STOP_TIME+":" + activitydataSource.data1[props.index].endtime;
                                                            Alert.alert(
                                                                activitydataSource.data1[props.index].name,
                                                                sentence,
                                                                [
                                                                    {
                                                                        text: 'Dismiss',
                                                                        onPress: () => {
                                                                            return null;
                                                                        },
                                                                    },
                                                                ],
                                                                { cancelable: false },
                                                            );
                                                        }
                                                    }
                                                }
                                            ];
                                        }
                                    }
                                }]}
                            />
                        </Svg>
                    </View>
                    <View style={{ alignItems: 'center', justifyContent: 'center', zindex: 9999, position: 'absolute', top: 130, zIndex: 9973, left: 108, width: 542, height: 542, borderRadius: 542, }}>
                        <Svg width={SIZES.width} height={SIZES.width} style={{ width: "100%", zIndex: 9982, height: "auto", position: 'relative', top: 25, left: 25 }}>
                            <VictoryPie
                                standalone={false}
                                data={activitydataSource.data1}
                                radius={({ datum }) => `${datum.y + 6}`}
                                innerRadius={({ datum }) => `${datum.y - 11}`}
                                cornerRadius={10}
                                // labelRadius={({ innerRadius }) => (SIZES.width * 14 + innerRadius) / 2.1}
                                // padAngle={({ datum }) => 0.6}
                                style={{
                                    labels: { fill: "transparent", fontSize: 0, color: "transparent" },
                                }}
                                width={SIZES.width * 0.95}
                                height={SIZES.width * 0.95}
                                colorScale={activityEndColorScales}
                                events={[{
                                    target: "data",
                                    eventHandlers: {
                                        onPress: () => {
                                            return [
                                                {
                                                    target: "labels",
                                                    mutation: (props) => {
                                                        // mutation: ({style}) => {
                                                        console.log("style3:", activitydataSource.data1[props.index].name)
                                                        // let categoryName = style.fill
                                                        if (activitydataSource.data1[props.index].starttime || activitydataSource.data1[props.index].endtime) {
                                                            let sentence = strings.START_TIME+":" + activitydataSource.data1[props.index].starttime + '\n' + strings.STOP_TIME+":" + activitydataSource.data1[props.index].endtime
                                                            Alert.alert(
                                                                activitydataSource.data1[props.index].name,
                                                                sentence,
                                                                [
                                                                    {
                                                                        text: 'Dismiss',
                                                                        onPress: () => {
                                                                            return null;
                                                                        },
                                                                    },
                                                                ],
                                                                { cancelable: false },
                                                            );
                                                        }
                                                    }
                                                }
                                            ];
                                        }
                                    }
                                }]}
                            />
                        </Svg>
                    </View>
                    {/* Activity End */}
                    {/* Medicine Start */}
                    <View style={{ alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 150, left: 120, zIndex: 9973, width: 505, height: 505, borderRadius: 505, }}>
                        <Svg width={SIZES.width} height={SIZES.width} style={{ width: "100%", zIndex: 9999, position: 'relative', top: 50, left: 55 }}>
                            <VictoryPie
                                standalone={false}
                                data={medicinedataSource.data1}
                                // labels={(datum) => `${datum.y}`}
                                radius={({ datum }) => SIZES.width * 0.4 - 160}
                                innerRadius={medicineInnerRadius}
                                cornerRadius={({ datum }) => 10}
                                padAngle={({ datum }) => 0.2}
                                labelRadius={({ innerRadius }) => (SIZES.width * 4 + innerRadius) / 2.1}
                                style={{
                                    labels: { fill: "transparent", fontSize: 0, color: "transparent" },
                                }}
                                width={SIZES.width * 0.9}
                                height={SIZES.width * 0.9}
                                colorScale={medColorScales && medColorScales !== null ? medColorScales : {}}
                                events={[{
                                    target: "data",
                                    eventHandlers: {
                                        onPress: () => {
                                            return [
                                                {
                                                    target: "labels",
                                                    mutation: (props) => {
                                                        // mutation: ({style}) => {
                                                        console.log("style:", medicinedataSource.data1[props.index].starttime)
                                                        // let categoryName = style.fill
                                                        let sentence = "Taken Time:" + medicinedataSource.data1[props.index].starttime
                                                        if (medicinedataSource.data1[props.index].starttime !== null) {
                                                            Alert.alert(
                                                                medicinedataSource.data1[props.index].name,
                                                                sentence,
                                                                [
                                                                    {
                                                                        text: 'Dismiss',
                                                                        onPress: () => {
                                                                            return null;
                                                                        },
                                                                    },
                                                                ],
                                                                { cancelable: false },
                                                            );
                                                        }
                                                    }
                                                }
                                            ];
                                        }
                                    }
                                }]}
                            />
                            <Circle cx="460" cy="460" r="236" fill="none" stroke={"#2b3249"} strokeWidth={2} />
                        </Svg>
                    </View>
                    {/* Medicine End */}
                    {/* Wellbeing Start */}
                    <View style={{ alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 185, left: 160, zIndex: 9973, width: 435, height: 435, borderRadius: 435 }}>
                        <Svg width={SIZES.width} height={SIZES.width} style={{ width: "100%", height: "auto", position: 'relative', top: 102, left: 102 }}>
                            <VictoryPie
                                standalone={false} // Android workaround
                                data={wellbeingdataSource.data1}
                                radius={({ datum }) => `${datum.y}` - 10}
                                innerRadius={wellbeingInnerRadius}
                                cornerRadius={({ datum }) => `${datum.cornerRadius}`}
                                padAngle={({ datum }) => 0.6}
                                labelRadius={({ innerRadius }) => (SIZES.width * 4 + innerRadius) / 2.1}
                                style={{
                                    labels: { fill: "transparent", fontSize: 0, color: "transparent" },
                                }}
                                width={SIZES.width * 0.8}
                                height={SIZES.width * 0.8}
                                colorScale={wellbeingColorScales && wellbeingColorScales !== null ? wellbeingColorScales : {}}
                                events={[{
                                    target: "data",
                                    eventHandlers: {
                                        onPress: () => {
                                            return [
                                                {
                                                    target: "labels",
                                                    mutation: (props) => {
                                                        // mutation: ({style}) => {
                                                        if (wellbeing[props.index].starttime || wellbeing[props.index].endtime) {
                                                            let sentence = strings.START_TIME+":" + wellbeing[props.index].starttime + '\n' + strings.STOP_TIME+":" + wellbeing[props.index].endtime
                                                            Alert.alert(
                                                                wellbeing[props.index].name,
                                                                sentence,
                                                                [
                                                                    {
                                                                        text: 'Dismiss',
                                                                        onPress: () => {
                                                                            return null;
                                                                        },
                                                                    },
                                                                ],
                                                                { cancelable: false },
                                                            );
                                                        }
                                                    }
                                                }
                                            ];
                                        }
                                    }
                                }]}
                            />
                        </Svg>

                        <View style={{ position: 'absolute', top: '35%', left: "35%", zIndex: 9972, width: 150, height: 150 }}>
                            <View style={{left:-10, top:-50, position:'absolute'}}>
                                <Stopwatch
                                    laps
                                    // msecs
                                    start={isStopwatchStart}
                                    // To start
                                    reset={resetStopwatch}
                                    // To reset
                                    // options={showStartBtn ? options : ''}
                                    options={showStartBtn ? options : ''}
                                    // Options for the styling
                                    getTime={(time) => {
                                    setTimeout(() => {
                                        // console.log(time)
                                        setStopwatchTime(time);
                                    }, 100);
                                    }}
                                />
                            </View>
                            <TouchableOpacity style={[styles.wheelInsideItems, { backgroundColor: '#779e74', left: 0 }]}
                                 onPress={chooseImage}
                            >
                                <Image source={require('../../../Image/photo-camera.png')} style={[styles.icons]} />
                            </TouchableOpacity>
                            <TouchableOpacity style={[styles.wheelInsideItems, { backgroundColor: '#fcb56b', top: -70, left: 80 }]}
                                onPressIn={chooseVideo}
                            >
                                <Image source={require('../../../Image/video-camera.png')} style={[styles.icons]} />
                            </TouchableOpacity>
                            {/* <TouchableHighlight
                                onPress={() => {
                                getLatestTime(isStopwatchStart);
                                setIsStopwatchStart(!isStopwatchStart);
                                setResetStopwatch(false);
                                showTimer(isStopwatchStart)
                                }}>
                                <Text style={styles.buttonText}>
                                {!isStopwatchStart ? 'START' : 'STOP'}
                                </Text>
                            </TouchableHighlight> */}
                            <TouchableOpacity style={[styles.wheelInsideItems, { backgroundColor: '#d25992', top: -60, left: 40 }]}
                                onPress={() => {
                                    getLatestTime(isStopwatchStart);
                                    setIsStopwatchStart(!isStopwatchStart);
                                    setResetStopwatch(false);
                                    showTimer(isStopwatchStart)
                                }}
                            >
                                <Image source={require('../../../Image/stopwatch.png')} style={styles.icons} />
                            </TouchableOpacity>
                            {/*<Text style={{ ...FONTS.h1, textAlign: 'center', color:'#fff' }}>{totalExpenseCount}</Text>
                        <Text style={{ ...FONTS.body3, textAlign: 'center', color:'#fff' }}>Nodes for the Wheel</Text>*/}
                        </View>
                    </View >
                    {/* Wellbeing End */}
                    {/* Clock Start */}
                    <View style={{ alignItems: 'center', justifyContent: 'center', position: 'absolute', top: 240, left: 220, width: 320, height: 320, borderRadius: 320, }}>
                        <Image source={icons.inner_clock} style={{ width: 320, height: 320, alignSelf: 'center', position: 'absolute', }} />
                    </View>
                    {/* Clock End */}

                    <MAW activityWheelList={activityWheelList} medicineWheelList={medicineWheelList} wellbeingWheelList={wellbeingWheelList} />
                </View>
                <View style={{ position: 'absolute', height: 80, width: 80, top: 470, right: 10, }}>
                    <TouchableOpacity
                        style={[styles.floatBtn, { shadowOpacity: 0.6 }]} //,
                        onPress={showModal}
                    >
                        {!visible ? <Icon name="add" color="white" size={30} /> :
                            <Icon name="close" color="white" size={30} />}
                    </TouchableOpacity>
                </View>
                <View style={!zindex3 ? { position: 'absolute', height: '100%', width: '100%', top: 0, alignSelf: 'center', alignItems: 'center' } : { height: '100%', width: '100%', top: 0, alignSelf: 'center', alignItems: 'center' }}>
                    <Provider>
                        <Portal>
                            <Modal visible={imageVisible} transparent={true} contentContainerStyle={containerStyle2}>
                                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 50 }}>
                                    <Text style={{ color: 'white', fontSize: 24, fontWeight:'bold' }}>Image Popup</Text>
                                    <View style={styles.closeBtn}>
                                        <TouchableOpacity onPress={hideModal4}>
                                            <Icon name="close" color="#81889f" size={20} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                {/* <Image
                                    source={{uri: config.apiUrl+allValues.image}}
                                    style={{ width: 220, height: 100, borderRadius: 10 }}
                                /> */}
                                <ImageViewer enableImageZoom={true} enablePreload={true} index={0} useNativeDriver={true} onCancel={() => setIsVisible(false)} enableSwipeDown={true} onSwipeDown={() => setIsVisible(false)} imageUrls={visible4} />
                            </Modal>
                            {/* Add Wellbeing Start*/}
                            <Modal visible={visible3} onDismiss={hideModal3} contentContainerStyle={containerStyle1}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 50 }}>
                                    <Text style={{ color: 'white', fontSize: 24, fontWeight:'bold' }}>{title}</Text>
                                    <View style={styles.closeBtn}>
                                        <TouchableOpacity onPress={hideModal3}>
                                            <Icon name="close" color="#81889f" size={20} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <KeyboardAwareScrollView
                                    contentContainerStyle={{ flexGrow: 1, flexDirection: 'row' }}
                                    enableOnAndroid={true} scrollEnabled>
                                    <View style={{ alignItems: 'center', marginTop: 30, }}>
                                        <View style={styles.viewContainer}>
                                            <View style={{ marginVertical: 20}}>
                                                
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.CHOOSE_FROM_AVAILABLY_WELLBEING} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                                <SafeAreaView style={styles.getActivityCSS}>
                                                    <ScrollView style={{ alignSelf: 'flex-start', flexDirection: 'column', }}>
                                                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                                            {
                                                                getWellbeinglist()
                                                            }
                                                        </View>
                                                    </ScrollView>
                                                </SafeAreaView>
                                                <View style={{flexDirection:"row", height:40, marginTop:10}}>
                                                    <Text style={{color:'#81889f', marginTop:10,}}>{strings.IF_SOME_WELLBEING_IS_MISSING}
                                                    </Text>
                                                    <TouchableOpacity onPress={() => { navigation.navigate('wellbeingScreenStack', { open: true }); setVisible2(false) }} style={[styles.labelCSS, { justifyContent: 'center', backgroundColor: "#516b93", alignSelf: 'center', height:40 }]}>
                                                    <Text flex left style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', alignSelf: 'center' }} >{strings.ADD_NEW_WELLBEING}
                                                    </Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                                <View style={{width:'50%'}}>
                                                    <Text style={{ color: 'white', }}>{strings.START_TIME} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                                    <TextInput
                                                        // left={20}
                                                        width={150}
                                                        placeholder={strings.SELECT_START_TIME}
                                                        placeholderTextColor={allValues.recurring ? 'white' : '#81889f'}
                                                        editable={false}
                                                        onChangeText={(text) => SetallValues({ ...allValues, ['start']: text })}
                                                        style={{ color: allValues.recurring ? 'white' : '#81889f', fontSize: 16, marginRight: 40, marginTop:20, fontWeight:'bold' }}
                                                        value={allValues.start}
                                                        onTouchStart={!allValues.enableCurrentTime ? () => { showDatePicker(true) } : ''}
                                                        borderBottomWidth={1}
                                                        borderColor={'#81889f'}
                                                    />
                                                    {startTimeErr && <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> {strings.START_TIME_CANN_SMALLER}</Text>}
                                                </View>
                                                <View style={{width:'50%'}}>
                                                    <Text style={{ color: 'white', }}>{strings.STOP_TIME} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                                    <TextInput
                                                        // left={20}
                                                        width={150}
                                                        placeholder={strings.SELECT_STOP_TIME}
                                                        placeholderTextColor={allValues.recurring ? 'white' : '#81889f'}
                                                        editable={false}
                                                        onChangeText={(text) => SetallValues({ ...allValues, ['end']: text })}
                                                        style={{ color: allValues.recurring ? 'white' : '#81889f', fontSize: 16, fontWeight:'bold', marginTop:20 }}
                                                        value={allValues.end}
                                                        onTouchStart={!allValues.enableCurrentTime ? () => { showDatePicker(false) } : ''}
                                                        borderBottomWidth={1}
                                                        borderColor={'#81889f'}
                                                    />
                                                    {endTimeErr && <Text style={{ color: '#fb7e7e' }}> {strings.STOP_TIME_CANNOT_GREATER}</Text>}
                                                </View>
                                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                                    <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.CURRENT_TIME}</Text>
                                                    <Switch style={[styles.headerText, { left: 30 }]}
                                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                        borderColor={'#81889f'}
                                                        borderWidth={1}
                                                        borderRadius={16}
                                                        thumbColor={"#fff"}
                                                        value={allValues.enableCurrentTime}
                                                        //onValueChange={val => SetallValues({ ...allValues, ['enableCurrentTime']: val })}
                                                        onValueChange={val => switchCurrentTime(val)}

                                                    />
                                                </View>
                                                <View style={{ marginTop: 20 }}>
                                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.DESCRIPTION}</Text>
                                                    <TextInput
                                                        style={{ width: 600, color: 'white', fontSize:16, fontWeight:'bold' }}
                                                        placeholder={strings.TYPE_DESCRIPTION_HERE}
                                                        placeholderTextColor={'#81889f'}
                                                        value={allValues.description}
                                                        onChangeText={val => SetallValues({ ...allValues, ['description']: val })}
                                                        borderBottomWidth={1}
                                                        borderColor={'#81889f'}
                                                    />
                                                </View>
                                            </View>
                                        {/* </View> */}
                                        <TouchableOpacity disabled={endTimeErr || startTimeErr} onPress={AddUpdateWellbeing} style={[styles.buttonStyle, { top: 30, backgroundColor: startTimeErr || endTimeErr ? '#81889f' : '#69c2d1' }]} activeOpacity={0.5}>
                                            <Text style={[styles.buttonTextStyle,]}>{type === "edit" ? (loadingText == '' ? strings.UPDATE : loadingText) : (loadingText == '' ? strings.SAVE : loadingText)}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </KeyboardAwareScrollView>
                            </Modal>
                            {/* Add Wellbeing End*/}
                            {/* Add Activity Start*/}
                            <Modal visible={visible2} onDismiss={hideModal2} contentContainerStyle={containerStyle1}>
                                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                                    <Text style={{ color: 'white', fontSize: 24, fontWeight:'bold' }}>{title}</Text>
                                    <View style={[styles.closeBtn, {marginTop:0,}]}>
                                        <TouchableOpacity onPress={hideModal2}>
                                            <Icon name="close" color="#81889f" size={20} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <KeyboardAwareScrollView
                                    contentContainerStyle={{ flexGrow: 1, flexDirection: 'row' }}
                                    enableOnAndroid={true} scrollEnabled>
                                    <View style={{ alignItems: 'center', marginTop: 0, }}>
                                        <View style={[styles.viewContainer, {alignSelf:'center'}]}>
                                        {allValues.inputType == 'image' && <>
                                            {/* Upload Image Start */}
                                            {!allValues.newimage && !allValues.image ?
                                            <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center' }]} >
                                                <Image
                                                    source={upload}
                                                    style={{ width: 78, height: 78, borderRadius: 10}}
                                                />
                                            </TouchableOpacity> :
                                            <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center' }]}>
                                                <Image
                                                    source={allValues.image ? {uri: config.apiUrl+allValues.image} : (allValues.newimage && allValues.filetype.split('/')[0] == 'image' ? {uri: allValues.newimage} : usericn) }
                                                    style={{ width: 220, height: 100, borderRadius: 10 }}
                                                />
                                                <TouchableOpacity style={{alignSelf:'center'}} onPress={() => SetallValues({ ...allValues, newimage: '', image:'' })} ><Icon name="close" color="#81889f" size={22} /></TouchableOpacity>
                                            </TouchableOpacity>
                                            }
                                            </>
                                        }
                                            {/* Upload Image End */}
                                            {/* Upload Video Start */}
                                        {allValues.inputType == 'video' && <>
                                            {!allValues.newimage && !allValues.image ?
                                            <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center' }]} >
                                                <Image
                                                    source={upload}
                                                    style={{ width: 78, height: 78, borderRadius: 10}}
                                                />
                                            </TouchableOpacity> :
                                            <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center' }]}>
                                                <Video
                                                    source={allValues.image ? {uri: config.apiUrl+allValues.image} : (allValues.newimage && allValues.filetype.split('/')[0] == 'video' ? {uri: allValues.newimage} : usericn) }
                                                    style={{ width: 220, height: 100, borderRadius: 10 }}
                                                    controls={true}
                                                />
                                                <TouchableOpacity style={{alignSelf:'center'}} onPress={() => SetallValues({ ...allValues, newimage: '', image:'' })} ><Icon name="close" color="#81889f" size={22} /></TouchableOpacity>
                                            </TouchableOpacity>
                                            }
                                            </>
                                            }
                                            {/* Upload Video End */}
                                            {/* {!allValues.newimage == '' && allValues.filetype.split('/')[0] == 'video' && <>
                                            <View style={{ marginVertical: 20 }}>
                                                <Video
                                                    source={{ uri: allValues.image != '' ? config.apiUrl+allValues.image : allValues.newimage }}
                                                    style={{ width: 200, height: 100, borderRadius: 10 }}
                                                    poster={config.apiUrl+"/images/logo-plain.png"}
                                                    controls={true}
                                                    onBuffer={videoBuffer}
                                                />
                                            </View>
                                            <TouchableOpacity onPress={() => SetallValues({ ...allValues, newimage: '' })} ><Icon name="close" color="#81889f" size={22} /></TouchableOpacity>
                                            </>
                                            } */}
                                            <View style={{ marginVertical: 20, }}>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.CHOOSE_FROM_AVAILABLY_ACTIVITY} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                                <SafeAreaView style={styles.getActivityCSS}>
                                                    <ScrollView style={{ alignSelf: 'flex-start', flexDirection: 'column', }}>
                                                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                                            {
                                                                getActivitylist()
                                                            }
                                                        </View>
                                                    </ScrollView>
                                                </SafeAreaView>
                                                <View style={{flexDirection:"row", height:40, marginTop:10}}>
                                                    <Text style={{color:'#81889f', marginTop:10,}}>{strings.IF_SOME_ACTIVITY_IS_MISSING}
                                                    </Text>
                                                    <TouchableOpacity onPress={() => { navigation.navigate('activityScreenStack', { open: true }); setVisible2(false) }} style={[styles.labelCSS, { justifyContent: 'center', backgroundColor: "#516b93", alignSelf: 'center', height:40 }]}>
                                                    <Text flex left style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', alignSelf: 'center' }} >{strings.ADD_NEW_ACTIVITY}
                                                    </Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <View style={{width:'50%'}}>
                                                <Text style={{ color: 'white', }}>{strings.START_TIME} <Text style={{ color: '#fb7e7e', marginTop: 8 }}> *</Text></Text>
                                                <TextInput
                                                    // left={20}
                                                    width={150}
                                                    placeholder={strings.SELECT_START_TIME}
                                                    placeholderTextColor={allValues.recurring ? 'white' : '#81889f'}
                                                    editable={false}
                                                    onChangeText={(text) => SetallValues({ ...allValues, ['start']: text })}
                                                    style={{ color: allValues.recurring ? 'white' : '#81889f', fontSize: 16, marginRight: 40, marginTop:20, fontWeight:'bold' }}
                                                    value={allValues.start}
                                                    onTouchStart={!allValues.enableCurrentTime ? () => { showDatePicker(true) } : ''}
                                                    borderBottomWidth={1}
                                                    borderColor={'#81889f'}
                                                />
                                                {startTimeErr && <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> {strings.START_TIME_CANN_SMALLER}</Text>}
                                            </View>
                                            <View style={{width:'50%'}}>
                                                <Text style={{ color: 'white', }}>{strings.STOP_TIME} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                                <TextInput
                                                    // left={20}
                                                    width={150}
                                                    placeholder={strings.SELECT_STOP_TIME}
                                                    placeholderTextColor={allValues.recurring ? 'white' : '#81889f'}
                                                    editable={false}
                                                    onChangeText={(text) => SetallValues({ ...allValues, ['end']: text })}
                                                    style={{ color: allValues.recurring ? 'white' : '#81889f', fontSize: 16, marginTop:20, fontWeight:'bold' }}
                                                    value={allValues.end}
                                                    onTouchStart={!allValues.enableCurrentTime ? () => { showDatePicker(false) } : ''}
                                                    borderBottomWidth={1}
                                                    borderColor={'#81889f'}
                                                />
                                                {endTimeErr && <Text style={{ color: '#fb7e7e' }}> {strings.STOP_TIME_CANNOT_GREATER}</Text>}
                                            </View>
                                            <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap",  width:'100%' }}>
                                                <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 0 }}>{strings.CURRENT_TIME}</Text>
                                                <Switch style={[styles.headerText, { left: 30 }]}
                                                    trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                    borderColor={'#81889f'}
                                                    borderWidth={1}
                                                    borderRadius={16}
                                                    thumbColor={"#fff"}
                                                    value={allValues.enableCurrentTime}
                                                    onValueChange={val => switchCurrentTime(val)}
                                                />
                                            </View>
                                            <View style={{ marginTop: 20, width:'100%' }}>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.DESCRIPTION}</Text>
                                                <TextInput
                                                    style={{ width: 600, color: 'white', fontSize:16, fontWeight:'bold' }}
                                                    placeholder={strings.TYPE_DESCRIPTION_HERE}
                                                    placeholderTextColor={'#81889f'}
                                                    value={allValues.description}
                                                    onChangeText={val => SetallValues({ ...allValues, ['description']: val })}
                                                    borderBottomWidth={1}
                                                    borderColor={'#81889f'}
                                                />
                                            </View>
                                        </View>
                                        <TouchableOpacity disabled={endTimeErr || startTimeErr} onPress={AddUpdateActivity} style={[styles.buttonStyle, { marginBottom: 20, top: 30, backgroundColor: startTimeErr || endTimeErr ? '#81889f' : '#69c2d1' }]} activeOpacity={0.5}>
                                            <Text style={styles.buttonTextStyle}>{type === "edit" ? (loadingText == '' ? strings.UPDATE : loadingText) : (loadingText == '' ? strings.SAVE : loadingText)}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </KeyboardAwareScrollView>
                            </Modal>
                            {/* Add Activity End*/}
                            {/* Add Medicine Start*/}
                            <Modal visible={visible1} onDismiss={hideModal1} contentContainerStyle={containerStyle1}>
                                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 50 }}>
                                    <Text style={{ color: 'white', fontSize: 24, fontWeight:'bold' }}>{title}</Text>
                                    <View style={styles.closeBtn}>
                                        <TouchableOpacity onPress={hideModal1}>
                                            <Icon name="close" color="#81889f" size={20} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                <KeyboardAwareScrollView
                                    contentContainerStyle={{ flexGrow: 1, flexDirection: 'row' }}
                                    enableOnAndroid={true} scrollEnabled>
                                    <View style={{ alignItems: 'center', marginTop: 30, }}>
                                        <View style={styles.viewContainer}>
                                            <View style={{ marginTop: 20 }}>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.CHOOSE_FROM_AVAILABLY_MEDICINE} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                                <SafeAreaView style={styles.getActivityCSS}>
                                                    <ScrollView style={{ alignSelf: 'flex-start', flexDirection: 'column', }}>
                                                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                                            {
                                                                getMedicinelist()
                                                            }
                                                        </View>
                                                    </ScrollView>
                                                </SafeAreaView>
                                                <View style={{flexDirection:"row", height:40, marginTop:10}}>
                                                    <Text style={{color:'#81889f', marginTop:10,}}>{strings.IF_SOME_ACTIVITY_IS_MISSING}
                                                    </Text>
                                                    <TouchableOpacity onPress={() => { navigation.navigate('medicineScreenStack', { open: true }); setVisible2(false) }} style={[styles.labelCSS, { justifyContent: 'center', backgroundColor: "#516b93", alignSelf: 'center', height:40 }]}>
                                                    <Text flex left style={{ color: 'white', fontWeight: 'bold', fontSize: 16, textAlign: 'center', alignSelf: 'center' }} >{strings.ADD_NEW_MEDICINE}
                                                    </Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                            <View style={{ width:'100%' }}>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.START_TIME} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                                
                                                <TextInput
                                                    width={95}
                                                    placeholder={strings.SELECT_TIME}
                                                    placeholderTextColor={allValues.recurring ? 'white' : '#81889f'}
                                                    editable={false}
                                                    onChangeText={(text) => SetallValues({ ...allValues, ['start']: text })}
                                                    style={{ color: allValues.recurring ? 'white' : '#81889f', fontSize: 16, marginRight: 40, marginTop:20, fontWeight:'bold' }}
                                                    value={allValues.start}
                                                    onTouchStart={!allValues.enableCurrentTime ? () => { showDatePicker(true) } : ''}
                                                    borderBottomWidth={1}
                                                    borderColor={'#81889f'}
                                                />
                                            </View>
                                            <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30, width:'100%' }}>
                                                <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.CURRENT_TIME}</Text>
                                                <Switch style={[styles.headerText, { left: 30 }]}
                                                    trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                    borderColor={'#81889f'}
                                                    borderWidth={1}
                                                    borderRadius={16}
                                                    thumbColor={"#fff"}
                                                    value={allValues.enableCurrentTime}
                                                    onValueChange={val => switchCurrentTime(val)}
                                                />
                                            </View>
                                            <View style={{ marginTop: 20, width:'100%' }}>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.DESCRIPTION}</Text>
                                                <TextInput
                                                    style={{ width: 600, color: 'white', fontSize:16, fontWeight:'bold' }}
                                                    placeholder={strings.TYPE_DESCRIPTION_HERE}
                                                    placeholderTextColor={'#81889f'}
                                                    value={allValues.description}
                                                    onChangeText={val => SetallValues({ ...allValues, ['description']: val })}
                                                    borderBottomWidth={1}
                                                    borderColor={'#81889f'}
                                                />
                                            </View>
                                        </View>
                                        <TouchableOpacity onPress={AddUpdateMedicine} style={[styles.buttonStyle, {  top: 10 }]} activeOpacity={0.5}>
                                            <Text style={styles.buttonTextStyle}>{type === "edit" ? (loadingText == '' ? strings.UPDATE : loadingText) : (loadingText == '' ? strings.SAVE : loadingText)}</Text>
                                        </TouchableOpacity>
                                    </View>

                                </KeyboardAwareScrollView>
                            </Modal>
                            {/* Add Medicine end*/}
                        </Portal>
                    </Provider>
                </View>
                {/* Add button start */}
                {
                    dateModal &&
                    <View style={{ width: 600, position: 'absolute', marginLeft: 300, marginTop: 270, zIndex: 9990 }}>
                        <DateTime
                            date={newcurrentDate}
                            changeDate={(date) => onChangeDate(date)}
                            format='YYYY-MM-DD'
                            renderChildDay={(day) => renderChildDay(day)}
                        />
                    </View>}
                {/* Add button end */}
                {/* Add button end */}
                <View style={{ width: 350, alignSelf: 'center', alignItems: 'center', zIndex: 9985 }}>
                    <Snackbar
                        style={{ width: 350, alignSelf: 'center', alignItems: 'center', }}
                        visible={visibleSnackbar}
                        duration={2000}
                        onDismiss={onDismissSnackBar}
                    >
                        {snackMsg}
                    </Snackbar>
                </View>
                <View>
                    <DateTimePickerModal
                        style={{ backgroundColor: '#2b3249', }}
                        headerTextIOS={'Select Time'}
                        textColor="white"
                        mode={'time'}
                        locale="en_GB"
                        // maximumDate={allValues.start}
                        isVisible={isDatePickerVisible}
                        minuteInterval={15}
                        is24Hour={true}
                        pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                        modalStyleIOS={{ width: 380, alignSelf: 'center' }}
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />
                </View>
            </View>
        )
    }
    if(count < 1) {
        setTimeout(() => {
            setCount(count + 1);
        }, 1000);
    }
    useEffect(() => getMedicineWheelData(), [newcurrentDate])
    useEffect(() => getActivityWheelData(), [newcurrentDate, count])
    useEffect(() => getWellbeingWheelData(), [newcurrentDate])
    useEffect(() => { onRefresh() }, [sessionUser])
    useEffect(() => { fetchActivityWheelList() }, [updateWheel]);
    useEffect(() => { fetchWellbeingWheelList() }, [updateWheel]);
    useEffect(() => { fetchMedicineWheelList() }, [updateWheel]);

    return (
        <View style={styles.mainBody}>
            <AwesomeAlert
                contentContainerStyle={{
                    backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 12,
                    },
                    shadowOpacity: 0.58,
                    shadowRadius: 16.00,

                    elevation: 24
                }}
                show={awesomeAlert}
                showProgress={false}
                title={<Text><View style={{ backgroundColor: popupColor, width: 20, height: 20, borderRadius: 20, }}></View> <Text style={{ fontWeight: '700', fontSize: 22, color: 'white', marginTop: -10, position: 'relative' }}>{popupSource} {strings.ALERT}</Text></Text>}
                message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                messageStyle={{ color: 'white', fontSize: 16, fontWeight: '600' }}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={strings.NO_CANCEL}
                confirmText={strings.YES_DELETE}
                cancelButtonColor="#516b93"
                confirmButtonColor="#69c2d1"
                confirmButtonBorderRadius={40}
                confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                onCancelPressed={() => {
                    setAwesomeAlert(false);
                }}
                onConfirmPressed={(id) => {
                    DeleteRow(popupId, popupIndex, popupSource);
                }}
            />
            <AwesomeAlert
                style={{ backgroundColor: "#ff0000" }}
                show={awesomeAlert1}
                showProgress={false}
                title={strings.DELETE_ALERT}
                message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={strings.NO_CANCEL}
                confirmText={strings.YES_DELETE}
                cancelButtonColor="#516b93"
                confirmButtonColor="#69c2d1"
                confirmButtonBorderRadius="0"
                onCancelPressed={() => {
                    setAwesomeAlert1(false);
                }}
                onConfirmPressed={(id) => {
                    alert("Asdf")
                }}
            />
            <View style={{ flex: 1, backgroundColor: COLORS.lightGray2 }}>
                <View style={{ backgroundColor: '#2b3249', height: '100%' }}>
                    {renderChart()}
                </View>

                {visible && <View style={{ flex: 1, position: 'absolute', bottom: 30, right: 100, }}>
                    <TouchableOpacity
                        style={{ flexDirection: 'row', flexWrap: 'wrap', backgroundColor: '#fb7e7e', paddingBottom: 0, width: 180, top: -15, marginLeft: 100, borderRadius: 40 }}
                        onPress={showModal2}
                    >
                        <Text style={styles.addTextBtn}>+ {strings.ADD_ACTIVITY}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flexDirection: 'row', flexWrap: 'wrap', backgroundColor: '#fb7e7e', paddingBottom: 0, width: 180, marginLeft: 50, borderRadius: 40 }} onPress={showModal1}>
                        <Text style={styles.addTextBtn}>+ {strings.ADD_MEDICINE}</Text>
                    </TouchableOpacity>
                    <TouchableOpacity style={{ flexDirection: 'row', flexWrap: 'wrap', backgroundColor: '#fb7e7e', paddingBottom: 0, top: 15, width: 180, marginLeft: 0, borderRadius: 40 }} onPress={showModal3}>
                        <Text style={styles.addTextBtn}>+ {strings.ADD_WELLBEING}</Text>
                    </TouchableOpacity>
                </View>}
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
    },
    shadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 2,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 3,
    },
    wheelInsideItems: {
        borderRadius: 60, height: 70, width: 70, position: 'relative', zIndex: 9979
    },
    icons: {
        width: 70, height: 70, alignSelf: 'center', marginTop: 10, top: -10
    },
    container: {
        flex: 1,
        color: "#fff",
        borderWidth: 1,
        borderColor: "#fff",
        width: 320,
        position: "absolute",
        left: 0
    },
    title: {
        padding: 16,
        fontSize: 20,
        textAlign: 'center',
        fontWeight: 'bold',
        color: "#fff"
    },
    rowTitle: {
        fontSize: 20,
        fontWeight: 'bold',
        color: "#e0e3ee",
        top: -10,
        // marginTop:20,
        // marginBottom:20,
        width: "70%",
    },
    rowActionBtn: {
        alignSelf: "flex-end",
        // alignItems:"flex-end",
        flexDirection: 'row',
        position: "relative",
        // justifyContent: 'flex-end'
    },
    innerCircle: {
        color: "#fff"
    },
    containerStyle: {
        flex: 1,
        marginHorizontal: 20,
        justifyContent: 'center',
    },
    textDescriptionStyle: {
        color: "#e0e3ee",
        fontSize: 18,
        // backgroundColor: "#3a4159",
        padding: 10,
        borderRadius: 10,
        // flexDirection:'column'
    },
    descriptionContainer: {
        // position:"relative",
        alignSelf: "flex-start",
        backgroundColor: "#3a4159",
        borderRadius: 10,
        // alignItems:"flex-end",
        flexDirection: 'column',
    },
    floatBtn: {
        position: 'absolute',
        height: 85,
        justifyContent: 'center',
        alignItems: "center",
        width: 85,
        borderRadius: 80,
        bottom: 0,
        right: 0,
        backgroundColor: '#eb8682',
        shadowColor: '#955555',
        shadowOpacity: 1, // IOS
        shadowRadius: 1,
        shadowColor: 'rgba(0,0,0, .4)', // IOS
        shadowOffset: { height: 1, width: 1 }
    },
    addTextBtn: {
        color: 'white', fontWeight: '700', fontSize: 16, padding: 10,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    buttonStyle: {
        alignSelf:'flex-end',
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 20,
        marginBottom: 20,
    },
    popupButtonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        position: 'absolute'
        // marginTop: 20,
        // marginBottom: 20,
    },
    labelCSS: {
        height: 40, borderRadius: 20, margin: 5, paddingLeft: 20, paddingRight: 20, color: '#fff', paddingTop: 0, textAlign: 'center',
    },
    closeBtn:{
        left: 350, marginTop: -20
    },
    viewContainer:{
        marginTop: 20, flexWrap: 'wrap', flexDirection: 'row', width: '80%'
    },
    getActivityCSS:{
        height: 100, alignSelf: 'center', borderWidth: 1, zIndex: 9985, borderRadius: 10, borderWidth: 2, borderColor: "#81889f", flexDirection: 'row', alignSelf: 'flex-start',
    },
    scrollView: {
        // flex: 1,
        flexDirection:'row',
        width:'100%',
        height:50,
        position:'relative',
        top:50,
        backgroundColor: 'pink',
        // alignItems: 'center',
        // justifyContent: 'center',
      },
})
const options = {
    container: {
      backgroundColor: '#FF0000',
      padding: 5,
      borderRadius: 5,
      width: 150,
      alignItems: 'center',
    //   position:'absolute',
      zindex:9999,
      height:40
    },
    text: {
      fontSize: 25,
      color: '#FFF',
      marginLeft: 7,
    },
  };

export default TimelineScreen;
