import React, { useState, useEffect } from 'react';
import { StyleSheet, Modal, Image, View, FlatList, Dimensions, TouchableOpacity, Alert, Text } from 'react-native';
import { useDispatch, useSelector } from 'react-redux'
// import * as ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import { FAB, Snackbar } from 'react-native-paper';
import ImageViewer from 'react-native-image-zoom-viewer';
import Icon from 'react-native-vector-icons/MaterialIcons';
import config from '../appconfig/config';
import Video from 'react-native-video';
import axios from 'axios'
import Loader from '../Components/Loader';
import ImageView from "react-native-image-viewing";
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';
import AwesomeAlert from 'react-native-awesome-alerts';

const formatData = (data, numColumns) => {
    if (data) {
        const numberOfFullRows = Math.floor(data.length / numColumns);
        let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
        while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
            data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
            numberOfElementsLastRow++;
        }
        return data;
    }
};



const numColumns = 4;
function LibraryScreen() {
    const [visible, setIsVisible] = useState(false);
    const [Index, setIndex] = useState(0);
    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const [libraryData, setLibraryData] = useState([]);
    const [Images, setImages] = useState([]);
    const [count, setCount] = useState([]);
    const [loading, setLoading] = useState(false);
    const onDismissSnackBar = () => setVisibleSnack(false);
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const showAlert = (id) => {
        setAwesomeAlert(true);
        setPopupId(id)
        // setPopupIndex(index)
    };

    const hideAlert = () => {
        setAwesomeAlert(false);
    };
    const handleChooseLibrary = (value) => {
        // alert(value)
        var options = {
            mediaType: 'mixed',
            quality: 1.0,
            mediaType: value
        };

        ImagePicker.openPicker({
            width: value == 'image' ? 300 : 'auto',
            height: value == 'image' ? 300 : 'auto',
            cropping: value == 'image' ? true : false,
            mediaType: value,
            // multiple: true,
        }).then(image => {
            console.log("cropped image::",image);
            // alert(image.sourceURL)
            // setUserData({ ...userData, filename: image.filename, filetype: image.mime, newimage: image.path, image:'', sourceURL:image.sourceURL, filesize:image.size, allimageData:image });
            // setUserData({ ...userData, filetype: image.mime });
            // setUserData({ ...userData, image: image.path });
            createFormData(image);
        });
        // ImagePicker.launchImageLibrary(options, response => {
        //     console.log("ImageVid Res:",response)
        //     if (response.didCancel) {
        //         console.log(strings.USER_CANCELLED_PHOTO_PICKER);
        //     } else if (response.error) {
        //         console.log(strings.IMAGE_PICKER_ERROR, response.error);
        //     } else if (response.customButton) {
        //         console.log(strings.USER_TAPPED_CCUSTOM_BUTTON, response.customButton);
        //     } else {
        //         console.log("data", response)
        //         let source = { uri: response.uri };
        //         createFormData(response);
        //     }
        // });
    }
    const handleChooseCamera = (value) => {
        var options = {
            mediaType: value,
            quality: 1.0,
        };

        ImagePicker.openCamera({
            width: value == 'image' ? 300 : 'auto',
            height: value == 'image' ? 300 : 'auto',
            cropping: value == 'image' ? true : false,
            mediaType: value,
        }).then(image => {
            console.log("cropped camera image::",image);
            // setUserData({ ...userData, filename: image.filename, filetype: image.mime, newimage: image.path, image:'', sourceURL:image.sourceURL, filesize:image.size, allimageData:image });
            createFormData(image);
        });
        // ImagePicker.launchCamera(options, response => {
        //     console.log("Image Res:",response)
        //     if (response.didCancel) {
        //         console.log('User cancelled photo picker');
        //     } else if (response.error) {
        //         console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //         console.log('User tapped custom button: ', response.customButton);
        //     } else {
        //         let source = { uri: response.uri };
        //         createFormData(response);
        //         // setUserData({ ...userData, image: 'data:image/jpeg;base64,' + response.base64 })
        //     }
        // });
    }

    const createFormData = (photo) => {
        setLoading(true);
        console.log("createFormData:", photo); 
        // return false;
        var fileType = photo.mime
        fileType = fileType.split('/')[0];
        // alert(fileType)
        const data = new FormData();
        data.append("file", {
            name: photo.filename ? photo.filename : 'videofile.mp4',
            type: fileType && fileType == 'image' ? photo.mime : 'video/mp4',
            uri:
                Platform.OS === "android" ? photo.sourceURL : photo.sourceURL.replace("file://", "")
        });
        if (sessionUser && sessionUser.userid) {
            data.append('userid', sessionUser.userid);
        }
        else {
            data.append('userid', currentUser.id);
        }
        data.append('appsecret', config.appsecret)
        data.append('base_url', config.apiUrl)
        data.append('action', 'addlibrary')
        headers = {
            'Content-Type': 'multipart/form-data',
        }
        console.log("data library:", data);
        axios.post(config.apiUrl + "library.php", data, headers).then(result => {
            console.log("data456:", result.data)
            if (result.data.status == 'success') {
                setLoading(false);
                setsnackMsg("Image uploaded successfully")
                setVisibleSnack(true)
            }else if(result.data.status == 'uploadError'){
                setsnackMsg("There is a problem uploading image, please try again")
                setVisibleSnack(true)
                setLoading(false);
            }else{
                setLoading(false);
                setsnackMsg(strings.PROBLEM_ADDING_DATA)
                setVisibleSnack(true)
            }
            getLibrary();
        }).catch(err => {
            console.log("error", err)
        })
    };

    const chooseOption = (value) => {
        Alert.alert(
            "",
            "Upload " + value + " from",
            [
                { text: strings.CHOOSE_FROM_LIBRARY, onPress: () => handleChooseLibrary(value) },
                { text: strings.TAKE_PICTURE, onPress: () => handleChooseCamera(value) },
                { text: strings.CANCEL, onPress: () => console.log("OK Pressed"), style: "cancel" }
            ]
        );
    }

    const deleteId = (id) => {
        showAlert(id)
    }

    // const deleteId = (id) => {
    //     Alert.alert(
    //         "",
    //         strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE,
    //         [
    //             { text: 'Delete', onPress: () => deleteLibrary(id) },
    //             { text: "Cancel", onPress: () => console.log("OK Pressed"), style: "cancel" }
    //         ]
    //     );
    // }


    useEffect(() => {
        getLibrary();
    }, [sessionUser]);

    const getLibrary = () => {
        setLoading(true);
        var dataToSend = {
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
            base_url:config.apiUrl,
            action: 'getlibrary',
            offset: 0
        };

        axios.post(config.apiUrl + "library.php", dataToSend).then(result => {
            setLoading(false);
            setLibraryData(result.data.data);
            console.log("result:=>", result.data); 
            const data = result.data.data;
            var dataArr = [];
            for (var i in data) {
                if (data[i].image !== undefined)
                    dataArr.push({ url: data[i].image })
            }
            setImages(dataArr)
            setCount(result.data.count);
        })
    }

    const deleteLibrary = (id) => {
        // alert(id); return false;
        setLoading(true);
        var dataToSend = {
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            addedby: sessionUser && sessionUser.userid ? currentUser.id : '',
            appsecret: config.appsecret,
            action: 'deleteimage',
            id: id
        };

        axios.post(config.apiUrl + "library.php", dataToSend).then(result => {
            setLoading(false);
            setsnackMsg(strings.IMAGE_DELETED_SUCCESSFULLY)
            setAwesomeAlert(false)
            setVisibleSnack(true)
            getLibrary();
        })
    }

    const videoBuffer = (isBuffer) => {
        console.log(isBuffer)
    }

    const uploadType = () => {
        Alert.alert(
            '',
            strings.UPLOAD_IMAGE_VIDEO,
            [
                {
                    text: strings.IMAGE,
                    onPress: () => {
                        chooseOption('image')
                    },
                },
                {
                    text: strings.VIDEO,
                    onPress: () => {
                        chooseOption('video')
                    },
                },
                {
                    text: strings.CANCEL,
                    onPress: () => {
                        return null;
                    },
                },
            ],
            { cancelable: false },
        );
    }
    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{ marginTop: 20, alignSelf:'center' }}>
                <Text style={{ color: '#ccc', fontSize: 18, fontWeight: 'bold' }}>{strings.NO_ITEMS_FOUND}</Text>
            </View>
        );
    };

    const renderItem = ({ item, index }) => {
        console.log("items to be displayed:",item)
        if (item.empty === true) {
            return <View style={[styles.item, styles.itemInvisible]} />;
        }
        if (!item.image.includes('mp4') && !item.image.includes('MP4')) {
            // console.log("images to be displayed:",item.image)
            return (
                <View key={index} style={styles.item} >
                    <View style={{ backgroundColor: '#eb8682', borderRadius: 20, padding: 5, marginTop: 5, right: 10, zIndex: 9999, position: 'absolute', top: 0, alignSelf: 'flex-end', }}>
                        <TouchableOpacity onPress={() => { deleteId(item.id) }}>
                            <Icon name="delete" color={'white'} size={24} />
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => { setIsVisible(true), setIndex(index) }}>
                        <Image style={{ height: 170, width: 230, borderRadius: 20 }} source={{ uri: item.image }} />
                    </TouchableOpacity>
                    <View style={{height:30, width:'100%', backgroundColor:'#000', opacity:0.5, borderBottomRightRadius:30, borderBottomLeftRadius:30, bottom:0, position:'absolute'}}>
                        <Text style={{color:"#fff", fontSize:16, fontFamily: 'HelveticaNeue-Bold', alignSelf:'center'}}>{item.eventmasterid ? 'Event' : ''} {item.activitymasterid ? 'Activity' : ''} {item.medicinemasterid ? 'Medicine' : ''}-{item.name}</Text>
                    </View>
                </View>

            );
        }
        else {
            // console.log("videos to be displayed:",item.image)
            return (
                <View key={index} style={styles.item} >
                    <View style={{ backgroundColor: '#eb8682', borderRadius: 20, padding: 5, marginTop: 5, right: 10, zIndex: 9999, position: 'absolute', top: 0, alignSelf: 'flex-end', }}>
                        <TouchableOpacity onPress={() => { deleteId(item.id) }}>
                            <Icon name="delete" color={'white'} size={24} />
                        </TouchableOpacity>
                    </View>
                    <Video
                        source={{ uri: item.image }}
                        style={{ width: 230, height: 170, borderRadius: 20 }}
                        // poster={config.apiUrl+"/images/logo-plain.png"}
                        controls={true}
                        onBuffer={videoBuffer}
                    />
                    <View style={{height:30, width:'100%', backgroundColor:'#000', opacity:0.5, borderBottomRightRadius:30, borderBottomLeftRadius:30, bottom:0, position:'absolute'}}>
                    <Text style={{color:"#fff", fontSize:16, fontFamily: 'HelveticaNeue-Bold', alignSelf:'center'}}>{item.eventmasterid ? 'Event' : ''} {item.activitymasterid ? 'Activity' : ''} {item.medicinemasterid ? 'Medicine' : ''}-{item.name}</Text>
                    </View>
                </View>
            )
        }
    };
    const hideModal4 = (key) => {
        setIsVisible(false)
    };
    // https://foobucketlambdanew.s3.us-east-2.amazonaws.com/QuestionMedia/5337.mp4
    return (
        <View style={styles.container}>
            <AwesomeAlert
                contentContainerStyle={{
                    backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 12,
                    },
                    shadowOpacity: 0.58,
                    shadowRadius: 16.00,

                    elevation: 24
                }}
                show={awesomeAlert}
                showProgress={false}
                title={strings.DELETE_RECORD}
                titleStyle={{ color: 'white' }}
                message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                messageStyle={{ color: 'white' }}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={strings.NO_CANCEL}
                confirmText={strings.YES_DELETE}
                cancelButtonColor="#516b93"
                confirmButtonColor="#69c2d1"
                confirmButtonBorderRadius="0"
                confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                onCancelPressed={() => {
                    setAwesomeAlert(false);
                }}
                onConfirmPressed={() => {
                    deleteLibrary(popupId)
                }}
            />
            <Loader loading={loading} />
            <FlatList
                showsVerticalScrollIndicator={false}
                data={formatData(libraryData, numColumns)}
                keyExtractor={(item, index) => index}
                style={styles.container}
                renderItem={renderItem}
                ListEmptyComponent={NoDataFound}
                numColumns={numColumns}
            />
            <Modal visible={visible} transparent={true} >
                <View style={styles.closeBtn}>
                    <TouchableOpacity onPress={hideModal4}>
                        <Icon name="close" color="#81889f" size={30} />
                    </TouchableOpacity>
                </View>
                <ImageViewer enableImageZoom={true} enablePreload={true} index={Index} useNativeDriver={true} onCancel={() => setIsVisible(false)} enableSwipeDown={true} onSwipeDown={() => setIsVisible(false)} imageUrls={Images} />
            </Modal>
            <FAB
                style={styles.floatBtn}
                color={'white'}
                theme={{ colors: { accent: '#eb8682' } }}
                icon="plus"
                onPress={() => uploadType()}
            >
            </FAB>
            <Snackbar
                style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                visible={visibleSnackbar}
                duration={3000}
                onDismiss={onDismissSnackBar}
            >
                {snackMsg}
            </Snackbar>

        </View>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        height: Dimensions.get('window').height,
    },
    col: {
        flex: 1,

    },
    floatBtn: {
        alignSelf: 'flex-end',
        zIndex: 1,
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    },
    item: {
        backgroundColor: '#555e7a',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        margin: 10,
        height: 170,
        borderRadius: 30

    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    itemText: {
        color: '#fff',
    },
    closeBtn:{
        left: '95%', top: 30, position:'absolute', zIndex:9993
    },
});

export default LibraryScreen;
