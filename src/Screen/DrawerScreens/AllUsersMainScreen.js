import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    Text,
    Alert,
    StyleSheet,
    RefreshControl,
    Button,
    FlatList,
    Keyboard,
    ActivityIndicator,
    KeyboardAvoidingView,
} from 'react-native';
import AdminScreen from './AllAdminScreen';
import HCPScreen from './AllHCPScreen';
import DoctorScreen from './AllDoctorScreen';
import PatientScreen from './AllPatientScreen';
import NurseScreen from './AllNurseScreen';
import AllpatientAcceptScreen from './AllpatientAcceptScreen';
import { medicineLoading } from '../redux/action'
import { useIsFocused } from '@react-navigation/native'
import * as ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { ColorPicker, fromHsv } from 'react-native-color-picker'
import upload from '../../../Image/upload-medicine.png'
import { bindActionCreators } from "redux";
import { medicineDataSource, allDataNull, medicineOffset, updateWheel } from '../redux/action'
import { connect } from "react-redux";
import moment from "moment";
import DropDownPicker from 'react-native-dropdown-picker';
import { Modal, Portal, Provider, FAB, Snackbar } from 'react-native-paper';
import deletebtn from '../../../Image/delete-btn.png'
import editbtn from '../../../Image/edit-btn.png'
import notificationbtn from '../../../Image/notification-btn.png'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import { ScrollView } from 'react-native-gesture-handler';
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const AllUsersMain = ({ navigation }) => {
    console.log("asdaddfdpp:", navigation)
    const [tab, setTab] = useState(1);
    const isFocused = useIsFocused()
    const currentUser = useSelector(state => state.auth.userLogin)
    const dispatch = useDispatch();
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const dataSource = useSelector(state => state.auth.medicineDataSource)
    const [dataSourceForList, setSourceForList] = useState([]);
    const offset = useSelector(state => state.auth.moffset)
    const isListEnd = useSelector(state => state.auth.medicineListLoading)
    const [refreshing, setRefreshing] = React.useState(false);
    const [dropValues, setDropDownList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [keyword, setKeyword] = useState('');
    const [title, setTitle] = useState(strings.ADD_MEDICINE);
    const [visible, setVisible] = React.useState(false);
    const [visible1, setVisible1] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [searchData, setsearchData] = useState('')
    const [check, setCheck] = useState('start');
    const [showAdmin, setShowAdmins] = useState(false);
    const [showHCP, setShowHCP] = useState(false);
    const [showDoctors, setShowDoctors] = useState(false);
    const [showPatients, setShowPatients] = useState(false);
    const [showNurse, setShowNurse] = useState(false);
    const [acceptedPatient, setShowAcceptedPatient] = useState(false);
    const [containerType, setContainerType] = useState(0);
    const showAlert = (id, index) => {
        setAwesomeAlert(true);
        setPopupId(id)
        setPopupIndex(index)
    };

    const hideAlert = () => {
        setAwesomeAlert(false);
    };

    const [type, setType] = useState('');
    const [errMsg, setErrMsg] = useState('');
    const [allValues, SetallValues] = useState({
        id: '',
        name: null,
        color: '',
        description: '',
        enablenotification: false,
        dose: null,
        timesperday: null,
        company: '',
        areaofuse: '',
        notes: '',
        activeingredients: '',
        recurring: false,
        enableStandardWheel: false,
        selecttime: '',
        image: ''
    });

    const [allValuesNotification, SetallNotificationValues] = useState({
        id: '',
        name: '',
        startDate: '',
        endDate: false,
        medicine: '',
        notification: '',
        expertise: '',
        address: '',
        time: [],
        areaofuse: '',
    });

    const setPermission = () => {
        // alert(currentUser.usertype)
        if (currentUser && currentUser.usertype === 'superadmin') {
            setShowAdmins(true);
            setShowHCP(true);
            setShowDoctors(true);
            setShowPatients(true);
            setShowNurse(true);
            setTab(1)
            setContainerType(0)
        }
        if (currentUser && currentUser.usertype === 'admin') {
            setShowAdmins(false);
            setShowHCP(true);
            setShowDoctors(true);
            setShowPatients(true);
            setShowNurse(true);
            setTab(2)
            setContainerType(1)
        }
        if (currentUser && currentUser.usertype === 'healthcareprovider') {
            setShowAdmins(false);
            setShowHCP(false);
            setShowDoctors(true);
            setShowPatients(true);
            setShowNurse(true);
            setTab(3)
            setContainerType(1)
        }
        if (currentUser && currentUser.usertype === 'doctor') {
            setShowAdmins(false);
            setShowHCP(true);
            setShowDoctors(false);
            setShowPatients(true);
            setShowNurse(true);
            setTab(2)
            setContainerType(2)
        }
        if (currentUser && currentUser.usertype === 'nurse') {
            setShowAdmins(false);
            setShowHCP(true);
            setShowDoctors(true);
            setShowPatients(true);
            setShowNurse(false);
            setTab(2)
            setContainerType(2)
        }
        if (currentUser && currentUser.usertype === 'patient') {
            setShowAdmins(false);
            setShowHCP(true);
            setShowDoctors(true);
            setShowPatients(false);
            setShowNurse(true);
            setShowAcceptedPatient(true)
            setTab(2)
            setContainerType(1)
        }
        if (currentUser && currentUser.usertype === '') {
            setShowAdmins(false);
            setShowHCP(true);
            setShowDoctors(true);
            setShowPatients(false);
            setShowNurse(true);
            setTab(2)
            setContainerType(1)
        }
    }
    useEffect(setPermission, []);
    const handleChooseLibrary = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                let source = { uri: response.uri };
                SetallValues({ ...allValues, image: 'data:image/jpeg;base64,' + response.base64 })
            }
        });
    }

    const handleChooseCamera = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };

        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                let source = { uri: response.uri };
                SetallValues({ ...allValues, image: 'data:image/jpeg;base64,' + response.base64 })
            }
        });
    }

    const chooseOption = () => {
        Alert.alert(
            "",
            strings.UPLOAD_IMAGE,
            [
                { text: strings.CHOOSE_FROM_LIBRARY, onPress: () => handleChooseLibrary() },
                { text: strings.TAKE_PICTURE, onPress: () => handleChooseCamera() },
                { text: strings.CANCEL, onPress: () => console.log("OK Pressed"), style: "cancel" }
            ]
        );
    }


    const onDismissSnackBar = () => setVisibleSnack(false);

    const showModal1 = (value) => {
        setVisible1(true);
        SetallNotificationValues({ ...allValuesNotification, ['medicine']: value });

    }
    const hideModal1 = () => {
        for (key in allValuesNotification) {
            if (allValuesNotification.hasOwnProperty(key)) {
                allValuesNotification[key] = null;
            }
        }
        setVisible1(false)

    };
    const showModal = () => {
        setVisible(true)
        setType('add')
    };
    const hideModal = (key) => {
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setVisible(false)
        setTitle(strings.ADD_MEDICINE)
        setType('add')

    };
    const [visibleColor, setVisibleColor] = React.useState(false);
    const showModalColor = () => setVisibleColor(true);
    const [time, setTime] = React.useState([]);
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const hideModalColor = () => setVisibleColor(false);
    const [index, setIndex] = useState();
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const containerStyle = { backgroundColor: '#2b3249', marginBottom: 25, padding: 10, alignItems: 'center', height: '100%', width: '100%', alignSelf: 'center', borderRadius: 0 };
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
        setDatePickerVisibility1(false);
    };
    const handleConfirm = (date) => {
        // SetallValues({ ...allValues, ['selecttime']: moment(date).format('HH:mm') })
        if (check === "start") {
            SetallNotificationValues({ ...allValuesNotification, ['prestartdate']: moment(date).format('DD/MM/YYYY'), ['startDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "end") {
            SetallNotificationValues({ ...allValuesNotification, ['preenddate']: moment(date).format('DD/MM/YYYY'), ['endDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "time") {
            SetallValues({ ...allValues, ['selecttime']: moment(date).format('HH:mm') })
        }
        if (check.includes("mul")) {
            let index = check.split(" ")[1];
            console.log("check", date)
            console.log("check", index)
            const list = [...time];
            list[index]['time'] = moment(date).format('HH:mm')
            setTime(list);
        }
        hideDatePicker();
    };

    const onEditMedicine = (data, index) => {
        allValues.id = data.id;
        setIndex(index);
        allValues.name = data.name;
        allValues.color = data.color;
        allValues.activeingredients = data.activeingredients;
        allValues.recurring = data.selecttime ? true : false;
        allValues.addtowheel = data.addtowheel == "1" ? true : false;
        allValues.notification = data.notification == "1" ? true : false;
        allValues.selecttime = data.selecttime;
        allValues.dose = data.dose;
        allValues.timesperday = data.timesperday;
        allValues.company = data.company;
        allValues.areaofuse = data.areaofuse;
        allValues.notes = data.notes;
        allValues.image = data.image;
        setTitle('Edit Medicine');
        setType('edit');
        setVisible(true);
    }


    const updatetoWheel = (id, val, index) => {
        setLoading(true)
        var data = {
            action: 'updatestandardwheelmedicine',
            userid: currentUser.id,
            appsecret: config.appsecret,
            standardwheel: val,
            id: id
        }
        dataSourceForList[index].addtowheel = !dataSourceForList[index].addtowheel;
        axios.post(config.apiUrl + "updatedata.php", data).then(result => {
            setLoading(false)
            if (result.data.status == 'success') {
                setsnackMsg(strings.ADD_TO_WHEEL_UPDATED_SUCCESSFULLY)
                setVisibleSnack(true)
                dispatch(updateWheel(val))
            }
        })
    }


    const AddUpdateMedicine = () => {
        if (allValues.name === '' || allValues.name === null) {
            return
        }
        if (allValues.dose === '' || allValues.dose === null) {
            return
        }
        if (allValues.timesperday === '' || allValues.timesperday === null) {
            return
        }
        setLoading(true)
        var data = allValues;
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            data.appsecret = config.appsecret;
        data.doses = data.dose;
        data.action = type == "edit" ? 'updatemedicine' : 'addmedicine';
        var filename = type == "edit" ? 'updatedata.php' : 'adddata.php';
        axios.post(config.apiUrl + filename, data).then(result => {
            setLoading(false)
            // console.log("result.data", result.data)
            if (result.data.status == 'success') {
                setLoading(false);
                hideModal();
                setType('add');
                setsnackMsg(type == "add" ? strings.MEDICINE_CREATE_SUCC : strings.MEDICINE_UPDATED_SUCC)
                setVisibleSnack(true)
                setVisible(false)
                onRefresh();
                dispatch(updateWheel(true));
            }
            else {
                setType('add');
                setVisible(false)
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC)
                setVisibleSnack(true)
            }

        })
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setsearchData('')
        Keyboard.dismiss()
        wait(2000).then(() => {
            dispatch(medicineOffset(0));
            setRefreshing(false);
            setVisible(false)
            setSourceForList([])
        });
    }, []);

    const AddNotification = () => {
        setLoading(true)
        // console.log("buton clicked", allValues)
        var data = allValuesNotification;
        data.patientid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            data.hcpid = 0;
        data.addedBy = currentUser.id;
        data.doctor = currentUser.id;
        data.appsecret = config.appsecret;
        data.areaexpertise = allValuesNotification.expertise
        data.prescriptionaddress = allValuesNotification.address
        data.enablePrescriptionNotification = allValuesNotification.notification
        data.dose = allValuesNotification.dose
        data.prestartdate = data.startDate
        data.preenddate = data.endDate
        data.time = time.map(x => x.time).join(',');
        data.action = 'addpatientmedicine';
        // alert("ASfd")
        console.log("notification::=>>", allValuesNotification);
        axios.post(config.apiUrl + 'adddata.php', data).then(result => {
            setLoading(false)
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                console.log("result", result.data)
                setLoading(false);
                hideModal1()
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC);
                setVisibleSnack(true)
            }
            else {
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC)
                setVisibleSnack(true)
            }
        })
    }

    const DeleteAlert = (id, index) => {
        showAlert(id, index)
        // Alert.alert(
        //     'Delete',
        //     'Are your sure you want to delete?',
        //     [
        //
        //         {
        //             text: 'No',
        //             onPress: () => { },
        //             style: 'cancel',
        //         },
        //         {
        //             text: 'Yes',
        //             onPress: () => DeleteMedicine(id, index)
        //         },
        //     ],
        //     { cancelable: false },
        // );
    }

    const DeleteMedicine = (id, index) => {
        setLoading(true)
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        data.appsecret = config.appsecret;
        data.id = id;
        data.action = 'deletemedicine'
        axios.post(config.apiUrl + 'deletedata.php', data).then(result => {
            setLoading(false)
            if (result.data.status == 'success') {
                dataSource.splice(index, 1);
                setVisible(false)
                setType('add');
                setsnackMsg(strings.MEDICINE_DELETED_SUCC)
                setAwesomeAlert(false)
                setVisibleSnack(true)
                dispatch(updateWheel(true));
            }
            else {
                setVisible(false)
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
        })
    }

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="black"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };


    const handleInputChange = (value, index) => {
        const list = [...time];
        list[index]['time'] = value;
        setTime(list);
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...time];
        list.splice(index, 1);
        setTime(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setTime([...time, { time: "" }]);
    };

    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{ marginTop: 20 }}>
                <Text style={{ color: '#ccc', fontSize: 18, fontWeight: 'bold' }}>{strings.NO_ITEMS_FOUND}</Text>
            </View>
        );
    };


    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.mainBody}>
                <AwesomeAlert
                    contentContainerStyle={{
                        backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                        shadowOffset: {
                            width: 0,
                            height: 12,
                        },
                        shadowOpacity: 0.58,
                        shadowRadius: 16.00,

                        elevation: 24
                    }}
                    show={awesomeAlert}
                    showProgress={false}
                    title={strings.DELETE_RECORD}
                    titleStyle={{ color: 'white' }}
                    message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                    messageStyle={{ color: 'white' }}
                    closeOnTouchOutside={true}
                    closeOnHardwareBackPress={false}
                    showCancelButton={true}
                    showConfirmButton={true}
                    cancelText={strings.NO_CANCEL}
                    confirmText={strings.YES_DELETE}
                    cancelButtonColor="#516b93"
                    confirmButtonColor="#69c2d1"
                    confirmButtonBorderRadius="0"
                    confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                    cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                    cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                    confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                    onCancelPressed={() => {
                        setAwesomeAlert(false);
                    }}
                    onConfirmPressed={() => {
                        DeleteMedicine(popupId, popupIndex);
                    }}
                />
                {/*Tab Container Start*/}
                <View style={{ flexDirection: 'row', alignSelf: 'center', justifyContent: 'center', textAlign: 'center', position: 'absolute', width: '100%', height: 80, top: 0 }}>
                    <View style={containerType === 0 ? styles.tabContainer : containerType === 2 ? styles.tabContainer2 : styles.tabContainer1}>
                        {/*Tabs Start*/}
                        {showAdmin && <View style={tab === 1 ? styles.weekTab : styles.monthTab}>
                            <TouchableOpacity onPress={() => setTab(1)} style={{ right: 0, top: 0 }} activeOpacity={0.5}>
                                <Text style={tab === 1 ? styles.weekText : styles.monthText}>{strings.ADMINS}</Text>
                            </TouchableOpacity>
                        </View>}

                        {showHCP && <View style={tab === 2 ? styles.weekTab : styles.monthTab}>
                            <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setTab(2)}>
                                <Text style={tab === 2 ? styles.weekText : styles.monthText}>{strings.HCPS}</Text>
                            </TouchableOpacity>
                        </View>}

                        {showDoctors && <View style={tab === 3 ? styles.weekTab : styles.monthTab}>
                            <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setTab(3)}>
                                <Text style={tab === 3 ? styles.weekText : styles.monthText}>{strings.DOCTORS}</Text>
                            </TouchableOpacity>
                        </View>}

                        {showPatients && <View style={tab === 4 ? styles.weekTab : styles.monthTab}>
                            <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setTab(4)}>
                                <Text style={tab === 4 ? styles.weekText : styles.monthText}>{strings.PATIENTS}</Text>
                            </TouchableOpacity>
                        </View>}

                        {showNurse && <View style={tab === 5 ? styles.weekTab : styles.monthTab}>
                            <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setTab(5)}>
                                <Text style={tab === 5 ? styles.weekText : styles.monthText}>{strings.NURSES}</Text>
                            </TouchableOpacity>
                        </View>}

                        {acceptedPatient && <View style={tab === 6 ? styles.weekTab : styles.monthTab}>
                            <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setTab(6)}>
                                <Text style={tab === 5 ? styles.weekText : styles.monthText}>{strings.PATIENTS}</Text>
                            </TouchableOpacity>
                        </View>}

                    </View>

                    {tab === 1 && <AdminScreen {...navigation} />}
                    {tab === 2 && <HCPScreen {...navigation} />}
                    {tab === 3 && <DoctorScreen {...navigation} />}
                    {tab === 4 && <PatientScreen {...navigation} />}
                    {tab === 5 && <NurseScreen {...navigation} />}
                    {tab === 6 && <AllpatientAcceptScreen {...navigation} />}
                </View>
                <Snackbar
                    style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                    visible={visibleSnackbar}
                    duration={3000}
                    onDismiss={onDismissSnackBar}
                >
                    {snackMsg}
                </Snackbar>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    tabContainer: {
        width: 700,
        height: 50,
        flexDirection: 'row',
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        top: 10,
        left: '14%',
        borderRadius: 50,
        backgroundColor: '#474f69',
    },
    tabContainer1: {
        width: 400,
        height: 50,
        flexDirection: 'row',
        // borderWidth:1,
        marginLeft: 160,
        marginRight: 160,
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        top: 10,
        left: '14%',
        borderRadius: 50,
        backgroundColor: '#474f69',
    },
    tabContainer2: {
        width: 300,
        height: 50,
        flexDirection: 'row',
        // borderWidth:1,
        marginLeft: 160,
        marginRight: 160,
        position: 'absolute',
        flex: 1,
        justifyContent: 'center',
        top: 10,
        left: '16%',
        borderRadius: 50,
        backgroundColor: '#474f69',
    },
    weekTab: {
        width: 150,
        // height:50,
        backgroundColor: '#69c2d1',
        fontSize: 18,
        borderRadius: 50
    },
    Date: {
        fontFamily: 'HelveticaNeue-Bold',
        width: 240,
        height: 45,
        backgroundColor: '#464e6a',
        margin: 5,
        borderRadius: 10,
        flexDirection: 'row'
    },
    weekText: {
        fontFamily: 'HelveticaNeue-Bold',
        color: '#fff',
        fontSize: 20,
        textAlign: 'center',
        paddingTop: 10,
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    monthTab: {
        fontFamily: 'HelveticaNeue-Bold',
        width: 150,
        // height:50,
        backgroundColor: '#474f69',
        fontSize: 18,
        borderRadius: 50,
    },
    monthText: {
        fontFamily: 'HelveticaNeue-Bold',
        color: '#fff',
        fontSize: 20,
        textAlign: 'center',
        paddingTop: 10,
        fontWeight: 'bold',
        textTransform: 'uppercase',
    },
    viewbtn: {
        fontFamily: 'HelveticaNeue-Bold',
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    headerText: {
        fontFamily: 'HelveticaNeue-Bold',
        color: 'white',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    medicine: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: 700,
        height: 50,
        borderRadius: 15,
        marginBottom: 17,
    },
    medicineText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    medicineColor: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        fontFamily: 'HelveticaNeue-Bold',
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        fontFamily: 'HelveticaNeue-Light',
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        fontFamily: 'HelveticaNeue-Light',
        alignSelf: 'flex-end',
        // position: 'absolute',
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8

    },
    imageBackStyle: {
        alignSelf: 'center',
        backgroundColor: '#39415b',
        borderRadius: 10,
        height: 100,
        left: 100,
        alignItems: 'center',
        width: 220

    },
    medicineColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        width: 18
    },
    // imageBackStyle: {
    //     // alignSelf: 'center',
    //     backgroundColor: '#39415b',
    //     borderRadius: 10,
    //     height: 100,
    //     alignItems: 'center',
    //     width: 200,
    //     marginLeft: 100

    // },

});


function mapStateToProps(state) {

    return {
        sessionUser: state.auth.requestedUser,
    };
}
// @ts-ignore
function matchDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(AllUsersMain);
