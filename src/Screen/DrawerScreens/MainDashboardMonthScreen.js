import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import {
  TextInput,
  Image,
  TouchableOpacity,
  View,
  Text,
  Alert,
  StyleSheet,
} from 'react-native';
import moment, { min } from "moment";
import axios from 'axios'
import Icon from 'react-native-vector-icons/MaterialIcons';
import { VictoryPie, VictoryChart, VictoryTheme, VictoryBar, VictoryLabel } from 'victory-native';
import Svg from 'react-native-svg';
import Loader from '../Components/Loader';
import userImg from '../../../Image/victor.png'
import config from '../appconfig/config';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const MainDashboardWeek = ({ navigation }) => {
  const currentUser = useSelector(state => state.auth.userLogin)
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = React.useState(true);
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [AsleepBarChartArray, setAsleepBarChartArray] = useState([]);
  const [AwakeBarChartArray, setAwakeBarChartArray] = useState([]);
  const [PainBarChartArray, setPainBarChartArray] = useState([]);
  const [UncomfortBarChartArray, setUncomfortBarChartArray] = useState([]);
  const [DistressDoughnut, setDistressDoughnut] = useState(0);
  const [SleepDoughnut, setSleepDoughnut] = useState(0);
  const [ComfortableDoughnut, setComfortableDoughnut] = useState(0);
  const [UncomfortDoughnut, setUnomfortableDoughnut] = useState(0);
  const [weeklyHour] = useState(720);
  const [SleepHours, setSleepHours] = useState("0:0");
  const [DistressHours, setDistressHours] = useState("0:0");
  const [ComfortHours, setComfortHours] = useState("0:0");
  const [UncomfortHours, setUncomfortHours] = useState("0:0");
  const [week, setWeek] = useState([]);
  const hours = (hh) => {
    if (hh !== undefined)
      return hh.split(":")[0];
  }
  const minutes = (mm) => {
    if (mm !== undefined)
      return mm.split(":")[1];
  }

  const showModal = () => {
    setVisible(true)
  };
  const hideModal = () => {
    setVisible(false)
  };

  useEffect(() => {
    appIntialize();
  }, [])



  const calulateTotalTime = (timeArray) => {
    const sum = timeArray.reduce((acc, time) => acc.add(moment.duration(time)), moment.duration());
    return [Math.floor(sum.asHours()), sum.minutes()].join(':')
  }


  const appIntialize = (date) => {
    var date = '01'
    var month = moment().format('MM-YYYY');
    var newDate = date + "-" + month;
    var currentWeek = parseInt(moment(newDate, 'DD/MM/YYYY').format('w')) - 1;
    console.log("current Week", currentWeek)
    setAsleepBarChartArray([]);
    setAwakeBarChartArray([]);
    setPainBarChartArray([]);
    setUncomfortBarChartArray([]);
    setSleepHours("0:0");
    setDistressHours("0:0");
    setComfortHours("0:0");
    setUncomfortHours("0:0");
    var week = [];
    // getDates(startOfWeek, endOfWeek)
    setLoading(true)
    var data = {};
    data.userid = currentUser.id;
    data.appsecret = config.appsecret;
    data.startOfMonth = month;
    data.action = 'getmonthlygraphdata';
    for (var i = 0; i < 4; i++) {
      var currentWeek = currentWeek + 1;
      week.push('W -' + currentWeek);
    }
    setWeek(oldArray => [...oldArray, week])
    console.log("weekQAZ",)
    console.log("week", week)
    axios.post(config.apiUrl + 'getgraphs.php', data).then(result => {
      setLoading(false)
      var data1 = result && result.data && result.data ? result.data : null;
      var chartData = data1.graphSumData ? data1.graphSumData : [];
      if (data1.status == "success") {
        if (chartData && chartData.length > 0) {
          var AwakeArray = [];
          var AsleepArray = [];
          var PainArray = [];
          var DistressArray = [];
          var SleepArray = [];
          var UncomfortArray = [];
          var UncomfortableArray = [];
          var ComfortArray = [];
          for (var i in chartData) {
            if (chartData[i].name == "Awake") {
              AwakeArray.push(chartData[i])

              ComfortArray.push(chartData[i].timediff)
            }
            if (chartData[i].name == "Asleep") {
              AsleepArray.push(chartData[i])
              SleepArray.push(chartData[i].timediff)
            }
            if (chartData[i].name == "Pain") {
              PainArray.push(chartData[i])
              DistressArray.push(chartData[i].timediff)
            }
            if (chartData[i].name == "Uncomfortable") {
              UncomfortArray.push(chartData[i])
              UncomfortableArray.push(chartData[i].timediff)
            }
          };
          var DistressHours = calulateTotalTime(DistressArray);
          setDistressHours(DistressHours)
          setDistressDoughnut(parseInt(DistressHours.split(":")[0]))
          var SleepHours = calulateTotalTime(SleepArray);
          setSleepHours(SleepHours)
          setSleepDoughnut(parseInt(SleepHours.split(":")[0]))
          var ComfortHours = calulateTotalTime(ComfortArray);
          setComfortHours(ComfortHours)
          setComfortableDoughnut(parseInt(ComfortHours.split(":")[0]))
          var UncomfortHours = calulateTotalTime(UncomfortableArray);
          setUncomfortHours(UncomfortHours);
          setUnomfortableDoughnut(parseInt(UncomfortHours.split(":")[0]))
          var Aindex = 0
          var Pindex = 0
          var Uindex = 0
          var Sindex = 0
          for (let i = 0; i <= 4; i++) {
            console.log("i", i)
            if (AsleepArray[Sindex] !== undefined) {
              if (i == parseInt(AsleepArray[Sindex].week)) {
                var tmphour = parseInt(AsleepArray[Sindex].timediff.split(":")[0])
                setAsleepBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: tmphour }])
                Sindex = Sindex + 1;
              }
              else {
                setAsleepBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: 0 }])
              }

            }
            else {
              setAsleepBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: 0 }])
            }
            if (PainArray[Pindex] !== undefined) {
              if (i == parseInt(PainArray[Pindex].week)) {
                var tmphour = parseInt(PainArray[Pindex].timediff.split(":")[0])
                setPainBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: tmphour }])
                Pindex = Pindex + 1;
              }
              else {
                setPainBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: 0 }])
              }
            }
            else {
              setPainBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: 0 }])
            }
            if (AwakeArray[Aindex] !== undefined) {
              if (i == parseInt(AwakeArray[Aindex].week)) {
                var tmphour = parseInt(AwakeArray[Aindex].timediff.split(":")[0])
                setAwakeBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: tmphour }])
                Aindex = Aindex + 1;
              }
              else {
                setAwakeBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: 0 }])
              }
            }
            else {
              setAwakeBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: 0 }])
            }
            if (UncomfortArray[Uindex] !== undefined) {
              if (i == parseInt(UncomfortArray[Uindex].week)) {
                var tmphour = parseInt(UncomfortArray[Uindex].timediff.split(":")[0])
                setUncomfortBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: tmphour }])
                Uindex = Uindex + 1;
              }
              else {
                setUncomfortBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: 0 }])
              }
            }
            else {
              setUncomfortBarChartArray(oldArray => [...oldArray, { x: week[i-1], y: 0 }])
            }
          }

        }
        else {
          AwakeBarChartArray = [{ x: week[0], y: 0 }, { x: week[1], y: 0 }, { x: week[2], y: 0 }, { x: week[3], y: 0 }, { x: week[4], y: 0 }];
          PainBarChartArray = [{ x: week[0], y: 0 }, { x: week[1], y: 0 }, { x: week[2], y: 0 }, { x: week[3], y: 0 }, { x: week[4], y: 0 }];
          AsleepBarChartArray = [{ x: week[0], y: 0 }, { x: week[1], y: 0 }, { x: week[2], y: 0 }, { x: week[3], y: 0 }, { x: week[4], y: 0 }];
          UncomfortBarChartArray = [{ x: week[0], y: 0 }, { x: week[1], y: 0 }, { x: week[2], y: 0 }, { x: week[3], y: 0 }, { x: week[4], y: 0 }];
        }

      }
    }, err => {
      loadingService.hide();
      var a = abc(a, b);
    });
  }
  console.log("As", AwakeBarChartArray)
  console.log("As", PainBarChartArray)
  console.log("As", AsleepBarChartArray)
  console.log("As", UncomfortBarChartArray)

  const graphRadius = 10;
  const domainPaddingX = 20;
  const chartWidth = 180;
  const chartHeight = 150;
  const dataWidth = 20;
  return (
    <View >
      <Loader loading={loading} />
      <View style={{ flexDirection: 'row', position: 'relative', top: 0, left: 0, alignItems: 'center', height: 50, width: '100%', flexDirection: 'row', padding: 0, margin: 0, }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={{ color: '#fff', fontSize: 16 }}>{strings.SORT_BY}</Text><Icon name="arrow-drop-down" size={22} color="#81889f" />
        </View>
        <View style={{ flexDirection: 'row', position: 'absolute', right: 0, }}>
          <View style={{ flexDirection: 'row' }}>
            <Icon name="circle" size={18} color="#6bb1d6" />
            <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.SLEEP}</Text>
          </View>
          <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
            <Icon name="circle" size={18} color="#afdb07" />
            <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.AWAKE_COMFORTABLE}</Text>
          </View>
          <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
            <Icon name="circle" size={18} color="#fad000" />
            <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.AWAKE_UNCOMFORTABLE}</Text>
          </View>
          <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
            <Icon name="circle" size={22} color="#e26060" />
            <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.AWAKE_DISTRESS}</Text>
          </View>
        </View>
      </View>
      {/*Info Container End*/}
      {/*Container Start*/}
      <View style={[styles.container, { borderRadius: 30, flexWrap: 'wrap', }]}>
        <View style={{ flexDirection: 'row', width: '100%', position: 'relative' }}>
          <View style={{ padding: 10 }}>
            <Image height={100} width={100} source={userImg} />
          </View>
          <View style={{ padding: 10 }}>
            <Text style={{ color: '#fff', fontSize: 22, fontWeight: 'bold' }}>Victor Elliot </Text>
            <Text style={{ color: '#aab2ce' }}>{strings.DIAGNOSIS}</Text>
            <View style={{ flexDirection: 'row', paddingTop: 10 }}>
              <Icon name="circle" size={22} color="#ef6d49" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.STATUS}</Text>
            </View>
            {!visible &&
              <View style={{ width: 90, height: 50, right: 20, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                <TouchableOpacity style={{ right: 0, backgroundColor: '#69c2d1', padding: 5, borderRadius: 25, height: 35, width: 150, top: 0 }} activeOpacity={0.5} onPress={showModal}>
                  <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold', alignSelf: 'center' }}>{strings.VIEW_MORE}</Text>
                </TouchableOpacity>
              </View>
            }
          </View>
          <View style={{ alignItems: 'center', flexDirection: 'row', marginTop: -30, marginLeft: -60 }}>
            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
              <VictoryPie
                data={[{ y: weeklyHour, label: " " }, { y: SleepDoughnut, label: " " }]}
                labels={(datum) => `${datum.y}`}
                radius={40}
                innerRadius={20}
                width={200}
                height={200}
                colorScale={['#6bb1d6', '#3d7c9e']}
                events={[{
                  target: "data",
                  eventHandlers: {
                    onPress: () => {
                      return [
                        {
                          target: "data",
                          mutation: ({ style }) => {
                            return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                          }
                        }, {
                          target: "labels",
                          mutation: ({ text }) => {
                            return text === "clicked" ? null : { text: "clicked" };
                          }
                        }
                      ];
                    }

                  }
                }]}
              />

              <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center', top: -45 }}>
                <Icon name="circle" size={22} color="#6bb1d6" />
                <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.SLEEP}-{SleepDoughnut}</Text>
              </View>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
              <VictoryPie
                data={[{ y: weeklyHour, label: " " }, { y: DistressDoughnut, label: " " }]}
                labels={(datum) => `${datum.y}`}
                radius={40}
                innerRadius={20}
                width={200}
                height={200}
                colorScale={['#fad000', '#967f0e']}
                events={[{
                  target: "data",
                  eventHandlers: {
                    onPress: () => {
                      return [
                        {
                          target: "data",
                          mutation: ({ style }) => {
                            return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                          }
                        }, {
                          target: "labels",
                          mutation: ({ text }) => {
                            return text === "clicked" ? null : { text: "clicked" };
                          }
                        }
                      ];
                    }

                  }
                }]}
              />

              <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center', top: -45 }}>
                <Icon name="circle" size={22} color="#fad000" />
                <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.DISTRESS}-{DistressDoughnut}</Text>
              </View>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
              <VictoryPie
                data={[{ y: weeklyHour, label: " " }, { y: ComfortableDoughnut, label: " " }]}
                labels={(datum) => `${datum.y}`}
                radius={40}
                innerRadius={20}
                width={200}
                height={200}
                colorScale={['#afdb07', '#728d0a']}
                events={[{
                  target: "data",
                  eventHandlers: {
                    onPress: () => {
                      return [
                        {
                          target: "data",
                          mutation: ({ style }) => {
                            return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                          }
                        }, {
                          target: "labels",
                          mutation: ({ text }) => {
                            return text === "clicked" ? null : { text: "clicked" };
                          }
                        }
                      ];
                    }

                  }
                }]}
              />
              <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center', top: -45 }}>
                <Icon name="circle" size={22} color="#afdb07" />
                <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.COMFORTABLE}-{ComfortableDoughnut}</Text>
              </View>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center', }}>
              <VictoryPie
                data={[{ y: weeklyHour, label: " " }, { y: UncomfortDoughnut, label: " " }]}
                labels={(datum) => `${datum.y}`}
                radius={40}
                innerRadius={20}
                width={200}
                height={200}
                colorScale={['#e26060', '#793e3e']}
                events={[{
                  target: "data",
                  eventHandlers: {
                    onPress: () => {
                      return [
                        {
                          target: "data",
                          mutation: ({ style }) => {
                            return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                          }
                        }, {
                          target: "labels",
                          mutation: ({ text }) => {
                            return text === "clicked" ? null : { text: "clicked" };
                          }
                        }
                      ];
                    }

                  }
                }]}
              />
              <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center', top: -45 }}>
                <Icon name="circle" size={22} color="#e26060" />
                <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.UNCOMFORTABLE}-{UncomfortDoughnut}</Text>
              </View>
            </View>
          </View>
        </View>

        {visible &&
          <View style={{ flexDirection: 'row', width: '100%', flexWrap: "wrap", position: 'relative', top: 0, right: 0 }}>
            <View style={styles.innerGraph}>
              <VictoryChart
                theme={VictoryTheme.material}
                domainPadding={{ x: domainPaddingX }}
                width={chartWidth} height={chartHeight}
                padding={{ bottom: 30, }}
              >
                <VictoryBar
                  cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
                  style={{
                    data: {
                      fill: "#7aecff",
                      width: dataWidth
                    }
                  }}
                  data={AsleepBarChartArray}
                />
              </VictoryChart>
            </View>
            <View style={styles.innerGraph}>
              <VictoryChart
                theme={VictoryTheme.material}
                domainPadding={{ x: domainPaddingX }}
                width={chartWidth} height={chartHeight}
                padding={{ bottom: 30, }}
              >
                <VictoryBar
                  cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
                  style={{
                    data: {
                      fill: "#fad000",
                      width: dataWidth
                    }
                  }}
                  data={PainBarChartArray}
                />
              </VictoryChart>
            </View>
            <View style={styles.innerGraph}>
              <VictoryChart
                theme={VictoryTheme.material}
                domainPadding={{ x: domainPaddingX }}
                width={chartWidth} height={chartHeight}
                padding={{ bottom: 30, }}
              >
                <VictoryBar
                  cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
                  style={{
                    data: {
                      fill: "#afdb07",
                      width: dataWidth
                    }
                  }}
                  data={AwakeBarChartArray}
                />
              </VictoryChart>
            </View>
            <View style={styles.innerGraph}>
              <VictoryChart
                theme={VictoryTheme.material}
                domainPadding={{ x: domainPaddingX }}
                width={chartWidth} height={chartHeight}
                padding={{ bottom: 30, }}
              >
                <VictoryBar
                  cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
                  style={{
                    data: {
                      fill: "#e26060",
                      width: dataWidth
                    }
                  }}
                  data={UncomfortBarChartArray}
                />
              </VictoryChart>
            </View>

            <View style={{ width: 220, height: 100, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' }}>
              <View style={{ borderBottomWidth: 3, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
                <Text style={{ color: '#81889f', fontSize: 16 }}>SLEEP HOURS</Text>
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 160, marginTop: 8 }}>
                <VictoryPie
                  data={[{ y: weeklyHour, label: " " }, { y: SleepDoughnut, label: " " }]}
                  labels={(datum) => `${datum.y}`}
                  radius={20}
                  innerRadius={20}
                  width={40}
                  height={40}
                  innerRadius={10}
                  cornerRadius={0}
                  colorScale={['#6bb1d6', '#3d7c9e']}
                />
              </View>
              <View style={{ flexDirection: 'row', width: 100, position: 'absolute', top: 40, right: 60 }}>
                <Text style={{ fontSize: 32, fontWeight: 'bold', color: '#fff' }}>{hours(SleepHours)} </Text>
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20 }}> hrs </Text>
                <Text style={{ fontSize: 32, fontWeight: 'bold', color: '#fff' }}> {minutes(SleepHours)} </Text>
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20 }}> min</Text>
              </View>
            </View>
            <View style={{ width: 220, height: 100, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' }}>
              <View style={{ borderBottomWidth: 3, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
                <Text style={{ color: '#81889f', fontSize: 16 }}>DISTRESS HOURS</Text>
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 160, marginTop: 8 }}>
                <VictoryPie
                  data={[{ y: weeklyHour, label: " " }, { y: DistressDoughnut, label: " " }]}
                  labels={(datum) => `${datum.y}`}
                  radius={20}
                  innerRadius={20}
                  width={40}
                  height={40}
                  innerRadius={10}
                  cornerRadius={0}
                  colorScale={['#fad000', '#967f0e']}
                />
              </View>
              <View style={{ flexDirection: 'row', width: 100, position: 'absolute', top: 40, right: 60 }}>
                <Text style={{ fontSize: 32, fontWeight: 'bold', color: '#fff' }}>{hours(DistressHours)} </Text>
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20 }}> hrs </Text>
                <Text style={{ fontSize: 32, fontWeight: 'bold', color: '#fff' }}> {minutes(DistressHours)} </Text>
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20 }}> min</Text>
              </View>
            </View>
            <View style={{ width: 220, height: 100, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' }}>
              <View style={{ borderBottomWidth: 3, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
                <Text style={{ color: '#81889f', fontSize: 16 }}>{strings.UNCOMFORTABLE_HOURS}</Text>
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 160, marginTop: 8 }}>
                <VictoryPie
                  data={[{ y: weeklyHour, label: " " }, { y: UncomfortDoughnut, label: " " }]}
                  labels={(datum) => `${datum.y}`}
                  radius={20}
                  innerRadius={20}
                  width={40}
                  height={40}
                  innerRadius={10}
                  cornerRadius={0}
                  colorScale={['#afdb07', '#728d0a']}
                />
              </View>
              <View style={{ flexDirection: 'row', width: 100, position: 'absolute', top: 40, right: 60 }}>
                <Text style={{ fontSize: 32, fontWeight: 'bold', color: '#fff' }}>{hours(UncomfortHours)} </Text>
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20 }}> hrs </Text>
                <Text style={{ fontSize: 32, fontWeight: 'bold', color: '#fff' }}> {minutes(UncomfortHours)} </Text>
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20 }}> min</Text>
              </View>
            </View>
            <View style={{ width: 220, height: 100, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' }}>
              <View style={{ borderBottomWidth: 3, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
                <Text style={{ color: '#81889f', fontSize: 16 }}>{strings.COMFORTABLE_HOURS}</Text>
              </View>
              <View style={{ alignItems: 'center', justifyContent: 'center', marginRight: 160, marginTop: 8 }}>
                <VictoryPie
                  data={[{ y: weeklyHour, label: " " }, { y: ComfortableDoughnut, label: " " }]}
                  labels={(datum) => `${datum.y}`}
                  radius={20}
                  innerRadius={20}
                  width={40}
                  height={40}
                  innerRadius={10}
                  cornerRadius={0}
                  colorScale={['#e26060', '#793e3e']}
                />
              </View>
              <View style={{ flexDirection: 'row', width: 100, position: 'absolute', top: 40, right: 60 }}>
                <Text style={{ fontSize: 32, fontWeight: 'bold', color: '#fff' }}>{hours(ComfortHours)} </Text>
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20 }}> {strings.HRS} </Text>
                <Text style={{ fontSize: 32, fontWeight: 'bold', color: '#fff' }}> {minutes(ComfortHours)} </Text>
                <Text style={{ fontSize: 12, fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20 }}> {strings.MIN}</Text>
              </View>
            </View>
          </View>

        }
        {visible &&
          <View style={{ width: '100%', height: 50, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
            <TouchableOpacity style={{ right: 0, backgroundColor: '#69c2d1', padding: 5, borderRadius: 25, height: 35, width: 150, top: 0 }} activeOpacity={0.5} onPress={hideModal}>
              <Text style={{ color: '#fff', fontSize: 18, fontWeight: 'bold', alignSelf: 'center' }}>{strings.VIEW_LESS}</Text>
            </TouchableOpacity>
          </View>
        }
      </View>
      {/*Container End*/}
    </View>
  );
};
const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: '#2b3249',
    padding: 10,
    alignContent: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabContainer: {
    width: 300,
    height: 50,
    flexDirection: 'row',
    // borderWidth:1,
    position: 'absolute',
    top: 10,
    left: 50,
    borderRadius: 50,
    backgroundColor: '#474f69',
  },
  weekTab: {
    width: 150,
    // height:50,
    backgroundColor: '#69c2d1',
    fontSize: 18,
    borderRadius: 50
  },
  weekText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 10,
    fontWeight: 'bold'
  },
  monthTab: {
    width: 150,
    // height:50,
    backgroundColor: '#474f69',
    fontSize: 18,
    borderRadius: 50
  },
  monthText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 10,
    fontWeight: 'bold',
  },
  middleDiv: {
    width: 400,
  },
  titleText: {
    color: '#fff',
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  rightDiv: {
    width: 300, flexDirection: "row", flexWrap: "wrap", position: 'absolute', top: 0, right: 0
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    marginTop: 10,
    margin: 10,
    width: 200,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#dadae8',
    textAlign: 'center'
  },
  inputStyle: {
    flex: 1,
    color: 'white',
    flexDirection: 'row',
    fontSize: 14,
    left: 8,
    paddingTop: 7
  },
  searchIcon: {
    paddingTop: 11,

  },
  container: {
    flexDirection: 'row', position: 'relative', top: 0, left: 0, alignItems: 'center', padding: 0, margin: 0, backgroundColor: '#22293e', width: '100%',
  },
  innerGraph: {
    width: 240, position: 'relative', alignItems: 'center', marginTop: 50
  }
});
export default MainDashboardWeek;
