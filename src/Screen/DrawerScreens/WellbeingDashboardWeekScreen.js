import React, { useState } from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';
import WeekScreen from './DashboardWeekScreen1'
import Monthlyscreen from './WellbeingDashboardMonthScreen'
import Quaterlyscreen from './WellBeingDashboardQuaterly'
import Yearlyscreen from './WellBeingDashboardYearly'
import DateTime from 'react-native-customize-selected-date'
import calendar from '../../../Image/calendar-icon3.png'
import Icon from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const WellbeingDashboard = ({ navigation }) => {

  const [tab, setTab] = useState(1);
  const [dateModal, setDateModal] = useState(false);
  const [currentDate, setDate] = useState(new Date());
  const onChangeDate = (date) => {
    console.log("date", date)
    setDate(date);
    setDateModal(false)
  }

  const renderChildDay = (day) => {
    if ((['2018-12-20'], day)) {
      return <Text></Text>
    }
    if ((['2018-12-18'], day)) {
      return <Text></Text>
    }
  }

  return (
    <View style={styles.mainBody}>
      {/*Tab Container Start*/}
      <View style={{ flexDirection: 'row', position: 'absolute', width: '100%', height: 80, top: 0, left: 0 }}>
        <View style={styles.tabContainer}>
            {/*Tabs Start*/}
            <View style={tab === 1 ? styles.weekTab : styles.monthTab}>
              <TouchableOpacity onPress={() => setTab(1)} style={{ right: 0, top: 0 }} activeOpacity={0.5}>
                <Text style={tab === 1 ? styles.weekText : styles.monthText}>{strings.WEEKLY}</Text>
              </TouchableOpacity>
            </View>
            <View style={tab === 2 ? styles.weekTab : styles.monthTab}>
              <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setTab(2)}>
                <Text style={tab === 2 ? styles.weekText : styles.monthText}>{strings.MONTHLY}</Text>
              </TouchableOpacity>
            </View>
            <View style={tab === 3 ? styles.weekTab : styles.monthTab}>
              <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setTab(3)}>
                <Text style={tab === 3 ? styles.weekText : styles.monthText}>{strings.QUARTERLY}</Text>
              </TouchableOpacity>
            </View>
            <View style={tab === 4 ? styles.weekTab : styles.monthTab}>
              <TouchableOpacity style={{ right: 0, top: 0 }} activeOpacity={0.5} onPress={() => setTab(4)}>
                <Text style={tab === 4 ? styles.weekText : styles.monthText}>{strings.YEARLY}</Text>
              </TouchableOpacity>
            </View>
            <TouchableOpacity activeOpacity={0.9} onPress={() => setDateModal(!dateModal)}>
              <View style={styles.Date}>
                <View style={styles.dateImg}>
                  <Image
                    source={calendar}
                    style={{ width: 30, height: 30, top: -1 }}
                  />
                </View>
                <Text style={{ fontSize: 24, top: 9, left: 10, letterSpacing: 2, fontWeight: '700', color: 'white' }}>
                  {moment(currentDate).format('DD.MM.YYYY')}
                </Text>
                <Text style={{ fontSize: 22, top: 1, left: 5 }}>
                  <Icon name="arrow-drop-down" color="#81889f" size={40} />
                </Text>
              </View>
            </TouchableOpacity>
        </View>


        {tab === 1 && <WeekScreen selectedDate={currentDate} />}
        {tab === 2 && <Monthlyscreen selectedDate={currentDate} />}
        {tab === 3 && <Quaterlyscreen selectedDate={currentDate} />}
        {tab === 4 && <Yearlyscreen selectedDate={currentDate} />}
      </View>
      {
        dateModal &&
        <View style={{ width: 600, position: 'absolute', marginRight: 100 }}>
          <DateTime
            date={currentDate}
            changeDate={(date) => onChangeDate(date)}
            format='YYYY-MM-DD'
            renderChildDay={(day) => renderChildDay(day)}
          />
        </View>
      }
    </View >
  );
};
const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: '#2b3249',
    padding: 10,
    alignContent: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  container: {
    flex: 1,
    zIndex: 9999
  },
  tabContainer: {
    width: 600,
    height: 50,
    flexDirection: 'row',
    // borderWidth:1,
    position: 'absolute',
    top: 10,
    left: 50,
    borderRadius: 50,
    backgroundColor: '#474f69',
  },
  weekTab: {
    width: 150,
    // height:50,
    backgroundColor: '#69c2d1',
    fontSize: 18,
    borderRadius: 50
  },
  Date: {
    width: 240,
    height: 45,
    backgroundColor: '#464e6a',
    margin: 5,
    borderRadius: 10,
    flexDirection: 'row'
  },
  weekText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 10,
    fontWeight: 'bold',
    textTransform:'uppercase',
  },
  monthTab: {
    width: 150,
    // height:50,
    backgroundColor: '#474f69',
    fontSize: 18,
    borderRadius: 50,
  },
  monthText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 10,
    fontWeight: 'bold',
    textTransform:'uppercase',
  },
  middleDiv: {
    width: 400,
  },
  titleText: {
    color: '#fff',
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  rightDiv: {
    width: 300, flexDirection: "row", flexWrap: "wrap", position: 'absolute', top: 0, right: 0
  },
  dateImg: {
    padding: 5,
    backgroundColor: '#74c2ce',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10

  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    marginTop: 10,
    margin: 10,
    width: 200,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#dadae8',
    textAlign: 'center'
  },
  inputStyle: {
    flex: 1,
    color: 'white',
    flexDirection: 'row',
    fontSize: 14,
    left: 8,
    paddingTop: 7
  },
  searchIcon: {
    paddingTop: 11,

  },
  container: {
    flexDirection: 'row', position: 'relative', top: 0, left: 0, alignItems: 'center', padding: 0, margin: 0, backgroundColor: 'transparent', width: '100%',
  },
  innerGraph: {
    width: 250, height: 120, position: 'relative', alignItems: 'center', marginTop: 0
  },
  registration: { width: 180, height: 110, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' },
  hours: { width: 220, height: 110, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' },
  average: { width: 150, height: 110, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' },
  h1: { fontSize: 32, fontWeight: 'bold', color: '#fff', alignItems: 'center' },
  h5: { fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 0, fontSize: 14 },
  hrs: { fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20, fontSize: 14 },
});
export default WellbeingDashboard;
