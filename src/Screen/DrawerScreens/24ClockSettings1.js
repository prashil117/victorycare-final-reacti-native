import React, { useState, useEffect } from 'react';
import {
    StyleSheet,
    View,
    Text,
    ScrollView,
    Image,
    TouchableOpacity,
    Picker,
    I18nManager,
} from 'react-native';
import RNRestart from 'react-native-restart';

import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng } from '../../helper/changeLng';

const ClockSettings = ({ navigation }) => {
    const [selectedValue, setSelectedValue] = useState('');
    useEffect(() => {
        selectedLng()
    }, [])

    const selectedLng = async() => {
        const lngData = await getLng()

        if (!!lngData || lngData != null) {
            strings.setLanguage(lngData)
            setSelectedValue(lngData)
        }
        console.log("selected Language data==>>>", lngData)
    }

    const onChangeLng = async(lng) => {
        console.log(lng)
        await I18nManager.forceRTL(false)
        setLng(lng)
        RNRestart.Restart()
        return;
    }
    return ( <
        View style = { styles.mainBody } > {
            /*visible && <View style={{position:'absolute', top:0, right:0 }}>
                          <Text style={styles.menuText}>English</Text>
                          <Text style={styles.menuText}>Norwegian</Text>
                          <Text style={styles.menuText}>Danish</Text>
                          <Text style={styles.menuText}>Swedish</Text>
                          <Text style={styles.menuText}>German</Text>
                          <Text style={styles.menuText}>Spanish</Text>
                          <Text style={styles.menuText}>French</Text>
                        </View>
                      */
        } <
        ScrollView >
        <
        TouchableOpacity style = { styles.menu } >
        <
        Text style = { styles.menuText } > { strings.LANGUAGE } < /Text> { /*<Text style={[styles.menuText, { left: 430 }]}>English</Text>*/ } <
        View style = { styles.card } >
        <
        Picker selectedValue = { selectedValue }
        style = {
            { height: 50, width: 150, borderWidth: 1, marginLeft: 400, top: -10, backgroundColor: '#2b3249', color: '#fff', fontSize: 25 } }
        onValueChange = {
            (itemValue, itemIndex) => onChangeLng(itemValue) }
        itemStyle = {
            { backgroundColor: "grey", color: "blue" } } >
        <
        Picker.Item label = "English"
        value = "en"
        style = {
            { fontSize: 22 } }
        /> <
        Picker.Item label = "Norwegian"
        value = "no" / >
        <
        Picker.Item label = "Danish"
        value = "da" / > {
            /*<Picker.Item label="Swedish" value="sv" />
                                    <Picker.Item label="German" value="de" />*/
        } <
        Picker.Item label = "Spanish"
        value = "es" / >
        <
        Picker.Item label = "French"
        value = "fr" / > { /*<Picker.Item label="Hindi" value="hi" />*/ } <
        /Picker> <
        /View> <
        /TouchableOpacity> <
        TouchableOpacity style = { styles.menu }
        onPress = {
            () => { navigation.navigate('SettingsWellbeingScreen') } } >
        <
        Text style = { styles.menuText } > { strings.EDIT_WELLBEING } < /Text> <
        /TouchableOpacity> <
        TouchableOpacity style = { styles.menu }
        onPress = {
            () => { navigation.navigate('SettingsMedicineScreen') } } >
        <
        Text style = { styles.menuText } > { strings.EDIT_MEDICINE } < /Text> <
        /TouchableOpacity> <
        TouchableOpacity style = { styles.menu }
        onPress = {
            () => { navigation.navigate('SettingsActivityScreen') } } >
        <
        Text style = { styles.menuText } > { strings.EDIT_ACTIVITY } < /Text> <
        /TouchableOpacity> <
        /ScrollView> <
        /View >
    );
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menu: {
        marginTop: 20,
        borderRadius: 15,
        marginBottom: 10,
        backgroundColor: '#39415b',
        height: 56,
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: 700,
        padding: 15,
    },
    menuText: {
        fontSize: 20,
        fontWeight: '600',
        color: 'white',
        marginLeft: 30
    }
});


export default ClockSettings;