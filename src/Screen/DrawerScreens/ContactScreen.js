import React, { useState, createRef, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux'
import {
    StyleSheet,
    TextInput,
    View,
    Text,
    ScrollView,
    Switch,
    Image,
    Alert,
    TouchableOpacity,
    Picker
} from 'react-native';
import { FAB, Provider, Modal, Portal, Snackbar } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { COLORS, FONTS, SIZES, icons, images } from '../../constants';
import deletebtn from '../../../Image/delete-btn.png'
import editbtn from '../../../Image/edit-btn.png'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import { DrawerActions } from '@react-navigation/native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import AwesomeAlert from 'react-native-awesome-alerts';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const ContactNotification = () => {

    const containerStyle = { backgroundColor: '#2b3249', padding: 10, marginBottom: 25, alignItems: 'center', width: '70%', alignSelf: 'center', borderRadius: 8 };
    const [loading, setLoading] = useState(false);
    const [contactData, setcontactData] = useState([]);
    const [userList, setUserList] = useState([]);
    const [sortType, setSortType] = useState('');
    const [isAscending, setIsAscending] = useState(true);
    const [isAscName, setIsAscName] = useState(true);
    const [isAscRelation, setisAscRelation] = useState(true);
    const [isAscAddress, setisAscAddress] = useState(true);
    const [isAscPhone, setisAscPhone] = useState(true);
    const [isAscStatus, setisAscStatus] = useState(true);
    const [emailErr, setEmailError] = useState('');
    const passwordInputRef = createRef();
    const [popupId, setPopupId] = useState('');
    const [popupIndex, setPopupIndex] = useState('');
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [searchClose, setSearchClose] = useState(false)
    const showAlert = (id, index, source) => {
        setAwesomeAlert(true);
    };

    const hideAlert = () => {
        setAwesomeAlert(false);
    };

    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)

    const [visible, setVisible] = React.useState(false);
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [type, setType] = useState('add');
    const showModal = () => setVisible(true);
    const hideModal = (key) => {
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setVisible(false);
    }
    const onDismissSnackBar = () => setVisibleSnack(false);
    const [count, setCount] = useState();
    const [allValues, SetallValues] = useState({
        name: '',
        email: '',
        invited_id: '',
        relationship: '',
        phone: '',
        address: '',
        notes: '',
        sendrequest:''
    });
    const [userEmail, setUserEmail] = useState('');
    const setEmail = (e) => {
        console.log("asdf:", e)
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (e === '' || reg.test(e) === false) {
            setUserEmail(e)
            setEmailError(strings.EMAIL_IN_PROPER_FORMAT)
        }
        else {
            console.log("eifelse", e)
            setUserEmail(e)
            setEmailError('')
        }
    }
    const clearEmailAddress = () => {
        // alert("ASdf")
        SetallValues({ ...allValues, ['email']: '' })
        setUserEmail('')
        setEmailError('')
    }
    const clearSearch = () => {
        // alert("ASdf")
        SetallValues({ ...allValues, ['email']: '' })
        setUserEmail('')
        setEmailError('')
    }
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    useEffect(() => { getContact() }, []);

    const getContact = () => {
        setLoading(true)
        var dataToSend = {
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            addedby: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
            keyword: '',
            action: 'getnextofkin',
            offset: 0
        };

        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
            console.log("data--",result.data.data)
            setLoading(false)
            setcontactData(result.data.data);
            setCount(result.data.count);
            return
        })
    }
    const AddUpdateContact = () => {
        setLoading(true)
        var data = allValues;
        data.action = type == "add" ? 'addcontact' : 'updatecontact';
        data.userid = currentUser.id;
        data.addeby = currentUser.id;
        data.appsecret = config.appsecret;
        data.addedby = currentUser.id;
        // console.log('contact-data:=>', data); return false
        var filename = type == "add" ? 'adddata.php' : 'updatedata.php';
        axios.post(config.apiUrl + filename, data).then(result => {
          console.log('server-data', result.data)
            setLoading(false)
            if (result.data.status == "success") {
                if (type == "add")
                    setsnackMsg(strings.CONTACT_ADDED_SUCCESSFULLY)
                else {
                    setsnackMsg(strings.CONTACT_UPDATED_SUCCESSFULLY)
                }
                setVisibleSnack(true)
                hideModal();
                setVisible(false)
                setType('add')
                getContact();
            }
            else {
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                hideModal();
            }
            return
        })
    }

    const DeleteAlert = (id, index) => {
        setPopupId(id);
        setPopupIndex(index);
        showAlert(id, index)
        // Alert.alert(
        //     'Delete',
        //     'Are your sure you want to delete?',
        //     [
        //         {
        //             text: 'No',
        //             onPress: () => { },
        //             style: 'cancel',
        //         },
        //         {
        //             text: 'Yes',
        //             onPress: () => DeleteContact(id, index)
        //         },
        //     ],
        //     { cancelable: false },
        // );
    }



    const DeleteContact = (id, index) => {
        setLoading(true)
        var data = {};
        data.userid = currentUser.id;
        data.appsecret = config.appsecret;
        data.id = id;
        data.action = 'deletecontact';
        axios.post(config.apiUrl + 'deletedata.php', data).then(result => {
            setLoading(false);
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                setsnackMsg(strings.CONTACT_DELETED_SUCCESSFULLY)
                getContact();
                setVisibleSnack(true)
                setAwesomeAlert(false)
            }
            else {
                setAwesomeAlert(false)
                setVisible1(false)
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }
        })
    }

    const onEdit = (data) => {
        console.log("data", data)
        SetallValues(data);
        showModal();
        setType('edit')
    }

    const onClickHeaderAscDesc = (type) => {

        var sorted

        if (isAscending) {
            sorted = [...contactData].sort((a, b) => {
                return b.type < a.type ? 1 : -1;
            });
            setIsAscending(false)
        } else {
            setIsAscending(true)
            sorted = [...contactData].sort((a, b) => {
                return b.type > a.type ? 1 : -1;
            });
        }

        setcontactData(sorted)

        setIsAscName(false)
        setisAscRelation(false)
        setisAscAddress(false)
        setisAscPhone(false)
        setisAscStatus(false)

        if (type === 'name') {
            if (isAscName) {
                setIsAscName(false)
            } else {
                setIsAscName(true)
            }
        }
        if (type === 'relationship') {
            if (isAscRelation) {
                setisAscRelation(false)
            } else {
                setisAscRelation(true)
            }
        } else if (type === 'address') {
            if (isAscAddress) {
                setisAscAddress(false)
            } else {
                setisAscAddress(true)
            }
        } else if (type === 'phone') {
            if (isAscPhone) {
                setisAscPhone(false)
            } else {
                setisAscPhone(true)
            }
        } else if (type === 'status') {
            if (isAscStatus) {
                setisAscStatus(false)
            } else {
                setisAscStatus(true)
            }
        }

    }
    const onRefresh = React.useCallback(() => {

        wait(2000).then(() => {
            setLoading(false);
            setVisible(false)
            getContact();
        });
    }, []);

    const getContactDataSearch = (key) => {
        setSearchClose(true)
        if (key == '') {
            onRefresh()
            return;
        }
        // setLoading(true);
        var dataToSend = {
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            addedby: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
            keyword: key,
            action: 'getnextofkin',
            offset: 0
        };

        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

            setLoading(false)
            setcontactData(result.data.data);
            console.log("data", result)
            setCount(result.data.count);
            return
        })

    }

    return (
        <View style={styles.mainBody}>
            <AwesomeAlert
                contentContainerStyle={{
                    backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 12,
                    },
                    shadowOpacity: 0.58,
                    shadowRadius: 16.00,

                    elevation: 24
                }}
                show={awesomeAlert}
                showProgress={false}
                title={strings.DELETE_RECORD}
                titleStyle={{ color: 'white' }}
                message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                messageStyle={{ color: 'white' }}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={strings.NO_CANCEL}
                confirmText={strings.YES_DELETE}
                cancelButtonColor="#516b93"
                confirmButtonColor="#69c2d1"
                confirmButtonBorderRadius={40}
                confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                onCancelPressed={() => {
                    setAwesomeAlert(false);
                }}
                onConfirmPressed={(id) => {
                    DeleteContact(popupId, popupIndex)
                }}
            />
            <Loader loading={loading} />
            <View style={{ ...styles.SectionStyle, alignSelf: 'flex-end', }}>
                <TextInput
                    style={styles.inputStyle}
                    placeholder={strings.TYPE_TO_SEARCH_HERE}
                    placeholderTextColor="#8b9cb5"
                    autoCapitalize="none"
                    onChangeText={(text) => { getContactDataSearch(text) }}
                    underlineColorAndroid="#f000"
                    blurOnSubmit={false}
                />
                <Icon style={styles.searchIcon} name="search" size={22} color="#81889f" />
                {/*<TouchableOpacity style={{ width: 22, height: 22, top: 5 }} onPress={() => clearSearch()}>
                    <Icon name="close" size={22} color={'#81889f'} />
                </TouchableOpacity>*/}
            </View>
            <View style={styles.main} >
                <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                    <View style={{ paddingTop: 45, marginLeft: 50, flexDirection: "row", flexWrap: "wrap" }}>

                        <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }}
                            onPress={() => onClickHeaderAscDesc('name')}>
                            <Text style={[styles.headerText, { width: 80, fontSize: 16, fontWeight: '600' }]}>{strings.NAME}</Text>
                            <Image source={icons.up_arrow} style={{ transform: isAscName ? [{ rotate: '0deg' }] : [{ rotate: '180deg' }], marginTop: 5, width: 14, height: 8, tintColor: "#81889f" }} />
                        </TouchableOpacity>

                        <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }}
                            onPress={() => onClickHeaderAscDesc('relationship')}>
                            <Text style={[styles.headerText, { width: 95, fontSize: 16, fontWeight: '600' }]}>{strings.RELATIONSHIP}</Text>
                            <Image source={icons.up_arrow} style={{ marginLeft: 15, transform: isAscRelation ? [{ rotate: '0deg' }] : [{ rotate: '180deg' }], marginTop: 5, width: 14, height: 8, tintColor: "#81889f" }} />
                        </TouchableOpacity>

                        <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }}
                            onPress={() => onClickHeaderAscDesc('address')}>
                            <Text style={[styles.headerText, { width: 120, fontSize: 16, fontWeight: '600' }]}>{strings.ADDRESS}</Text>
                            <Image source={icons.up_arrow} style={{ marginLeft: -20, transform: isAscAddress ? [{ rotate: '0deg' }] : [{ rotate: '180deg' }], marginTop: 5, width: 14, height: 8, tintColor: "#81889f" }} />
                        </TouchableOpacity>


                        <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }}
                            onPress={() => onClickHeaderAscDesc('phone')}>
                            <Text style={[styles.headerText, { width: 80, fontSize: 16, fontWeight: '600' }]}>{strings.PHONE_NUMBER}</Text>
                            <Image source={icons.up_arrow} style={{ transform: isAscPhone ? [{ rotate: '0deg' }] : [{ rotate: '180deg' }], marginTop: 5, width: 14, height: 8, tintColor: "#81889f" }} />
                        </TouchableOpacity>


                        <TouchableOpacity style={{ display: 'flex', flexDirection: 'row', justifyContent: "center", marginBottom: 17 }}
                            onPress={() => onClickHeaderAscDesc('status')}>
                            <Text style={[styles.headerText, { width: 60, fontSize: 16, fontWeight: '600' }]}>{strings.STATUS}</Text>
                            <Image source={icons.up_arrow} style={{ marginLeft: 10, transform: isAscStatus ? [{ rotate: '0deg' }] : [{ rotate: '180deg' }], marginTop: 5, width: 14, height: 8, tintColor: "#81889f" }} />
                        </TouchableOpacity>

                        <Text style={[styles.headerText, { width: 100, fontSize: 16, fontWeight: '600', marginBottom: 17 }]}>{strings.ACTION}</Text>

                    </View>
                </View>
                <ScrollView
                    keyboardShouldPersistTaps="handled"
                    showsVerticalScrollIndicator={false}
                    contentContainerStyle={{
                        justifyContent: 'center',
                        alignContent: 'center',
                    }}>
                    {contactData && contactData.length > 0 && contactData
                        .map(data => {
                            return (
                                <View key={data.id} style={[styles.contact, { marginLeft: 50 }]}>
                                    <Text style={{ ...styles.headerText, width: 150, fontSize: 16, marginLeft:0 }}>{data.name}</Text>
                                    <Text style={{ ...styles.headerText, width: 90, fontSize: 16 }}>{data.relationship}</Text>
                                    <Text style={{ ...styles.headerText, width: 160, fontSize: 16 }}>{data.address}</Text>
                                    <Text style={{ ...styles.headerText, width: 100, fontSize: 16 }}>{data.phone}</Text>
                                    <Text style={{ ...styles.headerText, width: 80, fontSize: 16 }}>{data.status}</Text>
                                    <View style={{ width: 100, left: 15, flexDirection: "row", flexWrap: "wrap" }}>
                                        <TouchableOpacity onPress={() => DeleteAlert(data.id)}>
                                            <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={() => onEdit(data)}>
                                            <Image style={styles.icons} height={35} width={35} source={editbtn} />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            )
                        })}
                    {!contactData && <Text style={{ color: 'white', alignItems: 'center', alignSelf: 'center', fontSize: 18 }}>No data found...</Text>}
                </ScrollView>
            </View>
            <Provider>
                <Portal>
                    <Modal visible={visible} onDismiss={() => setVisible(false)} contentContainerStyle={containerStyle}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            <Text style={{ color: 'white', fontWeight: '700', fontSize: 16, marginTop: 20 }}>{type == "add" ? strings.ADD_NEXT_OF_KIN : strings.EDIT_NEXT_OF_KIN}</Text>
                            <View style={{ left: 250, marginTop: 20 }}>
                                <TouchableOpacity onPress={hideModal}>
                                    <Icon name="close" color="#81889f" size={20} />
                                </TouchableOpacity>
                            </View>
                        </View>

                        <KeyboardAwareScrollView enableOnAndroid={true}>
                            <View style={{}}>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.NAME} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_NAME_HERE}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.name}
                                        onChangeText={val => SetallValues({ ...allValues, ['name']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                    <Text style={{ color: '#fb7e7e' }}>{allValues.name === '' ? strings.PLEASE_ENTER_PATIENTS_NAME : ''}</Text>
                                </View>
                                <View style={{ marginTop: 20, }}>
                                    <Text style={{ color: 'white', marginBottom: 8, }}>{strings.EMAIL} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white', borderBottomColor: emailErr != '' ? '#D26C67' : '#dadae8' }}
                                        onChangeText={(UserEmail) => {
                                            SetallValues({ ...allValues, ['email']: UserEmail })
                                            setEmail(UserEmail)
                                        }
                                        }
                                        placeholder={strings.TYPE_EMAIL_FOR_SHARING}
                                        placeholderTextColor={'#81889f'}
                                        autoCapitalize="none"
                                        keyboardType="email-address"
                                        returnKeyType="next"
                                        // editable={type = "edit" ? false : true}
                                        value={allValues.email}
                                        // onChangeText={val => SetallValues({ ...allValues, ['email']: val })}
                                        onSubmitEditing={() =>
                                            passwordInputRef.current &&
                                            passwordInputRef.current.focus()
                                        }
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                    {emailErr == '' && allValues.email !== '' ? <Icon style={{ top: 10, left: 400, position:'absolute' }} name="check" size={22} color="#4AC47F" /> : null}
                                    {userEmail !== '' ?
                                        <TouchableOpacity style={{ width: 22, height: 22, top: 10, left: 425, position:'absolute' }} onPress={() => clearEmailAddress()}>
                                            <Icon name="close" size={22} color={emailErr != '' ? "#D26C67" : "#81889f"} />
                                        </TouchableOpacity> : null}
                                </View>
                                <View>{emailErr != '' ? (
                                    <Text style={styles.errorTextStyle}>
                                        {emailErr}
                                    </Text>
                                ) : null}</View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.RELATIONSHIP} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_RELATIONSHIP}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.relationship}
                                        onChangeText={val => SetallValues({ ...allValues, ['relationship']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <Text style={{ color: '#fb7e7e' }}>{allValues.relationship === '' ? strings.TYPE_RELATIONSHIP : ''}</Text>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.PHONE_NUMBER} <Text style={{ color: '#fb7e7e', marginBottom: 8 }}> *</Text></Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.PLEASE_TYPE_PHONE_NUMBER}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.phone}
                                        onChangeText={val => SetallValues({ ...allValues, ['phone']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <Text style={{ color: '#fb7e7e' }}>{allValues.phone === '' ? strings.PLEASE_ENTER_PHONE_NUMBER : ''}</Text>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.NOTES}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_NOTES_HERE}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.notes}
                                        onChangeText={val => SetallValues({ ...allValues, ['notes']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.ADDRESS}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.typeaddres}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.address}
                                        onChangeText={val => SetallValues({ ...allValues, ['address']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8, width:130 }}>{strings.SEND_REQUEST}</Text>
                                    <Switch style={[styles.headerText, { left: -50, top:-5 }]}
                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                        borderColor={'#81889f'}
                                        borderWidth={1}
                                        borderRadius={16}
                                        thumbColor={"#fff"}
                                        value={allValues.sendrequest}
                                        onValueChange={val => SetallValues({ ...allValues, ['sendrequest']: val })}
                                    />
                                </View>
                                <TouchableOpacity onPress={() => AddUpdateContact()}
                                    style={[styles.buttonStyle, { alignItems: 'center', alignSelf: 'center', marginLeft: 0 }]}
                                    activeOpacity={0.5}
                                >
                                    <Text style={styles.buttonTextStyle}>{type == "add" ? strings.SAVE : strings.UPDATE}</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAwareScrollView>
                    </Modal>
                </Portal>
            </Provider>
            <Snackbar
                style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                visible={visibleSnackbar}
                duration={3000}
                onDismiss={onDismissSnackBar}
            >
                {snackMsg}
            </Snackbar>
            <FAB
                style={styles.floatBtn}
                fabStyle={{ height: 100 }}
                color={'white'}
                theme={{ colors: { accent: '#eb8682' } }}
                icon="plus"
                onPress={showModal}
            >
            </FAB>
        </View >
    );
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',

        alignItems: 'center',
    },
    main: {
        marginTop: 50,
        marginRight: 'auto',
    },
    headerText: {
        color: '#AEAEBD',
        fontWeight: '500',
        marginLeft: 50,
        textAlign: 'center',
        fontSize: 16,
    },
    icons: {
        marginLeft: 15,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    contact: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        height: 60,
        textAlign: 'left', alignSelf: 'stretch',
        marginBottom: 17,
        paddingTop: 10,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 15,
    },
    contactText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    contactColor: {
        backgroundColor: '#fb7e7e',
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    buttonStyle: {
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        backgroundColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        height: 40,
        top: 70,
        // margin: 10,
        right: 35,
        width: 200,
        justifyContent: 'center',
        textAlign: 'center'
    },
    nameText: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        right: 35,
        width: 200,
        justifyContent: 'center',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 16,
        left: 8,
        paddingTop: 7,
        fontFamily:'HelveticaNeue-Light',
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 50,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    }, errorTextStyle: {
        color: '#fb7e7e',
        textAlign: 'left',
        fontSize: 14,
        left: 12
    },

});


export default ContactNotification;
