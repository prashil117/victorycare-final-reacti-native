import React, { useState, useEffect } from 'react';
import { StyleSheet, TextInput, View, ScrollView, Image, Button, Keyboard, TouchableOpacity, KeyboardAvoidingView, Alert } from 'react-native';
// import * as ImagePicker from 'react-native-image-picker';
import ImagePicker from 'react-native-image-crop-picker';
import { useSelector } from 'react-redux'
import Loader from '../Components/Loader';
import RadioButton from '../Components/radioButton';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import AsyncStorage from '@react-native-community/async-storage';
import { Modal, Portal, Text, Provider, Snackbar } from 'react-native-paper';
import axios from 'axios'
import config from '../appconfig/config';
import Icon from 'react-native-vector-icons/MaterialIcons';
import upload from '../../../Image/upload-profile.png'
import RadioForm from 'react-native-simple-radio-button';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import DateTime from 'react-native-customize-selected-date';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';
import usericn from '../../../Image/nopreview.jpg';

const ViewEditProfile = ({navigation, route}) => {
    const [linkedusersdata] = useState(route.params)
    const { userid, userfirstname, userlastname, currusertype, editable } = route.params;
    console.log('navigation.userid:::5', navigation)
    const currentUser = useSelector(state => state.auth.userLogin)
    const editRight = currentUser && (currentUser.usertype == 'superadmin' || currentUser.usertype == 'admin' || currentUser.usertype == 'healthcareprovider') ? true : false
    // alert(editRight)
    const [visible, setVisible] = React.useState(false);
    const [modalVisible, setModalVisible] = useState(false);
    const [ModalErr, setModalErr] = useState(false);
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [snackMsg, setsnackMsg] = useState('');
    const [radioSex, setRadio] = useState();
    const showModal = () => setVisible(true);
    const hideModal = () => setVisible(false);
    const [userData, setUserData] = useState({
        firstname: '',
        lastname: '',
        birthdate: '',
        sex: '',
        email: '',
        mobile: '',
        street: '',
        city:'',
        state:'',
        zipcode:'',
        image: '',
        newimage: '',
        filename: '',
        filetype:'',
        sourceURL:'',
        filesize:'',
        allimageData: '',
        sm: 0

    })
    const [loading, setLoading] = useState(false);
    const [diagnosis, setInputDiagnosis] = useState([]);
    const [nextofKin, setNextofKin] = useState([]);
    const [allValues, SetallValues] = useState({
        name: '',
        email: '',
        relationship: '',
        phone: false,
        address: false,
        notes: false,
    });
    const [fnameErr, setFnameErrtext] = useState('');
    const [lnameErr, setLnameErrtext] = useState('');
    const [emailErr, setEmailErrtext] = useState('');
    const [mobileErr, setMobileErrtext] = useState('');
    const [bdErr, setBdErrtext] = useState('');
    const [streetErr, setStreetErrtext] = useState('');
    const [cityErr, setCityErrtext] = useState('');
    const [stateErr, setStateErrtext] = useState('');
    const [zipErr, setZipErrtext] = useState('');
    // console.log("setUserData", currentUser)
    const [value, setValue] = useState(0);
    const [date, setDate] = useState(new Date());
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const containerStyle = { backgroundColor: '#2b3249', padding: 10, marginBottom: 25, alignItems: 'center', width: '50%', alignSelf: 'center', borderRadius: 8 };

    const options = [
        {
            key: 'm',
            text: strings.MALE,
        }, {
            key: 'f',
            text: strings.FEMALE,
        },
    ]
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };
    const handleConfirm = (date) => {
        setUserData({ ...userData, ['birthdate']: date });
        hideDatePicker();
    };
    const renderChildDay = (day) => {
        if ((['2018-12-20'], day)) {
            return <Text></Text>
        }
        if ((['2018-12-18'], day)) {
            return <Text></Text>
        }
    }

    const handleChooseLibrary = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };

        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            // multiple: true,
        }).then(image => {
            console.log("cropped image::",image);
            // alert(image.sourceURL)
            setUserData({ ...userData, filename: image.filename, filetype: image.mime, newimage: image.path, image:'', sourceURL:image.sourceURL, filesize:image.size, allimageData:image });
            // setUserData({ ...userData, filetype: image.mime });
            // setUserData({ ...userData, image: image.path });
        });

        // ImagePicker.launchImageLibrary(options, response => {
        //     if (response.didCancel) {
        //         console.log('User cancelled photo picker');
        //     } else if (response.error) {
        //         console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //         console.log('User tapped custom button: ', response.customButton);
        //     } else {

        //         let source = { uri: response.uri };
        //         console.log("base64 url", response.base64)
        //         setUserData({ ...userData, image: 'data:image/jpeg;base64,' + response.base64 })
        //     }
        // });
    }
    const handleChooseCamera = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };

        ImagePicker.openCamera({
            width: 300,
            height: 300,
            cropping: true,
        }).then(image => {
            console.log("cropped camera image::",image);
            setUserData({ ...userData, filename: image.filename, filetype: image.mime, newimage: image.path, image:'', sourceURL:image.sourceURL, filesize:image.size, allimageData:image });
        });

        // ImagePicker.launchCamera(options, response => {
        //     if (response.didCancel) {
        //         console.log('User cancelled photo picker');
        //     } else if (response.error) {
        //         console.log('ImagePicker Error: ', response.error);
        //     } else if (response.customButton) {
        //         console.log('User tapped custom button: ', response.customButton);
        //     } else {
        //         let source = { uri: response.uri };

        //         setUserData({ ...userData, image: 'data:image/jpeg;base64,' + response.base64 })
        //     }
        // });
    }

    const chooseOption = () => {
        Alert.alert(
            "",
            "Upload Image",
            [
                { text: strings.CHOOSE_FROM_LIBRARY, onPress: () => handleChooseLibrary() },
                { text: strings.TAKE_PICTURE, onPress: () => handleChooseCamera() },
                { text: strings.CANCEL, onPress: () => console.log("OK Pressed"), style: "cancel" }
            ]
        );
    }

    // console.log("uswerfs", userData.image)
    const handleInputChange = (value, index) => {
        const list = [...diagnosis];
        list[index]['name'] = value;
        setInputDiagnosis(list);
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...diagnosis];
        list.splice(index, 1);
        setInputDiagnosis(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setInputDiagnosis([...diagnosis, { name: "", }]);
    };


    const handleInputChangeNext = (type, value, index) => {
        const list = [...nextofKin];
        if (type === "name")
            list[index]['name'] = value;
        if (type === "phone")
            list[index]['phone'] = value;

    };

    // handle click event of the Remove button
    const handleRemoveClickNext = index => {
        const list = [...nextofKin];
        list.splice(index, 1);
        setNextofKin(list);
    };

    // handle click event of the Add button
    const handleAddClickNext = () => {

        setNextofKin([...nextofKin, allValues]);
        var data = allValues;
        data.userid = route.params && route.params.userid ? route.params.userid : currentUser.id;
        data.addedby = route.params && route.params.userid ? route.params.userid : currentUser.id;
        data.appsecret = config.appsecret;
        data.action = 'addcontact';
        axios.post(config.apiUrl + "adddata.php", data).then(result => {
        }).catch(err => {
            console.log("error", err)
        })
        hideModal();
    };


    useEffect(() => {
        setLoading(true);
        getNextofKin();
        fetchCredentials();
    }, [currentUser]);

    const fetchCredentials = () => {
        var dataToSend = {
            userid: route.params && route.params.userid ? route.params.userid : currentUser.id,
            usertype: route.params && route.params.usertype ? route.params.usertype : currentUser.usertype,
            appsecret: config.appsecret,
            action: 'getuserbyid',
        };

        axios.post(config.apiUrl + "getuser.php", dataToSend).then(result => {
            setUserData({})
            setLoading(false);
            if (result.data.data) {
                // console.log("dadfasdf:", result.data.data)
                setUserData(result.data.data);

                if (result.data.data.sex === 'm') {
                    setRadio(0)
                }
                else {
                    // console.log("sfsdfsfsdf")
                    setRadio(1)
                }

            }
        }).catch(err => {
            setLoading(false);
            console.log("error", err)
        })
        var dataToSend2 = {
            userid: currentUser && currentUser.id ? currentUser.id : '',
            addedby: currentUser && currentUser.id ? currentUser.id : '',
            appsecret: config.appsecret,
            action: 'getdiagnosis',
        };
        setInputDiagnosis([])
        axios.post(config.apiUrl + "getdata.php", dataToSend2).then(result => {
            if (result.data.data.length > 0) {
                setLoading(false);
                setInputDiagnosis(result.data.data);
            }
            else {
                setInputDiagnosis([]);
            }
        }).catch(err => {
            setLoading(false);
            console.log("error", err)
        })
    }
    const getNextofKin = () => {
        var dataToSend1 = {
            userid: currentUser && currentUser.id ? currentUser.id : '',
            addedby: currentUser && currentUser.id ? currentUser.id : '',
            appsecret: config.appsecret,
            action: 'getnextofkin',
        };
        setNextofKin([])
        axios.post(config.apiUrl + "getdata.php", dataToSend1).then(result => {
            setLoading(false);
            if (result.data.data.length > 0) {
                setNextofKin(result.data.data)
            }
            else {
                setNextofKin([])
            }
        }).catch(err => {
            setLoading(false);
            console.log("error", err)
        })
    }
    const onSelect = (item) => {
        console.log("item", item)
        if (userData && userData.sex === item.key) {
            setUserData({ ...userData, ['sex']: item.key })
        } else {
            setUserData({ ...userData, ['sex']: item.key })
        }
    };



    const AddDiagnosis = () => {
        var data1 = {};
        data1.data = diagnosis;
        data1.userid = currentUser && currentUser.id ? currentUser.id : '';
        data1.addedby = currentUser && currentUser.id ? currentUser.id : '';
        data1.appsecret = config.appsecret;
        data1.action = 'adddiagnosis';
        // console.log("data1", data1)
        axios.post(config.apiUrl + "adddata.php", data1).then(result => {
            setLoading(false);
            // console.log("result", result.data)
        }).catch(err => {
            console.log("error", err)
        })
    }

    const onDismissSnackBar = () => setVisibleSnack(false);
    const updateUser = () => {
        if(editRight){
            if (userData.firstname == '' || userData.firstname == null) { setFnameErrtext(strings.FIRSTNAME_REQUIRED); return false;
            }else{ setFnameErrtext('') }

            if (userData.lastname == '' || userData.lastname == null) { setLnameErrtext(strings.LASTNAME_REQUIRED); return false; }else{ setLnameErrtext('') }

            if (userData.email == '' || userData.email == null) {
                setEmailErrtext(strings.EMAIL_REQUIRED); return false;
            }else{ setEmailErrtext('') }

            if (userData.birthdate == '' || userData.birthdate == null) { setBdErrtext(strings.PLEASE_ENTER_BIRTHDATE); return false; }else{ setBdErrtext('') }

            if (userData.mobile == '' || userData.mobile == null) { setMobileErrtext(strings.MOBILE_REQUIRED); return false; }else{ setMobileErrtext('') }

            if (userData.street == '' || userData.street == null) { setStreetErrtext(strings.STREET_REQUIRED); return false; 
            }else{ setStreetErrtext('') }

            if (userData.city == '' || userData.city == null) { setCityErrtext(strings.CITY_REQUIRED); return false; 
            }else{ setCityErrtext('') }

            if (userData.state == '' || userData.state == null) { setStateErrtext(strings.STATE_REQUIRED); return false;
            }else{ setStateErrtext('') }

            if (userData.zipcode == '' || userData.zipcode == null) { setZipErrtext(strings.ZIP_REQUIRED); return false;
            }else{ setZipErrtext('') }
          }
        setLoading(true);
        // var data = {};
        // data = userData;
        // data.userid = currentUser && currentUser.id ? currentUser.id : '';
        // data.appsecret = config.appsecret;
        // data.page = 'vieweditprofilescreen';
        // data.activity = 'updateuser';
        // data.action = 'updateuser';

        var data = new FormData();
        data.append('userid', userData.id);
        data.append('firstname', userData.firstname);
        data.append('lastname', userData.lastname);
        data.append('birthdate', userData.birthdate);
        data.append('sex', userData.sex);
        data.append('email', userData.email);
        data.append('mobile', userData.mobile);
        data.append('street', userData.street);
        data.append('city', userData.city);
        data.append('state', userData.state);
        data.append('zipcode', userData.zipcode);
        // data.append('userid',currentUser && currentUser.id ? currentUser.id : '');
        data.append('appsecret',config.appsecret);
        data.append('action','updateuser');
        data.append('base_url', config.apiUrl);
        data.append('page','vieweditprofilescreen');
        data.append('activity','updateuser');
        data.append('siblinguserid',currentUser && currentUser.id ? currentUser.id : '');
        // alert(currentUser.id);return false;
        if(userData.image != ''){
            data.append('image',userData.image);
        }else if(userData.newimage !== undefined){
            data.append('image',
            {
                uri:userData.newimage,
                name:userData.newimage,
            });
        }else{
            data.append('image','');
        }
        // alert(currentUser.id); return false;
        // console.log("data456:", data); return false;
        axios.post(config.apiUrl + "updateuser.php", data, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(result => {
            // alert("Adsf")
            console.log("data11::->", result.data)
            if (result.data.status === "success") {
                setsnackMsg(strings.PROFILE_UPDATED_SUCCESSFULLY)
                setVisibleSnack(true)
                // AsyncStorage.setItem('user', JSON.stringify(res));
                AddDiagnosis();
                // console.log("updated user", result.data)
                setLoading(false);
            }else if(result.data.status == 'uploadError'){
                setsnackMsg(strings.PROBLEM_UPLOADING_IMAGE)
                setVisibleSnack(true)
                setLoading(false);
            }else{
                setsnackMsg(strings.PROBLEM_ADDING_DATA)
                setVisibleSnack(true)
                setLoading(false);
            }
        }).catch(err => {
            setLoading(false);
            console.log("error", err)
        })
    }
    // console.log("sexRaduio", radioSex)

    return (
        <View style={styles.mainBody}>
            <Loader loading={loading} />
            <Text style={{ color: '#81889f' }}>{strings.YOU_CAN_EDIT_OR_FILLUP}</Text>
            <KeyboardAwareScrollView showsVerticalScrollIndicator={false} enableOnAndroid={true}>

                {/* <ScrollView
                style={styles.scrollView}
                contentContainerStyle={styles.contentContainer}>*/}
                <View>
                    <KeyboardAvoidingView enabled>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            <View>
                            {editable && <>
                            <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', fontSize: 14, top: 40, width: 200, }}>{strings.ADD_PHOTO}</Text>
                                {!userData.image && !userData.newimage ?
                                <TouchableOpacity onPress={chooseOption} style={[styles.imageBackStyle, { alignSelf: 'center' }]} >
                                    <Image
                                        source={upload}
                                        style={{ width: 78, height: 78, top: 30 }}
                                    />
                                </TouchableOpacity> :
                                <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center', top: -20 }]}>
                                    <Image
                                        source={{ uri: userData.image != '' ? config.apiUrl+userData.image : userData.newimage }}
                                        style={{ width: 220, height: 150, borderRadius: 10 }}
                                    />
                                    <TouchableOpacity onPress={() => setUserData({ ...userData, image: '' })} ><Icon name="close" color="#81889f" size={22} /></TouchableOpacity>
                                </TouchableOpacity>}
                            </>
                            }
                            {!editable && <>
                                {!userData.image && !userData.newimage ? 
                                    <Image
                                        source={usericn}
                                        style={{ width:200, height:200, borderRadius:100, alignSelf:'center' }}
                                    /> 
                                    :
                                    <Image
                                        source={{ uri: userData.image != '' ? config.apiUrl+userData.image : userData.newimage }}
                                        style={{ width:200, height:200, borderRadius:100, alignSelf:'center' }}
                                    />
                                }
                                </>
                            }
                                <View style={styles.labelSectionStyle}>
                                    <Text style={styles.label}>
                                        {strings.FIRST_NAME} 
                                    </Text>

                                </View>
                                <View style={styles.SectionStyle}>
                                    <TextInput
                                        style={styles.inputStyle}
                                        onChangeText={(name) => setUserData({ ...userData, ['firstname']: name })}
                                        value={userData.firstname}
                                        placeholder={strings.TYPE_YOUR_FIRSTNAME_HERE} //dummy@abc.com
                                        placeholderTextColor="#8b9cb5"
                                        autoCapitalize="none"
                                        underlineColorAndroid="#f000"
                                        blurOnSubmit={false}
                                        editable={editRight}
                                    />
                                </View>
                                {fnameErr != '' || fnameErr != null ? (<Text style={[styles.errorTextStyle, {top:0}]}> {fnameErr} </Text> ) : null}
                                <View style={styles.labelSectionStyle}>
                                    <Text style={styles.label}>
                                        {strings.LAST_NAME} 
                                    </Text>
                                </View>
                                <View style={styles.SectionStyle}>
                                    <TextInput
                                        style={styles.inputStyle}
                                        onChangeText={(name) => setUserData({ ...userData, ['lastname']: name })}
                                        value={userData.lastname}
                                        placeholder={strings.TYPE_YOUR_LASTNAME_HERE} //12345
                                        placeholderTextColor="#8b9cb5"
                                        underlineColorAndroid="#f000"
                                        editable={editRight}
                                    />
                                </View>
                                {lnameErr != '' || lnameErr != null ? (<Text style={[styles.errorTextStyle, {top:0}]}> {lnameErr} </Text> ) : null}
                                <View style={styles.labelSectionStyle}>
                                    <Text style={styles.label}>
                                        {strings.BIRTH_DATE} 
                                    </Text>
                                </View>
                                <View style={styles.SectionStyle}>
                                    <TextInput
                                        style={styles.inputStyle}
                                        placeholder={strings.BIRTH_DATE}
                                        editable={editRight}
                                        onTouchStart={editRight ? showDatePicker : ''}
                                        value={moment(userData.birthdate).format('DD.MM.YYYY')}
                                        placeholderTextColor="#8b9cb5"
                                        underlineColorAndroid="#f000"
                                        editable={editRight}
                                    />
                                </View>
                                {bdErr != '' || bdErr != null ? (<Text style={[styles.errorTextStyle, {top:0}]}> {bdErr} </Text> ) : null}
                                {/* <View>
                                    <DateTimePickerModal
                                        style={{ backgroundColor: '#2b3249', }}
                                        headerTextIOS={'Select Time'}
                                        textColor="white"
                                        date={new Date(userData.birthdate ? userData.birthdate : moment())}
                                        isVisible={isDatePickerVisible}
                                        pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                                        modalStyleIOS={{ width: 380, alignSelf: 'center' }}
                                        maximumDate={new Date()}
                                        mode="date"
                                        onConfirm={handleConfirm}
                                        onCancel={hideDatePicker}
                                    />
                                </View> */}
                                <View style={styles.labelSectionStyle}>
                                    <Text style={styles.label}>
                                        {strings.SEX} 
                                    </Text>
                                </View>
                                <View >
                                {editable && 
                                    <RadioButton
                                        selectedOption={userData.sex}
                                        onSelect={onSelect}
                                        options={options}
                                    />
                                }
                                {!editable && 
                                    <Text style={[styles.label, {top:20, left:10}]}>{userData.sex == 'm' ? 'Male' : 'Female'}</Text>
                                }
                                </View>
                                {/*userData.sex == '' && <Text style={{ color: '#fb7e7e', left: 10 }}>Please select sex</Text>*/}
                            </View>
                            <View style={{marginTop:50}}>
                                <View style={styles.labelSectionStyle}>
                                    <Text style={styles.label}>
                                        {strings.EMAIL} 
                                    </Text>
                                </View>
                                <View style={styles.SectionStyle}>
                                    <TextInput
                                        style={[styles.inputStyle, {color:'#aeaebd', fontWeight:'bold'}]}
                                        placeholder={strings.TYPE_EMAIL_FOR_SHARING} //12345
                                        editable={false}
                                        value={userData.email}
                                        placeholderTextColor="#8b9cb5"
                                        underlineColorAndroid="#f000"
                                        editable={editRight}
                                    />
                                </View>
                                <View style={styles.labelSectionStyle}>
                                    <Text style={styles.label}>
                                        {strings.MOBILE_NUMBER} 
                                    </Text>
                                </View>
                                <View style={styles.SectionStyle}>
                                    <TextInput
                                        style={styles.inputStyle}
                                        placeholder={strings.ENTER_MOBILE_NUMBER} //12345
                                        value={userData.mobile}
                                        placeholderTextColor="#8b9cb5"
                                        underlineColorAndroid="#f000"
                                        onChangeText={(value) => setUserData({ ...userData, ['mobile']: value })}
                                        editable={editRight}
                                    />
                                </View>
                                {mobileErr != '' || mobileErr != null ? (<Text style={[styles.errorTextStyle, {top:0}]}> {mobileErr} </Text> ) : null}
                                <View style={styles.labelSectionStyle}>
                                    <Text style={styles.label}>
                                        {strings.FULL_ADDRESS} 
                                    </Text>
                                </View>
                                {/* <View style={styles.SectionStyle}>
                                    <TextInput
                                        style={styles.inputStyle}
                                        placeholder={strings.TYPE_HERE_ADDRESS} //12345
                                        value={userData.street}
                                        onChangeText={(value) => setUserData({ ...userData, ['street']: value })}
                                        placeholderTextColor="#8b9cb5"
                                        underlineColorAndroid="#f000"
                                        editable={editRight}
                                    />
                                </View> */}
                                {editRight && <>
                                    <View style={styles.SectionStyle}>
                                        <TextInput
                                            style={styles.inputStyle}
                                            placeholder={strings.PLEASE_ENTER_STREET}
                                            placeholderTextColor={'#81889f'}
                                            editable={editRight}
                                            value={userData.street}
                                            onChangeText={val => setUserData({ ...userData, ['street']: val })}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    {streetErr != '' || streetErr != null ? (<Text style={[styles.errorTextStyle, {top:0}]}> {streetErr} </Text> ) : null}
                                    <View style={styles.SectionStyle}>
                                        <TextInput
                                            style={styles.inputStyle}
                                            placeholder={strings.PLEASE_ENTER_CITY}
                                            placeholderTextColor={'#81889f'}
                                            editable={editRight}
                                            value={userData.city}
                                            onChangeText={val => setUserData({ ...userData, ['city']: val })}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    {cityErr != '' || cityErr != null ? (<Text style={[styles.errorTextStyle, {top:0}]}> {cityErr} </Text> ) : null}
                                    <View style={styles.SectionStyle}>
                                        <TextInput
                                            style={styles.inputStyle}
                                            placeholder={strings.PLEASE_ENTER_STATE}
                                            placeholderTextColor={'#81889f'}
                                            editable={editRight}
                                            value={userData.state}
                                            onChangeText={val => setUserData({ ...userData, ['state']: val })}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    {stateErr != '' || stateErr != null ? (<Text style={[styles.errorTextStyle, {top:0}]}> {stateErr} </Text> ) : null}
                                    <View style={styles.SectionStyle}>
                                        <TextInput
                                            style={styles.inputStyle}
                                            placeholder={strings.PLEASE_ENTER_ZIP}
                                            placeholderTextColor={'#81889f'}
                                            editable={editRight}
                                            value={userData.zipcode}
                                            onChangeText={val => setUserData({ ...userData, ['zipcode']: val })}
                                            borderBottomWidth={2}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    {zipErr != '' || zipErr != null ? (<Text style={[styles.errorTextStyle, {top:0}]}> {zipErr} </Text> ) : null}
                                </>
                                }
                                {!editRight && <>
                                    <View style={[styles.SectionStyle, {alignSelf:'flex-start', width:'auto'}]}>
                                        {(userData.street == '' || userData.city == '' || userData.state == '' || userData.zipcode == '') && 
                                            <Text style={styles.inputStyle}>
                                                {strings.NO_ADDRESS_FOUND}
                                            </Text>
                                        }
                                        <Text>
                                        {userData.street && userData.street !== '' && <>
                                            <Text style={[styles.inputStyle, {marginTop:10, flex:0, justifyContent:'flex-start'}]}> {userData.street}</Text>
                                            </>
                                        }
                                        {userData.city && userData.city !== '' && <>
                                            <Text style={[styles.inputStyle, {marginTop:10, flex:0, justifyContent:'flex-start'}]}>{userData.city}</Text>
                                            </>
                                        }
                                        {userData.state && userData.state !== '' && <>
                                            <Text style={[styles.inputStyle, {marginTop:10, flex:0, justifyContent:'flex-start'}]}>{userData.state}</Text>
                                            </>
                                        }
                                        {userData.zipcode && userData.zipcode !== '' && <>
                                            <Text style={[styles.inputStyle, {marginTop:10, flex:0, justifyContent:'flex-start'}]}>{userData.zipcode}</Text>
                                            </>
                                        }
                                        </Text>
                                    </View>
                                </>
                                }
                                {editRight && <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignSelf: 'flex-start' }}>
                                    <TouchableOpacity
                                        style={styles.buttonStyle}
                                        activeOpacity={0.5}
                                        onPress={updateUser}
                                    >
                                        <Text style={styles.buttonTextStyle}>{strings.UPDATE}</Text>
                                    </TouchableOpacity>
                                </View>}
                            </View>
                        </View>
                    </KeyboardAvoidingView>
                </View>
            </KeyboardAwareScrollView>
            {/* </ScrollView> */}
            <Provider>
                <Portal>
                    <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                            <Text style={{ color: 'white', fontWeight: '700', fontSize: 16 }}>{strings.ADD_NEXT_OF_KIN}</Text>
                            <View style={{ left: 170 }}>
                                <TouchableOpacity onPress={hideModal}>
                                    <Icon name="close" color="#81889f" size={20} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <KeyboardAwareScrollView enableOnAndroid={true}>
                            <View style={{}}>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.TYPE_NAME_HERE}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_NAME_HERE}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.name}
                                        onChangeText={val => SetallValues({ ...allValues, ['name']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.EMAIL}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_EMAIL_FOR_SHARING}
                                        placeholderTextColor={'#81889f'}
                                        onChangeText={val => SetallValues({ ...allValues, ['email']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.RELATIONSHIP}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_RELATIONSHIP}
                                        placeholderTextColor={'#81889f'}
                                        onChangeText={val => SetallValues({ ...allValues, ['relationship']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.PHONE_NUMBER}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_RELATIONSHIP}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.description}
                                        onChangeText={val => SetallValues({ ...allValues, ['phone']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.NOTES}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_NOTES_HERE}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.description}
                                        onChangeText={val => SetallValues({ ...allValues, ['notes']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ color: 'white', marginBottom: 8 }}>{strings.ADDRESS}</Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white' }}
                                        placeholder={strings.TYPE_HERE_ADDRESS}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.description}
                                        onChangeText={val => SetallValues({ ...allValues, ['address']: val })}
                                        borderBottomWidth={1}
                                        borderColor={'#81889f'}
                                    />
                                </View>

                                <TouchableOpacity
                                    style={[styles.buttonStyle, { alignItems: 'center', alignSelf: 'center', marginLeft: 0 }]}
                                    activeOpacity={0.5}
                                    onPress={handleAddClickNext}
                                >
                                    <Text style={styles.buttonTextStyle}>{strings.SAVE}</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAwareScrollView>
                    </Modal>
                </Portal>
            </Provider>
            {isDatePickerVisible &&
                <View style={{ width: 600, position: 'absolute', marginLeft: 300, marginTop: 270 }}>
                    <DateTime
                        date={userData.birthdate ? moment(userData.birthdate) : moment()}
                        changeDate={(date) => handleConfirm(date)}
                        format='YYYY-MM-DD'
                        renderChildDay={(day) => renderChildDay(day)}
                    />
                </View>
            }
            <Snackbar
                style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                visible={visibleSnackbar}
                duration={3000}
                onDismiss={onDismissSnackBar}
            >
                {snackMsg}
            </Snackbar>
        </View >
    )
};

const styles = StyleSheet.create({
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        margin: 10,
        width: 420,
        justifyContent: 'center',
        textAlign: 'center'
    },

    imageBackStyle: {
        alignSelf: 'center',
        backgroundColor: '#39415b',
        borderRadius: 10,
        height: 150,
        alignItems: 'center',
        width: 220

    },
    labelSectionStyle: {
        fontFamily: 'HelveticaNeue-Light',
        flexDirection: 'row',
        marginTop: 25,
        top: 10,
        marginLeft: 13,
        fontSize: 18,
        width: 450,
        justifyContent: 'center',
        textAlign: 'center'
    },
    buttonStyle: {
        fontFamily: 'HelveticaNeue-Light',
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 25,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        fontFamily: 'HelveticaNeue-Light',
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    inputStyle: {
        fontFamily: 'HelveticaNeue-Light',
        flex: 1,
        color: 'white',
        paddingLeft: 5,
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        flexDirection: 'row',
    },
    registerTextStyle: {
        fontFamily: 'HelveticaNeue-Light',
        color: '#81889f',
        textAlign: 'center',
        fontSize: 16,
        alignSelf: 'center',
        padding: 10,
    },
    errorTextStyle: {
        fontFamily: 'HelveticaNeue-Light',
        color: '#fb7e7e',
        textAlign: 'left',
        fontSize: 14,
        left: 12
    },
    alertbox: {
        backgroundColor: '#000',
    },
    button: {
        backgroundColor: '#4ba37b',
        width: 100,
        borderRadius: 50,
        alignItems: 'center',
        marginTop: 100
    },
    label: {
        fontFamily: 'HelveticaNeue-Light',
        fontSize: 18,
        color: '#fff',
        flex: 1,
        flexDirection: 'row',
    },
    centeredView: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: 22
    },
    modalView: {
        margin: 20,
        backgroundColor: "white",
        borderRadius: 20,
        padding: 35,
        alignItems: "center",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    openButton: {
        backgroundColor: "#F194FF",
        borderRadius: 20,
        padding: 10,
        elevation: 2
    },
    textStyle: {
        color: "white",
        fontWeight: "bold",
        textAlign: "center"
    },
    modalText: {
        marginBottom: 15,
        textAlign: "center"
    },
    scrollView: {
        height: '100%',
        width: '100%',
        margin: 20,
        alignSelf: 'center',
        padding: 20,
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 50
    }
});


export default ViewEditProfile;
