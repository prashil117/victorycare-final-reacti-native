import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    Alert,
    StyleSheet,
    RefreshControl,
    ScrollView,
    Button,
    FlatList,
    Keyboard,
    ActivityIndicator,
} from 'react-native';
import DropDownPicker from 'react-native-dropdown-picker';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { ColorPicker, fromHsv } from 'react-native-color-picker'
import moment from "moment";
import { wellBeingDataSource, allDataNull, wellBeingOffset, wellBeingLoading, updateWheel } from '../redux/action'
import { Modal, Portal, Text, Provider, FAB, Snackbar } from 'react-native-paper';
import SnackBar from '../Components/snackbar'
import Icon from 'react-native-vector-icons/MaterialIcons';
import deletebtn from '../../../Image/delete-btn.png'
import editbtn from '../../../Image/edit-btn.png'
import notificationbtn from '../../../Image/notification-btn.png'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';


const WellBeingMaster = ({ navigation }) => {

    const currentUser = useSelector(state => state.auth.userLogin)
    const dispatch = useDispatch();
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const dataSource = useSelector(state => state.auth.wellBeingDataSource)
    const [dataSourceForList, setSourceForList] = useState([]);
    const offset = useSelector(state => state.auth.woffset)
    const isListEnd = useSelector(state => state.auth.wellBeingListLoading)
    console.log("isListEnd", isListEnd)
    const [loading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = React.useState(false);
    const [title, setTitle] = useState(strings.ADD_WELLBEING);
    const [visible, setVisible] = React.useState(false);
    const [visible1, setVisible1] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [searchData, setsearchData] = useState('')
    const showAlert = (id, index) => {
        setAwesomeAlert(true);
        setPopupId(id)
        setPopupIndex(index)
    };

    const hideAlert = () => {
        setAwesomeAlert(false);
    };
    const [dropValues, setDropDownList] = useState([]);
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isDatePickerVisible1, setDatePickerVisibility1] = useState(false);
    const [time, setTime] = React.useState([]);
    const [check, setCheck] = useState('start');
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const addAuditLog = (activity) => {
        // alert(activity); return false;
        var dataToSend = {
            action: 'insertlog',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            siblinguserid: currentUser && currentUser.id ? currentUser.id : '',
            page:'wellbeingmasterscreen',
            activity: activity,
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "auditlogs.php", dataToSend).then(result => {
            console.log("get audit logs:", result.data)
            if (result.data.status == 'success') {
                console.log("Log entered successfully!")
            }
        })
    }
    const [type, setType] = useState('');
    const [allValuesNotification, SetallNotificationValues] = useState({
        id: '',
        action: 'addpatientevent',
        prestartdate: '',
        preenddate: '',
        startDate: '',
        endDate: '',
        enablePrescriptionNotification: false,
        addtostandardwheel: false,
        event: null,
        recurringDaily: false,
    });

    const showModal1 = () => setVisible1(true);

    const hideModal1 = (key) => {
        for (key in allValuesNotification) {
            if (allValuesNotification.hasOwnProperty(key)) {
                allValuesNotification[key] = null;
            }
        }
        setVisible1(false)

    };

    const onDismissSnackBar = () => setVisibleSnack(false);
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }

    const handleInputChange = (value, index) => {
        const list = [...time];
        list[index]['time'] = value;
        setTime(list);
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...time];
        list.splice(index, 1);
        setTime(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setTime([...time, { time: "" }]);
    };

    const [allValues, SetallValues] = useState({
        id: '',
        name: null,
        color: '',
        description: null,
        notification: false,
        recurring: false,
        addtowheel: false,
        selecttime: ''
    });
    const showModal = () => setVisible(true);
    const hideModal = (key) => {
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setVisible(false)
        setTitle('Add WellBeing')
        setType('add')

    };
    const [visibleColor, setVisibleColor] = React.useState(false);
    const showModalColor = () => setVisibleColor(true);
    const hideModalColor = () => setVisibleColor(false);
    const [keyword, setKeyword] = useState('');
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
        setDatePickerVisibility1(false);
    };
    const handleConfirm = (date) => {
        if (check === "start") {
            SetallNotificationValues({ ...allValuesNotification, ['prestartdate']: moment(date).format('DD/MM/YYYY'), ['startDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "end") {
            SetallNotificationValues({ ...allValuesNotification, ['preenddate']: moment(date).format('DD/MM/YYYY'), ['endDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "time") {
            SetallValues({ ...allValues, ['selecttime']: moment(date).format('HH:mm') })
        }
        if (check.includes("mul")) {
            let index = check.split(" ")[1];
            const list = [...time];
            list[index]['time'] = moment(date).format('HH:mm')
            setTime(list);
        }

        hideDatePicker();
    };
    useEffect(() => {
        getDropDownList();
    }, [sessionUser]);

    const AddNotification = () => {
        setLoading(true)
        var data = allValuesNotification;
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            data.hcpid = 0;
        data.addedBy = currentUser.id;
        data.prestartdate = data.startDate
        data.preenddate = data.endDate
        data.appsecret = config.appsecret;
        data.startTime = time.map(x => x.time).join(',');;
        console.log("notification", data)
        axios.post(config.apiUrl + 'adddata.php', data).then(result => {
            setLoading(false)
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                dispatch(wellBeingOffset(0));
                setLoading(false);
                setVisible1(false);
                addAuditLog('notification added');
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC);
                setVisibleSnack(true)
                hideModal1()
                setTime([])
            } else {
                // setType('add');
                setLoading(false);
                addAuditLog('problem adding notification');
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
                hideModal1()
                setTime([])
            }
        })
    }

    const onEditWellBeing = (data) => {
        allValues.id = data.id;
        allValues.name = data.name;
        allValues.color = data.color;
        allValues.description = data.description;
        allValues.recurring = data.selecttime !== "" ? true : false;
        allValues.addtowheel = data.addtowheel == "1" ? true : false;
        allValues.notification = data.notification == "1" ? true : false;
        allValues.selecttime = data.selecttime;
        setTitle(strings.EDIT_WELLBEING);
        setVisible(true);
        setType('edit')
    }

    const containerStyle = { backgroundColor: '#2b3249', padding: 10, marginBottom: 25, alignItems: 'center', alignSelf: 'center', borderRadius: 8, width: '80%', height: '90%' };
    useEffect(() => { getWellBeingData() }, [sessionUser]);

    const getWellBeingData = () => {
        if (!isListEnd && keyword === '') {
            setLoading(true);
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                appsecret: config.appsecret,
                action: 'geteventmasterdata',
                keyword: '',
                offset: offset
            };
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                if (result.data.data0 !== null) {
                    if (offset === 0) {
                        dispatch(allDataNull())
                        setSourceForList([])
                    }
                    setLoading(false);
                    dispatch(wellBeingDataSource(result.data.data0));
                    dispatch(wellBeingOffset(offset + 10));
                    if (offset == 0)
                        setSourceForList(result.data.data0)
                    else
                        setSourceForList(dataSourceForList.concat(result.data.data0))
                }
                else {
                    dispatch(wellBeingLoading(true))
                    setLoading(false);
                }
            })
        }
    };


    const AddUpdateWellBeing = () => {
        if (allValues.name === '' || allValues.name === null) {
            return
        }
        if (allValues.description === '' || allValues.description === null) {
            return
        }
        setLoading(true)
        var data = allValues;
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        data.appsecret = config.appsecret;
        data.siblinguserid = sessionUser && sessionUser.userid ? currentUser.id : '';
        data.action = type == "edit" ? 'updateevent' : 'addevent';
        var filename = type == "edit" ? 'updatedata.php' : 'adddata.php';
        axios.post(config.apiUrl + filename, data).then(result => {
            if (result.data.status == 'success') {
                setLoading(false);
                setVisible(false);
                hideModal();
                onRefresh();
                // setType('add');
                addAuditLog(type == "add" ? 'wellbeing added' : 'wellbeing updated');
                setsnackMsg(type == "add" ? strings.WELLBEING_CREATED_SUCC : strings.WELLBEING_UPDATED_SUCC)
                setVisibleSnack(true)
                dispatch(updateWheel(true));
            }
            else {
                // setType('add');
                setVisible(false)
                addAuditLog(type == "add" ? 'problem adding wellbeing' : 'problem updating wellbeing');
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
                hideModal();
            }
        })
    }

    const DeleteAlert = (id, index) => {
        showAlert(id, index)
    }

    const DeleteWellBeing = (id, index) => {
        setLoading(true)
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
        data.appsecret = config.appsecret;
        data.id = id;
        data.action = 'deleteevent';
        data.siblinguserid = sessionUser && sessionUser.userid ? currentUser.id : '';
        axios.post(config.apiUrl + 'deletedata.php', data).then(result => {
            setLoading(false);
            if (result.data.status == 'success') {
                dataSourceForList.splice(index, 1);
                setVisible(false)
                // setType('add');
                addAuditLog('wellbeing deleted');
                setsnackMsg(strings.WELLBEING_CREATED_SUCC)
                setAwesomeAlert(false)
                setVisibleSnack(true)
                dispatch(updateWheel(true));
            }
            else {
                setVisible(false)
                addAuditLog('problem deleting wellbeing');
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
        })
    }

    const getDropDownList = () => {
        var data = {
            action: 'geteventmasterdatafulllist',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "getdata.php", data).then(result => {
            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            value: val.id,
                            icon: () => <View style={{width:20, height:20, borderRadius:10, backgroundColor:val.color}} />,
                            label: val.name,
                        }
                        arr.push(ob)
                    })
                    setDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }

    const updatetoWheel = (id, val, index) => {
        setLoading(true)
        var data = {
            action: 'updatestandardwheelevent',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
            standardwheel: val,
            id: id
        }
        dataSourceForList[index].addtowheel = !dataSourceForList[index].addtowheel;
        axios.post(config.apiUrl + "updatedata.php", data).then(result => {
            setLoading(false)

            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                addAuditLog('update add to wheel');
                setsnackMsg(strings.ADD_TO_WHEEL_UPDATED_SUCCESSFULLY)
                setVisibleSnack(true)
                dispatch(updateWheel(val))
            }else{
                addAuditLog('problem updating data');
                setsnackMsg(strings.PROBLEM_UPDATING_DATA)
                setVisibleSnack(true)
                dispatch(updateWheel(val))
            }

        })
    }


    const searchKeyword = async (value) => {
        console.log("value", value)
        await setKeyword(value);
        await setLoading(false);
        await setsearchData(value)
        await dispatch(allDataNull());
        setRefreshing(false);
        dispatch(wellBeingOffset(0));
    }

    useEffect(() => {
        getWellBeingDataSearch()
    }, [keyword]);


    const getWellBeingDataSearch = () => {

        if (!loading) {
            setLoading(true);
            if (keyword == '') {
                Keyboard.dismiss()
            }
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                appsecret: config.appsecret,
                hcpid: 0,
                action: 'geteventmasterdata',
                keyword: keyword,
                offset: 0
            };
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                console.log("wellbeing result:", result.data.data0)
                if (result.data.data0 !== null && result.data.data0.length > 0) {
                    setLoading(false);
                    dispatch(allDataNull())
                    setSourceForList([])
                    dispatch(wellBeingDataSource(result.data.data0));
                    dispatch(wellBeingOffset(0));
                    setSourceForList(result.data.data0)
                }
                else {
                    dispatch(allDataNull())
                    setSourceForList([])
                    dispatch(wellBeingLoading(true))
                    setLoading(false);
                }
            })
        }
        else {
            setLoading(false);
        }
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setsearchData('')
        dispatch(wellBeingOffset(0));

        Keyboard.dismiss()
        wait(2000).then(() => {
            setRefreshing(false);
            setVisible(false)
            dispatch(allDataNull());
            setSourceForList([])
            getWellBeingData();
        });
    }, []);


    const onEditNotification = (data) => {
        // allValuesNotification.id = data.id;
        allValuesNotification.event = data.id;
        setVisible1(true);
    }
    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="white"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };
    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{ marginTop: 20 }}>
                <Text style={{ color: '#ccc', fontSize: 18, fontWeight: 'bold' }}>{strings.NO_RESULT_FOUND}</Text>
            </View>
        );
    };
    const ItemView = ({ item, index }) => {
        return (
            // Flat List Item
            <View key={item.id} style={styles.event}>
                <View style={[styles.eventColor, { backgroundColor: item.color }]}>
                </View>
                <Text style={styles.eventText}>{item.name}</Text>
                <View style={{ paddingTop: 10, flexDirection: "row", flexWrap: "wrap" }}>
                    <TouchableOpacity onPress={() => DeleteAlert(item.id, index)}>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onEditWellBeing(item)}>
                        <Image style={styles.icons} height={35} width={35} source={editbtn} />
                    </TouchableOpacity>
                    <Switch style={[styles.headerText, { left: 18 }]}
                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                        borderColor={'#81889f'}
                        borderWidth={2}
                        borderRadius={16}
                        thumbColor={"#fff"}
                        onValueChange={val => updatetoWheel(item.id, val, index)}
                        value={item.addtowheel == "1" ? true : false}
                    />
                    <TouchableOpacity style={{ left: 53, width: 34 }} onPress={() => onEditNotification(item)}>
                        <Image style={{ marginTop: -3, height: 34, width: 34 }} height={34} width={34} source={notificationbtn} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.mainBody}>
                <AwesomeAlert
                  contentContainerStyle={{
                      backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                      shadowOffset: {
                          width: 0,
                          height: 12,
                      },
                      shadowOpacity: 0.58,
                      shadowRadius: 16.00,

                      elevation: 24
                  }}
                  show={awesomeAlert}
                  showProgress={false}
                  title={strings.DELETE_RECORD}
                  titleStyle={{ color: 'white' }}
                  message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                  messageStyle={{ color: 'white' }}
                  closeOnTouchOutside={true}
                  closeOnHardwareBackPress={false}
                  showCancelButton={true}
                  showConfirmButton={true}
                  cancelText={strings.NO_CANCEL}
                  confirmText={strings.YES_DELETE}
                  cancelButtonColor="#516b93"
                  confirmButtonColor="#69c2d1"
                  confirmButtonBorderRadius="0"
                  confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                  cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                  cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                  confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                    onCancelPressed={() => {
                        setAwesomeAlert(false);
                    }}
                    onConfirmPressed={() => {
                        DeleteWellBeing(popupId, popupIndex);
                    }}
                />
                {/* <Loader loading={loading} /> */}
                <TouchableOpacity style={styles.viewbtn} onPress={() => navigation.navigate('WellBeingNotificationScreen')}>
                    <Text style={{ color: 'white', fontSize: 16, fontWeight: '600' }}>{strings.VIEW_ALL_DOSAGES}</Text>
                </TouchableOpacity>
                <View style={{ right: 105, flexDirection: "row", flexWrap: "wrap" }}>
                    <View style={styles.SectionStyle}>
                        <TextInput
                            style={styles.inputStyle}
                            placeholder={strings.TYPE_TO_SEARCH_HERE}
                            placeholderTextColor="#81889f"
                            autoCapitalize="none"
                            onChangeText={text => searchKeyword(text)}
                            underlineColorAndroid="#81889f"
                            blurOnSubmit={false}
                            value={searchData}
                        />
                        <Icon style={styles.searchIcon} name="search" size={22} color="#81889f" />
                    </View>
                    <View style={{ paddingTop: 45, left: 200, flexDirection: "row", flexWrap: "wrap" }}>
                        <Text style={styles.headerText}>{strings.DELETE}</Text>
                        <Text style={styles.headerText}>{strings.EDIT}</Text>
                        <Text style={styles.headerText}>{strings.ADD_TO_WHEEL}</Text>
                        <Text style={styles.headerText}>{strings.NOTIFY}</Text>
                    </View>
                </View>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={dataSourceForList}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={ItemView}
                    ListEmptyComponent={NoDataFound}
                    ListFooterComponent={renderFooter}
                    onEndReached={getWellBeingData}
                    onEndReachedThreshold={0.5}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />}
                />
                <Provider>
                    <Portal>
                        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                                <Text style={{ fontFamily:'HelveticaNeue-Bold', color: 'white', fontWeight: '700', fontSize: 22, }}>{title}</Text>
                                <View style={{ left: 300, marginTop: -10 }}>
                                    <TouchableOpacity onPress={hideModal}>
                                        <Icon name="close" color="#81889f" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>

                            <KeyboardAwareScrollView enableOnAndroid={true}>
                                <View style={{ marginTop: 20, alignItems: 'center' }}>
                                    <View style={{ marginTop: 20 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.NAME_OF_WELLBEING} <Text style={{ color: "#e0675c", marginBottom: 8 }}> *</Text></Text>
                                        <TextInput
                                            style={{ width: 460, color: 'white', fontSize:16 }}
                                            placeholder={strings.MEDICINE_SEIZURE_HEADACHE}
                                            placeholderTextColor={'#81889f'}
                                            value={allValues.name}
                                            onChangeText={val => SetallValues({ ...allValues, ['name']: val })}
                                            borderBottomWidth={2}
                                            borderColor={'#81889f'}
                                        />
                                        <Text style={{ color: "#e0675c" }}>{allValues.name === '' ? strings.PLEASE_ENTER_WELLBEING : ''}</Text>
                                    </View>

                                    <View style={{ marginTop: 20, left: 10 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.CHOOSE_COLOR} <Text style={{ color: "#e0675c", marginBottom: 8 }}> *</Text></Text>
                                        <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                                            <View style={[styles.eventColorForm, { backgroundColor: allValues.color, marginBottom: 8, left:0 }]}>
                                            </View>
                                            <TextInput
                                                style={{ fontFamily:'HelveticaNeue-Light', width: 460, right: 20, fontSize:16, }}
                                                placeholder={'       '+strings.CHOOSE_COLOR}
                                                placeholderTextColor={'#81889f'}
                                                editable={false}
                                                onTouchStart={showModalColor}
                                                borderBottomWidth={2}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ marginTop: 20 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.DESCRIPTION}</Text>
                                        <TextInput
                                            style={{ fontFamily:'HelveticaNeue-Light', width: 460, color: 'white', fontSize:16 }}
                                            placeholder={strings.TYPE_DESCRIPTION_HERE}
                                            placeholderTextColor={'#81889f'}
                                            value={allValues.description}
                                            onChangeText={val => SetallValues({ ...allValues, ['description']: val })}
                                            borderBottomWidth={2}
                                            borderColor={'#81889f'}
                                        />
                                        <Text style={{ color: "#e0675c" }}>{allValues.description === '' ? 'Please enter description !' : ''}</Text>
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap" }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', width: 150, alignSelf: 'center', left: 5 }}>{strings.RECURRING_DAILY}</Text>
                                        <Switch style={[styles.headerText, { left: 5 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={2}
                                            value={allValues.recurring ? true : false}
                                            onValueChange={val => SetallValues({ ...allValues, ['recurring']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                        <TextInput
                                            marginLeft={100}
                                            width={150}
                                            placeholder={strings.SELECT_TIME}
                                            placeholderTextColor={allValues.recurring ? 'white' : '#81889f'}
                                            editable={false}
                                            style={{ fontFamily:'HelveticaNeue-Light', color: allValues.recurring ? 'white' : '#81889f', fontSize: 16 }}
                                            value={allValues.selecttime}
                                            onTouchStart={allValues.recurring ? () => { showDatePicker(true), setCheck('time') } : ''}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.ADD_TO_STANDARD_WHEEL}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={2}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                            value={allValues.addtowheel}
                                            onValueChange={val => SetallValues({ ...allValues, ['addtowheel']: val })}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATION}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={2}
                                            value={allValues.notification}
                                            onValueChange={val => SetallValues({ ...allValues, ['notification']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                    </View>
                                    <TouchableOpacity
                                        onPress={AddUpdateWellBeing}
                                        style={[styles.buttonStyle, { right: 80 }]}
                                        activeOpacity={0.5}
                                    >
                                        <Text style={styles.buttonTextStyle}>{type == "add" ? strings.SAVE : strings.UPDATE}</Text>
                                    </TouchableOpacity>
                                </View>
                            </KeyboardAwareScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                <Provider>
                    <Portal>
                        <Modal visible={visible1} onDismiss={hideModal1} contentContainerStyle={containerStyle}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                                <Text style={{ fontFamily:'HelveticaNeue-Bold', color: 'white', fontWeight: '700', marginTop: 50, fontSize:22 }}>{strings.ADD_WELLBEING_NOTIFICATION}</Text>
                                <View style={{ left: 240, }}>
                                    <TouchableOpacity onPress={hideModal1}>
                                        <Icon name="close" color="#81889f" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}
                                contentContainerStyle={styles.contentContainer}>
                                <View style={{ height: 400, marginTop: 20, width:'60%' }}>
                                    <Text style={{ fontFamily:'HelveticaNeue-Bold', color: 'white', marginBottom: 8 }}>{strings.NAME_OF_WELLBEING} <Text style={{ color: "#e0675c", marginBottom: 8 }}> *</Text></Text>
                                    <DropDownPicker
                                        items={dropValues}
                                        placeholder={strings.PLEASE_SELECT_WELLBEING}
                                        labelStyle={{ fontSize: 14, textAlign: 'left', color: 'white' }}
                                        defaultValue={allValuesNotification.event ? allValuesNotification.event : null}
                                        containerStyle={{ height: 40, color: 'white', borderWidth: 0 }}
                                        style={{ fontFamily:'HelveticaNeue-Bold', backgroundColor: '#2b3249', color: '#81889f', borderWidth: 0, borderBottomWidth: 1 }}
                                        itemStyle={{
                                            justifyContent: 'flex-start', color: 'white'
                                        }}
                                        dropDownStyle={{ backgroundColor: '#2b3249', color: 'white' }}
                                        onChangeItem={item => SetallNotificationValues({ ...allValuesNotification, ['event']: item.value })}
                                    />

                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between' }}>
                                        <View>
                                            <Text style={{ color: 'white', marginBottom: 8 }}>{strings.START_DATE} <Text style={{ color: "#e0675c", marginBottom: 8 }}> *</Text></Text>
                                            <TextInput
                                                style={{ width: 220, color: '81889f', fontSize:16 }}
                                                placeholder={strings.SELECT_START_DATE}
                                                placeholderTextColor={'#81889f'}
                                                editable={false}
                                                onTouchStart={() => { setDatePickerVisibility1(true); setCheck('start') }}
                                                value={allValuesNotification.prestartdate}
                                                borderBottomWidth={1}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                        <DateTimePickerModal
                                            style={{ backgroundColor: '#2b3249', }}
                                            headerTextIOS={strings.SELECT_TIME}
                                            pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                                            modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                                            textColor="white"
                                            onTouchStart={() => { setDatePickerVisibility1(true) }}
                                            isVisible={isDatePickerVisible1}

                                            mode="date"
                                            onConfirm={handleConfirm}
                                            onCancel={hideDatePicker}
                                        />
                                        <View style={{ marginLeft: 5 }}>
                                            <Text style={{ color: 'white', marginBottom: 8 }}>{strings.END_DATE} <Text style={{ color: "#e0675c", marginBottom: 8 }}> *</Text></Text>
                                            <TextInput
                                                style={{ width: 220, color: 'white', fontSize:16 }}
                                                placeholder={strings.SELECT_END_DATE}
                                                placeholderTextColor={'#81889f'}
                                                editable={false}
                                                onTouchStart={() => { setDatePickerVisibility1(true); setCheck('end') }}
                                                value={allValuesNotification.preenddate}
                                                borderBottomWidth={1}
                                                borderColor={'#81889f'}
                                            />
                                        </View>
                                    </View>
                                    <Text style={{ color: 'white', fontSize: 16, marginTop: 20 }}>
                                        {strings.TIME}
                                                </Text>
                                    {time.map((x, i) => {
                                        return (
                                            <View key={i} style={styles.SectionStyle}>
                                                <TextInput
                                                    style={styles.inputStyle}
                                                    value={x.time}
                                                    placeholder={strings.SELECT_TIME}
                                                    editable={false}
                                                    onTouchStart={() => { setDatePickerVisibility(true); setCheck(`mul ${i}`) }}
                                                    placeholderTextColor="#81889f"
                                                    underlineColorAndroid="#81889f"
                                                />
                                                <View>
                                                    {time.length !== 0 && <TouchableOpacity onPress={() => handleRemoveClick(i)} ><Icon name="close" color="#81889f" size={16} /></TouchableOpacity>}
                                                </View>
                                            </View>
                                        )
                                    })}
                                    <Button style={{ color: 'white' }} onPress={handleAddClick} title={"+ " + strings.ADD_WELLBEING_TIME}> {strings.ADD_WELLBEING_TIME}</Button>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', width: 135, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATION}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={2}
                                            value={allValuesNotification.enablePrescriptionNotification}
                                            onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['enablePrescriptionNotification']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                        <Text style={{ fontFamily:'HelveticaNeue-Light', color: 'white', width: 135, alignSelf: 'center', left: 30 }}>{strings.RECURRING_DAILY}</Text>
                                        <Switch style={[styles.headerText, { left: 30 }]}
                                            trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                            borderColor={'#81889f'}
                                            borderWidth={2}
                                            value={allValuesNotification.recurring}
                                            onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['recurring']: val })}
                                            borderRadius={16}
                                            thumbColor={"#fff"}
                                        />
                                    </View>
                                    <TouchableOpacity onPress={() => AddNotification()} style={[styles.buttonStyle, { right: 0 }]} activeOpacity={0.5}>
                                        <Text style={styles.buttonTextStyle}>{strings.SAVE}</Text>
                                    </TouchableOpacity>
                                </View>
                            </ScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                <Provider>
                    <Portal>
                        <Modal visible={visibleColor} onDismiss={hideModalColor} contentContainerStyle={{ alignSelf: 'center', marginBottom: 100 }}>
                            <View style={{ marginTop: 20 }}>
                                <ColorPicker
                                    hideSliders={true}
                                    color={allValues.color ? allValues.color : '#ff0058'}
                                    pickerSize={10}
                                    onColorSelected={color => { SetallValues({ ...allValues, ['color']: fromHsv(color) }) }, hideModalColor}
                                    onColorChange={color => SetallValues({ ...allValues, ['color']: fromHsv(color) })}
                                    style={{ width: 100, height: 100 }}
                                />
                            </View>
                        </Modal>
                    </Portal>
                </Provider>
                <View style={{ width: 200 }}>
                    <DateTimePickerModal
                        style={{ backgroundColor: '#2b3249', }}
                        headerTextIOS={strings.SELECT_TIME}
                        pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                        modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                        textColor="white"
                        locale="en_GB"
                        textColor="white"
                        isVisible={isDatePickerVisible}
                        mode="time"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />
                </View>
                <FAB
                    style={styles.floatBtn}
                    fabStyle={{ height: 100 }}
                    color={'white'}
                    theme={{ colors: { accent: '#eb8682' } }}
                    icon="plus"
                    onPress={showModal}
                >
                </FAB>
                <Snackbar
                    style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                    visible={visibleSnackbar}
                    duration={3000}
                    onDismiss={onDismissSnackBar}
                >
                    {snackMsg}
                </Snackbar>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 28,
        // width: 180,
        paddingHorizontal: 10,
        borderRadius: 20,
        right:50,
    },
    headerText: {
        fontFamily:'HelveticaNeue-Medium',
        fontSize:12,
        color: '#81889f',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    event: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: 700,
        height: 50,
        borderRadius: 15,
        marginBottom: 17,
    },
    eventText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    eventColor: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    eventColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        fontFamily:'HelveticaNeue-Light', 
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        fontFamily:'HelveticaNeue-Light',
        fontSize:16,
        color:'#81889f',
        flexDirection: 'row',
        fontSize: 16,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        // position: 'absolute',
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8
    },
    scrollView: {
        height: '63%',
        width: '100%',
        // margin: 20,
        alignSelf: 'center',
        // padding: 20,
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 250,
    }


});

export default WellBeingMaster;
