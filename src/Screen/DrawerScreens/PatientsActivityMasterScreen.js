import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import DateTimePickerModal from "react-native-modal-datetime-picker";
import moment from "moment";
import DropDownPicker from 'react-native-dropdown-picker';
import { Modal, Portal, Text, FAB, Provider, Snackbar, Menu } from 'react-native-paper';
import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    ScrollView,
    RefreshControl,
    Alert,
    StyleSheet,
    FlatList,
    ActivityIndicator,
    Button,
    Keyboard
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import deletebtn from '../../../Image/delete-btn.png'
import { activityDataSource, allDataNull, activityOffset, activityLoading, updateWheel } from '../redux/action'
import editbtn from '../../../Image/edit-btn.png'
import notificationbtn from '../../../Image/notification-btn.png'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import { ColorPicker, fromHsv } from 'react-native-color-picker'
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const PatientsActivityMaster = (navigation) => {
    console.log("hrlooprops",navigation)
    const currentUser = useSelector(state => state.auth.userLogin)
    const dispatch = useDispatch();
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const dataSource = useSelector(state => state.auth.activityDataSource)
    const [dataSourceForList, setSourceForList] = useState([]);
    const offset = useSelector(state => state.auth.aoffset)
    const isListEnd = useSelector(state => state.auth.activityListLoading)
    const [loading, setLoading] = useState(false);
    const [refreshing, setRefreshing] = React.useState(false);
    const [dropValues, setDropDownList] = useState([]);
    const [time, setTime] = React.useState([]);
    const [title, setTitle] = useState(strings.ADD_ACTIVITY);
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const addAuditLog = (activity) => {
        // alert(activity); return false;
        var dataToSend = {
            action: 'insertlog',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            siblinguserid: currentUser && currentUser.id ? currentUser.id : '',
            page:'patientsactivitymasterscreen',
            activity: activity,
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "auditlogs.php", dataToSend).then(result => {
            console.log("get audit logs:", result.data)
            if (result.data.status == 'success') {
                console.log("Log entered successfully!")
            }
        })
    }
    const [type, setType] = useState('');
    const [visible, setVisible] = React.useState(false);

    const [visible1, setVisible1] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [searchData, setsearchData] = useState('')
    const showAlert = (id, index) => {
        setAwesomeAlert(true);
        setPopupId(id)
        setPopupIndex(index)
    };

    const hideAlert = () => {
        setAwesomeAlert(false);
    };
    const [allValues, SetallValues] = useState({
        id: '',
        name: '',
        color: '',
        description: '',
        notification: false,
        recurring: false,
        addtowheel: false,
        selecttime: ''
    });

    const showModal = () => setVisible(true);
    const hideModal = (key) => {
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setVisible(false)
        setTitle(strings.ADD_ACTIVITY)
        setType('add')

    };
    const [visibleColor, setVisibleColor] = React.useState(false);
    const showModalColor = () => setVisibleColor(true);
    const hideModalColor = () => setVisibleColor(false);
    const [keyword, setKeyword] = useState('');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isDatePickerVisible1, setDatePickerVisibility1] = useState(false);
    const [check, setCheck] = useState('start');
    const [allValuesNotification, SetallNotificationValues] = useState({
        prestartdate: '',
        preenddate: '',
        startDate: '',
        endDate: '',
        addtostandardwheel: false,
        activity: null,
        recurringDaily: false,
    });

    const showModal1 = (value) => {
        setVisible1(true)
        SetallNotificationValues({ ...allValuesNotification, ['activity']: value })
    };
    const hideModal1 = (key) => {
        for (key in allValuesNotification) {
            if (allValuesNotification.hasOwnProperty(key)) {
                allValuesNotification[key] = null;
            }
        }
        setTime([])
        setVisible1(false)

    };
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
        setDatePickerVisibility1(false);
    };
    const handleConfirm = (date) => {
        if (check === "start") {
            SetallNotificationValues({ ...allValuesNotification, ['prestartdate']: moment(date).format('DD/MM/YYYY'), ['startDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "end") {
            SetallNotificationValues({ ...allValuesNotification, ['preenddate']: moment(date).format('DD/MM/YYYY'), ['endDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "time") {
            SetallValues({ ...allValues, ['selecttime']: moment(date).format('HH:mm') })
        }
        if (check.includes("mul")) {
            let index = check.split(" ")[1];
            console.log("check", date)
            console.log("check", index)
            const list = [...time];
            list[index]['time'] = moment(date).format('HH:mm')
            setTime(list);
        }

        hideDatePicker();
    };
    const onEditActivity = (data) => {
        // console.log("data", data)
        allValues.id = data.id;
        allValues.name = data.name;
        allValues.color = data.color;
        allValues.description = data.description;
        allValues.recurring = data.selecttime !== '' ? true : false;
        allValues.addtowheel = data.addtowheel == "1" ? true : false;;
        allValues.notification = data.notification = "1" ? true : false;
        allValues.selecttime = data.selecttime;
        setTitle(strings.EDIT_ACTIVITY)
        setVisible(true);
        setType('edit');
    }



    const updatetoWheel = (id, val, index) => {
        var data = {
            action: 'updatestandardwheelactivity',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
            standardwheel: val,
            id: id
        }
        dataSourceForList[index].addtowheel = !dataSourceForList[index].addtowheel;
        axios.post(config.apiUrl + "updatedata.php", data).then(result => {
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                addAuditLog('update add to wheel');
                setsnackMsg(strings.ADD_TO_WHEEL_UPDATED_SUCCESSFULLY)
                setVisibleSnack(true)
                dispatch(updateWheel(val))
            }else{
                addAuditLog('problem updating data');
                setsnackMsg(strings.PROBLEM_UPDATING_DATA)
                setVisibleSnack(true)
                dispatch(updateWheel(val))
            }

        })
    }

    const containerStyle = { backgroundColor: '#2b3249', padding: 10, marginBottom: 25, alignItems: 'center', alignSelf: 'center', borderRadius: 8, width: '80%', height: '90%' };

    useEffect(() => {
        getActivityData();
    }, [sessionUser]);

    useEffect(() => {
        getDropDownList();
    }, [sessionUser]);

    const getDropDownList = () => {
        var data = {
            action: 'getactivitymasterdatafulllist',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "getdata.php", data).then(result => {
            // console.log("result.data123123", result.data)
            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            value: val.id,
                            icon: () => <View style={{ width: 20, height: 20, borderRadius: 10, backgroundColor: val.color }} />,
                            label: val.name,
                        }
                        arr.push(ob)
                    })
                    setDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }

    const getActivityData = () => {

        if (!isListEnd && keyword === '') {
            setLoading(true);
            console.log("function called from update and add", offset);
            console.log('getData');
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                appsecret: config.appsecret,
                action: 'getactivitymasterdata',
                keyword: keyword,
                offset: offset
            };

            console.log("offset")
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                console.log('Activity!!!', "", "Result..." + result)
                setLoading(false);
                if (result.data.data0 !== null) {
                    if (offset === 0) {
                        dispatch(allDataNull())
                        setSourceForList([])
                    }
                    dispatch(activityDataSource(result.data.data0));
                    dispatch(activityOffset(offset + 10));
                    if (offset == 0)
                        setSourceForList(result.data.data0)
                    else
                        setSourceForList(dataSourceForList.concat(result.data.data0))
                }
                else {
                    dispatch(activityLoading(true))
                    setLoading(false);
                }
            })
        }
    };


    useEffect(() => {
        getActivityDataSearch()
    }, [keyword]);


    const getActivityDataSearch = () => {

            if (keyword == '') {
                Keyboard.dismiss()
            }
            setLoading(true);
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                appsecret: config.appsecret,
                hcpid: 0,
                action: 'getactivitymasterdata',
                keyword: keyword,
                offset: 0
            };
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

                if (result.data.data0 !== null) {
                    setLoading(false);
                    dispatch(allDataNull())
                    setSourceForList([])
                    setSourceForList(result.data.data0)
                    dispatch(activityDataSource(result.data.data0));
                    dispatch(activityOffset(0));
                }
                else {
                    dispatch(activityLoading(true))
                    setLoading(false);
                }
            })
    }




    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...time];
        list.splice(index, 1);
        setTime(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setTime([...time, { time: "" }]);
    };


    const AddUpdateActivity = () => {
        if (allValues.name === '' || allValues.name === null) {
            alert("Please enter name")
            return
        }
        if (allValues.description === '' || allValues.description === null) {
            alert("Please enter description")
            return
        }
        setLoading(true)
        console.log("buton clicked", allValues)
        var data = allValues;
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        data.appsecret = config.appsecret;
        data.preenddate = moment(data.preenddate).format('YYYY-MM-DD')
        data.prestartdate = moment(data.prestartdate).format('YYYY-MM-DD')
        data.hcpid = 0;
        data.action = type == "edit" ? 'updateactivity' : 'addactivity';
        var filename = type == "edit" ? 'updatedata.php' : 'adddata.php';
        console.log("daa", data)
        axios.post(config.apiUrl + filename, data).then(result => {
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                console.log("result", result.data)
                setLoading(false);
                setVisible(false)
                hideModal();
                // setType('add');
                addAuditLog(type == "add" ? 'activity added' : 'activity updated');
                setsnackMsg(type == "add" ? strings.ACTIVITY_CREATED_SUCC : strings.ACTIVITY_UPDATED_SUCC)
                setVisibleSnack(true)
                onRefresh();
                dispatch(updateWheel(true));
            }
            else {
                setType('add');
                setVisible(false)
                setLoading(false);
                addAuditLog(type == "add" ? 'problem adding activity' : 'problem updating activity');
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }
        })
    }
    const searchKeyword = async (value) => {
        console.log("value", value)
        await setKeyword(value);
        await setsearchData(value)
        await setLoading(false);
        await dispatch(allDataNull());
        dispatch(activityOffset(0));
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setsearchData('')
        Keyboard.dismiss()
        wait(2000).then(() => {
            dispatch(activityOffset(0));
            setRefreshing(false);
            setVisible(false)
            dispatch(allDataNull())
            setSourceForList([])
            getActivityData();
        });
    }, []);

    const AddNotification = () => {
        setLoading(true)
        console.log("buton clicked", allValues)
        var data = allValuesNotification;
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
        data.hcpid = 0;
        data.addedBy = currentUser.id;
        data.appsecret = config.appsecret;
        data.prestartdate = data.startDate
        data.preenddate = data.endDate
        data.starttime = time.map(x => x.time).join(',');
        data.action = 'addpatientactivity';
        console.log("notification", data)
        axios.post(config.apiUrl + 'adddata.php', data).then(result => {
            setLoading(false)
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                console.log("result", result.data)
                setLoading(false);
                hideModal1()
                addAuditLog('notification added');
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC);
                setVisibleSnack(true)

            }
            else {
                setLoading(false);
                addAuditLog('problem adding notification');
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setVisibleSnack(true)
            }
        })
    }


    const DeleteAlert = (id, index) => {
        showAlert(id, index)
        // Alert.alert(
        //     'Delete',
        //     'Are your sure you want to delete?',
        //     [
        //
        //         {
        //             text: 'No',
        //             onPress: () => { },
        //             style: 'cancel',
        //         },
        //         {
        //             text: 'Yes',
        //             onPress: () => DeleteActivity(id, index)
        //         },
        //     ],
        //     { cancelable: false },
        // );
    }

    const DeleteActivity = (id, index) => {
        setLoading(true)
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        data.appsecret = config.appsecret;
        data.id = id;
        data.action = 'deleteactivity';
        data.siblinguserid = sessionUser && sessionUser.userid ? currentUser.id : '';
        axios.post(config.apiUrl + 'deletedata.php', data).then(result => {
            setLoading(false);
            if (result.data.status == 'success') {
                dataSourceForList.splice(index, 1);
                console.log("resultDelted", result.data)
                setVisible(false)
                // setType('add');
                addAuditLog('activity deleted');
                setsnackMsg(strings.ACTIVITY_DELETED_SUCC)
                setAwesomeAlert(false)
                setVisibleSnack(true)
                dispatch(updateWheel(true));
            }
            else {
                setVisible(false)
                addAuditLog('problem deleting activity');
                setsnackMsg(strings.SOMETHING_WENT_WRONG)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
        })
    }

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="white"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };
    const onDismissSnackBar = () => setVisibleSnack(false);
    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{ marginTop: 20 }}>
                <Text style={{ color: '#ccc', fontSize: 18, fontWeight: 'bold' }}>{strings.NO_ITEMS_FOUND}</Text>
            </View>
        );
    };
    const ItemView = ({ item, index }) => {
        console.log("index", index)
        return (
            // Flat List Item
            <View key={item.id} style={styles.activity}>
                <View style={[styles.activityColor, { backgroundColor: item.color }]}>
                </View>
                <Text style={styles.activityText}>{item.name}</Text>
                <View style={{ paddingTop: 10, flexDirection: "row", flexWrap: "wrap" }}>
                    <TouchableOpacity onPress={() => DeleteAlert(item.id, index)}>
                        <Image style={styles.icons} height={38} width={38} source={deletebtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onEditActivity(item)}>
                        <Image style={styles.icons} height={38} width={38} source={editbtn} />
                    </TouchableOpacity>
                    <Switch style={[styles.headerText, { left: 18 }]}
                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                        borderColor={'#81889f'}
                        borderWidth={2}
                        borderRadius={16}
                        onValueChange={val => updatetoWheel(item.id, val, index)}
                        thumbColor={"#fff"}
                        value={item.addtowheel == "1" ? true : false}
                    />
                    <TouchableOpacity style={{ width: 34, left: 53 }} onPress={() => showModal1(item.id)}>
                        <Image style={{ marginTop: -3, height: 34, width: 34 }} height={34} width={34} source={notificationbtn} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };
    return (
        <SafeAreaView style={[styles.mainBody, { top: 100, height: 600 }]}>
            <AwesomeAlert
                contentContainerStyle={{
                    backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 12,
                    },
                    shadowOpacity: 0.58,
                    shadowRadius: 16.00,

                    elevation: 24
                }}
                show={awesomeAlert}
                showProgress={false}
                title={strings.DELETE_RECORD}
                titleStyle={{ color: 'white' }}
                message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                messageStyle={{ color: 'white' }}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={strings.NO_CANCEL}
                confirmText={strings.YES_DELETE}
                cancelButtonColor="#516b93"
                confirmButtonColor="#69c2d1"
                confirmButtonBorderRadius="0"
                confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                onCancelPressed={() => {
                    setAwesomeAlert(false);
                }}
                onConfirmPressed={() => {
                    DeleteActivity(popupId, popupIndex);
                }}
            />
            {/* <Loader loading={loading} /> */}
            <TouchableOpacity style={styles.viewbtn} onPress={() => navigation.navigate('ActivityNotificationScreen')}>
                <Text style={{ color: 'white', fontSize: 16, fontWeight: '600' }}>View All Dosages</Text>
            </TouchableOpacity>
            <View style={{ right: 125, flexDirection: "row", flexWrap: "wrap" }}>
                <View style={styles.SectionStyle}>
                    <TextInput
                        style={styles.inputStyle}
                        placeholder={strings.TYPE_TO_SEARCH_HERE}
                        placeholderTextColor="#81889f"
                        autoCapitalize="none"
                        underlineColorAndroid="#81889f"
                        onChangeText={(text) => { searchKeyword(text) }}
                        blurOnSubmit={false}
                        value={searchData}
                    />
                    <Icon style={styles.searchIcon} name="search" size={22} color="#81889f" />
                </View>
                <View style={{ paddingTop: 45, left: 200, flexDirection: "row", flexWrap: "wrap" }}>
                    <Text style={styles.headerText}>{strings.DELETE}</Text>
                    <Text style={styles.headerText}>{strings.EDIT}</Text>
                    <Text style={styles.headerText}>{strings.ADD_TO_WHEEL}</Text>
                    <Text style={styles.headerText}>{strings.NOTIFY}</Text>
                </View>
            </View>
            <FlatList
                showsVerticalScrollIndicator={false}
                data={dataSourceForList}
                keyExtractor={(item, index) => index.toString()}
                renderItem={ItemView}
                ListEmptyComponent={NoDataFound}
                ListFooterComponent={renderFooter}
                onEndReached={getActivityData}
                onEndReachedThreshold={0.5}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />}
            />
            <Provider>
                <Portal>
                    <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                            <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: 'white', fontWeight: '700', fontSize: 22, }}>{title}</Text>
                            <View style={{ left: 300, marginTop: -10 }}>
                                <TouchableOpacity onPress={hideModal}>
                                    <Icon name="close" color="#81889f" size={20} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <KeyboardAwareScrollView enableOnAndroid={true}>
                            <View style={{ marginTop: 20, alignItems: 'center' }}>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', marginBottom: 8, fontSize: 14 }}>{strings.NAME_OF_ACTIVITY} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                    <TextInput
                                        style={{ width: 460, color: 'white', fontSize: 16 }}
                                        placeholder={strings.MEDICINE_SEIZURE_HEADACHE}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.name}
                                        onChangeText={val => SetallValues({ ...allValues, ['name']: val })}
                                        borderBottomWidth={2}
                                        borderColor={'#81889f'}
                                    />
                                    <Text style={{ color: '#e0675c' }}>{allValues.name === '' ? strings.PLEASE_ENTER_ACTIVITY_NAME : ''}</Text>
                                </View>
                                <View style={{ marginTop: 20, left: 10 }}>
                                    <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.CHOOSE_COLOR} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                    <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                                        <View style={[styles.activityColorForm, { backgroundColor: allValues.color, marginBottom: 8, left: 0 }]}>
                                        </View>
                                        <TextInput
                                            style={{ fontFamily: 'HelveticaNeue-Light', width: 460, right: 20, fontSize: 16 }}
                                            placeholder={'       ' + strings.CHOOSE_COLOR}
                                            placeholderTextColor={'#81889f'}
                                            editable={false}
                                            onTouchStart={showModalColor}
                                            borderBottomWidth={2}
                                            borderColor={'#81889f'}
                                        />
                                    </View>
                                </View>
                                <View style={{ marginTop: 20 }}>
                                    <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.DESCRIPTION}</Text>
                                    <TextInput
                                        style={{ fontFamily: 'HelveticaNeue-Light', width: 460, color: 'white', fontSize: 16 }}
                                        placeholder={strings.TYPE_DESCRIPTION_HERE}
                                        placeholderTextColor={'#81889f'}
                                        value={allValues.description}
                                        onChangeText={val => SetallValues({ ...allValues, ['description']: val })}
                                        borderBottomWidth={2}
                                        borderColor={'#81889f'}
                                    />
                                    {/*<Text style={{ color: '#e0675c' }}>{allValues.name === '' ? 'Please enter description   ' : ''}</Text>*/}
                                </View>
                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap" }}>
                                    <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', width: 150, alignSelf: 'center', left: 5 }}>{strings.RECURRING_DAILY}</Text>
                                    <Switch style={[styles.headerText, { left: 5 }]}
                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                        borderColor={'#81889f'}
                                        borderWidth={1}
                                        value={allValues.recurring ? true : false}
                                        onValueChange={val => SetallValues({ ...allValues, ['recurring']: val })}
                                        borderRadius={16}
                                        thumbColor={"#fff"}
                                    />
                                    <TextInput
                                        marginLeft={100}
                                        width={150}
                                        placeholder={strings.SELECT_TIME}
                                        placeholderTextColor={allValues.recurring ? 'white' : '#81889f'}
                                        editable={false}
                                        style={{ color: allValues.recurring ? 'white' : '#81889f', fontSize: 14, fontFamily: 'HelveticaNeue-Light' }}
                                        value={allValues.selecttime}
                                        onTouchStart={allValues.recurring ? () => { showDatePicker(true), setCheck('time') } : ''}
                                        borderBottomWidth={2}
                                        borderColor={'#81889f'}
                                    />
                                </View>
                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                    <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.ADD_TO_STANDARD_WHEEL}</Text>
                                    <Switch style={[styles.headerText, { left: 30 }]}
                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                        borderColor={'#81889f'}
                                        borderWidth={1}
                                        borderRadius={16}
                                        thumbColor={"#fff"}
                                        value={allValues.addtowheel}
                                        onValueChange={val => SetallValues({ ...allValues, ['addtowheel']: val })}
                                    />
                                </View>
                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 150 }}>
                                    <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATION}</Text>
                                    <Switch style={[styles.headerText, { left: 30 }]}
                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                        borderColor={'#81889f'}
                                        borderWidth={1}
                                        value={allValues.notification}
                                        onValueChange={val => SetallValues({ ...allValues, ['notification']: val })}
                                        borderRadius={16}
                                        thumbColor={"#fff"}
                                    />
                                </View>
                                <TouchableOpacity onPress={AddUpdateActivity}
                                    style={[styles.buttonStyle, { right: 80 }]}
                                    activeOpacity={0.5}
                                >
                                    <Text style={styles.buttonTextStyle}>{type == "add" ? strings.SAVE : strings.UPDATE}</Text>
                                </TouchableOpacity>
                            </View>
                        </KeyboardAwareScrollView>
                    </Modal>
                </Portal>
            </Provider>
            <Provider>
                <Portal>
                    <Modal visible={visible1} onDismiss={hideModal1} contentContainerStyle={containerStyle}>
                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                            <Text style={{ fontFamily: 'HelveticaNeue-Bold', color: 'white', fontWeight: '700', marginTop: 50, fontSize: 22 }}>{strings.ADD_ACTIVITY_NOTIFICATION}</Text>
                            <View style={{ left: 240 }}>
                                <TouchableOpacity onPress={hideModal1}>
                                    <Icon name="close" color="#81889f" size={20} />
                                </TouchableOpacity>
                            </View>
                        </View>
                        <ScrollView showsVerticalScrollIndicator={false} style={styles.scrollView}
                            contentContainerStyle={styles.contentContainer}>
                            <View style={{ height: 400, marginTop: 20, width: '60%', }}>
                                <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.NAME_OF_ACTIVITY} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                <DropDownPicker
                                    items={dropValues}
                                    placeholder={strings.PLEASE_SELECT_ACTIVITY}
                                    labelStyle={{ fontSize: 14, textAlign: 'left', color: 'white' }}
                                    defaultValue={allValuesNotification.activity}
                                    containerStyle={{ height: 40, color: 'white', borderWidth: 0 }}
                                    style={{ fontFamily: 'HelveticaNeue-Light', backgroundColor: '#2b3249', color: 'white', borderWidth: 0, borderBottomWidth: 1, borderBottomColor: '#81889f' }}
                                    itemStyle={{
                                        justifyContent: 'flex-start', color: 'white'
                                    }}
                                    dropDownStyle={{ backgroundColor: '#2b3249', color: 'white' }}
                                    onChangeItem={item => SetallNotificationValues({ ...allValuesNotification, ['activity']: item.value })}
                                />
                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between' }}>
                                    <View>
                                        <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.START_DATE} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                        <TextInput
                                            style={{ width: 220, color: 'white', fontSize: 16 }}
                                            placeholder={strings.SELECT_START_DATE}
                                            placeholderTextColor={'#81889f'}
                                            editable={false}
                                            onTouchStart={() => { setDatePickerVisibility1(true); setCheck('start') }}
                                            value={allValuesNotification.prestartdate}
                                            borderBottomWidth={2}
                                            borderColor={'#81889f'}
                                        />
                                        <DateTimePickerModal
                                            style={{ backgroundColor: '#2b3249', }}
                                            headerTextIOS={strings.SELECT_TIME}
                                            textColor="white"
                                            onTouchStart={() => { setDatePickerVisibility1(true) }}
                                            isVisible={isDatePickerVisible1}
                                            pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                                            modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                                            mode="date"
                                            onConfirm={handleConfirm}
                                            onCancel={hideDatePicker}
                                        />
                                    </View>
                                    <View style={{ marginLeft: 30 }}>
                                        <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', marginBottom: 8 }}>{strings.END_DATE} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                                        <TextInput
                                            style={{ width: 220, color: 'white', fontSize: 16 }}
                                            placeholder={strings.SELECT_END_DATE}
                                            placeholderTextColor={'#81889f'}
                                            editable={false}
                                            onTouchStart={() => { setDatePickerVisibility1(true); setCheck('end') }}
                                            value={allValuesNotification.preenddate}
                                            borderBottomWidth={1}
                                            borderColor={'#81889f'}
                                        />
                                        <DateTimePickerModal
                                            style={{ backgroundColor: '#2b3249', }}
                                            headerTextIOS={strings.SELECT_TIME}
                                            locale="en_GB"
                                            textColor="white"
                                            isVisible={isDatePickerVisible}
                                            pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                                            modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                                            mode="time"
                                            onConfirm={handleConfirm}
                                            onCancel={hideDatePicker}
                                        />
                                    </View>
                                </View>
                                <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', fontSize: 14, marginTop: 20 }}>
                                    {strings.TIME}
                                </Text>
                                {time.map((x, i) => {
                                    return (
                                        <View key={i} style={styles.SectionStyle}>
                                            <TextInput
                                                style={styles.inputStyle}
                                                name="time"
                                                placeholder={strings.SELECT_TIME}
                                                editable={false}
                                                onTouchStart={() => { setDatePickerVisibility(true); setCheck(`mul ${i}`) }}
                                                value={x.time}
                                                placeholderTextColor="#8b9cb5"
                                                underlineColorAndroid="#f000"
                                            />
                                            <View>
                                                {time.length !== 0 && <TouchableOpacity onPress={() => handleRemoveClick(i)} ><Icon name="close" color="#81889f" size={16} /></TouchableOpacity>}
                                            </View>
                                        </View>
                                    )
                                })}
                                <Button style={{ fontFamily: 'HelveticaNeue-Light', color: 'white' }} onPress={handleAddClick} title={"+ " + strings.PLEASE_ENTER_ACTIVITY_TAKEN_TIME}> + {strings.ADD_ACTIVITY_TIME}</Button>
                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                    <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', width: 135, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATION}</Text>
                                    <Switch style={[styles.headerText, { left: 30 }]}
                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                        borderColor={'#81889f'}
                                        borderWidth={1}
                                        value={allValuesNotification.notification}
                                        onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['notification']: val })}
                                        borderRadius={16}
                                        thumbColor={"#fff"}
                                    />
                                </View>
                                <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                    <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', width: 135, alignSelf: 'center', left: 30 }}>{strings.RECURRING_DAILY}</Text>
                                    <Switch style={[styles.headerText, { left: 30 }]}
                                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                        borderColor={'#81889f'}
                                        borderWidth={1}
                                        value={allValuesNotification.recurringDaily}
                                        onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['recurringDaily']: val })}
                                        borderRadius={16}
                                        thumbColor={"#fff"}
                                    />
                                </View>
                                <TouchableOpacity onPress={() => AddNotification()} style={[styles.buttonStyle, { right: 0, top: 30 }]} activeOpacity={0.5}>
                                    <Text style={styles.buttonTextStyle}>{strings.SAVE}</Text>
                                </TouchableOpacity>
                            </View>

                        </ScrollView>
                    </Modal>
                </Portal>
            </Provider>
            <Provider>
                <Portal>
                    <Modal visible={visibleColor} onDismiss={hideModalColor} contentContainerStyle={{ alignSelf: 'center', marginBottom: 100 }}>
                        <View style={{ marginTop: 20 }}>
                            <ColorPicker
                                hideSliders={true}
                                color={allValues.color ? allValues.color : '#ff0058'}
                                pickerSize={10}
                                onColorSelected={color => { SetallValues({ ...allValues, ['color']: fromHsv(color) }) }, hideModalColor}
                                onColorChange={color => SetallValues({ ...allValues, ['color']: fromHsv(color) })}
                                style={{ width: 100, height: 100 }}
                            />
                        </View>
                    </Modal>
                </Portal>
            </Provider>
            <DateTimePickerModal
                style={{ backgroundColor: '#2b3249', }}
                headerTextIOS={strings.SELECT_TIME}
                locale="en_GB"
                textColor="white"
                isVisible={isDatePickerVisible}
                pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                mode="time"
                onConfirm={handleConfirm}
                onCancel={hideDatePicker}
            />
            <FAB
                style={styles.floatBtn}
                fabStyle={{ height: 100 }}
                color={'white'}
                theme={{ colors: { accent: '#eb8682' } }}
                icon="plus"
                onPress={showModal}
            >
            </FAB>
            <Snackbar
                style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                visible={visibleSnackbar}
                duration={3000}
                onDismiss={onDismissSnackBar}
            >
                {snackMsg}
            </Snackbar>
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 50,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 28,
        // width: 180,
        paddingHorizontal: 10,
        borderRadius: 20,
        right: 50,
    },
    headerText: {
        fontFamily: 'HelveticaNeue-Medium',
        fontSize: 12,
        color: '#81889f',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 38,
        width: 38
    },
    searchIcon: {
        paddingTop: 11,

    },
    activity: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 2,
        borderColor: '#474f69',
        width: 700,
        height: 54,
        borderRadius: 15,
        marginBottom: 17,
    },
    activityText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    activityColor: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    activityColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        fontFamily: 'HelveticaNeue-Light',
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 2,
        borderBottomColor: '#81889f',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        fontFamily: 'HelveticaNeue-Light',
        fontSize: 16,
        color: '#81889f',
        flexDirection: 'row',
        fontSize: 16,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        // position: 'absolute',
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8

    },
    scrollView: {
        height: '80%',
        width: '100%',
        // margin: 20,
        alignSelf: 'center',
        // padding: 20,
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 250,
    }

});

export default PatientsActivityMaster;
