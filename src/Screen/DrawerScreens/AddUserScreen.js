import React, { useState, useEffect } from 'react';
import { useSelector,useDispatch } from 'react-redux'

import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    StyleSheet,
    Alert,
    RefreshControl,
    FlatList,
    Button,
    ScrollView,
    ActivityIndicator,
    Keyboard
} from 'react-native';
import moment from "moment";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import ImagePicker from 'react-native-image-crop-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import usericn from '../../../Image/nopreview.jpg';
import upload from '../../../Image/upload-medicine.png'
import { Modal, Portal, Text, Provider, FAB, Snackbar } from 'react-native-paper';
import deletebtn from '../../../Image/delete-btn.png'
import downArrow from '../../../Image/up_arrow.png'
import editbtn from '../../../Image/edit-btn.png'
import RadioButton from '../Components/radioButton';
import notificationbtn from '../../../Image/notification-btn.png'
import { setSession, medicineLoading, activityLoading, wellBeingLoading, allDataNull } from '../redux/action'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';
import SearchableDropdown from 'react-native-searchable-dropdown';
import Autocomplete from 'react-native-autocomplete-input';
import DateTime from 'react-native-customize-selected-date';
import iid from '@react-native-firebase/iid';

const AddUserScreen = ( navigation ) => {
    console.log("navigation::89",navigation.route.params.toBeAddedUsertype)
    const toBeAddedUsertype = navigation.route.params.toBeAddedUsertype
    const currentUser = useSelector(state => state.auth.userLogin)
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const dispatch = useDispatch();
    const [refreshing, setRefreshing] = React.useState(false);
    const [loading, setLoading] = useState(false);
    const [dataSource, setDataSource] = useState([]);
    const [offset, setOffset] = useState(0);
    const [emailErr, setEmailError] = useState('');
    const [token, setToken] = useState('');
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const [dropValues, setDropDownList] = useState([]);
    const [hcpDropDownList, setHcpDropDownList] = useState([]);
    const [doctorDropDownList, setDoctorDropDownList] = useState([]);
    const [nurseDropDownList, setNurseDropDownList] = useState([]);
    const [state1, setState] = useState([]);
    const [type, setType] = useState('');
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    // const [isDatePickerVisible1, setDatePickerVisibility1] = useState(false);
    const onDismissSnackBar = () => setVisibleSnack(false);
    const [keyword, setKeyword] = useState('');
    const [searchData, setsearchData] = useState('');
    const [userEmail, setUserEmail] = useState('');
    const [userId, setUserId] = useState('');
    const [receiverType, setReceiverType] = useState('');
    const [firstname, setFirstname] = useState('');
    const [lastname, setLastname] = useState('');
    const [email, setEmail] = useState('');
    const [mobile, setMobile] = useState('');
    const [birthdate, setBirthdate] = useState('');
    const [street, setStreet] = useState('');
    const [zipcode, setZipcode] = useState('');
    const [city, setCity] = useState('');
    const [addressState, setAddressState] = useState('');
    const [country, setCountry] = useState('');
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [isListEnd, setIsListEnd] = useState(false);
    const [visible1, setVisible1] = React.useState(false);
    const [fieldDisable, setFieldDisable] = React.useState(true);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [doctorId, setDoctorId] = useState('');
    const [hcpId, setHcpId] = useState('');
    const [nurseId, setNursetId] = useState('');
    const [title, setTitle] = useState('Add User');
    const [fnameErr, setFnameErrtext] = useState('');
    const [lnameErr, setLnameErrtext] = useState('');
    const [mobileErr, setMobileErrtext] = useState('');
    const [bdErr, setBdErrtext] = useState('');
    const [streetErr, setStreetErrtext] = useState('');
    const [cityErr, setCityErrtext] = useState('');
    const [stateErr, setStateErrtext] = useState('');
    const [zipErr, setZipErrtext] = useState('');
    const showAlert = (id, index) => {
        setAwesomeAlert(true);
        setPopupId(id)
        setPopupIndex(index)
    };
    async function getToken() {
        const id = await iid().getToken();
        setToken(id);
        return id;
    }

    useEffect(() => {
        getToken();
    }, [])
    const setFnameErr = (e) => {
        if (e === '') {
            setFirstname(null)
            setFnameErrtext(strings.FIRSTNAME_REQUIRED)
        }
        else {
            setFirstname(e)
            setFnameErrtext(null)
        }
    }
    const setLnameErr = (e) => {
        if (e === '') {
            setLastname(null)
            setLnameErrtext(strings.LASTNAME_REQUIRED)
        }
        else {
            setLastname(e)
            setLnameErrtext(null)
        }
    }
    // const setBdErr = (e) => {
    //     if (e === '') {
    //         setBirthdate('')
    //         setBdErrtext('Birthdate is required !')
    //     }
    //     else {
    //         setBirthdate(e)
    //         setBdErrtext('')
    //     }
    // }
    const setMobileErr = (e) => {
        if (e === '') {
            setMobile(null)
            setMobileErrtext(strings.MOBILE_REQUIRED)
        }
        else {
            setMobile(e)
            setMobileErrtext(null)
        }
    }
    const setStreetErr = (e) => {
        if (e === '') {
            setStreet(null)
            setStreetErrtext(strings.STREET_REQUIRED)
        }
        else {
            setStreet(e)
            setStreetErrtext(null)
        }
    }
    const setCityErr = (e) => {
        if (e === '') {
            setCity(null)
            setCityErrtext(strings.CITY_REQUIRED)
        }
        else {
            setCity(e)
            setCityErrtext(null)
        }
    }
    const setStateErr = (e) => {
        if (e === '') {
            setState(null)
            setStateErrtext(strings.STATE_REQUIRED)
        }
        else {
            setState(e)
            setStateErrtext(null)
        }
    }
    const setZipErr = (e) => {
        if (e === '') {
            setZipcode(null)
            setZipErrtext(strings.ZIP_REQUIRED)
        }
        else {
            setZipcode(e)
            setZipErrtext(null)
        }
    }
    const hideAlert = () => {
        setAwesomeAlert(false);
    };
    const renderChildDay = (day) => {
        if ((['2018-12-20'], day)) {
            return <Text></Text>
        }
        if ((['2018-12-18'], day)) {
            return <Text></Text>
        }
    }
    const [MainJSON, setMainJSON] = useState([]);
 
    // Used to set Filter JSON Data.
    const [FilterData, setFilterData] = useState([]);
    
    // Used to set Selected Item in State.
    const [selectedItem, setselectedItem] = useState({});
    useEffect(() => {
        // setMainJSON([]);
        // fetch('https://jsonplaceholder.typicode.com/todos/')
        //   .then((res) => res.json())
        //   .then((json) => {
        //     console.log('result.data.dataq::', json)
        //     setMainJSON(json);
        //   })
        //   .catch((e) => {
        //     alert(e);
        //   });
        
        var data = {
          userid: currentUser.id,
          usertype: currentUser && currentUser.usertype ? currentUser.usertype : '',
          appsecret: config.appsecret,
          action: 'getallpatientsfordropdown',
          subuserids: (toBeAddedUsertype && toBeAddedUsertype == 'admin' ? 'adminids' : (toBeAddedUsertype && toBeAddedUsertype == 'healthcareprovider' ? 'hcpids' : (toBeAddedUsertype && toBeAddedUsertype == 'doctor' ? 'doctorids' : (toBeAddedUsertype && toBeAddedUsertype == 'nurse' ? 'nurseids' : 'patientids')))),
          toBeAddedUsertype:toBeAddedUsertype,
        }
        if (currentUser.usertype === 'healthcareprovider') {
            data.hcpid = currentUser.id;
            data.patientids = currentUser.patientids;
        }
        // console.log('result.data.data::dfadfa1', data); return false;
        // dataSourceForList[index].addtowheel = !dataSourceForList[index].addtowheel;
        axios.post(config.apiUrl + "getdata.php", data).then(result => {
            // setLoading(false)
            console.log('result.data.data::df12345', result.data)
            if (result.data.data) {
              setMainJSON(result.data.data);
            }
          }).catch(err => {
            // setLoading(false);
            console.log("error:", err)
          })
        }, []);
     
      const SearchDataFromJSON = (query) => {
        console.log("Querry:", MainJSON)
        if (query) {
          //Making the Search as Case Insensitive.
          const regex = new RegExp(`${query.trim()}`, 'i');
          setFilterData(
            MainJSON.filter((data) => data.email.search(regex) >= 0)
          );
        } else {
          setFilterData([]);
        }
      };

    const [time, setTime] = React.useState([]);
    const [check, setCheck] = useState('start');
    const [allValues, SetallValues] = useState({
        id: '',
        firstname: '',
        lastname: '',
        email: '',
        birthdate: '',
        mobile: '',
        sex:'m',
        street:'',
        zipcode:'',
        city:'',
        state:'',
        country:'',
        profilepic: '',
        image: '',
        newimage: '',
        filename: '',
        filetype:'',
        sourceURL:'',
        filesize:'',
        allimageData: '',
        name:'',
        color:'',
        company:'',
    });

    const showModal1 = () => {
        setVisible1(true);
    }
    const hideModal = (key) => {
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setVisible1(false)
        // setTitle(strings.ADD_MEDICINE)
        // setType('add')

    };
    const handleChooseLibrary = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };
        ImagePicker.openPicker({
            width: 300,
            height: 300,
            cropping: true,
            // multiple: true,
        }).then(image => {
            console.log("cropped image::",image);
            // alert(image.sourceURL)
            SetallValues({ ...allValues, filename: image.filename, filetype: image.mime, newimage: image.path, image:'', sourceURL:image.sourceURL, filesize:image.size, allimageData:image });
        });
    }

    const onSelect = (item) => {
        console.log("item", item)
        if (allValues && allValues.sex === item.key) {
            SetallValues({ ...allValues, ['sex']: item.key })
        } else {
            SetallValues({ ...allValues, ['sex']: item.key })
        }
    };

    const handleChooseCamera = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };

        ImagePicker.openCamera({
            width: 300,
            height: 300,
            cropping: true,
        }).then(image => {
            console.log("cropped camera image::",image);
            SetallValues({ ...allValues, filename: image.filename, filetype: image.mime, newimage: image.path, image:'', sourceURL:image.sourceURL, filesize:image.size, allimageData:image });
        });
    }

    const chooseOption = () => {
        Alert.alert(
            "",
            strings.UPLOAD_IMAGE,
            [
                { text: strings.CHOOSE_FROM_LIBRARY, onPress: () => handleChooseLibrary() },
                { text: strings.TAKE_PICTURE, onPress: () => handleChooseCamera() },
                { text: strings.CANCEL, onPress: () => console.log("OK Pressed"), style: "cancel" }
            ]
        );
    }
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const options = [
        {
            key: 'm',
            text: strings.MALE,
        }, {
            key: 'f',
            text: strings.FEMALE,
        },
    ]
    const hideDatePicker = () => {
        setDatePickerVisibility(false);
        // setDatePickerVisibility1(false);
    };
    const handleConfirm = (date) => {
        SetallValues({ ...allValues, ['birthdate']: date });
        hideDatePicker();
    };

    const containerStyle1 = { backgroundColor: '#2b3249', marginBottom: 25, padding: 10, alignItems: 'center', height: '100%', width: '98%', alignSelf: 'center', borderRadius: 10, };

    const searchKeyword = async (value, hId, dId, nId) => {
        console.log("value", value)
        await setKeyword(value);
        await setsearchData(value);
        await setIsListEnd(false);
        await setLoading(false);
        await setDoctorId(dId);
        await setHcpId(hId);
        await setNursetId(nId);
        await setDataSource([])
        await setOffset(0)
    }


    const startSession = (data) => {
        setLoading(true)
        data.userid = data.uid;
        dispatch(setSession(data))
        dispatch(medicineLoading(false));
        dispatch(activityLoading(false));
        dispatch(wellBeingLoading(false));
        dispatch(allDataNull([]));
        setTimeout(() => {
            navigation.navigate('medicineScreenStack')
        }, 1500)

    }
    const setEmailFormat = (e) => {
        let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
        if (e === '' || reg.test(e) === false) {
            // alert(e)
            setUserEmail(e)
            setEmailError(strings.EMAIL_IN_PROPER_FORMAT)
        }
        else {
            setUserEmail(e)
            setEmailError(null)
        }
    }
    const AddNewUser = () => {
        // alert(emailErr)
        if (!fnameErr) {
            setFnameErrtext(strings.FIRSTNAME_REQUIRED)
            return false;
          }
          if (!lnameErr) {
            setLnameErrtext(strings.LASTNAME_REQUIRED)
            return false;
          }
          if (emailErr != null) {
            setEmailError(strings.EMAIL_REQUIRED)
            return false;
          }
          if (!mobileErr) {
            setMobileErrtext(strings.MOBILE_REQUIRED)
            return false;
          }
          if (!streetErr) {
            setStreetErrtext(strings.STREET_REQUIRED)
            return false;
          }
          if (!cityErr) {
            setCityErrtext(strings.CITY_REQUIRED)
            return false;
          }
          if (!stateErr) {
            setStateErrtext(strings.STATE_REQUIRED)
            return false;
          }
          if (!zipErr) {
            setZipErrtext(strings.ZIP_REQUIRED)
            return false;
          }
        //   return false;
        // setTimeout(() => {
        //     // navigation.navigate('medicineScreenStack')
        //     navigation.goBack()
        // }, 1500); return false;
        setLoading(true)
        // if(fieldDisable){
            var data = allValues;
            data.senderid = currentUser.id
            data.sendertype = currentUser.usertype,
            data.receiverid = userId
            data.receivertype = receiverType,
            data.receivername = firstname,
            data.sendername = currentUser.firstname;
        // }else{
            // alert(currentUser.id); return false;
        // var data ={}
        // data.email = email,
        // data.senderid = currentUser.id,
        // data.sendertype = currentUser.usertype,
        // data.receiverid = userId,
        // data.receivertype = receiverType,
        // data.receivername = firstname,
        // data.sendername = currentUser.firstname;
        // }
        data.appsecret = config.appsecret
        data.action = 'sendaddpatientreqfromhcp'
        data.base_url = config.apiUrl,
        // console.log("adfd:", data); return false;
        axios.post(config.apiUrl + "insertrelationdata.php", data).then(result => {
        setLoading(false)
            console.log("result.data:", result.data)
            if (result.data.status == 'success') {
                setLoading(false);
                hideModal();
                setsnackMsg(strings.USER_ADDED_SUCCESSFULLY)
                clearAllFields()
                setVisibleSnack(true)
                setTimeout(() => {
                    // navigation.navigate('medicineScreenStack')
                    navigation.goBack()
                }, 1500)
            }else if(result.data.status == 'recordExists') {
                setLoading(false);
                hideModal();
                setsnackMsg(strings.REQUEST_ALREADY_SENT)
                setVisibleSnack(true)
                setTimeout(() => {
                    // navigation.navigate('medicineScreenStack')
                    navigation.goBack()
                }, 1500)
                // setVisible1(false)
            }else if(result.data.status == 'errorEmail'){
                // setVisible1(false)
                setsnackMsg(strings.USER_ADDED_BUT_EMAIL_COUDNT_SENT)
                setVisibleSnack(true)
            }else{
                setsnackMsg(strings.PROBLEM_ADDING_USER_CONTACT_ADMIN)
                setVisibleSnack(true)
            }

        })
    }
    const AddUser = () => {
        if(fieldDisable){
          if (fnameErr != null) {
            setFnameErrtext(strings.FIRSTNAME_REQUIRED)
            return false;
          }
          if (lnameErr != null) {
            setLnameErrtext(strings.LASTNAME_REQUIRED)
            return false;
          }
          if (emailErr != null) {
            setEmailError(strings.EMAIL_REQUIRED)
            return false;
          }
          if (mobileErr != null) {
            setMobileErrtext(strings.MOBILE_REQUIRED)
            return false;
          }
          if (streetErr != null) {
            setStreetErrtext(strings.STREET_REQUIRED)
            return false;
          }
          if (cityErr  != null) {
            setCityErrtext(strings.CITY_REQUIRED)
            return false;
          }
          if (stateErr != null) {
            setStateErrtext(strings.STATE_REQUIRED)
            return false;
          }
          if (zipErr != null) {
            setZipErrtext(strings.ZIP_REQUIRED)
            return false;
          }
        }
        setLoading(true)
        var data = new FormData();
        if(fieldDisable){
            // var data = allValues;
            // data.email = userEmail,
            // data.senderid = currentUser.id,
            // data.sendertype = currentUser.usertype,
            // data.receiverid = userId,
            // data.receivertype = toBeAddedUsertype,
            // data.receivername = firstname,
            // data.sendername = currentUser.firstname;
            // data.fcmtoken = token;
            // data.action = 'sendaddnewuserreq'
            // console.log('sendaddnewuserreq:', data)
            data.append('email', userEmail);
            data.append('senderid', currentUser.id);
            data.append('sendertype', currentUser.usertype);
            data.append('receiverid', userId);
            data.append('receivertype', toBeAddedUsertype);
            data.append('receivername', firstname);
            data.append('sendername', currentUser.firstname);
            data.append('firstname', allValues.firstname);
            data.append('lastname', allValues.lastname);
            data.append('mobile', allValues.mobile);
            data.append('birthdate', allValues.birthdate);
            data.append('sex', allValues.sex);
            data.append('street', allValues.street);
            data.append('city', allValues.city);
            data.append('state', allValues.state);
            data.append('zipcode', allValues.zipcode);
            data.append('fcmtoken', token);
            data.append('action', 'sendaddnewuserreq');
            data.append('appsecret', config.appsecret);
            data.append('base_url', config.apiUrl);
            data.append('activity', 'add new user request');
            data.append('page', 'adduserscreen');
            // alert("allvalues1:",allValues.image); return false;
            if(allValues.newimage != ''){
                data.append('image',
                {
                    uri:allValues.newimage,
                    name:allValues.newimage,
                });
            }else{
                data.append('image','');
            }
        }else{
            data.append('id', allValues.id);
            data.append('email', email);
            data.append('senderid', currentUser.id);
            data.append('sendertype', currentUser.usertype);
            data.append('receiverid', userId);
            data.append('receivertype', receiverType);
            data.append('receivername', firstname);
            data.append('sendername', currentUser.firstname);
            data.append('action', 'sendadduserreq');
            data.append('appsecret', config.appsecret);
            data.append('base_url', config.apiUrl);
            data.append('activity', 'add user request');
            data.append('page', 'adduserscreen');
            // console.log("allvalues:", allValues); return false;
            if(allValues.newimage !== ''){
                data.append('image',
                {
                    uri:allValues.newimage,
                    name:allValues.newimage,
                });
            }else{
                data.append('image','');
            }
            // var data ={}
            // data.email = email,
            // data.senderid = currentUser.id,
            // data.sendertype = currentUser.usertype,
            // data.receiverid = userId,
            // data.receivertype = receiverType,
            // data.receivername = firstname,
            // data.sendername = currentUser.firstname;
            // data.action = 'sendadduserreq'
        }
        // data.appsecret = config.appsecret
        // data.base_url = config.apiUrl,
        // console.log("adfd::", navigation.navigation.navigate); return false;
        axios.post(config.apiUrl + "insertrelationdata.php", data, {
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'multipart/form-data',
            }
        }).then(result => {
        setLoading(false)
            console.log("result.data:", result.data)
            if (result.data.status == 'success') {
                setLoading(false);
                hideModal();
                setsnackMsg(strings.USER_ADDED_SUCCESSFULLY)
                setVisibleSnack(true)
                clearAllFields()
                // return false;
                setTimeout(() => {
                    navigation.navigation.goBack()
                }, 1500)
            }else if(result.data.status == 'uploadError'){
                setsnackMsg(strings.PROBLEM_UPLOADING_IMAGE)
                setVisibleSnack(true)
                setLoading(false);
            }else if(result.data.status == 'recordExists') {
                setLoading(false);
                hideModal();
                setsnackMsg(strings.REQUEST_ALREADY_SENT)
                setVisibleSnack(true)
                clearAllFields()
                setTimeout(() => {
                    navigation.navigation.goBack()
                }, 1500)
                // setVisible1(false)
            }else if(result.data.status == 'errorEmail'){
                // setVisible1(false)
                setsnackMsg(strings.USER_ADDED_BUT_EMAIL_COUDNT_SENT)
                setVisibleSnack(true)
            }else{
                setsnackMsg(strings.PROBLEM_ADDING_DATA)
                setVisibleSnack(true)
                clearAllFields()
                setTimeout(() => {
                    navigation.navigation.goBack()
                }, 1500)
            }

        })
    }

    const getHcpDropDownList = () => {
        var dataToSend = {
            action: 'getallhcps',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        if (currentUser.usertype === 'admin') {
            dataToSend.adminid = currentUser.id;
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : "")))
        }
        if (currentUser.usertype === 'patient') {
            (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.hcpid = currentUser.id;
        }
        if (currentUser.usertype === 'doctor') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.doctorid = currentUser.id;
        }
        if (currentUser.usertype === 'nurse') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : '')))
            dataToSend.nurseid = currentUser.id;
        }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            id: val.uid,
                            // icon: () => <View style={{ width: 20, height: 20, borderRadius: 10, backgroundColor: "ff0000" }} />,
                            // name: <View style={{flexDirection:'row'}}><Image source={usericn} style={{ width: 40, height: 40, borderRadius: 30, borderWidth:1 }} /><Text>{val.firstname} {val.lastname}</Text></View>,
                            name: val.firstname + ' ' + val.lastname,
                        }
                        console.log("result.data12312345", ob)
                        arr.push(ob)
                    })
                    setHcpDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }
    const getdoctorDropDownList = () => {
        var dataToSend = {
            action: 'getalldoctors',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        if (currentUser.usertype === 'admin') {
            dataToSend.adminid = currentUser.id;
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : "")))
        }
        if (currentUser.usertype === 'healthcareprovider') {
            (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.hcpid = currentUser.id;
        }
        if (currentUser.usertype === 'patient') {
            // alert(navigation.currusertype)
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.doctorid = currentUser.id;
        }
        if (currentUser.usertype === 'nurse') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : '')))
            dataToSend.nurseid = currentUser.id;
        }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            id: val.uid,
                            name: val.firstname + ' ' + val.lastname,
                        }
                        console.log("result.data123123", ob)
                        arr.push(ob)
                    })
                    setDoctorDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }
    const getnurseDropDownList = () => {
        var dataToSend = {
            action: 'getallnurses',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        if (currentUser.usertype === 'admin') {
            dataToSend.adminid = currentUser.id;
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : "")))
        }
        if (currentUser.usertype === 'healthcareprovider') {
            (navigation.currusertype === 'doctor' ? dataToSend.doctorid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.hcpid = currentUser.id;
        }
        if (currentUser.usertype === 'patient') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.doctorid = currentUser.id;
        }
        if (currentUser.usertype === 'doctor') {
            (navigation.currusertype === 'healthcareprovider' ? dataToSend.hcpid = navigation.userid : (navigation.currusertype === 'patient' ? dataToSend.patientid = navigation.userid : (navigation.currusertype === 'nurse' ? dataToSend.nurseid = navigation.userid : '')))
            dataToSend.doctorid = currentUser.id;
        }
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            id: val.uid,
                            name: val.firstname + ' ' + val.lastname,
                        }
                        console.log("result.data123123", ob)
                        arr.push(ob)
                    })
                    setNurseDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }
    const clearAllFields=()=>{
        setselectedItem('{}')
        setFieldDisable(true)
        setUserEmail('')
        setEmail('')
        SetallValues({ ...allValues, filename: '', filetype: '', image: '', sourceURL:'', filesize:'', allimageData:'', email:'' });
        // SetallValues({ ...allValues, email: '' })
        // SetallValues({ ...allValues, firstname: '' })
        
        // setEmailError('')
    }
    const clearEmailAddress=()=>{
        // alert(userEmail)
        setselectedItem('{}')
        setUserEmail('')
        setEmail('')
        SetallValues({ ...allValues, email: '' })
        setEmailError('')
    }    
    return (
        
        <View style={styles.mainBody}>
            {/* <KeyboardAwareScrollView enableOnAndroid={true} style={{ width: '100%' }}> */}
                <View style={{ alignItems: 'flex-start', justifyContent: 'center', alignSelf: 'center', flexWrap: 'wrap', flexDirection: 'row', marginTop: 0, }}>
                    <View style={{ padding: 0, width: '45%', top:190, left:20, position:'absolute', zIndex:1000 }}>
                        <Autocomplete
                        autoCapitalize="none"
                        autoCorrect={false}
                        style={{ width: '100%', fontFamily: 'HelveticaNeue-Light', fontSize: 16, height:40, borderBottomColor:2, top:5, color:'#81889f', }}
                        // borderBottomWidth={2}
                        // borderColor={'#81889f'}
                        placeholderTextColor = "#81889f"
                        containerStyle={styles.AutocompleteStyle}
                        data={FilterData}
                        defaultValue={
                            JSON.stringify(selectedItem) === '{}' ? '' : selectedItem.email
                        }
                        keyExtractor={(item, i) => i.toString()}
                        onChangeText={(text) => {SearchDataFromJSON(text), setEmailFormat(text)} }
                        placeholder={strings.TYPE_OR_SEARCH_EMAIL}
                        renderItem={({item}) => (
                            <TouchableOpacity
                            onPress={() => {
                                // console.log("asdfasdd:", item)
                                item.birthdate = "****"
                                item.zipcode = "****"
                                item.street = "****"
                                item.city = "****"
                                item.state = "****"
                                item.country = "****"
                                item.mobile = "****"
                                setReceiverType(item.usertype)
                                setUserId(item.uid)
                                setFirstname(item.firstname)
                                setLastname(item.lastname)
                                setEmail(item.email)
                                setMobile(item.mobile)
                                setBirthdate(item.birthdate)
                                setStreet(item.street)
                                setZipcode(item.zipcode)
                                setCity(item.city)
                                setAddressState(item.state)
                                setCountry(item.country)
                                setFieldDisable(false);
                                setselectedItem(item);
                                setEmailError('')
                                setFilterData([]);
                                // alert(selectedItem.name)
                                // SetallNotificationValues({ ...allValuesNotification, ['title']: selectedItem.name });
                            }}>
                            <Text style={styles.SearchBoxTextItem}>
                                {item.email}
                            </Text>
                            </TouchableOpacity>
                        )}
                        />
                        <View>{emailErr != '' ? (
                        <Text style={styles.errorTextStyle}>
                            {emailErr}
                        </Text>
                        ) : null}
                        </View>
                    </View>
                    <View style={{ padding: 0, width: '45%' }}>
                        <Text style={{ fontFamily: 'HelveticaNeue-Light', color: 'white', fontSize: 14, top: 40, width: 200, }}>{strings.ADD_PHOTO}</Text>
                        {!fieldDisable ?
                            !selectedItem.profilepic ?
                            <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center' }]} >
                                <Image
                                    source={usericn}
                                    style={{ width: 146, height: 146, top: 0 }}
                                />
                            </TouchableOpacity> :
                            <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center', top: -20 }]}>
                                <Image
                                    source={{ uri: config.apiUrl+selectedItem.profilepic }}
                                    style={{ width: 256, height: 146, borderRadius: 10 }}
                                />
                                <TouchableOpacity onPress={() => SetallValues({ ...allValues, profilepic: '' })} ><Icon name="close" color="#81889f" size={22} /></TouchableOpacity>
                            </TouchableOpacity>
                        :
                        !allValues.image && !allValues.newimage ?
                            <TouchableOpacity onPress={chooseOption} style={[styles.imageBackStyle, { alignSelf: 'center' }]} >
                                <Image
                                    source={upload}
                                    style={{ width: 78, height: 78, top: 30 }}
                                />
                            </TouchableOpacity> :
                            <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center', top: -20 }]}>
                                <Image
                                    source={{ uri: allValues.image != '' ? config.apiUrl+allValues.image : allValues.newimage }}
                                    style={{ width: 256, height: 146, borderRadius: 10 }}
                                />
                                <TouchableOpacity onPress={() => SetallValues({ ...allValues, image: '' })} ><Icon name="close" color="#81889f" size={22} /></TouchableOpacity>
                            </TouchableOpacity>
                        }
                        <View style={{ marginTop: 20, }}>
                            <Text style={{ flexDirection: 'row',  }}>
                                <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.SEARCH_OR_ENTER_EMAIL}</Text>
                                <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text>
                            </Text>
                            {/* <Text style={{ color: '#e0675c' }}>{allValues.name === '' ? 'Please enter name' : ''}</Text> */}
                        </View>
                        <TouchableOpacity onPress={clearEmailAddress}> 
                            <Icon style={{position:'absolute', marginTop:10, left:450, zIndex:1000}} name="close"  size={22} color="#81889f"/>
                        </TouchableOpacity>
                        <View style={{ marginTop: 60 }}>
                            
                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.FIRST_NAME}<Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                            <TextInput
                                style={{ width: '100%', color: 'white', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                placeholder={strings.PLEASE_ENTER_FIRST_NAME}
                                placeholderTextColor={'#81889f'}
                                editable={fieldDisable}
                                // value={allValues.activeingredients}
                                value={fieldDisable ? allValues.firstname : selectedItem.firstname}
                                // onChangeText={val => {setFnameErr(val)}}
                                onChangeText={val => {SetallValues({ ...allValues, ['firstname']: val }), setFnameErr(val)}}
                                // onChangeText={(Name) => setfname(Name)}
                                borderBottomWidth={2}
                                borderColor={'#81889f'}
                            />
                            {fnameErr != '' ? (
                            <Text style={[styles.errorTextStyle, {top:0}]}>
                                {fnameErr}
                            </Text>
                            ) : null}
                        </View>
                        <View style={{ marginTop: 40 }}>
                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.LAST_NAME}<Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                            <TextInput
                                style={{ width: '100%', color: 'white', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                placeholder={strings.PLEASE_ENTER_LAST_NAME}
                                placeholderTextColor={'#81889f'}
                                editable={fieldDisable}
                                value={fieldDisable ? allValues.lastname : selectedItem.lastname}
                                onChangeText={val => {SetallValues({ ...allValues, ['lastname']: val }), setLnameErr(val)}}
                                borderBottomWidth={2}
                                borderColor={'#81889f'}
                            />
                            {lnameErr != '' ? (
                            <Text style={[styles.errorTextStyle, {top:0}]}>
                                {lnameErr}
                            </Text>
                            ) : null}
                        </View>
                        {/* <View style={{ marginTop: 40 }}>
                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.BIRTH_DATE}<Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                            <TextInput
                                style={{ width: '100%', color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                placeholder={strings.PLEASE_ENTER_BIRTHDATE}
                                placeholderTextColor={'#81889f'}
                                // editable={fieldDisable}
                                editable={false}
                                value={fieldDisable ? allValues.birthdate : selectedItem.birthdate}
                                // onChangeText={val => {SetallValues({ ...allValues, ['birthdate']: val }), setBdErr(val)}}
                                onTouchStart={showDatePicker}
                                borderBottomWidth={2}
                                borderColor={'#81889f'}
                            />
                            {bdErr != '' ? (
                            <Text style={[styles.errorTextStyle, {top:0}]}>
                                {bdErr}
                            </Text>
                            ) : null}
                        </View> */}
                    </View>
                    <View style={{ marginRight: 5, marginLeft: 50, marginTop: 15, width: '45%' }}>
                        <View style={{ marginTop: 20 }}>
                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.MOBILE_NUMBER}<Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                            <TextInput
                                style={{ width: '100%', color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                placeholder={strings.PLEASE_ENTER_MOBILE_NUMBER}
                                placeholderTextColor={'#81889f'}
                                editable={fieldDisable}
                                value={fieldDisable ? allValues.mobile : selectedItem.mobile}
                                onChangeText={val => {SetallValues({ ...allValues, ['mobile']: val }), setMobileErr(val)}}
                                borderBottomWidth={2}
                                borderColor={'#81889f'}
                            />
                            {mobileErr != '' ? (
                            <Text style={[styles.errorTextStyle, {top:0}]}>
                                {mobileErr}
                            </Text>
                            ) : null}
                        </View>
                        <View style={{ marginTop: 40 }}>
                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{strings.FULL_ADDRESS}<Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text></Text>
                            {/* {fieldDisable && <>  */}
                            <TextInput
                                style={{ width: '100%', color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                placeholder={strings.PLEASE_ENTER_STREET}
                                placeholderTextColor={'#81889f'}
                                editable={fieldDisable}
                                value={fieldDisable ? allValues.street : selectedItem.street}
                                onChangeText={val => {SetallValues({ ...allValues, ['street']: val }), setStreetErr(val)}}
                                borderBottomWidth={1}
                                borderColor={'#81889f'}
                            />
                            {streetErr != '' ? (
                            <Text style={[styles.errorTextStyle, {top:0}]}>
                                {streetErr}
                            </Text>
                            ) : null}
                            <TextInput
                                style={{ width: '100%', marginTop:20, color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                placeholder={strings.PLEASE_ENTER_CITY}
                                placeholderTextColor={'#81889f'}
                                editable={fieldDisable}
                                value={fieldDisable ? allValues.city : selectedItem.city}
                                onChangeText={val => {SetallValues({ ...allValues, ['city']: val }), setCityErr(val)}}
                                borderBottomWidth={1}
                                borderColor={'#81889f'}
                            />
                            {cityErr != '' ? (
                            <Text style={[styles.errorTextStyle, {top:0}]}>
                                {cityErr}
                            </Text>
                            ) : null}
                            <TextInput
                                style={{ width: '100%', marginTop:20, color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                placeholder={strings.PLEASE_ENTER_STATE}
                                placeholderTextColor={'#81889f'}
                                editable={fieldDisable}
                                value={fieldDisable ? allValues.state : selectedItem.state}
                                onChangeText={val => {SetallValues({ ...allValues, ['state']: val }), setStateErr(val)}}
                                borderBottomWidth={1}
                                borderColor={'#81889f'}
                            />
                            {stateErr != '' ? (
                            <Text style={[styles.errorTextStyle, {top:0}]}>
                                {stateErr}
                            </Text>
                            ) : null}
                            <TextInput
                                style={{ width: '100%', marginTop:20, color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                placeholder={strings.PLEASE_ENTER_ZIP}
                                placeholderTextColor={'#81889f'}
                                editable={fieldDisable}
                                value={fieldDisable ? allValues.zipcode : selectedItem.zipcode}
                                onChangeText={val => {SetallValues({ ...allValues, ['zipcode']: val }), setZipErr(val)}}
                                borderBottomWidth={2}
                                borderColor={'#81889f'}
                            />
                            {zipErr != '' ? (
                            <Text style={[styles.errorTextStyle, {top:0}]}>
                                {zipErr}
                            </Text>
                            ) : null}
                        </View>
                        {/* {!fieldDisable &&
                            <TextInput
                                style={{ width: '100%', color: '#81889f', fontFamily: 'HelveticaNeue-Light', fontSize: 16 }}
                                placeholder={strings.PLEASE_ENTER_ADDRESS}
                                placeholderTextColor={'#81889f'}
                                editable={fieldDisable}
                                value={fieldDisable ? (allValues.street+ ' '+allValues.city+ ' '+allValues.state+ ' '+allValues.country+ ' '+allValues.zipcode) : (selectedItem.street+ ' '+selectedItem.city+ ' '+selectedItem.state+ ' '+selectedItem.country+ ' '+selectedItem.zipcode)}
                                onChangeText={val => SetallValues({ ...allValues, ['street']: val })}
                                borderBottomWidth={2}
                                borderColor={'#81889f'}
                           /> 
                        } */}
                        <View style={{marginTop:40}}>
                            <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>
                                {strings.SEX} <Text style={{ color: '#e0675c', marginBottom: 8 }}> *</Text>
                            </Text>
                        </View>
                        <View >
                            {fieldDisable &&
                            <RadioButton
                                selectedOption={allValues.sex}
                                onSelect={onSelect}
                                options={options}
                            />
                            }
                            {!fieldDisable && <Text style={{ color: 'white', marginBottom: 8, fontFamily: 'HelveticaNeue-Light', fontSize: 14 }}>{selectedItem.zipcode}</Text>}
                        </View>
                    </View>
                    <View style={{ width: '100%', flexDirection:'row', alignSelf: 'flex-end', alignItems: 'flex-end', justifyContent: 'flex-end' }}>
                        <TouchableOpacity onPress={clearAllFields} style={[styles.buttonStyle, { top: 10, marginBottom: 40, fontFamily: 'HelveticaNeue-Light', backgroundColor:'#fb7e7e' }]} activeOpacity={0.5}>
                            <Text style={styles.buttonTextStyle}> {strings.CLEAR_DATA}</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={AddUser} style={[styles.buttonStyle, { right:100, top: 10, marginBottom: 40, fontFamily: 'HelveticaNeue-Light' }]} activeOpacity={0.5}>
                            <Text style={styles.buttonTextStyle}> {strings.ADD_USER}</Text>
                        </TouchableOpacity>
                    </View>
                    {/* <Text style={{ marginBottom: 10 }}>
                        <Text style={{ color: 'white' }}>is this a prescripted medicine. </Text>
                        <TouchableOpacity>
                            <Text style={{ color: '#69c2d1', textDecorationLine: 'underline' }}>click here to add more details</Text>
                        </TouchableOpacity>
                    </Text> */}
                </View>
            {/* </KeyboardAwareScrollView> */}
            {isDatePickerVisible &&
                <View style={{ width: 600, position: 'absolute', marginLeft: 300, marginTop: 270 }}>
                    <DateTime
                        // date={userData.birthdate ? moment(userData.birthdate) : moment()}
                        date={fieldDisable ? moment(allValues.birthdate) : moment(selectedItem.birthdate)}
                        changeDate={(date) => handleConfirm(date)}
                        format='YYYY-MM-DD'
                        renderChildDay={(day) => renderChildDay(day)}
                    />
                </View>
            }
            <Snackbar
                style={{ width: 300, top:-30, alignSelf: 'center', alignItems: 'center' }}
                visible={visibleSnackbar}
                duration={3000}
                onDismiss={onDismissSnackBar}
            >
                {snackMsg}
            </Snackbar>
        </View>
                    
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 50,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    headerText: {
        color: 'white',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    activity: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: '100%',
        height: 50,
        borderRadius: 15,
        marginBottom: 17,
    },
    activityText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 220,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    activityColor: {
        borderColor: 'black',
        // borderWidth: 1,
        borderRadius: 30,
        width: 45,
        height: 45,
        left: 15,
        marginTop: 2,
    },
    activityColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        // position: 'absolute',
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8

    },
    scrollView: {
        height: '80%',
        width: '100%',
        // margin: 20,
        alignSelf: 'center',
        // padding: 20,
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 250,
    },
    itemStyle:{
        padding: 10,
        marginTop: 2,
        backgroundColor: '#474F69',
        borderColor: '#474F69',
        borderWidth: 1,
        borderRadius: 5,
        color:'#fff'
    },
    textInputStyle:{
        padding: 8,
        borderWidth: 1,
        borderColor: 'transparent',
        backgroundColor: '#474F69',
        width: 200,
        borderRadius:5,
        color:'#fff'
    },
    imageBackStyle: {
        alignSelf: 'center',
        backgroundColor: '#39415b',
        borderRadius: 10,
        height: 146,
        left: 100,
        alignItems: 'center',
        width: 256
    },
    AutocompleteStyle: {
        // flex: 1,
        // left: 0,
        position: 'absolute',
        // right: 0,
        // top: 0,
        zIndex: 890,
        width:'100%',
        color:'#81889f',
    //    borderWidth:1
        fontFamily: 'HelveticaNeue-Light', fontSize: 16, height:40, borderBottomWidth:2, borderBottomColor:'#81889f', 
      },
      SearchBoxTextItem: {
        margin: 5,
        fontSize: 16,
        paddingTop: 4,
        zIndex:850,
        position:'relative',
        // top:-200,
        // left:50,
        // width:200,
        // height:200,
      },
      errorTextStyle: {
        top:40,
        color: '#e0675c',
        textAlign: 'left',
        fontSize: 14,
        left: 12
      },
});

export default AddUserScreen;
