import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux'
import {
  TextInput,
  Image,
  TouchableOpacity,
  Switch,
  SafeAreaView,
  View,
  Text,
  Alert,
  StyleSheet,
  RefreshControl,
  Button,
  FlatList,
  ActivityIndicator,
  Dimensions
} from 'react-native';
import moment from "moment";
import config from '../appconfig/config';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Loader from '../Components/Loader';
import { VictoryPie, VictoryChart, VictoryLabel, VictoryTheme, VictoryBar } from 'victory-native';
import Svg from 'react-native-svg';
import axios from 'axios';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const height = Dimensions.get('window').height;
console.log("height", height)
const WellbeingDashboardQuaterly = (props) => {
  const currentUser = useSelector(state => state.auth.userLogin)
  const sessionUser = useSelector(state => state.auth.requestedUser)
  const [loading, setLoading] = useState(false);
  const [visible, setVisible] = React.useState(true);
  const [selectedDate, setSelectedDate] = useState(new Date());
  const [AsleepBarChartArray, setAsleepBarChartArray] = useState([]);
  const [AwakeBarChartArray, setAwakeBarChartArray] = useState([]);
  const [PainBarChartArray, setPainBarChartArray] = useState([]);
  const [UncomfortBarChartArray, setUncomfortBarChartArray] = useState([]);
  const [DistressDoughnut, setDistressDoughnut] = useState(0);
  const [SleepDoughnut, setSleepDoughnut] = useState(0);
  const [months] = useState([strings.JAN_FEB_MAR_APR_MAY]);
  const [ComfortableDoughnut, setComfortableDoughnut] = useState(0);
  const [UncomfortDoughnut, setUnomfortableDoughnut] = useState(0);
  const [YearlyHour] = useState(8760);
  const [TotalSleep, setTotalSleep] = useState(0);
  const [TotalDistress, setTotalDistress] = useState(0);
  const [TotalComfort, setTotalComfort] = useState(0);
  const [TotalUncomfort, setTotalUncomfort] = useState(0);
  const [SleepHours, setSleepHours] = useState("0:0");
  const [DistressHours, setDistressHours] = useState("0:0");
  const [ComfortHours, setComfortHours] = useState("0:0");
  const [UncomfortHours, setUncomfortHours] = useState("0:0");
  const [week, setWeek] = useState([]);
  const hours = (hh) => {
    if (hh !== undefined)
      return hh.split(":")[0];
  }
  const minutes = (mm) => {
    if (mm !== undefined)
      return mm.split(":")[1];
  }

  const avg = (hh) => {
    if (hh !== undefined)
      return Math.ceil(hh.split(":")[0] / 12);

  }
  const showModal = () => {
    setVisible(true)
  };
  const hideModal = () => {
    setVisible(false)
  };

  useEffect(() => {
    appIntialize();
  }, [props.selectedDate])



  const calulateTotalTime = (timeArray) => {
    const sum = timeArray.reduce((acc, time) => acc.add(moment.duration(time)), moment.duration());
    return [Math.floor(sum.asHours()), sum.minutes()].join(':')
  }


  const appIntialize = (date) => {
    var year = moment(props.selectedDate).format('YYYY');
    console.log("date123",year)
    // console.log("date123",props.selectedDate)
    setAsleepBarChartArray([]);
    setAwakeBarChartArray([]);
    setPainBarChartArray([]);
    setUncomfortBarChartArray([]);
    setSleepHours("0:0");
    setDistressHours("0:0");
    setComfortHours("0:0");
    setUncomfortHours("0:0");
    setLoading(true)
    var data = {};
    data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
    data.appsecret = config.appsecret;
    data.currentYear = year;
    data.action = 'getyearlygraphdata';
    axios.post(config.apiUrl + 'getgraphs.php', data).then(result => {
      setLoading(false)
      var data1 = result && result.data && result.data ? result.data : null;
      var chartData = data1.graphSumData ? data1.graphSumData : [];
      if (data1.status == "success") {
        if (chartData && chartData.length > 0) {
          var AwakeArray = [];
          var AsleepArray = [];
          var PainArray = [];
          var DistressArray = [];
          var SleepArray = [];
          var UncomfortArray = [];
          var UncomfortableArray = [];
          var ComfortArray = [];
          var TotalC = 0;
          var TotalS = 0;
          var TotalD = 0;
          var TotalU = 0;
          for (var i in chartData) {
            if (chartData[i].name == "Awake") {
              AwakeArray.push(chartData[i])
              TotalC += parseInt(chartData[i].count_name);
              ComfortArray.push(chartData[i].timediff)
            }
            if (chartData[i].name == "Asleep") {
              AsleepArray.push(chartData[i])
              TotalS += parseInt(chartData[i].count_name);
              SleepArray.push(chartData[i].timediff)
            }
            if (chartData[i].name == "Pain") {
              PainArray.push(chartData[i])
              TotalD += parseInt(chartData[i].count_name);
              DistressArray.push(chartData[i].timediff)
            }
            if (chartData[i].name == "Uncomfortable") {
              UncomfortArray.push(chartData[i])
              TotalU += parseInt(chartData[i].count_name);
              UncomfortableArray.push(chartData[i].timediff)
            }
          };
          setTotalComfort(TotalC)
          setTotalDistress(TotalD)
          setTotalSleep(TotalS)
          setTotalUncomfort(TotalU)
          var DistressHours = calulateTotalTime(DistressArray);
          setDistressHours(DistressHours)
          setDistressDoughnut(parseInt(DistressHours.split(":")[0]))
          var SleepHours = calulateTotalTime(SleepArray);
          setSleepHours(SleepHours)
          setSleepDoughnut(parseInt(SleepHours.split(":")[0]))
          var ComfortHours = calulateTotalTime(ComfortArray);
          setComfortHours(ComfortHours)
          setComfortableDoughnut(parseInt(ComfortHours.split(":")[0]))
          var UncomfortHours = calulateTotalTime(UncomfortableArray);
          setUncomfortHours(UncomfortHours);
          setUnomfortableDoughnut(parseInt(UncomfortHours.split(":")[0]))
          var Aindex = 0
          var Pindex = 0
          var Uindex = 0
          var Sindex = 0
          for (let i = 1; i <= 12; i++) {
            if (AsleepArray[Sindex] !== undefined) {
              if (i == parseInt(AsleepArray[Sindex].month_no)) {
                var tmphour = parseInt(AsleepArray[Sindex].timediff.split(":")[0])
                console.log("temhouse", tmphour)
                setAsleepBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: tmphour }])
                Sindex = Sindex + 1;
              }
              else {
                setAsleepBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: 0 }])
              }

            }
            else {
              setAsleepBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: 0 }])
            }
            if (PainArray[Pindex] !== undefined) {
              if (i == parseInt(PainArray[Pindex].month_no)) {
                var tmphour = parseInt(PainArray[Pindex].timediff.split(":")[0])
                setPainBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: tmphour }])
                Pindex = Pindex + 1;
              }
              else {
                setPainBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: 0 }])
              }
            }
            else {
              setPainBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: 0 }])
            }
            if (AwakeArray[Aindex] !== undefined) {
              if (i == parseInt(AwakeArray[Aindex].month_no)) {
                var tmphour = parseInt(AwakeArray[Aindex].timediff.split(":")[0])
                setAwakeBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: tmphour }])
                Aindex = Aindex + 1;
              }
              else {
                setAwakeBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: 0 }])
              }
            }
            else {
              setAwakeBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: 0 }])
            }
            if (UncomfortArray[Uindex] !== undefined) {
              if (i == parseInt(UncomfortArray[Uindex].month_no)) {
                var tmphour = parseInt(UncomfortArray[Uindex].timediff.split(":")[0])
                setUncomfortBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: tmphour }])
                Uindex = Uindex + 1;
              }
              else {
                setUncomfortBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: 0 }])
              }
            }
            else {
              setUncomfortBarChartArray(oldArray => [...oldArray, { x: months[i - 1], y: 0 }])
            }
          }

        }
        else {
          setAwakeBarChartArray([{ x: months[0], y: 0 }, { x: months[1], y: 0 }, { x: months[2], y: 0 }, { x: months[3], y: 0 }, { x: months[4], y: 0 }, { x: months[5], y: 0 }, { x: months[6], y: 0 }, { x: months[7], y: 0 }, { x: months[8], y: 0 }, { x: months[9], y: 0 }, { x: months[10], y: 0 }, { x: months[11], y: 0 }]);
          setPainBarChartArray([{ x: months[0], y: 0 }, { x: months[1], y: 0 }, { x: months[2], y: 0 }, { x: months[3], y: 0 }, { x: months[4], y: 0 }, { x: months[5], y: 0 }, { x: months[6], y: 0 }, { x: months[7], y: 0 }, { x: months[8], y: 0 }, { x: months[9], y: 0 }, { x: months[10], y: 0 }, { x: months[11], y: 0 }])
          setAsleepBarChartArray([{ x: months[0], y: 0 }, { x: months[1], y: 0 }, { x: months[2], y: 0 }, { x: months[3], y: 0 }, { x: months[4], y: 0 }, { x: months[5], y: 0 }, { x: months[6], y: 0 }, { x: months[7], y: 0 }, { x: months[8], y: 0 }, { x: months[9], y: 0 }, { x: months[10], y: 0 }, { x: months[11], y: 0 }])
          setUncomfortBarChartArray([{ x: months[0], y: 0 }, { x: months[1], y: 0 }, { x: months[2], y: 0 }, { x: months[3], y: 0 }, { x: months[4], y: 0 }, { x: months[5], y: 0 }, { x: months[6], y: 0 }, { x: months[7], y: 0 }, { x: months[8], y: 0 }, { x: months[9], y: 0 }, { x: months[10], y: 0 }, { x: months[11], y: 0 }])
        }

      }
    }, err => {
      loadingService.hide();
      var a = abc(a, b);
    });
  }
  console.log("As", AwakeBarChartArray)
  console.log("As", PainBarChartArray)
  console.log("As", AsleepBarChartArray)
  console.log("As", UncomfortBarChartArray)

  const sampleData = [
    { x: 'Mo', y: 13 },
    { x: 'Tu', y: 15 },
    { x: 'We', y: 14 },
    { x: 'Th', y: 16 },
    { x: 'Fr', y: 10 },
    { x: 'Sa', y: 11 },
    { x: 'Su', y: 12 },
  ];
  const donutData = [
    { y: 25, label: " " },
    { y: 15, label: " " },
  ];

  const graphRadius = 10;
  const domainPaddingX = 15;
  const chartWidth = 300;
  const chartHeight = 130;
  const dataWidth = 20;
  return (
    <View style={{ marginTop: 85 }}>
<Loader loading={loading} />
      <View style={{ flexDirection: 'row', marginBottom: 100, width: '100%', position: 'absolute', }}>
        <View style={styles.registration}>
          <View style={{ borderBottomWidth: 5, borderBottomColor: '#6bb1d6', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.TOTAL}</Text>
          </View>
          <View style={{ width: 180, alignItems: 'center', position: 'relative', top: 10, right: 0, }}>
            <Text style={styles.h1}>{TotalSleep} </Text>
            <Text style={styles.h5}>{strings.REGISTRATIONS} </Text>
          </View>
        </View>
        <View style={styles.hours}>
          <View style={{ borderBottomWidth: 2, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14 }}>SLEEP HOURS</Text>
          </View>
          <View style={{ flexDirection: 'row', width: 250, alignItems: 'center', position: 'relative', top: 20, left: 30, }}>
            <Text style={styles.h1}>{hours(SleepHours)} </Text>
            <Text style={styles.hrs}>{strings.HRS.substring(0,3)} </Text>
            <Text style={styles.h1}>{minutes(SleepHours)} </Text>
            <Text style={styles.hrs}>{strings.MIN.substring(0,3)} </Text>
          </View>
        </View>
        <View style={{ width: 150, height: 120, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
          <Svg>
            <VictoryPie
              data={[{ y: YearlyHour, label: "168" }, { y: SleepDoughnut, label: " " }]}
              labels={(datum) => `${datum.y}`}
              radius={40}
              innerRadius={20}
              width={110}
              height={110}
              colorScale={['#6bb1d6', '#3d7c9e']}
              events={[{
                target: "data",
                eventHandlers: {
                  onPress: () => {
                    return [
                      {
                        target: "data",
                        mutation: ({ style }) => {
                          return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                        }
                      }, {
                        target: "labels",
                        mutation: ({ text }) => {
                          return text === "clicked" ? null : { text: "clicked" };
                        }
                      }
                    ];
                  }

                }
              }]}
            />
            <VictoryLabel
              textAnchor="middle"
              style={[{ fill: "#fff", fontSize: 14, fontWeight:'bold' }]}
              x={55} y={55}
              text={SleepDoughnut+'/\n'+YearlyHour}
            />
            </Svg>
            <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="circle" size={16} color="#6bb1d6" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.TOTAL}</Text>
              <Icon name="circle" size={16} color="#3d7c9e" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.CONSUME}</Text>
            </View>
          </View>
        </View>
        <View style={styles.average}>
          <View style={{ width: 150, height: 50, alignItems: 'center', position: 'relative', top: 20, right: 0, borderBottomWidth: 2, height: 90, borderBottomColor: '#81889f' }}>
            <Text style={styles.h1}>{avg(SleepHours)} </Text>
          </View>
          <View style={{ width: 150, alignItems: 'center', position: 'relative', top: 25, right: 0, }}>
            <Text style={styles.h5}>{strings.AVERAGE} </Text>
          </View>
        </View>
        <View style={styles.innerGraph}>
          <VictoryChart
            theme={VictoryTheme.material}
            domainPadding={{ x: domainPaddingX }}
            width={chartWidth} height={chartHeight}
            padding={{ bottom: 30, }}

          >
            <VictoryBar
              cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
              style={{
                data: {
                  fill: "#7aecff",
                  width: dataWidth
                },
                labels: {
                  fontSize: 15,
                  fill: ({ datum }) => "#fff"
                }
              }}
              labelComponent={<VictoryLabel dy={(datum)=>datum.y == 0 ? 15 : 0}/>}
              labels={({ datum }) => datum.y != 0 ? datum.y : ''}
              data={AsleepBarChartArray}
            />
          </VictoryChart>
        </View>
      </View>
      <View style={{ flexDirection: 'row', marginTop: 150, width: '100%', position: 'absolute' }}>
        <View style={styles.registration}>
          <View style={{ borderBottomWidth: 5, borderBottomColor: '#fad000', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.TOTAL}</Text>
          </View>
          <View style={{ width: 180, alignItems: 'center', position: 'relative', top: 10, right: 0, }}>
            <Text style={styles.h1}>{TotalDistress} </Text>
            <Text style={styles.h5}>{strings.REGISTRATIONS} </Text>
          </View>
        </View>
        <View style={styles.hours}>
          <View style={{ borderBottomWidth: 2, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14 }}>DISTRESS HOURS</Text>
          </View>
          <View style={{ flexDirection: 'row', width: 250, alignItems: 'center', position: 'relative', top: 20, left: 30 }}>
            <Text style={styles.h1}>{hours(DistressHours)} </Text>
            <Text style={styles.hrs}>{strings.HRS.substring(0,3)} </Text>
            <Text style={styles.h1}>{minutes(DistressHours)}</Text>
            <Text style={styles.hrs}>{strings.MIN.substring(0,3)} </Text>
          </View>
        </View>
        <View style={{ width: 150, height: 120, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
          <Svg>
            <VictoryPie
              data={[{ y: YearlyHour, label: " " }, { y: DistressDoughnut, label: " " }]}
              labels={(datum) => `${datum.y}`}
              radius={40}
              innerRadius={20}
              width={110}
              height={110}
              colorScale={['#fad000', '#967f0e']}
              events={[{
                target: "data",
                eventHandlers: {
                  onPress: () => {
                    return [
                      {
                        target: "data",
                        mutation: ({ style }) => {
                          return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                        }
                      }, {
                        target: "labels",
                        mutation: ({ text }) => {
                          return text === "clicked" ? null : { text: "clicked" };
                        }
                      }
                    ];
                  }

                }
              }]}
            />
            <VictoryLabel
              textAnchor="middle"
              style={[{ fill: "#fff", fontSize: 14, fontWeight:'bold' }]}
              x={55} y={55}
              text={DistressDoughnut+'/\n'+YearlyHour}
            />
            </Svg>
            <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="circle" size={16} color="#fad000" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.TOTAL}</Text>
              <Icon name="circle" size={16} color="#967f0e" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.CONSUME}</Text>
            </View>
          </View>
        </View>
        <View style={styles.average}>
          <View style={{ width: 150, height: 50, alignItems: 'center', position: 'relative', top: 20, right: 0, borderBottomWidth: 2, height: 90, borderBottomColor: '#81889f' }}>
            <Text style={styles.h1}>{avg(DistressHours)} </Text>
          </View>
          <View style={{ width: 150, alignItems: 'center', position: 'relative', top: 25, right: 0, }}>
            <Text style={styles.h5}>{strings.AVERAGE} </Text>
          </View>
        </View>
        <View style={styles.innerGraph}>
          <VictoryChart
            theme={VictoryTheme.material}
            domainPadding={{ x: domainPaddingX }}
            width={chartWidth} height={chartHeight}
            padding={{ bottom: 30, }}

          >
            <VictoryBar
              cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
              style={{
                data: {
                  fill: "#fad000",
                  width: dataWidth
                },
                labels: {
                  fontSize: 15,
                  fill: ({ datum }) => "#fff"
                }
              }}
              labelComponent={<VictoryLabel dy={(datum)=>datum.y == 0 ? 15 : 0}/>}
              labels={({ datum }) => datum.y != 0 ? datum.y : ''}
              data={PainBarChartArray}
            />
          </VictoryChart>
        </View>
      </View>
      <View style={{ flexDirection: 'row', marginTop: 300, width: '100%', position: 'absolute' }}>
        <View style={styles.registration}>
          <View style={{ borderBottomWidth: 5, borderBottomColor: '#afdb07', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.TOTAL}</Text>
          </View>
          <View style={{ width: 180, alignItems: 'center', position: 'relative', top: 10, right: 0, }}>
            <Text style={styles.h1}>{TotalUncomfort} </Text>
            <Text style={styles.h5}>{strings.REGISTRATIONS} </Text>
          </View>
        </View>
        <View style={styles.hours}>
          <View style={{ borderBottomWidth: 2, borderBottomColor: '#81889f', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14 }}>UNCOMFORTABLE HOURS</Text>
          </View>
          <View style={{ flexDirection: 'row', width: 250, alignItems: 'center', position: 'relative', top: 20, left: 30 }}>
            <Text style={styles.h1}>{hours(UncomfortHours)}</Text>
            <Text style={styles.hrs}>{strings.HRS.substring(0,3)} </Text>
            <Text style={styles.h1}>{minutes(UncomfortHours)}</Text>
            <Text style={styles.hrs}>{strings.MIN.substring(0,3)} </Text>
          </View>
        </View>
        <View style={{ width: 150, height: 120, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
          <Svg>
            <VictoryPie
              data={[{ y: YearlyHour, label: " " }, { y: UncomfortDoughnut, label: " " }]}
              labels={(datum) => `${datum.y}`}
              radius={40}
              innerRadius={20}
              width={110}
              height={110}
              colorScale={['#afdb07', '#728d0a']}
              events={[{
                target: "data",
                eventHandlers: {
                  onPress: () => {
                    return [
                      {
                        target: "data",
                        mutation: ({ style }) => {
                          return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                        }
                      }, {
                        target: "labels",
                        mutation: ({ text }) => {
                          return text === "clicked" ? null : { text: "clicked" };
                        }
                      }
                    ];
                  }

                }
              }]}
            />
            <VictoryLabel
              textAnchor="middle"
              style={[{ fill: "#fff", fontSize: 14, fontWeight:'bold' }]}
              x={55} y={55}
              text={UncomfortDoughnut+'/\n'+YearlyHour}
            />
            </Svg>
            <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="circle" size={16} color="#afdb07" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.TOTAL}</Text>
              <Icon name="circle" size={16} color="#728d0a" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.CONSUME}</Text>
            </View>
          </View>
        </View>
        <View style={styles.average}>
          <View style={{ width: 150, height: 50, alignItems: 'center', position: 'relative', top: 20, right: 0, height: 90, borderBottomWidth: 2, borderBottomColor: '#81889f' }}>
            <Text style={styles.h1}>{avg(UncomfortHours)} </Text>
          </View>
          <View style={{ width: 150, alignItems: 'center', position: 'relative', top: 25, right: 0, }}>
            <Text style={styles.h5}>{strings.AVERAGE} </Text>
          </View>
        </View>
        <View style={styles.innerGraph}>
          <VictoryChart
            theme={VictoryTheme.material}
            domainPadding={{ x: domainPaddingX }}
            width={chartWidth} height={chartHeight}
            padding={{ bottom: 30, }}

          >
            <VictoryBar
              cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
              style={{
                data: {
                  fill: "#afdb07",
                  width: dataWidth
                },
                labels: {
                  fontSize: 15,
                  fill: ({ datum }) => "#fff"
                }
              }}
              labelComponent={<VictoryLabel dy={(datum)=>datum.y == 0 ? 15 : 0}/>}
              labels={({ datum }) => datum.y != 0 ? datum.y : ''}
              data={UncomfortBarChartArray}
            />
          </VictoryChart>
        </View>
      </View>
      <View style={{ flexDirection: 'row', marginTop: 450, width: '100%', position: 'absolute' }}>
        <View style={styles.registration}>
          <View style={{ borderBottomWidth: 5, borderBottomColor: '#e26060', padding: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14, textTransform:'uppercase' }}>{strings.TOTAL}</Text>
          </View>
          <View style={{ width: 180, alignItems: 'center', position: 'relative', top: 10, right: 0, }}>
            <Text style={styles.h1}>{TotalComfort} </Text>
            <Text style={styles.h5}>{strings.REGISTRATIONS} </Text>
          </View>
        </View>
        <View style={styles.hours}>
          <View style={{ borderBottomWidth: 2, borderBottomColor: '#81889f', paddingTop: 5, alignItems: 'center' }}>
            <Text style={{ color: '#81889f', fontSize: 14 }}>COMFORTABLE HOURS</Text>
          </View>
          <View style={{ flexDirection: 'row', width: 250, alignItems: 'center', position: 'relative', top: 20, left: 30 }}>
            <Text style={styles.h1}>{hours(ComfortHours)}</Text>
            <Text style={styles.hrs}>{strings.HRS.substring(0,3)} </Text>
            <Text style={styles.h1}>{minutes(ComfortHours)}</Text>
            <Text style={styles.hrs}>{strings.MIN.substring(0,3)} </Text>
          </View>
        </View>
        <View style={{ width: 150, height: 120, justifyContent: 'center', alignItems: 'center', }}>
          <View style={{ justifyContent: 'center', alignItems: 'center', }}>
          <Svg>
            <VictoryPie
              data={[{ y: YearlyHour, label: " " }, { y: ComfortableDoughnut, label: " " }]}
              labels={(datum) => `${datum.y}`}
              radius={40}
              innerRadius={20}
              width={110}
              height={110}
              colorScale={['#e26060', '#793e3e']}
              events={[{
                target: "data",
                eventHandlers: {
                  onPress: () => {
                    return [
                      {
                        target: "data",
                        mutation: ({ style }) => {
                          return style.fill === "#c43a31" ? null : { style: { fill: "#c43a31" } };
                        }
                      }, {
                        target: "labels",
                        mutation: ({ text }) => {
                          return text === "clicked" ? null : { text: "clicked" };
                        }
                      }
                    ];
                  }

                }
              }]}
            />
            <VictoryLabel
              textAnchor="middle"
              style={[{ fill: "#fff", fontSize: 14, fontWeight:'bold' }]}
              x={55} y={55}
              text={ComfortableDoughnut+'/\n'+YearlyHour}
            />
            </Svg>
            <View style={{ flexDirection: 'row', paddingTop: 0, width: 100, height: 'auto', alignItems: 'center', justifyContent: 'center' }}>
              <Icon name="circle" size={16} color="#e26060" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.TOTAL}</Text>
              <Icon name="circle" size={16} color="#793e3e" />
              <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.CONSUME}</Text>
            </View>
          </View>
        </View>
        <View style={styles.average}>
          <View style={{ width: 150, height: 50, alignItems: 'center', position: 'relative', height: 90, top: 20, right: 0, borderBottomWidth: 2, borderBottomColor: '#81889f' }}>
            <Text style={styles.h1}>{avg(ComfortHours)} </Text>
          </View>
          <View style={{ width: 150, alignItems: 'center', position: 'relative', top: 25, right: 0, }}>
            <Text style={styles.h5}>{strings.AVERAGE} </Text>
          </View>
        </View>
        <View style={styles.innerGraph}>
          <VictoryChart
            theme={VictoryTheme.material}
            domainPadding={{ x: domainPaddingX }}
            width={chartWidth} height={chartHeight}
            padding={{ bottom: 30, }}

          >
            <VictoryBar
              cornerRadius={{ topLeft: ({ datum }) => graphRadius, topRight: graphRadius, bottomLeft: ({ datum }) => graphRadius, bottomRight: graphRadius }}
              style={{
                data: {
                  fill: "#e26060",
                  width: dataWidth
                },
                labels: {
                  fontSize: 15,
                  fill: ({ datum }) => "#fff"
                }
              }}
              labelComponent={<VictoryLabel dy={(datum)=>datum.y == 0 ? 15 : 0}/>}
              labels={({ datum }) => datum.y != 0 ? datum.y : ''}
              data={AwakeBarChartArray}
            />
          </VictoryChart>
        </View>
      </View>
      <View style={{ flexDirection: 'row', position: 'absolute', top: height - 160, left: 250 }}>
        <View style={{ flexDirection: 'row' }}>
          <Text style={{ color: '#fff', paddingRight: 15 }}>{strings.STATUS_OF_WELLBEING}</Text>
        </View>
        <View style={{ flexDirection: 'row' }}>
          <Icon name="circle" size={18} color="#6bb1d6" />
          <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.SLEEP}</Text>
        </View>
        <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
          <Icon name="circle" size={18} color="#afdb07" />
          <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.AWAKE_COMFORTABLE}</Text>
        </View>
        <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
          <Icon name="circle" size={18} color="#fad000" />
          <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.AWAKE_UNCOMFORTABLE}</Text>
        </View>
        <View style={{ flexDirection: 'row', paddingLeft: 20 }}>
          <Icon name="circle" size={22} color="#e26060" />
          <Text style={{ color: '#fff', paddingLeft: 5 }}>{strings.AWAKE_DISTRESS}</Text>
        </View>
      </View>
    </View >
  );
};
const styles = StyleSheet.create({
  mainBody: {
    flex: 1,
    backgroundColor: '#2b3249',
    padding: 10,
    alignContent: 'center',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  tabContainer: {
    width: 300,
    height: 50,
    flexDirection: 'row',
    // borderWidth:1,
    position: 'absolute',
    top: 10,
    left: 50,
    borderRadius: 50,
    backgroundColor: '#474f69',
  },
  weekTab: {
    width: 150,
    // height:50,
    backgroundColor: '#69c2d1',
    fontSize: 14,
    borderRadius: 50
  },
  weekText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 10,
    fontWeight: 'bold'
  },
  monthTab: {
    width: 150,
    // height:50,
    backgroundColor: '#474f69',
    fontSize: 14,
    borderRadius: 50
  },
  monthText: {
    color: '#fff',
    fontSize: 20,
    textAlign: 'center',
    paddingTop: 10,
    fontWeight: 'bold',
  },
  middleDiv: {
    width: 400,
  },
  titleText: {
    color: '#fff',
    fontSize: 26,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  rightDiv: {
    width: 300, flexDirection: "row", flexWrap: "wrap", position: 'absolute', top: 0, right: 0
  },
  SectionStyle: {
    flexDirection: 'row',
    height: 40,
    marginTop: 10,
    margin: 10,
    width: 200,
    justifyContent: 'center',
    borderBottomWidth: 1,
    borderBottomColor: '#dadae8',
    textAlign: 'center'
  },
  inputStyle: {
    flex: 1,
    color: 'white',
    flexDirection: 'row',
    fontSize: 14,
    left: 8,
    paddingTop: 7
  },
  searchIcon: {
    paddingTop: 11,

  },
  container: {
    flexDirection: 'row', position: 'relative', top: 0, left: 0, alignItems: 'center', padding: 0, margin: 0, backgroundColor: 'transparent', width: '100%',
  },
  innerGraph: {
    width: 310, height: 120, position: 'relative', alignItems: 'center', marginTop: 0
  },
  registration: { width: 170, height: 140, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' },
  hours: { width: 200, height: 140, marginLeft: 20, borderRadius: 20, backgroundColor: '#3a4159' },
  average: { width: 150, height: 140, marginLeft: 0, borderRadius: 20, backgroundColor: '#3a4159' },
  h1: { fontSize: 40, fontWeight: 'bold', color: '#fff', alignItems: 'center' },
  h5: { fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 0, fontSize: 14, textTransform:'uppercase' },
  hrs: { fontWeight: 'bold', color: '#81889f', marginBottom: 0, paddingTop: 20, fontSize: 14 },
});
export default WellbeingDashboardQuaterly;
