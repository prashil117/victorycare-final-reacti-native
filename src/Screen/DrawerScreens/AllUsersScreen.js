import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'
import {
    TextInput,
    Image,
    TouchableOpacity,
    Switch,
    SafeAreaView,
    View,
    Text,
    Alert,
    StyleSheet,
    RefreshControl,
    Button,
    FlatList,
    Keyboard,
    ActivityIndicator,
    KeyboardAvoidingView,
} from 'react-native';
import { medicineLoading } from '../redux/action'
import { useIsFocused } from '@react-navigation/native'
import * as ImagePicker from 'react-native-image-picker';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import { ColorPicker, fromHsv } from 'react-native-color-picker'
import upload from '../../../Image/upload-medicine.png'
import { bindActionCreators } from "redux";
import { medicineDataSource, allDataNull, medicineOffset, updateWheel } from '../redux/action'
import { connect } from "react-redux";
import moment from "moment";
import DropDownPicker from 'react-native-dropdown-picker';
import { Modal, Portal, Provider, FAB, Snackbar } from 'react-native-paper';
import deletebtn from '../../../Image/delete-btn.png'
import editbtn from '../../../Image/edit-btn.png'
import notificationbtn from '../../../Image/notification-btn.png'
import Loader from '../Components/Loader';
import axios from 'axios'
import config from '../appconfig/config';
import { ScrollView } from 'react-native-gesture-handler';
import AwesomeAlert from 'react-native-awesome-alerts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const AllUsers = (props) => {
    const isFocused = useIsFocused()
    const currentUser = useSelector(state => state.auth.userLogin)
    const dispatch = useDispatch();
    const sessionUser = useSelector(state => state.auth.requestedUser)
    const dataSource = useSelector(state => state.auth.medicineDataSource)
    const [dataSourceForList, setSourceForList] = useState([]);
    const offset = useSelector(state => state.auth.moffset)
    const isListEnd = useSelector(state => state.auth.medicineListLoading)
    const [refreshing, setRefreshing] = React.useState(false);
    const [dropValues, setDropDownList] = useState([]);
    const [loading, setLoading] = useState(false);
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [keyword, setKeyword] = useState('');
    const [title, setTitle] = useState(strings.ADD_MEDICINE);
    const [visible, setVisible] = React.useState(false);
    const [visible1, setVisible1] = React.useState(false);
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const [searchData, setsearchData] = useState('')
    const [check, setCheck] = useState('start');
    const [isDatePickerVisible1, setDatePickerVisibility1] = useState(false);

    const showAlert = (id, index) => {
        setAwesomeAlert(true);
        setPopupId(id)
        setPopupIndex(index)
    };

    const hideAlert = () => {
        setAwesomeAlert(false);
    };

    const [type, setType] = useState('');
    const [errMsg, setErrMsg] = useState('');
    const [allValues, SetallValues] = useState({
        id: '',
        name: null,
        color: '',
        description: '',
        enablenotification: false,
        dose: null,
        timesperday: null,
        company: '',
        areaofuse: '',
        notes: '',
        activeingredients: '',
        recurring: false,
        enableStandardWheel: false,
        selecttime: '',
        image: ''
    });

    const [allValuesNotification, SetallNotificationValues] = useState({
        id: '',
        name: '',
        startDate: '',
        endDate: false,
        medicine: '',
        notification: '',
        expertise: '',
        address: '',
        time: [],
        areaofuse: '',
    });



    const handleChooseLibrary = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };
        ImagePicker.launchImageLibrary(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                let source = { uri: response.uri };
                SetallValues({ ...allValues, image: 'data:image/jpeg;base64,' + response.base64 })
            }
        });
    }

    const handleChooseCamera = () => {
        var options = {
            mediaType: 'image/*',
            quality: 0.5,
            includeBase64: true
        };

        ImagePicker.launchCamera(options, response => {
            if (response.didCancel) {
            } else if (response.error) {
            } else if (response.customButton) {
            } else {
                let source = { uri: response.uri };
                SetallValues({ ...allValues, image: 'data:image/jpeg;base64,' + response.base64 })
            }
        });
    }

    const chooseOption = () => {
        Alert.alert(
            "",
            strings.UPLOAD_IMAGE,
            [
                { text: strings.CHOOSE_FROM_LIBRARY, onPress: () => handleChooseLibrary() },
                { text: strings.TAKE_PICTURE, onPress: () => handleChooseCamera() },
                { text: strings.CANCEL, onPress: () => console.log("OK Pressed"), style: "cancel" }
            ]
        );
    }


    const onDismissSnackBar = () => setVisibleSnack(false);

    const showModal1 = (value) => {
        setVisible1(true);
        SetallNotificationValues({ ...allValuesNotification, ['medicine']: value });

    }
    const hideModal1 = () => {
        for (key in allValuesNotification) {
            if (allValuesNotification.hasOwnProperty(key)) {
                allValuesNotification[key] = null;
            }
        }
        setVisible1(false)

    };
    const showModal = () => {
        setVisible(true)
        setType('add')
    };
    const hideModal = (key) => {
        for (key in allValues) {
            if (allValues.hasOwnProperty(key)) {
                allValues[key] = null;
            }
        }
        setVisible(false)
        setTitle(strings.ADD_MEDICINE)
        setType('add')

    };
    const [visibleColor, setVisibleColor] = React.useState(false);
    const showModalColor = () => setVisibleColor(true);
    const [time, setTime] = React.useState([]);
    const wait = (timeout) => {
        return new Promise(resolve => setTimeout(resolve, timeout));
    }
    const hideModalColor = () => setVisibleColor(false);
    const [index, setIndex] = useState();
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const containerStyle = { backgroundColor: '#2b3249', marginBottom: 25, padding: 10, alignItems: 'center', height: '100%', width: '100%', alignSelf: 'center', borderRadius: 0 };
    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
        setDatePickerVisibility1(false);
    };
    const handleConfirm = (date) => {
        // SetallValues({ ...allValues, ['selecttime']: moment(date).format('HH:mm') })
        if (check === "start") {
            SetallNotificationValues({ ...allValuesNotification, ['prestartdate']: moment(date).format('DD/MM/YYYY'), ['startDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "end") {
            SetallNotificationValues({ ...allValuesNotification, ['preenddate']: moment(date).format('DD/MM/YYYY'), ['endDate']: moment(date).format('YYYY-MM-DD') })
        }
        if (check === "time") {
            SetallValues({ ...allValues, ['selecttime']: moment(date).format('HH:mm') })
        }
        if (check.includes("mul")) {
            let index = check.split(" ")[1];
            console.log("check", date)
            console.log("check", index)
            const list = [...time];
            list[index]['time'] = moment(date).format('HH:mm')
            setTime(list);
        }
        hideDatePicker();
    };

    const onEditMedicine = (data, index) => {
        allValues.id = data.id;
        setIndex(index);
        allValues.name = data.name;
        allValues.color = data.color;
        allValues.activeingredients = data.activeingredients;
        allValues.recurring = data.selecttime ? true : false;
        allValues.addtowheel = data.addtowheel == "1" ? true : false;
        allValues.notification = data.notification == "1" ? true : false;
        allValues.selecttime = data.selecttime;
        allValues.dose = data.dose;
        allValues.timesperday = data.timesperday;
        allValues.company = data.company;
        allValues.areaofuse = data.areaofuse;
        allValues.notes = data.notes;
        allValues.image = data.image;
        setTitle('Edit Medicine');
        setType('edit');
        setVisible(true);
    }


    const updatetoWheel = (id, val, index) => {
        setLoading(true)
        var data = {
            action: 'updatestandardwheelmedicine',
            userid: currentUser.id,
            appsecret: config.appsecret,
            standardwheel: val,
            id: id
        }
        dataSourceForList[index].addtowheel = !dataSourceForList[index].addtowheel;
        axios.post(config.apiUrl + "updatedata.php", data).then(result => {
            setLoading(false)
            if (result.data.status == 'success') {
                setsnackMsg(strings.ADD_TO_WHEEL_UPDATED_SUCCESSFULLY)
                setVisibleSnack(true)
                dispatch(updateWheel(val))
            }
        })
    }



    useEffect(() => {
        getMedicineData()
    }, [sessionUser]);

    useEffect(() => {
        getDropDownList();
    }, [sessionUser]);

    const getDropDownList = () => {
        var data = {
            action: 'getmedicinemasterdatafulllist',
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
        }
        axios.post(config.apiUrl + "getdata.php", data).then(result => {
            console.log("result.data123123", result.data);
            if (dropValues.length == 0) {
                setLoading(false);
                var arr = [];
                if (result.data.data.length > 0) {
                    result.data.data.map(val => {
                        let ob = {
                            value: val.id,
                            label: val.name,
                        }
                        arr.push(ob)
                        console.log("arr::", arr)
                    })
                    setDropDownList(arr);
                }
            }
            else {
                setLoading(false);
            }
        })
    }

    const getMedicineData = () => {

        if (!isListEnd && searchData === '') {
            setLoading(true);
            var dataToSend = {
                userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
                appsecret: config.appsecret,
                hcpid: 0,
                action: 'getmedicinemasterdata',
                keyword: '',
                offset: offset
            };
            console.log("dataTosedn", dataToSend)
            axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {
                console.log("result", result)
                if (result.data.data0 !== null) {
                    if (offset === 0) {
                        dispatch(allDataNull())
                        setSourceForList([])
                    }
                    setLoading(false);
                    dispatch(medicineDataSource(result.data.data0));
                    dispatch(medicineOffset(offset + 10))
                    if (offset == 0)
                        setSourceForList(result.data.data0)
                    else
                        setSourceForList(dataSourceForList.concat(result.data.data0))
                }
                else {
                    dispatch(medicineLoading(true))
                    setLoading(false);
                }
            })
        }
    };

    const getMedicineDataSearch = (key) => {

        if (key == '') {
            Keyboard.dismiss()
            onRefresh();
            return;
        }
        setsearchData(key)
        // setLoading(true);
        var dataToSend = {
            userid: sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            appsecret: config.appsecret,
            hcpid: 0,
            action: 'getmedicinemasterdata',
            keyword: key,
            offset: 0
        };
        axios.post(config.apiUrl + "getdata.php", dataToSend).then(result => {

            if (result.data.data0 !== null) {
                setLoading(false);
                dispatch(allDataNull())
                setSourceForList([])
                dispatch(medicineDataSource(result.data.data0));
                dispatch(medicineOffset(0))
                setSourceForList(result.data.data0)
            }
            else {
                dispatch(medicineLoading(true))
                setLoading(false);
            }
        })
    }


    const AddUpdateMedicine = () => {
        if (allValues.name === '' || allValues.name === null) {
            return
        }
        if (allValues.dose === '' || allValues.dose === null) {
            return
        }
        if (allValues.timesperday === '' || allValues.timesperday === null) {
            return
        }
        setLoading(true)
        var data = allValues;
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            data.appsecret = config.appsecret;
        data.doses = data.dose;
        data.action = type == "edit" ? 'updatemedicine' : 'addmedicine';
        var filename = type == "edit" ? 'updatedata.php' : 'adddata.php';
        axios.post(config.apiUrl + filename, data).then(result => {
            setLoading(false)
            // console.log("result.data", result.data)
            if (result.data.status == 'success') {
                setLoading(false);
                hideModal();
                setType('add');
                setsnackMsg(type == "add" ? strings.MEDICINE_CREATE_SUCC : strings.MEDICINE_UPDATED_SUCC)
                setVisibleSnack(true)
                setVisible(false)
                onRefresh();
                dispatch(updateWheel(true));
            }
            else {
                setType('add');
                setVisible(false)
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC)
                setVisibleSnack(true)
            }

        })
    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setsearchData('')
        Keyboard.dismiss()
        wait(2000).then(() => {
            dispatch(medicineOffset(0));
            setRefreshing(false);
            setVisible(false)
            setSourceForList([])
            getMedicineData();
            // useEffect(() => {
            //     getMedicineData()
            // });
        });
    }, []);

    const AddNotification = () => {
        setLoading(true)
        // console.log("buton clicked", allValues)
        var data = allValuesNotification;
        data.patientid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id,
            data.hcpid = 0;
        data.addedBy = currentUser.id;
        data.doctor = currentUser.id;
        data.appsecret = config.appsecret;
        data.areaexpertise = allValuesNotification.expertise
        data.prescriptionaddress = allValuesNotification.address
        data.enablePrescriptionNotification = allValuesNotification.notification
        data.dose = allValuesNotification.dose
        data.prestartdate = data.startDate
        data.preenddate = data.endDate
        data.time = time.map(x => x.time).join(',');
        data.action = 'addpatientmedicine';
        // alert("ASfd")
        console.log("notification::=>>", allValuesNotification);
        axios.post(config.apiUrl + 'adddata.php', data).then(result => {
            setLoading(false)
            console.log("result.data", result.data)
            if (result.data.status == 'success') {
                console.log("result", result.data)
                setLoading(false);
                hideModal1()
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC);
                setVisibleSnack(true)
            }
            else {
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC)
                setVisibleSnack(true)
            }
        })
    }

    const DeleteAlert = (id, index) => {
        showAlert(id, index)
        // Alert.alert(
        //     'Delete',
        //     'Are your sure you want to delete?',
        //     [
        //
        //         {
        //             text: 'No',
        //             onPress: () => { },
        //             style: 'cancel',
        //         },
        //         {
        //             text: 'Yes',
        //             onPress: () => DeleteMedicine(id, index)
        //         },
        //     ],
        //     { cancelable: false },
        // );
    }

    const DeleteMedicine = (id, index) => {
        setLoading(true)
        var data = {};
        data.userid = sessionUser && sessionUser.userid ? sessionUser.userid : currentUser.id;
        data.appsecret = config.appsecret;
        data.id = id;
        data.action = 'deletemedicine'
        axios.post(config.apiUrl + 'deletedata.php', data).then(result => {
            setLoading(false)
            if (result.data.status == 'success') {
                dataSource.splice(index, 1);
                setVisible(false)
                setType('add');
                setsnackMsg(strings.MEDICINE_DELETED_SUCC)
                setAwesomeAlert(false)
                setVisibleSnack(true)
                dispatch(updateWheel(true));
            }
            else {
                setVisible(false)
                setsnackMsg(strings.NOTIFICATION_ADDED_SUCC)
                setAwesomeAlert(false)
                setVisibleSnack(true)
            }
        })
    }

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="black"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };


    const handleInputChange = (value, index) => {
        const list = [...time];
        list[index]['time'] = value;
        setTime(list);
    };

    // handle click event of the Remove button
    const handleRemoveClick = index => {
        const list = [...time];
        list.splice(index, 1);
        setTime(list);
    };

    // handle click event of the Add button
    const handleAddClick = () => {
        setTime([...time, { time: "" }]);
    };

    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{ marginTop: 20 }}>
                <Text style={{ color: '#ccc', fontSize: 18, fontWeight: 'bold' }}>{strings.NO_ITEMS_FOUND}</Text>
            </View>
        );
    };
    const ItemView = ({ item, index }) => {
        // console.log("item:",item)
        return (
            // Flat List Item
            <View key={item.id} style={styles.medicine}>
                <View style={[styles.medicineColor, { backgroundColor: item.color }]}>
                </View>
                <Text style={styles.medicineText}>{item.name}</Text>
                <View style={{ paddingTop: 10, flexDirection: "row", flexWrap: "wrap" }}>
                    <TouchableOpacity onPress={() => { DeleteAlert(item.id, index) }}>
                        <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() => onEditMedicine(item, index)}>
                        <Image style={styles.icons} height={35} width={35} source={editbtn} />
                    </TouchableOpacity>
                    <Switch style={[styles.headerText, { left: 18 }]}
                        trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                        borderColor={'grey'}
                        borderWidth={1}
                        borderRadius={16}
                        thumbColor={"#fff"}
                        onValueChange={val => updatetoWheel(item.id, val, index)}
                        value={item.addtowheel == "1" ? true : false}
                    />
                    <TouchableOpacity style={{ left: 53, width: 34 }} onPress={() => showModal1(item.id)}>
                        <Image style={{ marginTop: -3, height: 34, width: 34 }} height={34} width={34} source={notificationbtn} />
                    </TouchableOpacity>
                </View>
            </View>
        );
    };

    return (
        <SafeAreaView style={{ flex: 1 }}>
            <View style={styles.mainBody}>
                <AwesomeAlert
                  contentContainerStyle={{
                      backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                      shadowOffset: {
                          width: 0,
                          height: 12,
                      },
                      shadowOpacity: 0.58,
                      shadowRadius: 16.00,

                      elevation: 24
                  }}
                  show={awesomeAlert}
                  showProgress={false}
                  title={strings.DELETE_RECORD}
                  titleStyle={{ color: 'white' }}
                  message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                  messageStyle={{ color: 'white' }}
                  closeOnTouchOutside={true}
                  closeOnHardwareBackPress={false}
                  showCancelButton={true}
                  showConfirmButton={true}
                  cancelText={strings.NO_CANCEL}
                  confirmText={strings.YES_DELETE}
                  cancelButtonColor="#516b93"
                  confirmButtonColor="#69c2d1"
                  confirmButtonBorderRadius="0"
                  confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                  cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                  cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                  confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                    onCancelPressed={() => {
                        setAwesomeAlert(false);
                    }}
                    onConfirmPressed={() => {
                        DeleteMedicine(popupId, popupIndex);
                    }}
                />
                {/* <TouchableOpacity style={styles.viewbtn} onPress={() => props.navigation.navigate('MedicineNotificationScreen')}>
                    <Text style={{ color: 'white', fontSize: 16, fontWeight: '600' }}>{strings.VIEW_ALL_NOTIFICATIONS}</Text>
                </TouchableOpacity> */}
                <View style={{ right: 105, flexDirection: "row", flexWrap: "wrap" }}>
                    <View style={styles.SectionStyle}>
                        <Icon style={styles.searchIcon} name="search" size={22} color="grey" />
                        <TextInput
                            style={styles.inputStyle}
                            placeholder={strings.TYPE_TO_SEARCH_HERE}
                            placeholderTextColor="#8b9cb5"
                            autoCapitalize="none"
                            onChangeText={(text) => { getMedicineDataSearch(text) }}
                            underlineColorAndroid="#f000"
                            blurOnSubmit={false}
                            value={searchData}
                        />
                    </View>
                    <View style={{ paddingTop: 45, left: 190, flexDirection: "row", flexWrap: "wrap" }}>
                        <Text style={styles.headerText}>{strings.DELETE}</Text>
                        <Text style={styles.headerText}>{strings.EDIT}</Text>
                        <Text style={styles.headerText}>{strings.ADD_TO_WHEEL}</Text>
                        <Text style={styles.headerText}>{strings.NOTIFY}</Text>
                    </View>
                </View>
                <FlatList
                    showsVerticalScrollIndicator={false}
                    data={dataSourceForList}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={ItemView}
                    ListEmptyComponent={NoDataFound}
                    ListFooterComponent={renderFooter}
                    onEndReached={getMedicineData}
                    onEndReachedThreshold={0.5}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefresh}
                        />}
                />
                <Provider>
                    <Portal>
                        <Modal visible={visible} onDismiss={hideModal} contentContainerStyle={containerStyle}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 20 }}>
                                <Text style={{ color: 'white', fontWeight: '700', fontSize: 22, }}>{title}</Text>
                                <View style={{ left: 400 }}>
                                    <TouchableOpacity onPress={hideModal}>
                                        <Icon name="close" color="grey" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <KeyboardAwareScrollView enableOnAndroid={true} style={{ width: '100%' }}>
                                <View style={{ alignItems: 'flex-start', justifyContent: 'center', alignSelf: 'center', flexWrap: 'wrap', flexDirection: 'row', marginTop: 30, }}>
                                    <View style={{ padding: 0, width: '45%' }}>
                                        <Text style={{ color: 'white', fontSize: 10, fontWeight: '600', top: 20, width: 200, }}>{strings.ADD_PICTURE_OF_MEDICINE_PACKAGE}</Text>
                                        {!allValues.image ?
                                            <TouchableOpacity onPress={chooseOption} style={[styles.imageBackStyle, { alignSelf: 'center' }]} >
                                                <Image
                                                    source={upload}
                                                    style={{ width: 40, height: 40, top: 20 }}
                                                />
                                            </TouchableOpacity> :
                                            <TouchableOpacity style={[styles.imageBackStyle, { alignSelf: 'center', top: -20 }]}>
                                                <Image
                                                    source={{ uri: allValues.image }}
                                                    style={{ width: 150, height: 100, borderRadius: 10 }}
                                                />
                                                <TouchableOpacity onPress={() => SetallValues({ ...allValues, image: '' })} ><Icon name="close" color="grey" size={22} /></TouchableOpacity>
                                            </TouchableOpacity>}

                                        <View style={{ marginTop: 20, }}>
                                            <Text style={{ flexDirection: 'row' }}>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.MEDICINE_NAME}</Text>
                                                <Text style={{ color: 'red', marginBottom: 8 }}> *</Text>
                                            </Text>
                                            <TextInput
                                                style={{ color: 'white' }}
                                                placeholder={strings.TYPE_NAME_OF_MEDICINE_HER}
                                                placeholderTextColor={'grey'}
                                                value={allValues.name}
                                                onChangeText={val => SetallValues({ ...allValues, ['name']: val })}
                                                borderBottomWidth={1}
                                                borderColor={'grey'}
                                            />
                                            <Text style={{ color: 'red' }}>{allValues.name === '' ? strings.PLEASE_ENTER_NAME_OF_MEDICINE : ''}</Text>
                                        </View>

                                        <View style={{ marginTop: 20, left: 0 }}>
                                            <Text style={{ color: 'white', marginBottom: 8 }}>{strings.CHOOSE_COLOR}</Text>
                                            <View style={{ flexDirection: "row", flexWrap: "wrap" }}>
                                                <View style={[allValues.color ? styles.medicineColorForm : '', { backgroundColor: allValues.color, marginBottom: 8 }]}>
                                                </View>
                                                <TextInput
                                                    style={{ width: allValues.color ? '100%' : '100%', right: allValues.color ? 20 : 0 }}
                                                    placeholder={allValues.color !== '' ? '' : strings.CHOOSE_COLOR}
                                                    placeholderTextColor={'grey'}
                                                    editable={false}
                                                    onTouchStart={showModalColor}
                                                    borderBottomWidth={1}
                                                    borderColor={'grey'}
                                                />
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                            <Text style={{ color: 'white', width: 150, alignSelf: 'center', left: 30 }}>{strings.ADD_TO_STANDARD_WHEEL}</Text>
                                            <Switch style={[styles.headerText, { left: 30 }]}
                                                trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                borderColor={'grey'}
                                                borderWidth={1}
                                                borderRadius={16}
                                                thumbColor={"#fff"}
                                                value={allValues.addtowheel}
                                                onValueChange={val => SetallValues({ ...allValues, ['addtowheel']: val })}
                                            />
                                        </View>
                                        <View style={{ marginTop: 20 }}>
                                            <Text style={{ color: 'white', marginBottom: 8 }}>{strings.ACTIVE_INGREDIENTS}</Text>
                                            <TextInput
                                                style={{ width: '100%', color: 'white' }}
                                                placeholder={strings.TYPE_ACTIVE_INGREDIENTS_H}
                                                placeholderTextColor={'grey'}
                                                value={allValues.activeingredients}
                                                onChangeText={val => SetallValues({ ...allValues, ['activeingredients']: val })}
                                                borderBottomWidth={1}
                                                borderColor={'grey'}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ marginRight: 5, marginLeft: 50, width: '45%' }}>
                                        <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between' }}>
                                            <View style={{ width: '45%' }}>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.DOSE}</Text>
                                                <TextInput
                                                    style={{ color: 'white' }}
                                                    placeholder={strings.TYPE_DOSAGE}
                                                    placeholderTextColor={'grey'}
                                                    value={allValues.dose}
                                                    onChangeText={val => SetallValues({ ...allValues, ['dose']: val })}
                                                    borderBottomWidth={1}
                                                    borderColor={'grey'}
                                                />
                                                <Text style={{ color: 'red' }}>{allValues.dose === '' ? strings.PLEASE_ENTER_DOSE : ''}</Text>
                                            </View>
                                            <View style={{ marginLeft: 30, width: '45%' }}>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.TIMES_PER_DAY}</Text>
                                                <TextInput
                                                    style={{ color: 'white' }}
                                                    placeholder={strings.ZERO0_DAY}
                                                    placeholderTextColor={'grey'}
                                                    value={allValues.timesperday}
                                                    onChangeText={val => SetallValues({ ...allValues, ['timesperday']: val })}
                                                    borderBottomWidth={1}
                                                    borderColor={'grey'}
                                                />
                                                <Text style={{ color: 'red' }}>{allValues.timesperday === '' ? strings.PLEASE_ENTER_TIMES_PER_DAY : ''}</Text>
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 20 }}>
                                            <Text style={{ color: 'white', marginBottom: 8 }}>{strings.COMPANY}</Text>
                                            <TextInput
                                                style={{ width: '100%', color: 'white' }}
                                                placeholder={strings.TYPE_COMPANY_HERE}
                                                placeholderTextColor={'grey'}
                                                value={allValues.company}
                                                onChangeText={val => SetallValues({ ...allValues, ['company']: val })}
                                                borderBottomWidth={1}
                                                borderColor={'grey'}
                                            />
                                        </View>
                                        <View style={{ marginTop: 25 }}>
                                            <Text style={{ color: 'white', marginBottom: 8 }}>{strings.AREA_OF_USE}</Text>
                                            <TextInput
                                                style={{ width: '100%', color: 'white' }}
                                                placeholder={strings.TYPE_AREA_OF_USE_HERE}
                                                placeholderTextColor={'grey'}
                                                value={allValues.areaofuse}
                                                onChangeText={val => SetallValues({ ...allValues, ['areaofuse']: val })}
                                                borderBottomWidth={1}
                                                borderColor={'grey'}
                                            />
                                        </View>
                                        <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                            <Text style={{ color: 'white', width: 135, alignSelf: 'center', left: 30 }}>Add to Notification</Text>
                                            <Switch style={[styles.headerText, { left: 30 }]}
                                                trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                borderColor={'grey'}
                                                borderWidth={1}
                                                value={allValues.notification}
                                                onValueChange={val => SetallValues({ ...allValues, ['notification']: val })}
                                                borderRadius={16}
                                                thumbColor={"#fff"}
                                            />
                                        </View>
                                        <View style={{ marginTop: 25 }}>
                                            <Text style={{ color: 'white', marginBottom: 8 }}>{strings.DESCRIPTION}</Text>
                                            <TextInput
                                                style={{ width: '100%', color: 'white' }}
                                                placeholder={strings.TYPE_NOTES_HERE}
                                                placeholderTextColor={'grey'}
                                                value={allValues.areaofuse}
                                                onChangeText={val => SetallValues({ ...allValues, ['description']: val })}
                                                borderBottomWidth={1}
                                                borderColor={'grey'}
                                            />
                                        </View>
                                        <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", width: '100%' }}>
                                            <Text style={{ color: 'white', alignSelf: 'center', }}>{strings.RECURRING_DAILY}</Text>
                                            <Switch style={[styles.headerText, {}]}
                                                trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                borderColor={'grey'}
                                                borderWidth={1}
                                                value={allValues.recurring ? true : false}
                                                onValueChange={val => SetallValues({ ...allValues, ['recurring']: val })}
                                                borderRadius={16}
                                                thumbColor={"#fff"}
                                            />
                                            <TextInput

                                                placeholder={strings.SELECT_TIME}
                                                placeholderTextColor={allValues.recurring ? 'white' : 'grey'}
                                                editable={false}
                                                style={{ width: '60%', color: allValues.recurring ? 'white' : 'grey', fontSize: 14 }}
                                                value={allValues.selecttime}
                                                onTouchStart={() => { allValues.recurring ? () => { showDatePicker(true), setCheck('time') } : '' }}
                                                borderBottomWidth={1}
                                                borderColor={'grey'}
                                            />
                                        </View>
                                    </View>
                                    <View style={{ width: '100%', alignSelf: 'flex-end', alignItems: 'flex-end', justifyContent: 'flex-end' }}>

                                        <TouchableOpacity onPress={AddUpdateMedicine} style={[styles.buttonStyle, { right: 40, top: 10, marginBottom: 40 }]} activeOpacity={0.5}>
                                            <Text style={styles.buttonTextStyle}>{type == "add" ? strings.SAVE : strings.UPDATE}</Text>
                                        </TouchableOpacity>
                                    </View>
                                    {/* <Text style={{ marginBottom: 10 }}>
                                        <Text style={{ color: 'white' }}>is this a prescripted medicine. </Text>
                                        <TouchableOpacity>
                                            <Text style={{ color: '#69c2d1', textDecorationLine: 'underline' }}>click here to add more details</Text>
                                        </TouchableOpacity>
                                    </Text> */}
                                </View>
                            </KeyboardAwareScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                <Provider>
                    <Portal>
                        <Modal visible={visible1} onDismiss={hideModal1} contentContainerStyle={containerStyle}>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
                                <Text style={{ color: 'white', fontWeight: '700', marginTop: 50, fontSize: 22 }}>{strings.ADD_MEDICINE_NOTIFICATION}</Text>
                                <View style={{ left: 350 }}>
                                    <TouchableOpacity onPress={hideModal1}>
                                        <Icon name="close" color="grey" size={20} />
                                    </TouchableOpacity>
                                </View>
                            </View>
                            <KeyboardAwareScrollView enableOnAndroid={true}>
                                <View style={{ alignItems: 'center', flexWrap: 'wrap', flexDirection: 'row', marginTop: 50 }}>
                                    <View style={{ height: 500, width: '45%' }}>
                                        <Text style={{ color: 'white', marginBottom: 8 }}>{strings.NAME_OF_MEDICINE}</Text>
                                        <DropDownPicker
                                            items={dropValues}
                                            labelStyle={{ fontSize: 14, textAlign: 'left', color: 'white' }}
                                            defaultValue={allValuesNotification.medicine}
                                            containerStyle={{ height: 40, color: 'white', borderWidth: 0 }}
                                            style={{ backgroundColor: '#2b3249', color: 'white', borderWidth: 0, borderBottomWidth: 1 }}
                                            itemStyle={{
                                                justifyContent: 'flex-start', color: 'white'
                                            }}
                                            dropDownStyle={{ backgroundColor: '#2b3249', color: 'white' }}
                                            onChangeItem={item => SetallNotificationValues({ ...allValuesNotification, ['medicine']: item.value })}
                                        />
                                        <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", justifyContent: 'space-between' }}>
                                            <View>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.START_DATE}</Text>
                                                <TextInput
                                                    style={{ width: 135, color: 'white' }}
                                                    placeholder={strings.SELECT_START_TIME}
                                                    placeholderTextColor={'grey'}
                                                    editable={false}
                                                    onTouchStart={() => { setDatePickerVisibility1(true); setCheck('start') }}
                                                    value={allValuesNotification.startDate}
                                                    // onChangeText={val => SetallNotificationValues({ ...allValuesNotification, ['startDate']: val })}
                                                    borderBottomWidth={1}
                                                    borderColor={'grey'}
                                                />
                                                <DateTimePickerModal
                                                    style={{ backgroundColor: '#2b3249', }}
                                                    headerTextIOS={strings.SELECT_TIME}
                                                    textColor="white"
                                                    onTouchStart={() => { setDatePickerVisibility1(true) }}
                                                    isVisible={isDatePickerVisible1}
                                                    pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                                                    modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                                                    mode="date"
                                                    onConfirm={handleConfirm}
                                                    onCancel={hideDatePicker}
                                                />
                                            </View>
                                            <View style={{ marginLeft: 30 }}>
                                                <Text style={{ color: 'white', marginBottom: 8 }}>{strings.END_DATE}</Text>
                                                <TextInput
                                                    style={{ width: 135, color: 'white' }}
                                                    placeholder={strings.SELECT_END_DATE}
                                                    placeholderTextColor={'grey'}
                                                    editable={false}
                                                    onTouchStart={() => { setDatePickerVisibility1(true); setCheck('end') }}
                                                    value={allValuesNotification.endDate}
                                                    // onChangeText={val => SetallNotificationValues({ ...allValuesNotification, ['endDate']: val })}
                                                    borderBottomWidth={1}
                                                    borderColor={'grey'}
                                                />
                                                <DateTimePickerModal
                                                    style={{ backgroundColor: '#2b3249', }}
                                                    headerTextIOS={'Select Time'}
                                                    locale="en_GB"
                                                    textColor="white"
                                                    isVisible={isDatePickerVisible}
                                                    pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                                                    modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                                                    mode="time"
                                                    onConfirm={handleConfirm}
                                                    onCancel={hideDatePicker}
                                                />
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 20, flexDirection: "row", flexWrap: "wrap", right: 30 }}>
                                            <Text style={{ color: 'white', width: 135, alignSelf: 'center', left: 30 }}>{strings.NOTIFICATIONS}</Text>
                                            <Switch style={[styles.headerText, { left: 30 }]}
                                                trackColor={{ false: "#2b3249", true: "#74c2ce" }}
                                                borderColor={'grey'}
                                                borderWidth={1}
                                                value={allValuesNotification.notification}
                                                onValueChange={val => SetallNotificationValues({ ...allValuesNotification, ['notification']: val })}
                                                borderRadius={16}
                                                thumbColor={"#fff"}
                                            />
                                        </View>
                                    </View>

                                    <View style={{ marginRight: 5, marginLeft: 50, height: 500, width: '45%' }}>
                                        <Text style={{ color: 'white', marginBottom: 8 }}>{strings.AREA_OF_EXPERTISE}</Text>
                                        <TextInput
                                            style={{ color: 'white' }}
                                            placeholder={strings.TYPE_HERE_TEXT}
                                            placeholderTextColor={'grey'}
                                            value={allValuesNotification.expertise}
                                            onChangeText={val => SetallNotificationValues({ ...allValuesNotification, ['expertise']: val })}
                                            borderBottomWidth={1}
                                            borderColor={'grey'}
                                        />
                                        <View style={{ marginTop: 25 }}>
                                            <Text style={{ color: 'white', marginBottom: 8 }}>{strings.ADDRESS}</Text>
                                            <TextInput
                                                style={{ color: 'white' }}
                                                placeholder={strings.TYPE_HERE_ADDRESS}
                                                placeholderTextColor={'grey'}
                                                value={allValuesNotification.address}
                                                onChangeText={val => SetallNotificationValues({ ...allValuesNotification, ['address']: val })}
                                                borderBottomWidth={1}
                                                borderColor={'grey'}
                                            />
                                        </View>
                                        {/* <View> */}
                                        <Text style={{ color: 'white', fontSize: 16, marginTop: 20 }}>
                                            Time
                                                </Text>
                                        {time.map((x, i) => {
                                            return (
                                                <View key={i} style={styles.SectionStyle}>
                                                    <TextInput
                                                        style={styles.inputStyle}
                                                        // name="diagnosis"
                                                        // placeholder="00:00"
                                                        // onChangeText={e => handleInputChange(e, i)}
                                                        name="time"
                                                        placeholder={strings.ADD_TIME}
                                                        editable={false}
                                                        onTouchStart={() => { setDatePickerVisibility(true); setCheck(`mul ${i}`) }}
                                                        value={x.time}
                                                        placeholderTextColor="#8b9cb5"
                                                        underlineColorAndroid="#f000"
                                                    />
                                                    <View>
                                                        {time.length !== 0 && <TouchableOpacity onPress={() => handleRemoveClick(i)} ><Icon name="close" color="grey" size={16} /></TouchableOpacity>}
                                                    </View>
                                                </View>
                                            )
                                        })}
                                        <Button style={{ color: 'white' }} onPress={handleAddClick} title={strings.ADD_MEDICINE_TAKEN_TIME}> +{strings.ADD_MEDICINE_TAKEN_TIME}</Button>
                                        {/* </View> */}
                                        <TouchableOpacity onPress={() => AddNotification()} style={[styles.buttonStyle, { right: 80, top: 30 }]} activeOpacity={0.5}>
                                            <Text style={styles.buttonTextStyle}>{strings.SAVE}</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </KeyboardAwareScrollView>
                        </Modal>
                    </Portal>
                </Provider>
                <Provider>
                    <Portal>
                        <Modal visible={visibleColor} onDismiss={hideModalColor} contentContainerStyle={{ alignSelf: 'center', marginBottom: 100 }}>
                            <View style={{ marginTop: 20 }}>
                                <ColorPicker
                                    hideSliders={true}
                                    color={allValues.color ? allValues.color : '#ff0058'}
                                    pickerSize={10}
                                    onColorSelected={color => { SetallValues({ ...allValues, ['color']: fromHsv(color) }) }, hideModalColor}
                                    onColorChange={color => SetallValues({ ...allValues, ['color']: fromHsv(color) })}
                                    style={{ width: 100, height: 100 }}
                                />
                            </View>
                        </Modal>
                    </Portal>
                </Provider>
                <FAB
                    style={styles.floatBtn}
                    fabStyle={{ height: 100 }}
                    color={'white'}
                    theme={{ colors: { accent: '#eb8682' } }}
                    icon="plus"
                    onPress={showModal}
                >
                </FAB>
                <Snackbar
                    style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                    visible={visibleSnackbar}
                    duration={3000}
                    onDismiss={onDismissSnackBar}
                >
                    {snackMsg}
                </Snackbar>
                <View style={{ width: 150 }}>
                    <DateTimePickerModal
                        style={{ backgroundColor: '#2b3249', }}
                        headerTextIOS={'Select Time'}
                        locale="en_GB"
                        textColor="white"
                        isVisible={isDatePickerVisible}
                        pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
                        modalStyleIOS={{ width: 350, alignSelf: 'center' }}
                        mode="time"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                    />

                </View>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    headerText: {
        color: 'white',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    medicine: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: 'grey',
        width: 700,
        height: 50,
        borderRadius: 15,
        marginBottom: 17,
    },
    medicineText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 392,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    medicineColor: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        marginTop: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        // position: 'absolute',
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8

    },
    imageBackStyle: {
        alignSelf: 'center',
        backgroundColor: '#39415b',
        borderRadius: 10,
        height: 100,
        left:100,
        alignItems: 'center',
        width: 220

    },
    medicineColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        width: 18
    },
    // imageBackStyle: {
    //     // alignSelf: 'center',
    //     backgroundColor: '#39415b',
    //     borderRadius: 10,
    //     height: 100,
    //     alignItems: 'center',
    //     width: 200,
    //     marginLeft: 100

    // },

});


function mapStateToProps(state) {

    return {
        sessionUser: state.auth.requestedUser,


    };
}
// @ts-ignore
function matchDispatchToProps(dispatch) {
    return bindActionCreators({

    }, dispatch)
}

export default connect(mapStateToProps, matchDispatchToProps)(AllUsers);
