import React, { useState, useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux'

import {
    TextInput,
    Image,
    TouchableOpacity,
    SafeAreaView,
    View,
    StyleSheet,
    RefreshControl,
    FlatList,
    ActivityIndicator,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import usericn from '../../../Image/nopreview.jpg';
import { Text } from 'react-native-paper';
import deletebtn from '../../../Image/delete-btn.png'
import editbtn from '../../../Image/edit-btn.png'
import { setSession, medicineLoading, activityLoading, wellBeingLoading, allDataNull,setRouteBack } from '../redux/action'
import axios from 'axios'
import config from '../appconfig/config';
import AwesomeAlert from 'react-native-awesome-alerts';
import strings from '../../constants/lng/LocalizedStrings';
import { useRoute } from '@react-navigation/native';


const AllPatientAcceptScreen = (navigation) => {
    console.log("navigation+navigation::6", navigation)
    const route = useRoute();
    console.log(route.name);
    const currentUser = useSelector(state => state.auth.userLogin)
    const dispatch = useDispatch();
    const [refreshing, setRefreshing] = React.useState(false);
    const [loading, setLoading] = useState(false);
    const [requestData, setData] = useState([]);
    const [searchData, setsearchData] = useState('');
    const [awesomeAlert, setAwesomeAlert] = useState(false)
    const [popupId, setPopupId] = useState(false)
    const [popupIndex, setPopupIndex] = useState(false)
    const showAlert = (id, index) => {
        setAwesomeAlert(true);
        setPopupId(id)
        setPopupIndex(index)
    };

    const fetchRequest = () => {
        setLoading(true)
        setData([{firstname:'xxxx', lastname:'xxxx', phone:'xxxx', city:'xxxx'}])
        var data = {
            appsecret: config.appsecret,
            invitedId: currentUser.id,
            action: 'getRequest',
        }
        // console.log("Data", data)

        axios.post(config.apiUrl + 'getdata.php', data).then(result => {
            setRefreshing(false)
            setData('');
            console.log("data:", result.data.data)
            setLoading(false)
            if (result.data.status === 'success') {
                setData(result.data.data)
            }
        })
    }
    useEffect(fetchRequest, []);

    const searchKeyword = async (value) => {
        console.log("value", value)
        await setKeyword(value);
        await setsearchData(value);
        await setLoading(false);
    }

    const startSession = (data) => {
        setLoading(true)
        dispatch(setSession(data))
        dispatch(medicineLoading(false));
        dispatch(activityLoading(false));
        dispatch(wellBeingLoading(false));
        dispatch(allDataNull([]));
        dispatch(setRouteBack(route.name));
        setTimeout(() => {
            navigation.navigate('medicineScreenStack')
        }, 1500)

    }

    const onRefresh = React.useCallback(() => {
        setRefreshing(true);
        setsearchData('')
        fetchRequest();
    }, []);

    const renderFooter = () => {
        return (
            // Footer View with Loader
            <View style={styles.footer}>
                {loading ? (
                    <ActivityIndicator
                        color="white"
                        style={{ margin: 15 }} />
                ) : null}
            </View>
        );
    };
    const NoDataFound = ({ item, index }) => {
        return (
            <View style={{ marginTop: 20 }}>
                <Text style={{ color: '#ccc', fontSize: 18, fontWeight: 'bold', textAlign: 'center' }}>{strings.NO_RESULT_FOUND}</Text>
            </View>
        );
    };
    const ItemView = ({ item, index }) => {
        if (item.status === 'approve') {
            return (
                // Flat List Item
                <View key={item.id} style={styles.activity}>
                    <TouchableOpacity onPress={() => startSession(item)}>
                        <View style={[styles.activityColor, { backgroundColor: "#49526e" }]}>

                            {!item.profilepic ?
                                <Image
                                    source={usericn}
                                    style={{ width: 40, height: 40, borderRadius: 30, left: 3, top: 2 }}
                                />
                                :
                                <Image
                                    source={{ uri: config.apiUrl+item.profilepic }}
                                    style={{ width: 45, height: 45, borderRadius: 30, left: 0, top: 0 }}
                                />
                            }
                        </View>
                    </TouchableOpacity>
                    <Text onPress={() => startSession(item)} style={styles.activityText}>{item.name} {item.lastname}</Text>
                    <View style={{ paddingTop: 10, flexDirection: "row", flexWrap: "wrap", }}>
                        <Text style={{ marginLeft: 0, fontSize: 18, color: "white", width: 150, textAlign: 'right' }}>{(item.phone ? item.phone : 'Not Available')}</Text>
                        <Text style={{ marginLeft: 45, fontSize: 18, color: "white", width: 200, }}>{(item.city ? item.city : 'Not Available')}</Text>

                        <TouchableOpacity onPress={() => onEditActivity(item)}>
                            <Image style={styles.icons} height={35} width={35} source={editbtn} />
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => DeleteAlert(item.id, index)}>
                            <Image style={styles.icons} height={35} width={35} source={deletebtn} />
                        </TouchableOpacity>
                    </View>
                </View>
            );
        }
    };

    const DeleteAlert = (id, index) => {
        showAlert(id, index)
    }
    return (
        <SafeAreaView style={{
            flex: 1, marginTop: 100, justifyContent: 'center',
            alignItems: 'center', height: 600,
        }}>
            <AwesomeAlert
                contentContainerStyle={{
                    backgroundColor: '#2b3249', height: 180, width: 400, borderRadius: 10, shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 12,
                    },
                    shadowOpacity: 0.58,
                    shadowRadius: 16.00,

                    elevation: 24
                }}
                show={awesomeAlert}
                showProgress={false}
                title={strings.DELETE_RECORD}
                titleStyle={{ color: 'white' }}
                message={strings.ARE_YOU_SURE_YOU_WANT_TO_DELETE}
                messageStyle={{ color: 'white' }}
                closeOnTouchOutside={true}
                closeOnHardwareBackPress={false}
                showCancelButton={true}
                showConfirmButton={true}
                cancelText={strings.NO_CANCEL}
                confirmText={strings.YES_DELETE}
                cancelButtonColor="#516b93"
                confirmButtonColor="#69c2d1"
                confirmButtonBorderRadius="0"
                confirmButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonStyle={{ borderRadius: 30, height: 45, width: 130, justifyContent: 'center', alignItems: 'center' }}
                cancelButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                confirmButtonTextStyle={{ fontSize: 16, fontWeight: '600' }}
                onCancelPressed={() => {
                    setAwesomeAlert(false);
                }}
                onConfirmPressed={() => {
                    DeleteActivity(popupId, popupIndex);
                }}
            />
            {/* <View><Text style={{ color: '#fff', fontSize: 18, fontFamily: 'HelveticaNeue-Light' }}><Text style={{ color: "#fff", fontWeight: 'bold', fontSize: 22 }}>Your</Text> and <Text style={{ color: "#fff", fontWeight: 'bold', fontSize: 22 }}>{navigation.userfirstname} {navigation.userlastname}</Text>'s Mutual Patients</Text></View> */}
            <View style={{ right: 170, top: 0, flexDirection: "row", flexWrap: "wrap" }}>
                <View style={styles.SectionStyle}>
                    <Icon style={styles.searchIcon} name="search" size={22} color="#81889f" />
                    <TextInput
                        style={styles.inputStyle}
                        placeholder={strings.TYPE_TO_SEARCH_HERE}
                        placeholderTextColor="#8b9cb5"
                        autoCapitalize="none"
                        underlineColorAndroid="#f000"
                        onChangeText={(text) => { searchKeyword(text) }}
                        blurOnSubmit={false}
                        value={searchData}
                    />
                </View>
                <View style={{ paddingTop: 45, left: 100, flexDirection: "row", flexWrap: "wrap" }}>
                    <Text style={[styles.headerText, {}]}>{strings.PHONE}</Text>
                    <Text style={[styles.headerText, { left: 50 }]}>{strings.ADDRESS}</Text>
                    <Text style={[styles.headerText, { left: 210 }]}>{strings.EDIT}</Text>
                    <Text style={[styles.headerText, { left: 220 }]}>{strings.DELETE}</Text>
                </View>
            </View>
            <FlatList
                style={{ width: '80%', top: 0, }}
                showsVerticalScrollIndicator={false}
                data={requestData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={ItemView}
                ListEmptyComponent={NoDataFound}
                ListFooterComponent={renderFooter}
                onEndReachedThreshold={0.5}
                refreshControl={
                    <RefreshControl
                        refreshing={refreshing}
                        onRefresh={onRefresh}
                    />}
            />
        </SafeAreaView >
    );
};

const styles = StyleSheet.create({
    footer: {
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
        flexDirection: 'row',
    },
    floatBtn: {
        alignSelf: 'flex-end',
        marginLeft: 300,
        bottom: 50,
        alignContent: 'center',
        alignItems: 'center',
        shadowColor: '#955555',
        shadowOpacity: 0.8,

    },
    mainBody: {
        flex: 1,
        backgroundColor: '#2b3249',
        padding: 10,
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    viewbtn: {
        alignSelf: 'flex-end',
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#74c2ce',
        height: 40,
        width: 180,
        padding: 3,
        borderRadius: 20
    },
    headerText: {
        color: 'white',
        marginLeft: 20,
    },
    icons: {
        marginLeft: 20,
        marginTop: -4,
        height: 35,
        width: 35
    },
    searchIcon: {
        paddingTop: 11,

    },
    activity: {
        color: 'white',
        flexDirection: 'row',
        flexWrap: 'wrap',
        borderWidth: 1,
        borderColor: '#81889f',
        width: '100%',
        height: 50,
        borderRadius: 15,
        marginBottom: 17,
    },
    activityText: {
        flexDirection: 'row',
        fontSize: 18,
        width: 220,
        fontWeight: '500',
        left: 30,
        marginTop: 12,
        color: 'white'
    },
    activityColor: {
        borderColor: 'black',
        // borderWidth: 1,
        borderRadius: 30,
        width: 45,
        height: 45,
        left: 15,
        marginTop: 2,
    },
    activityColorForm: {
        borderColor: 'black',
        borderWidth: 1,
        borderRadius: 30,
        height: 18,
        left: 15,
        width: 18
    },
    buttonStyle: {
        backgroundColor: '#69c2d1',
        borderWidth: 0,
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        height: 40,
        width: 150,
        alignItems: 'center',
        borderRadius: 30,
        marginLeft: 155,
        marginTop: 20,
        marginBottom: 20,
    },
    buttonTextStyle: {
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
    },
    SectionStyle: {
        flexDirection: 'row',
        height: 40,
        marginTop: 20,
        margin: 10,
        width: 200,
        justifyContent: 'center',
        borderBottomWidth: 1,
        borderBottomColor: '#dadae8',
        textAlign: 'center'
    },
    inputStyle: {
        flex: 1,
        color: 'white',
        flexDirection: 'row',
        fontSize: 14,
        left: 8,
        paddingTop: 7
    },
    floatBtn: {
        alignSelf: 'flex-end',
        // position: 'absolute',
        marginLeft: 300,
        bottom: 35,
        shadowColor: '#955555',
        shadowOpacity: 0.8

    },
    scrollView: {
        height: '80%',
        width: '100%',
        // margin: 20,
        alignSelf: 'center',
        // padding: 20,
    },
    contentContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        paddingBottom: 250,
    }
});

export default AllPatientAcceptScreen;
