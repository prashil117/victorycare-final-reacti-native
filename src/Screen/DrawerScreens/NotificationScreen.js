import React, { useEffect, useState } from 'react';
import { StyleSheet, View, Text, TouchableOpacity, TextInput, Switch } from 'react-native';
import RadioForm from 'react-native-simple-radio-button';
import config from '../appconfig/config';
import { useSelector } from 'react-redux'
import axios from 'axios';
import DropDownPicker from 'react-native-dropdown-picker';
import Loader from '../Components/Loader';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Modal, Portal, Provider, FAB, Snackbar } from 'react-native-paper';
import RadioButton from '../Components/radioButton';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const PrivacyScreen = ({ navigation }) => {
    const [checked, setChecked] = React.useState('first');
    const currentUser = useSelector(state => state.auth.userLogin)
    const [dropValues, setDropDownList] = useState([
        { value: '+0 minutes', label: strings.ON_TIME },
        { value: '+15 minutes', label: strings.DEFAULT_15_MINUTES },
        { value: '+30 minutes', label: strings.THIRTY_MINUTES },
        { value: '+45 minutes', label: strings.FORTY_MINUTES },
        { value: '+1 hour', label: strings.ONE_HOUR }
    ]);
    const [value, setValue] = useState(0);
    const [visible, setVisible] = useState(0);
    const [loading, setLoading] = useState(false);
    const [snackMsg, setsnackMsg] = useState('');
    const [visibleSnackbar, setVisibleSnack] = React.useState(false);
    const [eventtime, setEventTime] = useState('');
    const [activitytime, setActivityTime] = useState('');
    const [medicinetime, setMedicineTime] = useState('');
    const [timeType, setTimeType] = useState('');
    const [isNotification, setNotification] = useState(0);
    const [isStatistical, setStatistical] = useState(0);
    const hideModalColor = () => {
        setVisible(false);
    }
    const radio_props = [
        { text: strings.YES, key: "1" },
        { text: strings.NO  , key: "0" }
    ];
    let index = 0;
    const data = [
        { key: index++, section: true, label: 'Fruits' },
        { key: index++, label: 'Red Apples' },
        { key: index++, label: 'Cherries' },
        { key: index++, label: 'Cranberries', accessibilityLabel: 'Tap here for cranberries' },
        // etc...
        // Can also add additional custom keys which are passed to the onChange callback
        { key: index++, label: 'Vegetable', customKey: 'Not a fruit' }
    ];


    useEffect(() => {
        appIntialize();
    }, [])
    const appIntialize = () => {
        var data = {
            userid: currentUser.id,
            appsecret: config.appsecret,
            action: 'getNotificationSettings',
        }
        axios.post(config.apiUrl + 'notificationsettings.php', data).then(result => {
            console.log("result", result.data)
            setEventTime(result.data.data.event_time);
            setActivityTime(result.data.data.activity_time);
            setMedicineTime(result.data.data.medicine_time);
            var notification = result.data.data.isNotification ? result.data.data.isNotification : 0
            console.log("notiication", isNotification)
            var statistical = result.data.data.isStatistical ? result.data.data.isStatistical : 0
            setNotification(notification)
            setStatistical(statistical)

        }), err => {
            console.log("error", err)
        }
    }
    const setItem = (value) => {
        if (timeType === 'm') {
            setMedicineTime(value)
        }
        if (timeType === 'a') {
            setActivityTime(value)
        }
        if (timeType === 'w') {
            setEventTime(value)
        }
        hideModalColor()
    }
    const onDismissSnackBar = () => setVisibleSnack(false);
    const updateNotification = () => {
        setLoading(true)
        var data = {
            userid: currentUser.id,
            appsecret: config.appsecret,
            activitytime: activitytime,
            medicinetime: medicinetime,
            eventtime: eventtime,
            isNotification: isNotification.toString(),
            // isActivity: this.isActivity,
            isStatistical: isStatistical.toString(),
            // isAccept: this.isAccept,
            action: 'timesettings',
        }
        console.log("data", data)
        axios.post(config.apiUrl + 'notificationsettings.php', data).then(result => {
            setLoading(false)
            console.log(result.data)
            if (result.data.status == "success") {
                setsnackMsg(strings.DATA_SAVED_SUCCESSFULLY);
                setVisibleSnack(true)
            }
        }), err => {
            this.loadingService.hide();
            console.log("error : ", err)
        }
    }
    const onSelect = (item) => {
        console.log("item", item)
        if (isNotification === item.key) {
            setNotification(item.key)
        } else {
            setNotification(item.key)
        }
    };
    const onSelectStatistical = (item) => {
        console.log("item", item)
        if (isStatistical === item.key) {
            setStatistical(item.key)
        } else {
            setStatistical(item.key)
        }
    };

    return (
        <View style={styles.mainBody}>
            <Loader loading={loading} />
            <View style={{ width: '60%', marginTop:50 }}>
                <View>
                    <Text style={styles.menuText}>
                        {strings.APPLY_NOTIFICATIONS_TO_YO}
                </Text>
                <View style={{left:20}}>
                        <RadioButton
                            selectedOption={isNotification}
                            onSelect={onSelect}
                            options={radio_props}
                        />
                    </View>
                </View>
                <View style={{ top: 20 }}>
                    <Text style={styles.menuText}>
                        {strings.DO_YOU_WANT_TO_RECIEVE_A}
                    </Text>
                    <View style={{left:20}}>
                        <RadioButton
                            selectedOption={isStatistical}
                            onSelect={onSelectStatistical}
                            options={radio_props}
                        />
                    </View>
                </View>
                <View style={{ marginTop: 50, height: 40 }}>
                    <View style={{ marginLeft: 30, flexDirection: 'row', borderBottomWidth: 3, borderColor: 'white' }}>
                        <View style={{ left: 0, borderColor: 'white', top: -5 }}>
                            <Text style={{ color: 'white', fontFamily: 'HelveticaNeue-Light', fontSize:16 }}>
                                {strings.MEDICINE_NOTIFICATION_FRE}
                            </Text>
                        </View>
                        <View style={{ left: 235, borderColor: 'white', top: -5 }}>
                            <TouchableOpacity onPress={() => { setVisible(true), setTimeType('m') }}>
                                <Text style={{ color: 'white', fontWeight: '700' }}>
                                    {medicinetime}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ left: 235, borderColor: 'white', top: -10 }}>
                            <Text style={{ color: 'white', fontWeight: '700' }}>
                                <Icon name="arrow-drop-down" color="#81889f" size={30} />
                            </Text>
                        </View>
                    </View>

                </View>
                <View style={{ marginTop: 20, height: 40 }}>
                    <View style={{ marginLeft: 30, flexDirection: 'row', borderBottomWidth: 3, borderColor: 'white' }}>
                        <View style={{ left: 0, borderColor: 'white', top: -5 }}>
                            <Text style={{ color: 'white', fontFamily: 'HelveticaNeue-Light', fontSize:16 }}>
                                {strings.ACTIVITY_NOTIFICATION_FRE}
                            </Text>
                        </View>
                        <View style={{ left: 240, borderColor: 'white', top: -5 }}>
                            <TouchableOpacity onPress={() => { setVisible(true), setTimeType('a') }}>
                                <Text style={{ color: 'white', fontWeight: '700' }}>
                                    {activitytime}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ left: 240, borderColor: 'white', top: -10 }}>
                            <Text style={{ color: 'white', fontWeight: '700' }}>
                                <Icon name="arrow-drop-down" color="#81889f" size={30} />
                            </Text>
                        </View>
                    </View>

                </View>
                <View style={{ marginTop: 20, height: 40 }}>
                    <View style={{ marginLeft: 30, flexDirection: 'row', borderBottomWidth: 3, borderColor: 'white' }}>
                        <View style={{ left: 0, borderColor: 'white', top: -5 }}>
                            <Text style={{ color: 'white', fontFamily: 'HelveticaNeue-Light', fontSize:16 }}>
                                {strings.WELL_BEING_NOTIFICATION_F}
                            </Text>
                        </View>
                        <View style={{ left: 225, borderColor: 'white', top: -5 }}>
                            <TouchableOpacity onPress={() => { setVisible(true), setTimeType('w') }}>
                                <Text style={{ color: 'white', fontWeight: '700' }}>
                                    {eventtime}
                                </Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ left: 225, borderColor: 'white', top: -10 }}>
                            <Text style={{ color: 'white', fontWeight: '700' }}>
                                <Icon name="arrow-drop-down" color="#81889f" size={30} />
                            </Text>
                        </View>
                    </View>

                </View>
            </View>
            <View>
                <TouchableOpacity
                    style={styles.buttonStyle}
                    activeOpacity={0.5}
                    onPress={updateNotification}
                >
                    <Text style={styles.buttonTextStyle}>{strings.SAVE}</Text>
                </TouchableOpacity>
            </View>
            <Provider>
                <Portal>
                    <Modal visible={visible} onDismiss={hideModalColor} contentContainerStyle={{ alignSelf: 'center' }}>
                        <DropDownPicker
                            items={dropValues}
                            placeholder={'select Activity'}
                            labelStyle={{ fontSize: 14, textAlign: 'left', color: 'white', borderWidth: 0 }}
                            defaultValue={activitytime ? activitytime : null}
                            containerStyle={{ height: 40, color: 'white', borderWidth: 0, width: 200, }}
                            style={{ backgroundColor: '#2b3249', color: 'white', width: 200, borderWidth: 0, borderBottomWidth: 1 }}
                            itemStyle={{
                                justifyContent: 'flex-start', color: 'white', borderWidth: 0
                            }}
                            dropDownStyle={{ backgroundColor: '#2b3249', color: 'white', }}
                            onChangeItem={(item) => { setItem(item.value) }}
                        />
                    </Modal>
                </Portal>
            </Provider>
            <Snackbar
                style={{ width: 300, alignSelf: 'center', alignItems: 'center' }}
                visible={visibleSnackbar}
                duration={3000}
                onDismiss={onDismissSnackBar}
            >
                {snackMsg}
            </Snackbar>
        </View >
    );
};

const styles = StyleSheet.create({
    mainBody: {
        fontFamily: 'HelveticaNeue-Light',
        flex: 1,
        paddingLeft: '5%',
        marginTop: 0,
        backgroundColor: '#2b3249',
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuText: {
        fontFamily: 'HelveticaNeue-Light',
        fontSize: 18,
        fontWeight: '600',
        color: 'white',
        marginLeft: 30
    },

    buttonStyle: {
        fontFamily: 'HelveticaNeue-Light',
        backgroundColor: '#69c2d1',
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        alignSelf: 'flex-end',
        height: 40,
        width: 130,
        left: 230,
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 50,
    },

    buttonTextStyle: {
        fontFamily: 'HelveticaNeue-Light',
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 18,
        fontWeight: '600'
    },

});


export default PrivacyScreen;
