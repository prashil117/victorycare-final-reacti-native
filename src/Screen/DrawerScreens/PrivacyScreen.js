import React from 'react';
import { StyleSheet, View, Text, TouchableOpacity, } from 'react-native';
import { useState } from 'react/cjs/react.development';
import RadioButton from '../Components/radioButton';
import strings from '../../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../../helper/changeLng';

const PrivacyScreen = ({ navigation }) => {
    const[isprivacy,setPrivacy]=useState(1);
    const radio_props = [
        { text: strings.YES, key: 1 },
        { text: strings.NO, key: 0 }
    ];
    const onSelectStatistical = (item) => {
        console.log("item", item)
        if (isprivacy === item.key) {
            setPrivacy(item.key)
        } else {
            setPrivacy(item.key)
        }
    };

    return (
        <View style={styles.mainBody}>
            <View style={{width:'60%'}}>
                <Text style={styles.menuText}>
                    {strings.DO_YOU_ACCEPT_YOUR_DATA_W}
                </Text>
                <View style={{left:20}}>
                        <RadioButton
                            selectedOption={isprivacy}
                            onSelect={onSelectStatistical}
                            options={radio_props}
                        />
                    </View>
                <TouchableOpacity
                    style={styles.buttonStyle}
                    activeOpacity={0.5}
                // onPress={handleSubmitPress}
                >
                    <Text style={styles.buttonTextStyle}>{strings.SAVE}</Text>
                </TouchableOpacity>
            </View>
        </View >
    );
};

const styles = StyleSheet.create({
    mainBody: {
        fontFamily: 'HelveticaNeue-Light',
        flex:1,
        paddingLeft: '5%',
        marginTop: -480,
        backgroundColor: '#2b3249',
        alignContent: 'center',
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center',
    },
    menuText: {
        fontFamily: 'HelveticaNeue-Light',
        fontSize: 14,
        fontWeight: '600',
        color: 'white',
        marginLeft: 30
    },
    buttonStyle: {
        fontFamily: 'HelveticaNeue-Bold',
        backgroundColor: '#69c2d1',
        color: '#FFFFFF',
        borderColor: '#69c2d1',
        alignSelf:'flex-end',
        height: 40,
        width: 130,
        marginRight:50,
        alignItems: 'center',
        borderRadius: 30,
        marginTop: 20,
    },
    buttonTextStyle: {
        fontFamily: 'HelveticaNeue-Bold',
        color: '#FFFFFF',
        paddingVertical: 10,
        fontSize: 16,
        fontWeight:'600'
    },

});


export default PrivacyScreen;
