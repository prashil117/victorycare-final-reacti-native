import React, { useState, useEffect } from 'react';
import { Image, View, Text, StyleSheet, TouchableOpacity, TextInput, Button } from 'react-native';
import { useSelector, useDispatch } from 'react-redux'
import { createStackNavigator } from '@react-navigation/stack';
import { createDrawerNavigator } from '@react-navigation/drawer';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { setModal } from '../Screen/redux/action'
import ProfileScreen from './DrawerScreens/ProfileScreen';
import TestScreen from './DrawerScreens/TestScreen';
import AllUsersScreen from './DrawerScreens/AllUsersMainScreen';
import AllUsersSubScreen from './DrawerScreens/AllUsersSubScreen';
import ViewEditProfileScreen from './DrawerScreens/ViewEditProfileScreen';
import AddUserScreen from './DrawerScreens/AddUserScreen';
import MedicineMasterScreen from './DrawerScreens/MedicineMasterScreen';
import MedicineUsersTabsScreen from './DrawerScreens/MedicineUsersTabsScreen';
import ActivityUsersTabsScreen from './DrawerScreens/ActivityUsersTabsScreen';
import WellbeingUsersTabsScreen from './DrawerScreens/WellbeingUsersTabsScreen';
import OnboardScreen from './DrawerScreens/OnboardScreen';
import WellBeingMasterScreen from './DrawerScreens/WellBeingMasterScreen';
import ActivityMasterScreen from './DrawerScreens/ActivityMasterScreen';
import ActivityNotificationScreen from './DrawerScreens/ActivityNotificationScreen';
import WellBeingNotificationScreen from './DrawerScreens/WellBeingNotificationScreen';
import MedicineNotificationScreen from './DrawerScreens/MedicineNotificationScreen';
import LibraryScreen from './DrawerScreens/LibraryScreen';
import SettingsScreen from './DrawerScreens/SettingsScreen';
import PrivacyScreen from './DrawerScreens/PrivacyScreen';
import TimelineScreen from './DrawerScreens/TimelineScreen';
import NotificationScreen from './DrawerScreens/NotificationScreen';
import HowWorkScreen from './DrawerScreens/HowWorkScreen';
import TimelClockSettingsScreen from './DrawerScreens/24ClockSettings';
import RequestsScreen from './DrawerScreens/RequestsScreen';
import SettingsWellbeingScreen from './DrawerScreens/SettingsWellbeingScreen';
import SettingsActivityScreen from './DrawerScreens/SettingsActivityScreen';
import SettingsMedicineScreen from './DrawerScreens/SettingsMedicineScreen';
import MainDashboardMonthScreen from './DrawerScreens/MainDashboardMonthScreen';
import WellbeingDashboardMonthScreen from './DrawerScreens/WellbeingDashboardMonthScreen';
import WellbeingDashboardWeekScreen from './DrawerScreens/WellbeingDashboardWeekScreen';
import AboutScreen from './DrawerScreens/AboutVictoryScreen';
import TermsAndConditonScreen from './DrawerScreens/TermsAndConditonScreen';
import MainDashboardWeekScreen from './DrawerScreens/MainDashboardWeekScreen';
import ContactScreen from './DrawerScreens/ContactScreen';
import CustomSidebarMenu from './Components/CustomSidebarMenu';
import UserSession from './Components/userSession';
import NavigationDrawerHeader from './Components/NavigationDrawerHeader';
import AsyncStorage from '@react-native-community/async-storage';
import calendar from '../../Image/calendar-icon3.png'
import AwesomeAlert from 'react-native-awesome-alerts';
import moment from 'moment';
import strings from '../constants/lng/LocalizedStrings';
import { setLng, getLng, getLanguageData } from '../helper/changeLng';

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const profileScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator initialRouteName="ProfileScreen">
      <Stack.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{
          title: strings.MY_PROFILE, //Set Header Title
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerStyle: {
            backgroundColor: '#2b3249', //Set Header color
            borderColor: '#2b3249',
            shadowColor: 'transparent',
            shadowRadius: 0,
            shadowOffset: {
              height: 0,
            }
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
            fontSize:18
          },
        }}
      />
    </Stack.Navigator>
  );
};

// if(sessionUser && sessionUser.userid){
  const medicineScreenStack = ({ navigation }) => {
    const sessionUser = useSelector(state => state.auth.requestedUser)
    // const dependency = useSelector(state => state.auth.dependancy)
    const [dependency, setDependency] = useState({});
    useEffect(() => {
      async function getDependency() {
        AsyncStorage.getItem('dependency', (err, result) => {
          setDependency(result);
        })
      }
      getDependency();
    }, []);
    
    if((sessionUser && sessionUser.userid) || (dependency && dependency == 'dependent')){
      return (
        <Stack.Navigator
          initialRouteName="MedicineUsersTabsScreen"
          screenOptions={{
            headerStyle: {
              backgroundColor: '#2b3249', //Set Header color
              borderColor: '#2b3249',
              shadowColor: 'transparent',
              shadowRadius: 0,
              shadowOffset: {
                height: 0,
              }
            },
            headerTintColor: '#fff', //Set Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Set Header text style
              fontSize:18,
            },
          }}
        >
          <Stack.Screen
            name="MedicineUsersTabsScreen"
            component={MedicineUsersTabsScreen}
            options={{
              headerLeft: () => (
                <NavigationDrawerHeader navigationProps={navigation} />
              ),
              title: strings.MEDICINES, //Set Header Title
              headerTitleStyle: {
                fontWeight: 'bold', //Set Header text style
                fontSize:18,
              },
              headerRight: () => (
                <UserSession {...navigation} />
              ),
            }}
          />
          <Stack.Screen
            name="MedicineNotificationScreen"
            component={MedicineNotificationScreen}
            options={{
              title: strings.MEDICINE_NOTIFICATION_LIS, //Set Header Title
            }}
          />
        </Stack.Navigator>
      );
    }else{
      return (
        <Stack.Navigator
          initialRouteName="MedicineMasterScreen"
          screenOptions={{
            headerStyle: {
              backgroundColor: '#2b3249', //Set Header color
              borderColor: '#2b3249',
              shadowColor: 'transparent',
              shadowRadius: 0,
              shadowOffset: {
                height: 0,
              }
            },
            headerTintColor: '#fff', //Set Header text color
            headerTitleStyle: {
              fontWeight: 'bold', //Set Header text style
              fontSize:18,
            },
          }}
        >
          <Stack.Screen
            name="MedicineMasterScreen"
            component={MedicineMasterScreen}
            options={{
              headerLeft: () => (
                <NavigationDrawerHeader navigationProps={navigation} />
              ),
              title: strings.MEDICINES, //Set Header Title
              headerTitleStyle: {
                fontWeight: 'bold', //Set Header text style
                fontSize:18,
              },
              headerRight: () => (
                <UserSession />
              ),
            }}
          />
          <Stack.Screen
            name="MedicineNotificationScreen"
            component={MedicineNotificationScreen}
            options={{
              title: strings.MEDICINE_NOTIFICATION_LIS, //Set Header Title
            }}
          />
        </Stack.Navigator>
      );
    }
  };
// }else{

// }

const allUsersScreenStack = ({ navigation, route }) => {
  // console.log("NAvigtion::::",navigation);
  // const { userid, userfirstname, userlastname, currusertype } = route.params;
  const currentUser = useSelector(state => state.auth.userLogin)
  return (
    <Stack.Navigator
      initialRouteName="AllUsersScreen"
      screenOptions={{
        headerStyle: {
          backgroundColor: '#2b3249', //Set Header color
          borderColor: '#2b3249',
          shadowColor: 'transparent',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          }
        },
        headerTintColor: '#fff', //Set Header text color
        headerTitleStyle: {
          fontWeight: 'bold', //Set Header text style
        },
      }}
    >
      <Stack.Screen
        name="AllUsersScreen"
        component={AllUsersScreen}
        options={{
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          title: strings.ALL_USERS, //Set Header Title
          // title:currentUser.firstname + " " + currentUser.lastname,
          headerTitleStyle: {
            fontSize: 18
          },
          headerRight: () => (
            <UserSession />
          ),
        }}
      />
      <Stack.Screen
        name="MedicineNotificationScreen"
        component={MedicineNotificationScreen}
        options={{
          title: strings.MEDICINE_NOTIFICATION_LIS, //Set Header Title
        }}
      />
      <Stack.Screen
        name="AllUsersSubScreen"
        component={AllUsersSubScreen}
        // options={{
        //   title: "All Users", //Set Header Title
        // }}
        options={({ route }) => ({ title: route.params && route.params.userfirstname ? route.params.userfirstname+"'s Profile" : "All Users" })}
        initialParams={{ userid: currentUser.id, userfirstname:currentUser.firstname, userlastname:currentUser.lastname }}
      />
      <Stack.Screen
        name="AddUserScreen"
        component={AddUserScreen}
        options={{
          title: "Add User", //Set Header Title
        }}
        initialParams={{ userid: currentUser.id, userfirstname:currentUser.firstname, userlastname:currentUser.lastname }}
      />
      <Stack.Screen
        name="ViewEditProfileScreen"
        component={ViewEditProfileScreen}
        options={{
          title: "User's Profile", //Set Header Title
        }}
        initialParams={{ userid: currentUser.id, userfirstname:currentUser.firstname, userlastname:currentUser.lastname }}
      />
    </Stack.Navigator>
  );
};

// const allUsersSubScreenStack = ({ navigation }) => {
//   const currentUser = useSelector(state => state.auth.userLogin)
//   return (
//     <Stack.Navigator
//       initialRouteName="AllUsersSubScreen"
//       screenOptions={{
//         headerStyle: {
//           backgroundColor: '#2b3249', //Set Header color
//           borderColor: '#2b3249',
//           shadowColor: 'transparent',
//           shadowRadius: 0,
//           shadowOffset: {
//             height: 0,
//           }
//         },
//         headerTintColor: '#fff', //Set Header text color
//         headerTitleStyle: {
//           fontWeight: 'bold', //Set Header text style
//         },
//       }}
//     >
//       <Stack.Screen
//         name="AllUsersSubScreen"
//         component={AllUsersScreen}
//         options={{
//           headerLeft: () => (
//             <NavigationDrawerHeader navigationProps={navigation} />
//           ),
//           title: strings.ALL_USERS, //Set Header Title
//           headerTitleStyle: {
//             fontSize: 18
//           },
//           headerRight: () => (
//             <UserSession />
//           ),
//         }}
//         initialParams={{ userid: currentUser.id, userfirstname:currentUser.firstname, userlastname:currentUser.lastname, }}
//       />
//     </Stack.Navigator>
//   );
// };

const onboardScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="OnboardScreen"
      screenOptions={{
        headerStyle: {
          backgroundColor: '#2b3249', //Set Header color
          borderColor: '#2b3249',
          shadowColor: 'transparent',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          }
        },
        headerTintColor: '#fff', //Set Header text color
        headerTitleStyle: {
          fontWeight: 'bold', //Set Header text style
          fontSize:18,
        },
      }}
    >
      <Stack.Screen
        name="OnboardScreen"
        component={OnboardScreen}
        options={{
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          title: strings.ONBOARD, //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
};

const settingScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="SettingsScreen"
      screenOptions={{
        headerStyle: {
          backgroundColor: '#2b3249', //Set Header color
          borderColor: '#2b3249',
          shadowColor: 'transparent',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          }
        },
        headerTintColor: '#fff', //Set Header text color
        headerTitleStyle: {
          fontWeight: 'bold', //Set Header text style
          fontSize:18,
        },
      }}>
      <Stack.Screen
        name="SettingsScreen"
        component={SettingsScreen}
        options={{
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          title: strings.SETTINGS, //Set Header Title
        }}
      />
      <Stack.Screen
        name="ProfileScreen"
        component={ProfileScreen}
        options={{
          title: strings.MY_PROFILE, //Set Header Title
        }}
      />
      <Stack.Screen
        name="PrivacyScreen"
        component={PrivacyScreen}
        options={{
          title: strings.PRIVACY, //Set Header Title
        }}
      />
      <Stack.Screen
        name="NotificationScreen"
        component={NotificationScreen}
        options={{
          title: strings.NOTIFICATIONS, //Set Header Title
        }}
      />
      <Stack.Screen
        name="HowWorkScreen"
        component={HowWorkScreen}
        options={{
          title: strings.HOW_DOES_IT_WORK, //Set Header Title
        }}
      />
      <Stack.Screen
        name="AboutScreen"
        component={AboutScreen}
        options={{
          title: strings.ABOUT_VICTOR_Y_CARE, //Set Header Title
        }}
      />
      <Stack.Screen
        name="TermsAndConditonScreen"
        component={TermsAndConditonScreen}
        options={{
          title: strings.TERMS_CONDITONS_AND_PR, //Set Header Title
        }}
      />
      <Stack.Screen
        name="TestScreen"
        component={TestScreen}
        options={{
          title: 'Test Screen', //Set Header Title
        }}
      />
      <Stack.Screen
        name="MainDashboardWeekScreen"
        component={MainDashboardWeekScreen}
        options={{
          title: strings.MAIN_DASHBOARD, //Set Header Title
        }}
      />
      <Stack.Screen
        name="MainDashboardMonthScreen"
        component={MainDashboardMonthScreen}
        options={{
          title: strings.MAIN_DASHBOARD, //Set Header Title
        }}
      />
      <Stack.Screen
        name="WellbeingDashboardWeekScreen"
        component={WellbeingDashboardWeekScreen}
        options={{
          title: strings.WELLBEING_DASHBOARD,
          headerRight: () => (
            <UserSession />
          ),
          //Set Header Title
        }}
      />
      <Stack.Screen
        name="WellbeingDashboardMonthScreen"
        component={WellbeingDashboardMonthScreen}
        options={{
          title: strings.WELLBEING_DASHBOARD, //Set Header Title
          headerRight: () => (
            <UserSession />
          ),
        }}
      />
      <Stack.Screen
        name="TimelClockSettingsScreen"
        component={TimelClockSettingsScreen}
        options={{
          title: strings.TWENTYFOUR_HOURS_CLOCK_SETTINGS, //Set Header Title

        }}
      />
      <Stack.Screen
        name="RequestsScreen"
        component={RequestsScreen}
        options={{
          title: strings.REQUEST_TO_ACCESS_PROFILE, //Set Header Title
          headerRight: () => (
            <UserSession />
          ),
        }}
      />
      <Stack.Screen
        name="SettingsActivityScreen"
        component={SettingsActivityScreen}
        options={{
          title: strings.EDIT_ACTIVITY, //Set Header Title
        }}
      />
      <Stack.Screen
        name="SettingsMedicineScreen"
        component={SettingsMedicineScreen}
        options={{
          title: strings.EDIT_MEDICINE, //Set Header Title
        }}
      />
      <Stack.Screen
        name="SettingsWellbeingScreen"
        component={SettingsWellbeingScreen}
        options={{
          title: strings.EDIT_WELLBEING, //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
};

const timelineScreenStack = ({ navigation }) => {
  const currentUser = useSelector(state => state.auth.userLogin)
  const sessionUser = useSelector(state => state.auth.requestedUser)
  const dispatch = useDispatch();
  const currentDate = useSelector(state => state.auth.currentDate)
  console.log("CUrrent Date", currentDate)
    return (
      <Stack.Navigator
        initialRouteName="TimelineScreen"
        screenOptions={{
          headerLeft: () => (
            <NavigationDrawerHeader navigationProps={navigation} />
          ),
          headerRight: () => (
            <View right={300}>
              <View style={{ width: 200, top: 10, }}>
                <TouchableOpacity activeOpacity={0.9} onPress={() => dispatch(setModal(true))}>
                  <View style={styles.Date}>
                    <View style={styles.dateImg}>
                      <Image
                        source={calendar}
                        style={{ width: 30, height: 30, top: -1 }}
                      />
                    </View>
                    <Text style={{ fontSize: 24, top: 9, left: 10, letterSpacing: 2, fontWeight: '700', color: 'white' }}>
                      {moment(currentDate).format('DD.MM.YYYY')}
                    </Text>
                    <Text style={{ fontSize: 22, top: 1, left: 5 }}>
                      <Icon name="arrow-drop-down" color="grey" size={40} />
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{ width:400,left: 300, top: -40 }}>
                <UserSession />
              </View>
            </View>
          )
        }}>
        <Stack.Screen
          name="TimelineScreen"
          component={TimelineScreen} 
          options={{
            headerStyle: {
              elevation: 0,
              shadowOpacity: 0,
              borderBottomWidth: 0,
              borderBottomWidth: 0,
              backgroundColor: '#2b3249', //Set Header color
            },
            title: '', //Set Header Title
          }}
        />
      </Stack.Navigator>
    );
};

const ItemView = () => {
  return (
    <View style={{ width: 150 }}>
      <DateTimePickerModal
        style={{ backgroundColor: '#2b3249', }}
        headerTextIOS={strings.SELECT_TIME}
        locale="en_GB"
        textColor="white"
        isVisible={isDatePickerVisible}
        pickerContainerStyleIOS={{ backgroundColor: '#2b3249' }}
        modalStyleIOS={{ width: 350, alignSelf: 'center' }}
        mode="time"
        onConfirm={handleConfirm}
        onCancel={hideDatePicker}
      />

    </View>
  );
};

const activityScreenStack = ({ navigation }) => {
  const sessionUser = useSelector(state => state.auth.requestedUser)
  // const dependency = useSelector(state => state.auth.dependancy)
  const [dependency, setDependency] = useState({});
    useEffect(() => {
      async function getDependency() {
        AsyncStorage.getItem('dependency', (err, result) => {
          setDependency(result);
        })
      }
      getDependency();
    }, []);
  if((sessionUser && sessionUser.userid) || (dependency && dependency == 'dependent')){
    return (
      <Stack.Navigator
        initialRouteName="ActivityUsersTabsScreen"
        screenOptions={{
          headerStyle: {
            backgroundColor: '#2b3249', //Set Header color
            borderColor: '#2b3249',
            shadowColor: 'transparent',
            shadowRadius: 0,
            shadowOffset: {
              height: 0,
            }
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
            fontSize:18,
          },
        }}>
        <Stack.Screen
          name="ActivityUsersTabsScreen"
          component={ActivityUsersTabsScreen}
          options={{
            headerLeft: () => (
              <NavigationDrawerHeader navigationProps={navigation} />
            ),
            title: strings.ACTIVITIES, //Set Header Title
            headerRight: () => (
              <UserSession />
            ),
          }}
        />
        <Stack.Screen
          name="ActivityNotificationScreen"
          component={ActivityNotificationScreen}
          options={{
            title: strings.ACTIVITY_NOTIFICATION_LIS, //Set Header Title
            headerRight: () => (
              <UserSession />
            ),
          }}
        />
      </Stack.Navigator>
    );
  }else{
    return (
      <Stack.Navigator
        initialRouteName="ActivityMasterScreen"
        screenOptions={{
          headerStyle: {
            backgroundColor: '#2b3249', //Set Header color
            borderColor: '#2b3249',
            shadowColor: 'transparent',
            shadowRadius: 0,
            shadowOffset: {
              height: 0,
            }
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
            fontSize:18,
          },
        }}>
        <Stack.Screen
          name="ActivityMasterScreen"
          component={ActivityMasterScreen}
          options={{
            headerLeft: () => (
              <NavigationDrawerHeader navigationProps={navigation} />
            ),
            title: strings.ACTIVITIES, //Set Header Title
            headerRight: () => (
              <UserSession />
            ),
          }}
        />
        <Stack.Screen
          name="ActivityNotificationScreen"
          component={ActivityNotificationScreen}
          options={{
            title: strings.ACTIVITY_NOTIFICATION_LIS, //Set Header Title
            headerRight: () => (
              <UserSession />
            ),
          }}
        />
      </Stack.Navigator>
    );
  }
};

const wellbeingScreenStack = ({ navigation }) => {
  const sessionUser = useSelector(state => state.auth.requestedUser)
  // const dependency = useSelector(state => state.auth.dependancy)
  const [dependency, setDependency] = useState({});
    useEffect(() => {
      async function getDependency() {
        AsyncStorage.getItem('dependency', (err, result) => {
          setDependency(result);
        })
      }
      getDependency();
    }, []);
  if((sessionUser && sessionUser.userid) || (dependency && dependency == 'dependent')){
    return (
      <Stack.Navigator
        initialRouteName="WellbeingUsersTabsScreen"
        screenOptions={{
          headerStyle: {
            backgroundColor: '#2b3249', //Set Header color
            borderColor: '#2b3249',
            shadowColor: 'transparent',
            shadowRadius: 0,
            shadowOffset: {
              height: 0,
            }
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
            fontSize:18,

          },
        }}>
        <Stack.Screen
          name="WellbeingUsersTabsScreen"
          component={WellbeingUsersTabsScreen}
          options={{
            headerLeft: () => (
              <NavigationDrawerHeader navigationProps={navigation} />
            ),
            title: strings.WELLBEING, //Set Header Title
            headerRight: () => (
              <UserSession />
            ),
          }}
        />
        <Stack.Screen
          name="WellBeingNotificationScreen"
          component={WellBeingNotificationScreen}
          options={{
            title: strings.WELLBEING_NOTIFICATION_LI, //Set Header Title
          }}
        />
      </Stack.Navigator>
    );
  }else{
    return (
      <Stack.Navigator
        initialRouteName="WellBeingMasterScreen"
        screenOptions={{
          headerStyle: {
            backgroundColor: '#2b3249', //Set Header color
            borderColor: '#2b3249',
            shadowColor: 'transparent',
            shadowRadius: 0,
            shadowOffset: {
              height: 0,
            }
          },
          headerTintColor: '#fff', //Set Header text color
          headerTitleStyle: {
            fontWeight: 'bold', //Set Header text style
            fontSize:18,

          },
        }}>
        <Stack.Screen
          name="WellBeingMasterScreen"
          component={WellBeingMasterScreen}
          options={{
            headerLeft: () => (
              <NavigationDrawerHeader navigationProps={navigation} />
            ),
            title: strings.WELLBEING, //Set Header Title
            headerRight: () => (
              <UserSession />
            ),
          }}
        />
        <Stack.Screen
          name="WellBeingNotificationScreen"
          component={WellBeingNotificationScreen}
          options={{
            title: strings.WELLBEING_NOTIFICATION_LI, //Set Header Title
          }}
        />
      </Stack.Navigator>
    );
  }
};

const libraryScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="LibraryScreen"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerRight: () => (
          <UserSession />
        ),
        headerStyle: {
          backgroundColor: '#2b3249', //Set Header color
          borderColor: '#2b3249',
          shadowColor: 'transparent',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          }
        },
        headerTintColor: '#fff', //Set Header text color
        headerTitleStyle: {
          fontWeight: 'bold', //Set Header text style
          fontSize:18,
        },
      }}>
      <Stack.Screen
        name="LibraryScreen"
        component={LibraryScreen}
        options={{
          title: strings.LIBRARY, //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
};

const contactsScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="ContactScreen"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerRight: () => (
          <UserSession />
        ),
        headerStyle: {
          backgroundColor: '#2b3249', //Set Header color
          borderColor: '#2b3249',
          shadowColor: 'transparent',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          }
        },
        headerTintColor: '#fff', //Set Header text color
        headerTitleStyle: {
          fontWeight: 'bold', //Set Header text style
          fontSize:18,
        },
      }}>
      <Stack.Screen
        name="ContactScreen"
        component={ContactScreen}
        options={{
          title: strings.CONTACTS, //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
};

const dashboardScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="dasboardScreen"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerStyle: {
          backgroundColor: '#2b3249', //Set Header color
          borderColor: '#2b3249',
          shadowColor: 'transparent',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          }
        },
        headerTintColor: '#fff', //Set Header text color
        headerTitleStyle: {
          fontWeight: 'bold', //Set Header text style
          fontSize:18,
        },
      }}>
      <Stack.Screen
        name="WellbeingDashboardWeekScreen"
        component={WellbeingDashboardWeekScreen}
        options={{
          title: strings.DASHBOARD, //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
};

const WellBeingdashboardScreenStack = ({ navigation }) => {
  return (
    <Stack.Navigator
      initialRouteName="WellBeingDasboardScreen"
      screenOptions={{
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
        headerStyle: {
          backgroundColor: '#2b3249', //Set Header color
          borderColor: '#2b3249',
          shadowColor: 'transparent',
          shadowRadius: 0,
          shadowOffset: {
            height: 0,
          }
        },
        headerTintColor: '#fff', //Set Header text color
        headerTitleStyle: {
          fontWeight: 'bold', //Set Header text style
          fontSize:18,
        },
      }}>
      <Stack.Screen
        name="MainDashboardWeekScreen"
        component={MainDashboardWeekScreen}
        options={{
          title: strings.WELLBEING_DASHBOARD, //Set Header Title
        }}
      />
    </Stack.Navigator>
  );
};


const DrawerNavigatorRoutes = (props) => {
  const [userData, setUserData] = useState({});
  const currentUser = useSelector(state => state.auth.userLogin)
  const sessionUser = useSelector(state => state.auth.requestedUser)
  useEffect(() => { selectedLng() }, [])
  const selectedLng = async () => {
    const lngData = await getLng()
    if (!!lngData || lngData != null) {
      strings.setLanguage(lngData)
    }
  }
  

  useEffect(() => {
    async function fetchCredentials() {
      AsyncStorage.getItem('user', (err, result) => {
        // console.log("result:", JSON.parse(result))
        setUserData(result);
      })
    }
    fetchCredentials();
  }, []);

  return (
    <Drawer.Navigator
      drawerStyle={{
        backgroundColor: '#c6cbef',
        width: 100,
      }}
      drawerContentOptions={{
        backgroundColor: '#3a4159',
        activeTintColor: 'transparent',
        activeBackgroundColor: 'transparent',
        showsVerticalScrollIndicator: false,
        itemStyle: { width: "100%", height: 90, color: 'white', right: 10, borderBottomWidth: 1, borderBottomColor: '#697395' },
      }}
      screenOptions={{
        headerShown: false,
        headerLeft: () => (
          <NavigationDrawerHeader navigationProps={navigation} />
        ),
      }}
      drawerContent={props => <CustomSidebarMenu {...props} />}
    >

      <Drawer.Screen
        name="homeScreenStack"
        options={{
          drawerIcon: ({ focused, size, activeBackgroundColor, backgroundColor }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              {console.log("userData.image::", userData)}
              {userData.image && <Image style={[styles.icon, { borderRadius: 15, width: 50, height: 50, borderRadius: 50, top: -50 }]}
                source={{ uri: userData.image }}
              />
              }
              {
                !userData.image && <Image style={styles.icon}
                  source={focused ? require('../../Image/patients-white.png') : require('../../Image/patients.png')}
                />
              }
              <Text style={[styles.drawerText, { top: userData.image ? -40 : -20 }]}>{strings.MY_PROFILE}</Text>
            </View>
          )
        }}
        component={profileScreenStack}
      />

      <Drawer.Screen
        name="dashboardScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/dashboard-white.png') : require('../../Image/dashboard.png')}
              />
              <Text style={styles.drawerText}>{strings.WELLBEING}{'\n'}{strings.DASHBOARD}</Text>
            </View>
          )
        }}
        component={dashboardScreenStack}
      />
      <Drawer.Screen
        name="allUsersScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/users-white.png') : require('../../Image/users.png')}
              />
              <Text style={styles.drawerText}>{strings.ALL_USERS}</Text>
              <View>
                <UserSession />
              </View>
            </View>

          )
        }}
        component={allUsersScreenStack}
      />
      {/* <Drawer.Screen
        name="WellBeingdashboardScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/dashboard-white.png') : require('../../Image/dashboard.png')}
              />
              <Text style={styles.drawerText}>{strings.DASHBOARD}</Text>
            </View>
          )
        }}
        component={WellBeingdashboardScreenStack}
      /> */}
      {(currentUser && currentUser.usertype==="patient") && <Drawer.Screen
        name="timelineScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/timeline-white.png') : require('../../Image/timeline.png')}
              />
              <Text style={styles.drawerText}>{strings.TIMELINE}</Text>
            </View>
          )
        }}
        component={timelineScreenStack}
      />
      } 
      {/* {alert(sessionUser.usertype)} */}

      {(sessionUser && sessionUser.usertype==="patient") && <Drawer.Screen
        name="timelineScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/timeline-white.png') : require('../../Image/timeline.png')}
              />
              <Text style={styles.drawerText}>{strings.TIMELINE}</Text>
            </View>
          )
        }}
        component={timelineScreenStack}
      />
      }
      <Drawer.Screen
        name="medicineScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/medicines-white.png') : require('../../Image/medicines.png')}
              />
              <Text style={styles.drawerText}>{strings.MEDICINES}</Text>
              <View>
                <UserSession />
              </View>
            </View>

          )
        }}
        component={medicineScreenStack}
      />
      <Drawer.Screen
        name="activityScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/calendar-white.png') : require('../../Image/calendar.png')}
              />
              <Text style={styles.drawerText}>{strings.ACTIVITIES}</Text>
            </View>
          )
        }}
        component={activityScreenStack}
      />
      <Drawer.Screen
        name="wellbeingScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/event-white.png') : require('../../Image/event.png')}
              />
              <Text style={styles.drawerText}>{strings.WELLBEING}</Text>
            </View>)
        }}
        component={wellbeingScreenStack}
      />
      <Drawer.Screen
        name="allUsersScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/users-white.png') : require('../../Image/users.png')}
              />
              <Text style={styles.drawerText}>{strings.ALL_USERS}</Text>
              <View>
                <UserSession />
              </View>
            </View>

          )
        }}
        component={allUsersScreenStack}
      />
      <Drawer.Screen
        name="libraryScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/library-white.png') : require('../../Image/library.png')}
              />
              <Text style={styles.drawerText}>{strings.LIBRARY}</Text>
            </View>
          )
        }}
        component={libraryScreenStack}
      />
      <Drawer.Screen
        name="contactsScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/contacts-white.png') : require('../../Image/contacts.png')}
              />
              <Text style={styles.drawerText}>{strings.CONTACTS}</Text>
            </View>
          )
        }}
        component={contactsScreenStack}
      />
      <Drawer.Screen
        name="settingScreenStack"
        options={{
          drawerIcon: ({ focused, size }) => (
            <View style={styles.main}>
              <View style={[styles.iconBack, { backgroundColor: focused ? '#69c2d1' : '#474f69' }]}>
              </View>
              <Image style={styles.icon}
                source={focused ? require('../../Image/settings-white.png') : require('../../Image/settings.png')}
              />
              <Text style={styles.drawerText}>{strings.SETTINGS}</Text>
            </View>
          )
        }}
        component={settingScreenStack}
      />
    </Drawer.Navigator>

  );
};

const styles = StyleSheet.create({
  main: {
    height: 100, width: "100%", alignItems: 'center', top: -8
  },
  iconBack: {
    backgroundColor: '#474f69', borderRadius: 40, height: 50, width: 50
  },
  Date: {
    width: 230,
    height: 45,
    backgroundColor: '#464e6a',
    margin: 5,
    borderRadius: 10,
    flexDirection: 'row'
  },
  dateImg: {
    padding: 5,
    backgroundColor: '#74c2ce',
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10

  },
  icon: {
    top: -40, width: 30, height: 30, marginBottom: 5
  },
  drawerText: {
    color: 'white', top: -30, fontWeight: '600', fontSize: 12, textTransform: 'uppercase',
  }
})
export default DrawerNavigatorRoutes;
