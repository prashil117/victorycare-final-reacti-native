import 'react-native-gesture-handler';
import React, { useEffect } from 'react';
import { StatusBar, Alert, AppRegistry } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SplashScreen from './src/Screen/SplashScreen';
import LoginScreen from './src/Screen/LoginScreen';
import RegisterScreen from './src/Screen/RegisterScreen';
import OnBoardScreen from './src/Screen/DrawerScreens/OnboardScreen';
import WelcomeScreen from './src/Screen/WelcomeScreen';
import DrawerNavigationRoutes from './src/Screen/DrawerNavigationRoutes';
import Icon from 'react-native-vector-icons/MaterialIcons'
import Icon1 from 'react-native-vector-icons/FontAwesome'
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import allReducers from './src/Screen/redux/Allreducer';
import { applyMiddleware, createStore } from 'redux';
import userSession from './src/Screen/Components/userSession';
import messaging from '@react-native-firebase/messaging';
import iid from '@react-native-firebase/iid';
import axios from 'axios'
import config from './src/Screen/appconfig/config';
import { setToken } from './src/Screen/redux/action'
import { Platform } from 'react-native';
Icon.loadFont();
Icon1.loadFont();

const Stack = createStackNavigator();
const Auth = () => {
  return (
    <Stack.Navigator initialRouteName="App">
      <Stack.Screen
        name="WelcomeScreen"
        component={WelcomeScreen}
        options={{ headerShown: false }}
      />
      <Stack.Screen
        name="LoginScreen"
        component={LoginScreen}
        options={{
          title: '', //Set Header Title
          headerStyle: {
            backgroundColor: '#2b3249', //Set Header color
            borderColor: '#2b3249',
            shadowColor: 'transparent',
            shadowRadius: 0,
            shadowOffset: {
              height: 0,
            }
          },
          headerTintColor: '#fff', //Set Header text color
          // headerTitleStyle: {
          //     fontWeight: 'bold', //Set Header text style
          // },
        }}
      />

      <Stack.Screen
        name="RegisterScreen"
        component={RegisterScreen}
        options={{
          title: '', //Set Header Title
          headerStyle: {
            backgroundColor: '#2b3249', //Set Header color
            borderColor: '#2b3249',
            shadowColor: 'transparent',
            shadowRadius: 0,
            shadowOffset: {
              height: 0,
            }
          },
          headerTintColor: '#fff', //Set Header text color
          // headerTitleStyle: {
          //     fontWeight: 'bold', //Set Header text style
          // },
        }}
      />

    </Stack.Navigator>
  );
};
const Board = () => {
  return (
    <Stack.Navigator initialRouteName="App">
      <Stack.Screen
        name="OnBoardScreen"
        component={OnBoardScreen}
        options={{ headerShown: false }}
      />
    </Stack.Navigator>
  );
};
let store = createStore(allReducers, applyMiddleware(thunk));

async function getToken() {
  const id = await iid().getToken();
  return id;
}

//Receiving Notification message in background


//AppRegistry.registerComponent('TimelineScreen', () => TimelineScreen);
messaging().setBackgroundMessageHandler(async (remoteMessage) => {
  console.log('Message handled in the background!', remoteMessage);
})
const App = ({ navigation }) => {

  // console.log("FCM Registration Token!!!", getToken())
  // //  Receiving Notification message in Foreground
  // useEffect(() => {

  //     const unsubscribe = messaging().onMessage(async remoteMessage => {

  //         Alert.alert('A new FCM message arrived!', JSON.stringify(remoteMessage));
  //     });

  //     // return unsubscribe;
  // }, []);
  useEffect(() => {
    if (Platform === 'android') {
      registerAppWithFCM();
    }
    requestPermission();

    messaging().onNotificationOpenedApp(remoteMessage => {
      console.log(
        'Notification caused app to open from background state:',
        remoteMessage.notification,
      );
      if (remoteMessage) {
        Alert.alert(remoteMessage.notification.title, remoteMessage.notification.body);
        // var id = JSON.parse(data['gcm.notification.data']);
        // console.log("helllllo", id);
        // if (data.aps.alert.title == "Add event" || data.aps.alert.title == "Add activity" || data.aps.alert.title == "Add medicine") {
        //   if (id.eventData) {
        //     updatenotification(id)
        //     // navigation.replace('DrawerNavigationRoutes', { screen: 'timelineScreenStack' });
        //     // this.navCtrl.setRoot(AppTimelinePage, { 'eventnot': id.eventData, type: 'notification' });
        //   }
        //   else if (id.activityData) {
        //     updatenotification(id)
        //     // navigation.replace('DrawerNavigationRoutes', { screen: 'timelineScreenStack' });
        //     // this.navCtrl.setRoot(AppTimelinePage, { 'activitynot': id.activityData, type: 'notification' });
        //   }
        //   else if (id.medicineData) {
        //     updatenotification(id)
        //     // navigation.replace('DrawerNavigationRoutes', { screen: 'timelineScreenStack' });
        //     // this.navCtrl.setRoot(AppTimelinePage, { 'medicinenot': id.medicineData, type: 'notification' });
        //   }
        // }
      } else {
        // console.log("get notification", data)
      };
    });

    // Check whether an initial notification is available
    messaging().getInitialNotification().then(remoteMessage => {
      if (remoteMessage) {
        console.log(
          'Notification caused app to open from quit state:',
          remoteMessage.notification,
        );
        console.log("get notification foreground", remoteMessage)
        Alert.alert(
          'Notification caused app to open from quit state:',
          JSON.stringify(remoteMessage),
        );
      }
    });

    const unsubscribe = messaging().onMessage(async remoteMessage => {
      Alert.alert(remoteMessage.notification.title, remoteMessage.notification.body);

      // PushNotification.localNotification({
      //   title: 'notification.title',
      //   message: 'notification.body!',
      // });
    });

    return unsubscribe;
  }, []);

  async function registerAppWithFCM() {
    await messaging().registerDeviceForRemoteMessages();
  }

  async function requestPermission() {
    const granted = await messaging().requestPermission({
      alert: true,
      announcement: false,
      badge: true,
      carPlay: true,
      provisional: false,
      sound: true,
    });
    if (granted) {
      const fcmToken = await messaging().getToken();
      console.log(fcmToken);
      console.log('User granted messaging permissions!');
    } else {
      console.log('User declined messaging permissions :(');
    }
  }

  const updatenotification = (id) => {
    var edata = {};
    if (id.eventData) {
      edata.eventDataid = id.eventData.eventDataid;
      edata.action = "eventnotify";
    }
    else if (id.activityData) {
      edata.activityDataid = id.activityData.activityDataid;
      edata.action = "activitynotify";
    }
    else if (id.medicineData) {
      edata.medicineDataid = id.medicineData.medicineDataid;
      edata.action = "medicinenotify";
    }
    edata.appsecret = this.appsecret;
    console.log("data", edata);
    axios.post(config.apiUrl + "updatedata.php", edata).subscribe(edata1 => {
      let result = JSON.parse(edata1);
      console.log("status ::::>", result.data.status);
    })

  }




  return (
    <Provider store={store}>
      <NavigationContainer>
        <StatusBar hidden={true} />
        <Stack.Navigator initialRouteName="SplashScreen">
          {/* SplashScreen which will come once for 5 Seconds */}
          <Stack.Screen
            name="SplashScreen"
            component={SplashScreen}
            // Hiding header for Splash Screen
            options={{ headerShown: false }}
          />
          {/* Auth Navigator: Include Login and Signup */}
          <Stack.Screen
            name="Auth"
            component={Auth}
            options={{ headerShown: false }}
          />
          <Stack.Screen
            name="Board"
            component={Board}
            options={{ headerShown: false }}
          />
          {/* Navigation Drawer as a landing page */}
          <Stack.Screen
            name="DrawerNavigationRoutes"
            component={DrawerNavigationRoutes}
            // Hiding header for Navigation Drawer
            options={{
              headerShown: false,
              headerLeft: { userSession }
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
};

export default App;
